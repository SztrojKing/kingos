include "../../include/app_lib/app_kit.sc"

function main: ptr arg_list
	
	log("Afficheur d'ecran demarre.\n")
	hide_console()
	u32 ticks=get_time_ticks()
	u32 screen_refresh_ticks
	u32 screen_refresh_total_ticks=0
	
	u32 stats_fps_counter=0
	u32 stats_ticks=get_time_ticks()
	u32 stats_screen_refresh_total_ticks=0
	u32 max_fps = object_list_get_at(arg_list, 1)
	
	while 1

		
		if [u32](get_time_ticks()-ticks) >= 1000/max_fps
			ticks=get_time_ticks()
			screen_refresh_ticks=get_time_ticks()
			refresh_screen()
			screen_refresh_total_ticks+=(get_time_ticks()-screen_refresh_ticks)
			stats_screen_refresh_total_ticks+=(get_time_ticks()-screen_refresh_ticks)
			stats_fps_counter++
			screen_refresh_total_ticks=0
		endif
		
		
		/*if [u32](get_time_ticks()-stats_ticks) >= 1000
			cls()
			log("FPS: ")
			logn(stats_fps_counter)
			log(" [MAX=")
			logn(max_fps)
			log("]\nAverage refreshing time per frame: ")
			logn(stats_screen_refresh_total_ticks/stats_fps_counter)
			log("ms")
			log("\nCPU usage: ")
			logn(stats_screen_refresh_total_ticks/10)
			log("%")
			stats_ticks=get_time_ticks()
			stats_fps_counter=0
			stats_screen_refresh_total_ticks=0
		endif*/
		
		
		!shedule()
		



		
		sleep(1000/(max_fps*5))
	endwhile

endfunction

/*
function main
	!log("Afficheur d'ecran demarre.\n")
	u32 fps_counter=0
	u32 ticks=get_time_ticks()
	u32 screen_refresh_ticks
	u32 screen_refresh_total_ticks=0
	while 1
		!log("Raffraichissement..")
		screen_refresh_ticks=get_time_ticks()
		refresh_screen()
		screen_refresh_total_ticks+=(get_time_ticks()-screen_refresh_ticks)
		fps_counter++
		!log("Ok ")
		shedule_later()
		if [u32](get_time_ticks()-ticks) > 1000
			cls()
			log("FPS: ")
			logn(fps_counter)
			log("\nAverage refreshing time per frame: ")
			logn(screen_refresh_total_ticks/fps_counter)
			log("ms")
			log("\nCPU usage: ")
			logn(screen_refresh_total_ticks/10)
			log("%")
			ticks=get_time_ticks()
			fps_counter=0
			screen_refresh_total_ticks=0
		endif
	endwhile

endfunction
*/
