#!/bin/sh

RED="\e[31m"
GREEN="\e[32m"
NORMAL="\e[0m"
BLUE="\e[34m"


appname="$(basename "$PWD")"

	
	printf "*******************************************************************************\n"
	printf "*                                                                             *\n"
	printf "*                        $BLUE Building $appname application $NORMAL\n"
	printf "*                                                                             *\n"
	printf "*******************************************************************************\n"

	sudo umount /mnt/floppyKing
	sudo mount -o loop ../../bin/disk.img /mnt/floppyKing/

	printf "*********************** Compilation ************************\n"
	./../../scc32_linux -no_auto_compil -e main.sc -org 0x8300000

	if test "$?" = "0"
	then
		sudo nasm -O0 -f bin sortie.asm -o /mnt/floppyKing/system/x86/apps/$appname.app
		if test "$?" = "0"
		then
			sudo umount /mnt/floppyKing
			printf "									$GREEN[OK]$NORMAL\n"
		else
			printf "$RED							            [ERREUR]$NORMAL\n"
		fi
	else
		printf "$RED							            [ERREUR]$NORMAL\n"
	fi




