include "..\..\include\app_lib\app_kit.sc"


function main: ptr arg_list
	hide_console()
	bool continue=true
	ptr title = object_list_get_at(arg_list, 1)
	ptr text = object_list_get_at(arg_list, 2)
	ptr win = window_create(300, 150, title)
	ptr buttonOk = button_create(win, 350,700,300, "OK")
	ptr event
	label_create(win, 100, 100, 800, 600, text)
	!window_add_widget(win, label_create(win, 100, 100, 800, 600, text))
	!window_add_widget(win, buttonOk)
	windows_manager_add_window(win)
	
	while continue==true
		event = window_get_last_event(win)
		if event =/= null
			if window_event_get_widget(event) == buttonOk || window_event_get_cat(event) == SIGNAL_EVENT_KILL
				continue = false
			endif
			window_delete_last_event(win)
		else
			shedule()
		endif
	endwhile
	windows_manager_close_window(win)
	window_free(win)
endfunction


