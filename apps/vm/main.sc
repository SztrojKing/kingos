include "..\..\include\app_lib\app_kit.sc"
include "include\stack.sc"

sptr code_ = null

function main: ptr arg_list
	ptr file_name = object_list_get_at(arg_list, 1)	
	ptr code_arg_list = object_list_get_at(arg_list, 2)	
	
	log("VM: launching ")
	log(file_name)
	log("\n")
	ptr f = read_file(file_name)
	if f =/= null
		
		stack = malloc(stack_size)
		internal_stack = stack
		u32 return_code=run(f, get_file_size(file_name), code_arg_list)
		log("VM: The VM stopped with return code ")
		logn(return_code)
		log("\n")
		free(f)
		free(internal_stack)
		
	else
		log("VM: The file doesn't exist.")
	endif
	while 1
		sleep(10000)
	endwhile
endfunction

function debug_log:ptr str
	!log(str)
endfunction

function debug_logn: u32 n
	!logn(n)
endfunction

function debug_logx: u32 n
	!logx(n)
endfunction

function run: ptr code, ptr code_size, ptr arg_list
	code_ = code
	u8 tmp8=0
	u32 tmp32=0
	
	u32 reg_eax=current_pid
	u32 reg_ebx=arg_list
	u32 reg_ecx=0
	u32 reg_edx=0
	
	u32 reg_esi=0
	u32 reg_edi=0 
	
	u32 reg_ebp=stack_size
	
	u32 code_offset=0
	u32 eflag=0
	
	u32 c=0
				
	while 1
		u8 op = [u8]#(code+code_offset)
		code_offset++


		
		switch op
		
			! add reg, reg
			case 0x01
				! switch through reg combination
				switch [u8]#(code+code_offset)
				
					! eax, ebx
					case 0xd8
						debug_log("add eax, ebx")
						reg_eax+=reg_ebx
					endcase
					
					case default
						log("Invalid register combination 7 0x")
						logx([u8]#(code+code_offset))
						log("\n")
						
						return 0
					endcase
				endswitch
				
				code_offset++
			endcase
			
			! add reg, [LABEL]
			case 0x03
				! switch through reg combination
				switch [u8]#(code+code_offset)
				
					! edi, [LABEL]
					case 0x3d
						debug_log("add edi, [")
						tmp32 = [u32]#(code+code_offset+1)
						
						if tmp32 == 0x110000
							tmp32 = code
							debug_log("CORRECTED PIC")
						else
							tmp32 = #[u32](tmp32)
							debug_log("0x")
							debug_logn(tmp32)
						endif
						debug_log("]")
						
						reg_edi+=tmp32
					endcase
					
					! esi, [LABEL]
					case 0x35
						debug_log("add esi, [")
						tmp32 = [u32]#(code+code_offset+1)
						
						if tmp32 == 0x110000
							tmp32 = code
							debug_log("CORRECTED PIC")
						else
							tmp32 = #[u32](tmp32)
							debug_log("0x")
							debug_logn(tmp32)
						endif
						debug_log("]")
						
						reg_esi+=tmp32
					endcase
					
					! eax, [LABEL]
					case 0x05
						debug_log("add eax, [")
						tmp32 = [u32]#(code+code_offset+1)
						
						if tmp32 == 0x110000
							tmp32 = code
							debug_log("CORRECTED PIC")
						else
							tmp32 = #[u32](tmp32)
							debug_log("0x")
							debug_logn(tmp32)
						endif
						debug_log("]")
						
						reg_eax+=tmp32
					endcase
					
					case default
						log("Invalid register combination 5 0x")
						logx([u8]#(code+code_offset))
						log(" @0x")
						logx(code_offset-1)
						log("\n")
						
						return 0
					endcase
					
					
				endswitch
				
				code_offset+=5
			endcase
			
			! add eax, X
			case 0x05
				debug_log("add eax, ")
				debug_logn(#[u32](code+code_offset))
				
				reg_eax += #[u32](code+code_offset)
				code_offset+=4
			endcase
			! xor reg, reg
			case 0x31
			! switch through reg combination
				switch [u8]#(code+code_offset)
				
					! eax, eax
					case 0xc0
						debug_log("xor eax, eax")
						reg_eax=0
					endcase
					
					! edx, edx
					case 0xd2
						debug_log("xor edx, edx")
						reg_edx=0
					endcase
					
					case default
						log("Invalid register combination 13 0x")
						logx([u8]#(code+code_offset))
						log("\n")
						
						return 0
					endcase
				endswitch
				
				code_offset++
			endcase
			
			! cmp
			case 0x39
				! switch through reg combination
				switch [u8]#(code+code_offset)
				
					! ecx, ebx
					case 0xd9
						debug_log("cmp ecx, ebx")
						
						
						ecx = reg_ecx
						ebx = reg_ebx
						asm cmp ecx, ebx
						
						/*
						! get eflag value
						asm pushfd
						asm pop eax*/
						
						asm lahf
						eflag = eax
					endcase
					
					case default
						log("Invalid register combination 12 0x")
						logx([u8]#(code+code_offset))
						log("\n")
						
						return 0
					endcase
				endswitch
				code_offset++
			endcase
			
			! cmp eax, VALUE32
			case 0x3d
				tmp32 = #[u32](code+code_offset)
				
				debug_log("cmp eax(")
				debug_logn(reg_eax)
				debug_log("), ")
				debug_logn(tmp32)
				debug_log("\n")
				
				ebx = tmp32
				eax = reg_eax
				asm cmp eax, ebx
				
				/*
				! get eflag value
				asm pushfd
				asm pop eax
				*/
				
				asm lahf
				eflag = eax
				
				code_offset+=4
			endcase
			
			
			! jb 8bit offset
			case 0x72
				debug_log("jb *8BIT*")
				
				eax = eflag
				/*
				asm push eax
				asm popfd
				*/
				asm sahf
				asm jb .0x72_equal
				asm jmp .0x72_done
				
				asm .0x72_equal:
				code_offset+=cast_signed8([u8]#(code+code_offset))
				asm .0x72_done:
				code_offset++
				
			endcase
			
			
			! jae 8bit offset
			case 0x73
			
				debug_log("jae *8BIT*")
				eax = eflag
				/*
				asm push eax
				asm popfd
				*/
				asm sahf
				asm jae .0x73_equal
				asm jmp .0x73_done
				
				asm .0x73_equal:
				debug_log("equal\n")
				code_offset+=cast_signed8([u8]#(code+code_offset))
				asm .0x73_done:
				debug_log("not equal\n")
				code_offset++
				
			endcase
			
			! je 8bit offset
			case 0x74
				debug_log("je *8BIT*")
				eax = eflag
				/*
				asm push eax
				asm popfd
				*/
				asm sahf
				asm je .0x74_equal
				asm jmp .0x74_done
				
				asm .0x74_equal:
				debug_log("equal\n")
				code_offset+=cast_signed8([u8]#(code+code_offset))
				asm .0x74_done:
				debug_log("not equal\n")
				code_offset++
				
			endcase
			
			! jne 8bit offset
			case 0x75
				debug_log("jne *8BIT*")
				eax = eflag
				/*
				asm push eax
				asm popfd
				*/
				asm sahf
				asm jne .0x75_equal
				asm jmp .0x75_done
				
				asm .0x75_equal:
				code_offset+=cast_signed8([u8]#(code+code_offset))
				asm .0x75_done:
				code_offset++
				
			endcase
			
			! jbe 8bit offset
			case 0x76
				debug_log("jbe *8BIT*")
				eax = eflag
				/*
				asm push eax
				asm popfd
				*/
				asm sahf
				asm jbe .0x76_equal
				asm jmp .0x76_done
				
				asm .0x76_equal:
				code_offset+=cast_signed8([u8]#(code+code_offset))
				asm .0x76_done:
				code_offset++
			endcase
			
			! 2 bytes opcode
			case 0x0f
				! switch through reg combination
				switch [u8]#(code+code_offset)
				
					! je 32 bits
					case 0x84
						debug_log("je *32BIT*")
						eax = eflag
						/*
						asm push eax
						asm popfd
						*/
						asm sahf
						asm je .0x84_equal
						asm jmp .0x84_done
						
						asm .0x84_equal:

						code_offset+= [u32]#(code+code_offset+1)
						asm .0x84_done:
						code_offset+=4
					endcase

					
					case default
						log("Invalid register combination 8 0x")
						logx([u8]#(code+code_offset))
						log("\n")
						
						return 0
					endcase
					
					
				endswitch
				code_offset++
			endcase
			
			! jmp OFFSET x32 => 4
			case 0xe9
				code_offset+= 4 +  [u32]#(code+code_offset)
				
				debug_log("jmp OFFSET 0x")
				debug_logx(code_offset)
				
			endcase
			
			! call OFFSET x32 => 4
			case 0xe8
				! push code offset to stack
				push32(code_offset + 4)
				
				code_offset+= 4 +  [u32]#(code+code_offset)
				
				debug_log("call OFFSET 0x")
				debug_logx(code_offset)

			endcase
			
			! ret
			case 0xc3
				debug_log("ret")
				! pop code offset from stack
				code_offset = pop32()
			endcase
			! push eax
			case 0x50
				debug_log("push eax")
				push32(reg_eax)
			endcase
			! push ecx
			case 0x51
				debug_log("push ecx")
				push32(reg_ecx)
			endcase
			! push edx
			case 0x52
				debug_log("push edx")
				push32(reg_edx)
			endcase
			! push ebx
			case 0x53
				debug_log("push ebx")
				push32(reg_ebx)
			endcase
			! push esp
			case 0x54
				debug_log("push esp")
				push32(reg_esp)
			endcase
			! push ebp
			case 0x55
				debug_log("push ebp")
				push32(reg_ebp)
			endcase
			! push esi
			case 0x56
				debug_log("push esi")
				push32(reg_esi)
			endcase
			! push edi
			case 0x57
				debug_log("push edi")
				push32(reg_edi)
			endcase
			
			! pop eax
			case 0x58
				debug_log("pop eax")
				reg_eax=pop32()
			endcase
			! pop ecx
			case 0x59
				debug_log("pop ecx")
				reg_ecx=pop32()
			endcase
			! pop edx
			case 0x5a
				debug_log("pop edx")
				reg_edx=pop32()
			endcase
			! pop ebx
			case 0x5b
				debug_log("pop ebx")
				reg_ebx=pop32()
			endcase
			! pop esp
			case 0x5c
				debug_log("pop esp")
				reg_esp=pop32()
			endcase
			! pop ebp
			case 0x5d
				debug_log("pop ebp")
				reg_ebp=pop32()
			endcase
			! pop esi
			case 0x5e
				debug_log("pop esi")
				reg_esi=pop32()
			endcase
			! pop edi
			case 0x5f
				debug_log("pop edi")
				reg_edi=pop32()
			endcase
			
			! mov [__SCC16_SVAR__2], reg8
			case 0x88
				
				! switch through reg combination
				switch [u8]#(code+code_offset)
				
					!mov [__SCC16_SVAR__2], cl
					case 0x0d
						debug_log("mov [LABEL], cl")
						
						tmp32 = [u32]#(code+code_offset+1)
						if tmp32 >= code_size 
							fatal_error("Segmentation fault")
						endif
						tmp8 = (reg_ecx&0xff)
						#(tmp32) = [u8]tmp8
						
						code_offset+=4
						
					endcase
					
					!mov [edi], cl
					case 0x0f
						debug_log("mov [edi], cl")
						
						tmp8 = reg_ecx
						
						#[u8](reg_edi) = [u8]tmp8
					endcase
					
					!mov [ebx], al
					case 0x03
						debug_log("mov [ebx], al")
						
						tmp8 = reg_eax
						
						#[u8](reg_ebx) = [u8]tmp8
					endcase
					
					!mov [ebp + X], al
					case 0x45
						debug_log("mov [ebp-X], al")
						code_offset++
						tmp32 = cast_signed8([u8]#(code+code_offset))
						#[u8](stack+reg_ebp+tmp32) = [u8](reg_eax)
					endcase
					
					case default
						log("Invalid register combination 14 0x")
						logx([u8]#(code+code_offset))
						log("\n")
						
						return 0
					endcase
					
					
				endswitch
				code_offset++
			endcase
			
			! mov REG, REG => 1
			case 0x89
				
				! switch through reg combination
				switch [u8]#(code+code_offset)
					!mov [edi], eax
					case 0x07
						debug_log("mov [edi], eax")
						#[u32](reg_edi) = reg_eax
					endcase
					
					!mov [ebx], eax
					case 0x03
						debug_log("mov [ebx], eax")
						#[u32](reg_ebx) = reg_eax
					endcase
					
					
					!mov ebp, esp
					case 0xe5
						debug_log("mov ebp, esp")
						reg_ebp = reg_esp
					endcase
					
					!mov esp, ebp
					case 0xec
						debug_log("mov esp, ebp")
						reg_esp = reg_ebp
					endcase
					
					! mov ebx, eax
					case 0xc3
						debug_log("mov ebx, eax")
						reg_ebx = reg_eax
					endcase
					!mov [ebp-4], eax
					case 0x45
						debug_log("mov [ebp-X], eax")
						code_offset++
						tmp32 = cast_signed8([u8]#(code+code_offset))
						#[u32](stack+reg_ebp+tmp32) = reg_eax
					endcase
					
					!mov [ebp-4], ebx
					case 0x5d
						debug_log("mov [ebp-X], ebx")
						code_offset++
						tmp32 = cast_signed8([u8]#(code+code_offset))
						#[u32](stack+reg_ebp+tmp32) = reg_ebx
					endcase
					
					!mov ecx, eax
					case 0xc1
						debug_log("mov ecx, eax")
						reg_ecx = reg_eax
					endcase
					
					!mov eax, edx
					case 0xd0
						debug_log("mov eax, edx")
						reg_eax = reg_edx
					endcase
					
					!mov esi, eax
					case 0xc6
						debug_log("mov esi, eax")
						reg_esi = reg_eax
					endcase
					
					!mov edi, ebx
					case 0xdf
						debug_log("mov edi, ebx")
						reg_edi = reg_ebx
					endcase
					
					
					case default
						log("Invalid register combination 1 0x")
						logx([u8]#(code+code_offset))
						log(" @0x")
						logx(code_offset-1)
						log("\n")
						log("\n")
						
						return 0
					endcase
				endswitch
				
				code_offset++
			endcase
			
			! mov eax, value
			case 0xb8
				reg_eax = [u32]#(code+code_offset)		
				code_offset+=4
				
				debug_log("mov eax, ")
				debug_logn(reg_eax)
			endcase
			
			! mov esi, value
			case 0xbe
				reg_esi = [u32]#(code+code_offset)		
				code_offset+=4
				
				debug_log("mov esi, ")
				debug_logn(reg_edi)
			endcase
			
			! mov edi, value
			case 0xbf
				reg_edi = [u32]#(code+code_offset)		
				code_offset+=4
				
				debug_log("mov edi, ")
				debug_logn(reg_edi)
			endcase
			
			!mov [__SCC16_SVAR__1], eax
			case 0xa3
				tmp32 = [u32]#(code+code_offset)
				if tmp32 >= code_size 
					fatal_error("Segmentation fault")
				endif
				
				#[u32](tmp32) = reg_eax
				
				code_offset+=4
				
				debug_log("mov [LABEL], eax")
			endcase
			! sub/add reg, value
			case 0x81
				! switch through reg combination
				switch [u8]#(code+code_offset)
				
					
					! sub esp, value
					case 0xec
						debug_log("sub esp, ")
						debug_logn([u32]#(code+code_offset+1))
						
						reg_esp-=[u32]#(code+code_offset+1)
					endcase
					
					! add esp, value
					case 0xc4
						debug_log("add esp, ")
						debug_logn([u32]#(code+code_offset+1))
						
						reg_esp+=[u32]#(code+code_offset+1)
					endcase
					
					case default
						log("Invalid register combination 2 0x")
						logx([u8]#(code+code_offset))
						log("\n")
						
						return 0
					endcase
				endswitch
				code_offset+=5
				
			endcase
			
			! add reg, reg
			case 0x29
				! switch through reg combination
				switch [u8]#(code+code_offset)
				
					
					! sub eax, ebx
					case 0xd8
						debug_log("sub eax, ebx")
						
						reg_eax-=reg_ebx
					endcase
					
					
					case default
						log("Invalid register combination 15 0x")
						logx([u8]#(code+code_offset))
						log("\n")
						
						return 0
					endcase
				endswitch
				code_offset++
			
			endcase
			
			! mul/div reg
			case 0xf7
				! switch through reg combination
				switch [u8]#(code+code_offset)
				
					
					! mul ebx
					case 0xe3
						debug_log("mul ebx")
						reg_eax*=reg_ebx
					endcase
					
					! mul ecx
					case 0xe1
						debug_log("mul ecx")
						reg_eax*=reg_ecx
					endcase
					
					! mul edx
					case 0xe2
						debug_log("mul edx")
						reg_eax*=reg_edx
					endcase
					
					! div ebx
					case 0xf3
						debug_log("div ebx")
						tmp32 = reg_eax
						reg_eax = tmp32/reg_ebx
						reg_edx = tmp32%reg_ebx
					endcase
					
					case default
						log("Invalid register combination 6 0x")
						logx([u8]#(code+code_offset))
						log("\n")
						
						return 0
					endcase
				endswitch
				code_offset++
			endcase
			
			! int value
			case 0xcd
				debug_log("int 0x")
				debug_logx([u8]#(code+code_offset))
				!sleep(5000)
				do_interrupt([u8]#(code+code_offset), reg_eax, reg_ebx, reg_ecx, reg_edx)
				reg_eax = eax
				reg_ebx = ebx
				reg_ecx = ecx
				reg_edx = edx
				
				reg_esi = esi
				reg_edi = edi
				
				code_offset++
		
			endcase
			
			! mov reg, [reg+offset]
			case 0x8b
				! switch through reg combination
				switch [u8]#(code+code_offset)
				
					
					! eax, ebp
					case 0x45
						debug_log("mov eax, [ebp+*] => stack @")
						
						tmp32 = reg_ebp+cast_signed8([u8]#(code+code_offset+1))
												
						
						reg_eax = [u32]#(stack+tmp32)

						
						code_offset+=2
					endcase
					
					! ebx, ebp
					case 0x5d
						debug_log("mov ebx, [ebp+*] => stack @")
						
						tmp32 = reg_ebp+cast_signed8([u8]#(code+code_offset+1))
												
						
						reg_ebx = [u32]#(stack+tmp32)

						
						code_offset+=2
					endcase
					
					! ecx, ebp
					case 0x4d
						debug_log("mov ecx, [ebp+*] => stack @")
						
						tmp32 = reg_ebp+cast_signed8([u8]#(code+code_offset+1))
												
						
						reg_ecx = [u32]#(stack+tmp32)

						
						code_offset+=2
					endcase
					
					! edx, ebp
					case 0x55
						debug_log("mov edx, [ebp+*] => stack @")
						
						tmp32 = reg_ebp+cast_signed8([u8]#(code+code_offset+1))
												
						
						reg_edx = [u32]#(stack+tmp32)

						
						code_offset+=2
					endcase
					
					! eax, esi
					case 0x06
						debug_log("mov eax, [esi]")
						
												
						
						reg_eax = [u32]#(reg_esi)

						
						code_offset++
					endcase
					
					! eax, esi
					case 0x01
						debug_log("mov eax, [ecx]")
						
						reg_eax = [u32]#(reg_ecx)

						code_offset++
					endcase
					
					case default
						log("Invalid register combination 3 0x")
						logx([u8]#(code+code_offset))
						log("\n")
						
						return 0
					endcase
				endswitch
				
			endcase
			
			! mov reg8, [reg]
			case 0x8a
				! switch through reg combination
				switch [u8]#(code+code_offset)
				
					
					! mov al, [ecx]
					case 0x01
						debug_log("mov al, [ecx]")
						
						tmp8 = #(reg_ecx)
						reg_eax = ((reg_eax&0xffffff00) | tmp8&0xff)		

						
						code_offset++
					endcase
					
					! mov al, [esi]
					case 0x06
						debug_log("mov al, [esi]")
						
						tmp8 = #(reg_esi)
						reg_eax = ((reg_eax&0xffffff00) | tmp8&0xff)		

						
						code_offset++
					endcase
					
					! mov al, [ebp+X]
					case 0x45
						debug_log("mov al, [ebp+X]")

						tmp32 = reg_ebp+cast_signed8([u8]#(code+code_offset+1))

						tmp8 = [u8]#(stack+tmp32)

						
						reg_eax = ((reg_eax&0xffffff00) | tmp8&0xff)		

						
						code_offset+=2
					endcase
					
					
					
					case default
						log("Invalid register combination 9 0x")
						logx([u8]#(code+code_offset))
						log("\n")
						
						return 0
					endcase
				endswitch
			endcase
			
			! and reg, reg
			case 0x21
			! switch through reg combination
				switch [u8]#(code+code_offset)
				
					
					! mov eax, ebx
					case 0xd8
						debug_log("and eax, ebx")

						reg_eax = (reg_eax&reg_ebx)	

						code_offset++
					endcase
					
					
					case default
						log("Invalid register combination 10 0x")
						logx([u8]#(code+code_offset))
						log("\n")
						
						return 0
					endcase
				endswitch
			endcase
			
			! or reg, reg
			case 0x09
			! switch through reg combination
				switch [u8]#(code+code_offset)
				
					
					! mov eax, ebx
					case 0xd8
						debug_log("or eax, ebx")

						reg_eax = (reg_eax|reg_ebx)	

						code_offset++
					endcase
					
					
					case default
						log("Invalid register combination 11 0x")
						logx([u8]#(code+code_offset))
						log("\n")
						
						return 0
					endcase
				endswitch
			endcase
			
			case 0xff
				! switch through reg combination
				switch [u8]#(code+code_offset)
				
					! inc dword [ebp+X]
					case 0x45
						debug_log("inc dword [ebp+X]")
						tmp32 = stack+reg_ebp+cast_signed8([u8]#(code+code_offset+1))
						#[u32](tmp32) = (#[u32](tmp32)) + 1
						
						code_offset++
					endcase
					
					! dec dword [ebp+X]
					case 0x4d
						debug_log("dec dword [ebp+X]")
						tmp32 = stack+reg_ebp+cast_signed8([u8]#(code+code_offset+1))
						#[u32](tmp32) = (#[u32](tmp32)) - 1
						
						code_offset++
					endcase
					
					! inc dword [ebx]
					case 0x03
						debug_log("inc dword [ebx]")
						#[u32](reg_ebx) = #[u32](reg_ebx) + 1
					endcase
					
					! dec dword [ebx]
					case 0x0b
						debug_log("dec dword [ebx]")
						#[u32](reg_ebx) = #[u32](reg_ebx) - 1
					endcase
					
					case default
						log("Invalid register combination 4 0x")
						logx([u8]#(code+code_offset))
						log(" @0x")
						logx(code_offset-1)
						log("\n")
						
						return 0
					endcase
				endswitch
				
				code_offset++
			endcase
			
			! cld
			case 0xfc
				debug_log("cld")
				
				eax = eflag
				
				asm sahf
				asm cld
				asm lahf
				
				eflag = eax
						
			endcase
			
			! std
			case 0xfd
				debug_log("std")
				
				eax = eflag
				
				asm sahf
				asm std
				asm lahf
				
				eflag = eax
						
			endcase
			
			! hlt
			case 0xf4
				debug_log("hlt")
				asm hlt
						
			endcase
			
			case default
				log("Invalid op code 0x")
				logx(op)
				log(" @0x")
				logx(code_offset-1)
				log("\n")
				return 0
			endcase
			

		endswitch
		debug_log("\n")
		
		!log("------------------------------\n")
		!dump_reg(reg_eax, reg_ebx, reg_ecx, reg_edx, reg_ebp)
		!log("STACK ebp   : ")
		!rdump(stack+reg_ebp, 16)
		
		!log("\n")
		!log("STACK esp+4o: ")
		!rdump(stack+reg_esp+16, 16)
		!log("\n")
		!log("\n------------------------------\n")
		!sleep(1)
		!c++
		/*if c%10000 == 0
		!logn(c)
		!log("\n")
		dump_reg(reg_eax, reg_ebx, reg_ecx, reg_edx)
		!log("------------------------------\n")
		endif
		*/
	endwhile
	
	return 42
	
endfunction

function do_interrupt: u8 n, u32 reg_eax, u32 reg_ebx, u32 reg_ecx, u32 reg_edx
	u32 res = 0
	switch n
		case 48
			switch reg_eax
				
				case default
					res = do_unparsed_interrupt(n ,reg_eax, reg_ebx, reg_ecx, reg_edx)
				endcase

			endswitch
			
		endcase
		
		case default
			fatal_error("Unauthorized interrupt")
		endcase
	endswitch
	

	return res
endfunction

function do_unparsed_interrupt:  u8 n, u32 reg_eax, u32 reg_ebx, u32 reg_ecx, u32 reg_edx
	al = n
	asm mov [int_number], al
	edx = reg_edx
	ecx=reg_ecx
	ebx=reg_ebx
	eax = reg_eax
	asm db 0xCD
	asm int_number: db 0
	return eax
endfunction

function dump:ptr data, u32 len
	u32 i=0
	while i<len
		log("0x")
		logx([u8]#(data+i))
		log(" ")
		i++
	endwhile
endfunction

function rdump:ptr data, u32 len
	u32 i=0
	while len>0
		log("0x")
		logx([u32]#(data-i))
		log(" ")
		len--
		i+=4
	endwhile
endfunction

function dump_reg: u32 reg_eax, u32 reg_ebx, u32 reg_ecx, u32 reg_edx, u32 reg_ebp
	log("EAX: 0x")
	logx(reg_eax)
	log(" | EBX: 0x")
	logx(reg_ebx)
	log(" | ECX: 0x")
	logx(reg_ecx)
	log(" | EDX: 0x")
	logx(reg_edx)
	log("\n")
	log("stack(esp): 0x")
	logx(reg_esp)
	log(" | ebp: 0x")
	logx(reg_ebp)
	log("\n")
endfunction

function cast_signed8: u8 n
	asm xor ebx, ebx
	bl = n
	asm movsx eax, bl
	return eax
endfunction

function convert_memory_to_absolute: u32 address
	return address+code_
endfunction
