@echo off

cd /d %~dp0
for %%a in (.) do set appName=%%~na
title Building Application %appName%

Set argC=0
for %%x in (%*) do Set /A argC+=1

:start
color 09
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
if %argC% == 0 (
	cls
)
echo *******************************************************************************
echo.
echo                               Building %appName%(x86)
echo.
echo *******************************************************************************
echo.


..\..\scc32.exe -no_auto_compil -e main.sc -s %appName%.bin -org 0 -p_e entry -pic
if ERRORLEVEL 1 ( goto exit_fail )
..\..\Stupid_Loader\tools\nasm.exe -O0 -f bin sortie.asm -o %appName%.bin
if ERRORLEVEL 1 ( goto exit_fail )
rem del sortie.asm
del rawcode.asm
copy /b %appName%.bin "..\..\fs\system\x86\apps\%appName%.app"
if ERRORLEVEL 1 ( goto exit_fail )
del %appName%.bin



if %argC% == 0 (
	rem ..\..\Stupid_Loader\tools\winimage\winimage.exe ..\..\bin\disk.img fs /I /H /Q
	echo Test (qemu)

	rem "C:\Program Files\qemu\qemu-system-i386.exe" -m 256 -drive file=..\..\bin\disk.img,index=0,if=floppy,format=raw -localtime
	pause
	goto start
)


goto :eof

:exit_fail
color 0c
echo 									[ERREUR]
pause
goto start


