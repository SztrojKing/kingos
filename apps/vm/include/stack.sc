sptr stack = null
sptr internal_stack=null
su32 stack_size=1048576
su32 reg_esp=1048576
/*
function push8:u8 n
	#(stack+reg_esp) = n
	reg_esp--
	
	if reg_esp<4
		fatal_error("Stack overflow")
	endif
endfunction

function push16:u16 n
	#(stack+reg_esp) = n
	reg_esp-=2
	
	if reg_esp<4
		fatal_error("Stack overflow")
	endif
endfunction
*/
function push32:u32 n
	reg_esp-=4
	#(stack+reg_esp) = n

	
	if reg_esp < 4
		fatal_error("Stack overflow")
	endif
endfunction

/*
function pop8

	
	if reg_esp > stack_size
		fatal_error("Stack overflow")
	endif
	
	reg_esp++
	
	return [u8]#(stack+reg_esp)
endfunction

function pop16

	if reg_esp > stack_size
		fatal_error("Stack overflow")
	endif
	
	reg_esp+=2
	
	return [u16]#(stack+reg_esp)
endfunction
*/
function pop32
	u32 res = [u32]#(stack+reg_esp)
	
	if reg_esp > stack_size
		fatal_error("Stack overflow")
	endif
	
	reg_esp+=4
	
	return res
endfunction



