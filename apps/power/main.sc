include "..\..\include\app_lib\app_kit.sc"


function main: ptr arg_list
	hide_console()
	
	ptr win = window_create(500, 100, "Selectionnez la prochaine action")
	ptr event
	ptr button_shutdown = button_create(win, 205,500, 190, "Eteindre")
	ptr button_reboot = button_create(win, 400,500, 190, "Redemarrer")
	ptr button_cancel = button_create(win, 595,500, 190, "Annuler")
	bool continue=true
	
		
	button_set_color(button_shutdown, COLOR_WHITE)
	button_set_color(button_reboot, COLOR_WHITE)
	button_set_color(button_cancel, COLOR_WHITE)
	
	button_set_background_color(button_shutdown, 0x3e7d36)
	button_set_background_color(button_reboot, 0xa50303)
	button_set_background_color(button_cancel, COLOR_DARK_GREY)
	
	windows_manager_add_window(win)
	
	while continue
		event = window_get_last_event(win)
		if event =/= null
			if window_event_get_widget(event) == [ptr](button_shutdown)
				shutdown()
			endif
			if window_event_get_widget(event) == [ptr](button_reboot)
				reboot()
			endif
			if window_event_get_widget(event) == [ptr](button_cancel)
				continue=false
			endif
			if window_event_get_cat(event) == SIGNAL_EVENT_KILL
					continue = false
			endif
			window_delete_last_event(win)
		endif
		sleep(100)
	endwhile
endfunction

