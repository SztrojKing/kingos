include "..\..\include\app_lib\app_kit.sc"

define BUF_TMP 32
define PARTITION_SIZE 2880
function main: ptr arg_list
	log("Installing...\n")
	sleep(500)
	install()
	/*
	log("\nOk\nBootloader..\n")
	ptr bootloader = read_file("root0/system/x16/boot/fat12.bin")
	if bootloader =/= null
		write_disk_raw(128, 0, 1, bootloader)
		log("OK\n")
	else
	
		log("Bootloader error")
	endif*/
	log("ok")
	sleep(50000)
endfunction


function install
	ptr tmp=malloc(BUF_TMP*512)
	
	u32 i=0
	
	while i<PARTITION_SIZE
		read_disk_raw(0, i, BUF_TMP, tmp)
		write_disk_raw(128, i, BUF_TMP, tmp)
		i+=BUF_TMP
		log("#")
		sleep(1)
	endwhile
	free(tmp)
endfunction
