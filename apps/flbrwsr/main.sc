include "..\..\include\app_lib\app_kit.sc"
include "..\..\include\common\fat12filelister.sc"

function main: ptr arg_list
	ptr win = window_create(800, 800, "Explorateur de fichier")
	hide_console()
	ptr path = clone_str("root0")
	! did we passed a path ?
	if object_list_get_size(arg_list) > 1
		path = object_list_get_at(arg_list, 1)
	endif
	
	windows_manager_add_window(win)
	show_directory(win, path)
	window_free(win)
endfunction


function show_directory: ptr win, ptr path
				
	window_free_content(win)

	ptr file_list = null
	struct filedescriptor_struct current_file_desc
	ptr buttonText = malloc(256)
	ptr buttonsList
	ptr filesList
	ptr buttonTmp = null
	ptr buttonActualiser = button_create(win, 850,20,140, "Actualiser")

	ptr textbox_path = textbox_create(win, 120, 20, 720, null, 128)
	textbox_set_text(textbox_path, path)
	
	ptr vscroll = vscroll_create(win, 200, 100, 800, 900)
	ptr vscroll_container = container_get_container(vscroll)
	
	bool continue = true
	u32 i = 0
	u32 x = 0
	u32 y = 0
	
	ptr fileName = malloc(64)
	
	ptr event = null
	u32 pos = 0
	ptr next_path = null

	file_list = directory_list(path)
	

	u32 list_size

	

	if file_list =/= null
		list_size = object_list_get_size(file_list)
		filesList = object_list_create(list_size)
		buttonsList = object_list_create(list_size)
		label_create(win, 20,20,100,100, "Chemin:")
		while i < list_size
			current_file_desc = object_list_get_at(file_list, i)
			! if dir
			if #current_file_desc.attributes & FAT12_DIRECTORY =/= 0
				strcpy(fileName, #current_file_desc.name)
				remove_useless_spaces(fileName)
				strcpy(buttonText, fileName)
				buttonTmp = button_create(vscroll_container, 0,y,1000, buttonText) 
				button_set_color(buttonTmp, 0xff8040)
				object_list_add(buttonsList, buttonTmp)
				object_list_add(filesList, current_file_desc)

				y += 48
			endif
			i++
		endwhile
		
		i=0
		while i < list_size
			current_file_desc = object_list_get_at(file_list, i)
		
			! if file
			if #current_file_desc.attributes & FAT12_DIRECTORY == 0
				strcpy(fileName, #current_file_desc.name)
				remove_useless_spaces(fileName)
				strcpy(buttonText, fileName)
				strcat(buttonText, " | ")
				if #current_file_desc.file_size < 1024
					strcatn(buttonText, #current_file_desc.file_size)
					strcat(buttonText, "o")
				
				else 
				if #current_file_desc.file_size < 1024*1024
					strcatn(buttonText, #current_file_desc.file_size/1024)
					strcat(buttonText, "ko")			
				else 
				if #current_file_desc.file_size < 1024*1024*1024
					strcatn(buttonText, #current_file_desc.file_size/1024/1024)
					strcat(buttonText, "mo")						
				else
					strcatn(buttonText, #current_file_desc.file_size/1024/1024/1024)
					strcat(buttonText, "go")			
				endif
				endif
				endif

				buttonTmp = button_create(vscroll_container, 0,y,1000, buttonText) 
				object_list_add(buttonsList, buttonTmp)

				object_list_add(filesList, current_file_desc)
				y += 48
			endif
			i++
		endwhile
		free(file_list)
		free(fileName)
	
		
		while continue
			event = window_get_last_event(win)
			if event =/= null
				! did we clicked on a button that contains a file link ?
				pos = object_list_get_position(buttonsList, window_event_get_widget(event))
				if pos < object_list_get_size(filesList)
					current_file_desc = object_list_get_at(filesList, pos)
					next_path = get_next_path(path, #current_file_desc.name)
					if [u8]#current_file_desc.attributes & FAT12_DIRECTORY =/= 0
						continue = false
					else
						exec(next_path, null)
						log(next_path)
						free(next_path)
					endif
					
				endif
				if window_event_get_widget(event) == buttonActualiser
					!fatal_error("Fonctionnalite desactive")
					continue = false
					next_path = textbox_get_text(textbox_path)
					if [u8]#(next_path+strlen(next_path)-1) == '/'
						#(next_path+strlen(next_path)-1) = [u8](0)
					endif
				endif
				if window_event_get_cat(event) == SIGNAL_EVENT_KILL
					continue = false
					next_path=null
				endif
				window_delete_last_event(win)
			endif
			sleep(100)
		endwhile
	else
		fatal_error("Le chemin n'existe pas")
	endif
	! browse the next directory
	if next_path =/= null
		show_directory(win, next_path)
		free(next_path)
	endif
endfunction

! return the path of the concatened links
function get_next_path: ptr previous_path, ptr new_file_link
	ptr res = malloc(512)
	u32 i
	if strcmp(".          ", new_file_link) == 0
		memcpy(res, previous_path, 5)
	else
	if strcmp("..         ", new_file_link) == 0
		strcat(res, previous_path)
		i = strlen(res)
		while i>0
			if [u8]#(res+i) == '/'
				#(res+i) = [u8](0)
				i=0
			else
				#(res+i) = [u8](0)
				i--
			endif
		endwhile
	else
		strcat(res, previous_path)
		if [u8]#(res+strlen(res)-1) =/= '/'
			strcat(res, "/")
		endif
		strcat(res, new_file_link)
	endif
	endif
	remove_useless_spaces(res)
	return res
endfunction

! remove useless spaces in a path
function remove_useless_spaces: ptr path
	uint32_t i
	i = strlen(path)
	if i>6 && [uint8_t]#(path+i-4) == ' ' && [uint8_t]#(path+i-3) =/= ' '
		#(path+i-4)=[uint8_t]('.')
	endif
	i=0
	! remove spaces
	while #[uint8_t](path+i) =/= 0
		if #[uint8_t](path+i) == ' '
			strcpy((path+i), (path+i+1))
			i--
		endif
		i++
	endwhile
endfunction