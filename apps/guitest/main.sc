include "..\..\include\app_lib\app_kit.sc"


function main: ptr arg_list
	hide_console()
	gui_tests()
endfunction



function gui_tests
	ptr win = window_create(500, 500, "My Window")
	ptr button = button_create(win, 400,500,200, "Click me !")
	ptr buttonNewWindow = button_create(win, 800,50,200, "New window")
	ptr buttonStop = button_create(win, 800,0,200, "Close window")
	ptr event
	ptr textbox = [ptr](textbox_create(win, 300,100,400, "You can enter text here", 32))
	ptr label_random_number = label_create(win, 400, 900, 1000, 1000, null)
	windows_manager_add_window(win)
	label_create(win, 0,0,1000, 100, "Hello GUI world !")

	window_attach_pid(win, MAX_TASKS)
	bool continue = true
	ptr random_number_str_label = malloc(128)
	ptr random_number_str = malloc(32)
	ptr textbox_text = null

	
	ptr label = null
	while continue
		event = window_get_last_event(win)
		if event =/= null
			if window_event_get_widget(event) == [ptr](button)
				if label =/= null
					window_remove_widget(win, label)
					widget_free(label)
				endif
				#random_number_str_label = 0
				
				strcat(random_number_str_label, "Random number: ")
				strcatn(random_number_str_label, rand(0, 999))
				label_set_text(label_random_number, random_number_str_label)
				textbox_text = textbox_get_text(textbox)
				label = label_create(win, 0,100, 200, 100, textbox_text)
				free(textbox_text)
				
			endif
			if window_event_get_widget(event) == [ptr](buttonStop) || window_event_get_cat(event) == SIGNAL_EVENT_KILL
				continue = false
			endif
			if window_event_get_widget(event) == [ptr](buttonNewWindow)
				window_lock(win, true)

				gui_tests()
				window_lock(win, false)
			endif
			window_delete_last_event(win)
		endif
		shedule()
	endwhile
	free(random_number_str_label)
	free(random_number_str)
	windows_manager_close_window(win)
	window_free(win)
endfunction