@echo off
set runCount=0

:start

if %runCount%==1 goto :not_executed_end
cls
call Stupid_Loader\build.bat
call build4floppy.bat

echo Test (qemu)
"C:\Program Files\qemu\qemu-system-i386.exe" -m 256 -drive file=bin\disk.img,index=0,if=floppy,format=raw -localtime
if ERRORLEVEL 1 ( goto exit_fail )

:end
pause
set runCount=1
goto start

:not_executed_end
set runCount=0
pause
goto start

:exit_fail
color 0c
echo 									[ERREUR]
pause
goto start