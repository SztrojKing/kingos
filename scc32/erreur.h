#ifndef ERREUR_H_INCLUDED
#define ERREUR_H_INCLUDED
#include <iostream>
int out_str(std::string msg);
void out_warning(std::string fichier, int ligne, std::string mot_erreur, std::string msg);
int out_error(std::string fichier, int ligne, std::string mot_erreur, std::string msg);
int out_debug(std::string fichier, int ligne, std::string mot_erreur, std::string msg);
#endif // ERREUR_H_INCLUDED
