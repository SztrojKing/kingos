/*
 *****************************************************************************
 Source code wrote in "Sztrojka Code" by LANEZ Gaël - 2014                   *
 Permission to use, copie and modifie this code.                             *
 ***************************************************************************** 
*/

define _int {$ int}
define _xor {$ xor}
define _pop {$ pop}
define _push {$ push}
define _goto {$ jmp}

define true 1
define false 0

define bool uint8_t
