/*
*******************************************************************************
* Source code wrote in "Sztrojka Code" by LANEZ Ga�l - 2014                   *
* Permission to use, copie and modifie this code.                             *
*******************************************************************************
*/
include "include\asm_def.sc"
include "include\bios.sc"
include "include\string.sc"



! clear screen then print the title
function cls
 	call bios_cls
	call bios_prints:COLOR_GREY, {"##################################", 0}
	call bios_prints:COLOR_GREEN, {" STUPID OS ",0}
	call bios_prints:COLOR_GREY, {"##################################", 0}
	call bios_move_cursor:0,24
	call bios_prints:COLOR_GREY, {"###############################################################################", 0}
	call bios_move_cursor:0,2
	
endfunction

! bios_prints with default color
function prints:ptr str
	call bios_prints:DEFAULT_COLOR, str
endfunction

define SHUTDOWN_MENU_ABORT 1
define SHUTDOWN_MENU_SHUTDOWN 2
define SHUTDOWN_MENU_REBOOT 3

! shutdown or reboot menu
function shutdown_menu
	uint8_t key
	uint8_t choice
	
	
	choice = SHUTDOWN_MENU_ABORT
	
	key = 0
	
	! while key =/= KEY_ENTER
	while key =/= 0x1c
	
		! up
		if key = 0x48
			choice--
			if choice < 1
				choice = 3
			endif
		else
			! down
			if key = 0x50
				choice++
				if choice > 3
					choice = 1
				endif
			endif
		endif
		
		! print menu
		call cls
		call prints:{"Selectionnez l'action a suivre\n \n", 0}
		if choice = SHUTDOWN_MENU_ABORT
			call bios_prints:0x70, {"  |  Annuler   |  ",0}
		else
			call bios_prints:0x07, {"  |  Annuler   |  ",0}
			
		endif
		
		call bios_prints:0x07, {" \n",0}
		
		if choice = SHUTDOWN_MENU_SHUTDOWN
			call bios_prints:0x70, {"  |  Arreter   |  ",0}
		else
			call bios_prints:0x07, {"  |  Arreter   |  ",0}
		endif
		
		call bios_prints:0x07, {" \n",0}
		
		if choice = SHUTDOWN_MENU_REBOOT
			call bios_prints:0x70, {"  | Redemarrer |  ",0}
		else
			call bios_prints:0x07, {"  | Redemarrer |  ",0}
		endif
		
		! hide the cursor
		call bios_move_cursor:0,0xff
		! select choice
		call bios_wait_key
		key = getreturn
		
	endwhile
	
	if choice = SHUTDOWN_MENU_SHUTDOWN
		call bios_shutdown
	else
		if choice = SHUTDOWN_MENU_REBOOT
			call bios_reboot
		endif
	endif

endfunction

function main_menu: uint8_t bootdrive

	uint8_t key
	uint8_t choice
	
	while 1=1
	
		choice = 1
		key = 0
		
		! while key =/= KEY_ENTER
		while key =/= 0x1c
		
			! up
			if key = 0x48
				choice--
				if choice < 1
					choice = 2
				endif
			else
				! down
				if key = 0x50
					choice++
					if choice > 2
						choice = 1
					endif
				endif
			endif
			
			! print menu
			call cls
			call prints:{"Selectionnez l'action a suivre\n \n", 0}
			if choice = 1
				call bios_prints:0x70, {"  | Installer  |  ",0}
			else
				call bios_prints:0x07, {"  | Installer  |  ",0}
				
			endif
			
			call bios_prints:0x07, {" \n",0}
			
			if choice = 2
				call bios_prints:0x70, {"  |  Arreter   |  ",0}
			else
				call bios_prints:0x07, {"  |  Arreter   |  ",0}
			endif
			

			! hide the cursor
			call bios_move_cursor:0,0xff
			! select choice
			call bios_wait_key
			key = getreturn
			
		endwhile
		
		if choice = 1
			call install:bootdrive
		else
			if choice = 2
				call shutdown_menu
			endif
		endif
	
	endwhile

endfunction

define OS_LBA 0
define OS_SIZE 2880
define OS_BUFF 0x1000
! 20 MO
define BUFFER 1400000
function install:uint8_t boot_drive
		uint8_t install_drive = 0

		call cls
		call bios_prints:DEFAULT_COLOR, {"Disque de demarrage: ", 0} 
		call print_number:0x0d, boot_drive
		
		
		call bios_prints:DEFAULT_COLOR, {" - Disque d'installation: ", 0} 

		! choose on wich drive we will install
		if boot_drive =/= 0x80
			install_drive = 0x80
			call bios_get_drive_type: install_drive
			! is disk found ?
			if getreturn =/= 0x03
				! no
				install_drive = 0x81
				call bios_get_drive_type: install_drive
				! is disk found ?
				if getreturn =/= 0x03
					! no, cancel installation
					call bios_prints:COLOR_RED, {"<non trouve>\nArret.\n", 0} 
					call bios_eject_media: boot_drive
					! shutdown
					call pause
					exit
				endif
			endif
			call bios_prints:COLOR_GREEN, {"<trouve> ", 0} 
		endif
		if boot_drive = 0x80
			install_drive = 0x81
			call bios_get_drive_type: install_drive
			! is disk found ?
			if getreturn =/= 0x03
				! no, cancel installation
				call bios_prints:COLOR_RED, {"<non trouve>\nArret.\n", 0} 
				call bios_eject_media: boot_drive
				! shutdown
				call pause
				exit
			endif
			call bios_prints:COLOR_GREEN, {"<trouve> ", 0} 
		endif
		
		! print selected drive
		

		call print_number:0x0d, install_drive
		
		! ask the user to press enter
		call bios_prints:DEFAULT_COLOR, {"\nAppuyez sur [",0}
		call bios_prints:COLOR_LIGHT_BLUE, {"ENTER",0}
		call bios_prints:DEFAULT_COLOR, {"] pour installer, ou [",0}
		call bios_prints:COLOR_LIGHT_BLUE, {"ESCAPE",0}
		call bios_prints:DEFAULT_COLOR, {"] pour abandonner.\n", 0}
		uint8_t key = 0
		call bios_wait_key
		key = getreturn
		! while not press ENTER
		while key =/= 0x1c
			! ESCAPE pressed: cancel installation
			if key = 0x01
				call bios_prints:COLOR_RED, {"Arret.\n", 0}
				call bios_eject_media: boot_drive
				! shutdown
				call pause
				exit
			endif
			call bios_wait_key
			key = getreturn
		endwhile
		
		call bios_prints:DEFAULT_COLOR, {"Installation..\nLecture depuis le disque de demarrage..\n", 0}
		call read_disk_raw:boot_drive, OS_LBA, OS_SIZE, BUFFER
		! error ?
		if getreturn =/= 0
			
				! fatal error
				call bios_prints:COLOR_RED, {"    [ERREUR]\nArret.\n", 0}
				call bios_eject_media: boot_drive
				call pause
				exit
		endif
		
		! done, write data now
		call bios_prints:COLOR_GREEN, {"    [OK]", 0}
		call bios_prints:DEFAULT_COLOR, {"\nEcriture sur le disque..\n", 0}
		call write_disk_raw:install_drive, 0, OS_SIZE, BUFFER
		! error ?
		if getreturn =/= 0
			call bios_prints:COLOR_RED, {"    [ERREUR]\nArret.\n", 0}
			call bios_eject_media: boot_drive
			call pause
			exit
		endif
		
		call bios_prints:COLOR_GREEN, {"    [OK]", 0}
		call bios_prints:DEFAULT_COLOR, {" \n", 0}
		call prints:{"Installation reussi !\nEjection du disque\n",0}
		call bios_eject_media: boot_drive
		call prints:{"Redemarrage...\n",0}
		call pause
		call bios_reboot
endfunction

