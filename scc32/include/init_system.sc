/*
*******************************************************************************
* Source code writen in ""Sztrojka Code" by LANEZ Ga�l - 2015                 *
* Permission to use, copie and modifie this code.                             *
*******************************************************************************
*/
function init_system

	$ cli
	! segments
	ax = 0
	ds = ax
	es = ax
	fs = ax
	gs = ax
	
	! stack almost 30 KiB 	
	ax = 0x0000
    ss = ax
    sp = 0x7c00 
	
	! go to flat real mode (unreal mode)
	$	xor ax, ax       ; make it zero
	$   mov ds, ax             ; DS=0
	$   mov ss, ax             ; stack starts at seg 0
	$   mov sp, 0xf000       ; 2000h past code start, 
	$   push ds                ; save real mode
	$ 
	$   lgdt [gdtinfo]         ; load gdt register
	$ 
	$   mov  eax, cr0          ; switch to pmode by
	$   or al,1                ; set pmode bit
	$   mov  cr0, eax
	$ 
	$   jmp $+2                ; tell 386/486 to not crash
	$ 
	$   mov  bx, 0x08          ; select descriptor 1
	$   mov  ds, bx            ; 8h = 1000b
	$ 
	$   and al,0xFE            ; back to realmode
	$   mov  cr0, eax          ; by toggling bit again
	$ 
	$   pop ds                 ; get back old segment
	$   sti
	
	! done
	exit
	
	
	$ gdtinfo:
	$   dw gdt_end - gdt - 1   ;last byte in table
	$   dd gdt                 ;start of table
	$ 
	$ gdt         dd 0,0        ; entry 0 is always unused
	$ flatdesc    db 0xff, 0xff, 0, 0, 0, 10010010b, 11001111b, 0
	$ gdt_end:
	

endfunction
