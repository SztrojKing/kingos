#ifndef VARIABLE_H
#define VARIABLE_H

#include <string>

enum type_var_size { VAR_UINT8_T = 1, VAR_UINT16_T, VAR_UINT32_T, VAR_PTR};
enum type_delim { DELIM_ERROR = 0, DELIM_EQUAL, DELIM_CMP_EQUAL, DELIM_NOT_EQUAL, DELIM_SMALLER, DELIM_GREATER, DELIM_SMALLER_OR_EQUAL, DELIM_GREATER_OR_EQUAL, DELIM_AND, DELIM_OR};
enum var_type{UNKNOW_VAR=0, LOCAL_VAR, STATIC_VAR, GLOBAL_VAR};
#define STATIC_VAR_PREFIX "__SCC16_SVAR__"
class variable
{
    public:
        variable(std::string name, std::string nasm_address, char var_type, type_var_size var_size);
        variable(std::string name, std::string nasm_address, char var_type, std::string struct_name);
        variable();
        virtual ~variable();
        char varType() { return m_var_type; }
        void setVarType(char val) { m_var_type = val; }
        type_var_size size() { return m_var_size; }
        std::string name() {return *m_Name;}
        std::string address() { return *m_nasm_address; }
        std::string localOffset()
        {
            std::string buf(*m_nasm_address);
            buf.erase(0,3);
            return buf;
        }
        void setDefaultValue(std::string value) {*m_default_value=value;}
        std::string defaultValue() {return *m_default_value;}
        bool isStruct(){
        	return (m_struct_name==NULL?false:true);
        }

        std::string getStructName(){
        	return (isStruct()?*m_struct_name:NULL);
        }
    protected:
    private:
        std::string *m_Name;
        std::string *m_default_value;
        char m_var_type;
        type_var_size m_var_size;
        std::string *m_nasm_address;
        std::string *m_struct_name;
};

#endif // VARIABLE_H
