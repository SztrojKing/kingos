/*
*******************************************************************************
* Source code writen in "Sztrojka Code" by LANEZ Ga�l - 2015                   *
* Permission to use, copie and modifie this code.                             *
*******************************************************************************
*/

include "include\asm_def.sc"
include "include\bios.sc"
include "include\jumble.sc"
include "include\string.sc"
include "include\fat12.sc"

! convert lba to chs
function lba2chs:uint8_t drive, uint16_t lba, ptr ptr_cylinder, ptr ptr_head, ptr ptr_sector
	uint8_t heads_per_cylinder
	uint8_t sectors_per_track
	uint8_t cylinders_per_disk
	
	
	uint8_t cylinder
	uint8_t head
	uint8_t sector
	uint16_t temp
	
	call bios_get_drive_parameters:drive, @cylinders_per_disk, @heads_per_cylinder, @sectors_per_track

	temp = lba % (heads_per_cylinder * sectors_per_track)
	

	cylinder = lba / (heads_per_cylinder * sectors_per_track)
	#ptr_cylinder = cylinder
	
	head = temp / sectors_per_track
	#ptr_head = head
	
	sector = (temp % sectors_per_track) + 1
	#ptr_sector = sector
	
	
endfunction

! print a value
function print_number:uint8_t color, uint32_t number

	uint8_t buf={"     ",0}
	
	call itoa:number, @buf
	call bios_prints:color, @buf
	
endfunction

! wait for a key
function pause
	call prints:{"Appuyez sur une touche pour continuer...\n", 0}
	call bios_wait_key
endfunction

function memcpy_16_to_32:ptr dest, uint16_t src_seg, uint16_t src_off, uint32_t size
	uint8_t b
	$ push es
	ax = src_seg
	es = ax
	while size > 0
		bx = src_off
		$ mov cl, [es:bx]
		b = cl
		#dest = b
		size--
		dest++
		src_off++
	endwhile
	$ pop es
endfunction

function memcpy_32_to_16:uint16_t dest_seg, uint16_t dest_off, ptr src, uint32_t size

	uint8_t buf
	$ push es
	ax = dest_seg
	es = ax
	while size > 0
		
		buf = #src
		cl = buf
		bx = dest_off
		$ mov [es:bx], cl
		size--
		dest_off++
		src++
	endwhile
	$ pop es
endfunction

function memcpy:ptr dest, ptr src, uint32_t size
	
	uint8_t b
	while size > 0
		b = #src
		#dest = b
		size--
		src++
		dest++
	endwhile
endfunction

function memcmp:ptr src1, ptr src2, uint32_t size
	
	uint8_t a
	uint8_t b
	while size > 0
		a = #src1
		b = #src2
		if a =/= b
			return 1
		endif
		src1++
		src2++
		size--
	endwhile
	return 0
endfunction

! sleep (in millisec)
function sleep: uint32_t time
	time = time*1000
	call bios_sleep: time
endfunction