#ifndef STRUCT_LISTER_H
#define STRUCT_LISTER_H

#include <iostream>
#include <vector>
#include "structure.h"

class Struct_lister
{
	public:
		Struct_lister();
		virtual ~Struct_lister();
		void addStruct(Structure *structure){
			m_struct_lister->push_back(structure);
		};
		Structure* at(std::string name);
	protected:
	private:
		std::vector<Structure*> *m_struct_lister;
};

#endif // STRUCT_LISTER_H
