#ifndef OPTIMIZER_H
#define OPTIMIZER_H
#include <iostream>
#include <vector>
enum optimizer_registers {REG_EAX, REG_EBX, REG_ECX, REG_EDX, REG_UNKNOWN};

class Optimizer
{
	public:
		Optimizer();
		virtual ~Optimizer();
		optimizer_registers getRegFromString(std::string reg);
		bool optimizeCode(std::istream &in, std::ofstream &outStream, bool isPositionIndependentCode);
		std::vector<std::string> decoupe_no_point(std::string &ligne, int cur_l);
	protected:
	private:

};

#endif // OPTIMIZER_H
