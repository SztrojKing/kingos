/*
*******************************************************************************
* Source code wrote in "Sztrojka Code" by LANEZ Ga�l - 2014                   *
* Permission to use, copie and modifie this code.                             *
*******************************************************************************
*/


define 16_BITS_MAX_DIGITS 5

! compare 2 chaines de caractere et retourne 0 si elles sont �gales
function strcmp:ptr str_1, ptr str_2
	uint8_t char_str_1
	uint8_t char_str_2
	
	char_str_1= #str_1
	char_str_2= #str_2
	while char_str_1=char_str_2

		if char_str_1=0
			return 0
		endif
		str_1++
		str_2++
		char_str_1= #str_1
		char_str_2= #str_2
		
	endwhile
	return 1
endfunction



! convert a number to a string
function itoa:uint32_t number, ptr buf

	uint32_t buf_number
	uint8_t buf_char
	uint8_t i
	buf_char='0'
	buf=buf+16_BITS_MAX_DIGITS
	buf --
	i = 16_BITS_MAX_DIGITS
	
	while i>0

		buf_number = number%10
		buf_char='0'
		buf_char=buf_char+buf_number
		#buf=buf_char


		number = number/10

		i--
		buf--
	endwhile
	
	exit

endfunction


