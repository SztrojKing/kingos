#ifndef SWITCH_H
#define SWITCH_H

#include <string>

class Switch
{
    public:
        Switch();
        virtual ~Switch();
        std::string add_case(const std::string &comp_value);
        std::string add_end_case();
        std::string add_default();
        std::string add_end_switch();
    protected:

    private:
        int n_case;
        static int n_switch;
        bool isDefaultCase;
        int current_switch;
};

#endif // SWITCH_H
