#ifndef STRUCTURE_H
#define STRUCTURE_H




#include <iostream>
#include "variable.h"
#include <vector>



class Structure
{
	public:
		Structure(std::string name, bool isPackedStruct);
		virtual ~Structure();
		std::string name() { return *m_name; }
		bool addVar(std::string name, type_var_size var_size);
		variable* at(std::string name);
		int get_current_offset(){
			return this->m_current_offset;
		}
	protected:
	private:
		std::string *m_name;
		int m_current_offset;
		std::vector<variable*> *m_var_lister;
		bool m_is_packed_struct;
};

#endif // STRUCTURE_H
