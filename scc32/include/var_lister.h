#ifndef VAR_LISTER_H
#define VAR_LISTER_H

#include "variable.h"
#include "structure.h"
#include <iostream>
#include <vector>

#define ALIGNEMENT_MACRO ("times (((16) - (($-$$) % (16))) % (16)) nop\n")

class var_lister
{
    public:
        var_lister();
        virtual ~var_lister();
        bool addLocalVar(std::string name, std::string default_value, type_var_size var_size);
        bool addLocalStruct(std::string struct_name, std::string name);
        bool addStaticVar(std::string name, std::string default_value, type_var_size var_size);
        bool addGlobalVar(std::string name, std::string default_value, type_var_size var_size);
        std::string getVarAddress(std::string name);
        var_type checkVar(std::string name);
        std::string getStaticVarsBuffer();
        variable* at(std::string name);

        void clearLocal() {m_local_var_lister->clear();m_static_local_var_lister->clear();m_stack_offset=4;}
    protected:

    private:
        std::vector<variable*> *m_local_var_lister;
        std::vector<variable*> *m_static_local_var_lister;
        std::vector<variable*> *m_global_var_lister;
        std::string *m_static_buffer;
        int m_stack_offset;
        int m_static_var_counter;


};

#endif // VAR_LISTER_H
