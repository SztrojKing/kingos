/*
*******************************************************************************
* Source code writen in "Sztrojka Code" by LANEZ Ga�l - 2015                  *
* Permission to use, copie and modifie this code.                             *
*******************************************************************************
*/
include "include\bios.sc"
include "include\asm_def.sc"


define FAT_bpbRootEntries 224
define _18MB 1300000
define FILE_NAME_SIZE 11
function fat_read_file:uint8_t drive, ptr file_name, ptr file_buffer

	! fat struct
	uint16_t bpbBytesPerSector
	uint8_t bpbSectorsPerCluster
	uint16_t bpbReservedSectors
	uint8_t bpbNumberOfFATs
	uint16_t bpbRootEntries
	uint16_t bpbTotalSectors
	uint8_t bpbMedia
	uint16_t bpbSectorsPerFAT
	! other
	uint16_t root_entry_size
	uint16_t root_entry_start
	
	uint16_t data_region
	
	bool found
	uint16_t entry_left
	ptr buffer=_18MB
	call read_disk_raw:drive, 0, 1, buffer
	if getreturn =/= 0
		return 1
	endif
	! read fat properties from boot loader
	bpbBytesPerSector = #(buffer+0x0b)
	bpbSectorsPerCluster = #(buffer+0x0d)
	bpbReservedSectors = #(buffer+0x0e)
	bpbNumberOfFATs = #(buffer+0x10)
	bpbRootEntries = #(buffer+0x11)
	bpbTotalSectors = #(buffer+0x13)
	bpbMedia = #(buffer+0x15)
	bpbSectorsPerFAT = #(buffer+0x16)
	
	! calc root entry properties
	root_entry_size = (32*bpbRootEntries)/bpbBytesPerSector
	root_entry_start = (bpbNumberOfFATs*bpbSectorsPerFAT)+bpbReservedSectors
	
	! read root entry
	call read_disk_raw:drive, root_entry_start, root_entry_size, buffer
	if getreturn =/= 0
		return 0
	endif
	
	! search file
	found = false
	entry_left = bpbRootEntries
	while found =/= true
		call memcmp:buffer, file_name, FILE_NAME_SIZE
		if getreturn = 0
			found = true
		else
			buffer = buffer+32
			entry_left--
		endif
		! end of root entry ?
		if entry_left = 0
			call prints:{"Fichier non trouve.\n",0}
			return 0
		endif
	endwhile
	
	! we found the file

	! calc fat properties
	uint16_t cluster
	uint16_t fat_size
	uint16_t lba_to_read
	uint32_t file_size
	file_size = #(buffer+0x1c)
	cluster = #(buffer+0x1a)
	fat_size = bpbNumberOfFATs * bpbSectorsPerFAT
	
	! load fat
	call read_disk_raw:drive, bpbReservedSectors, fat_size, buffer
	if getreturn =/= 0
		return 0
	endif
	
	data_region = root_entry_start + root_entry_size
	
	! read clusters until the end cluster (0x0ff0)
	while cluster < 0x0FF0 
		call cluster2lba:cluster, bpbSectorsPerCluster
		lba_to_read = getreturn
		lba_to_read = lba_to_read + data_region
		
		! read current data
		call read_disk_raw:drive, lba_to_read, bpbSectorsPerCluster, file_buffer
		
		! calculate next sector
			! get low 12 bits
			if (cluster%2) = 1
				cluster = (#(buffer+((cluster/2)+cluster)) >> 0x04)
			else
				cluster = (#(buffer+((cluster/2)+cluster)) & 0x0FFF)
			endif
		  
		  
		file_buffer = file_buffer +(bpbSectorsPerCluster*bpbBytesPerSector)
	endwhile
	return file_size
endfunction

function cluster2lba:uint16_t cluster, uint16_t sector_per_cluser
	uint16_t lba
	lba = (cluster-2)*sector_per_cluser
	return lba
endfunction