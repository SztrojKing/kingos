/*
*******************************************************************************
* Source code wrote in "Sztrojka Code" by LANEZ Ga�l - 2014                   *
* Permission to use, copie and modifie this code.                             *
*******************************************************************************
*/

include "include\asm_def.sc"


! affiche une chaine termin� par 0
function bios_prints:ptr ptr_s

	short char=0
	char= #ptr_s

	while char =/= 0
	
		! bios printc
		al = char
		ah = 0x0e
		_int 0x10
		
		ptr_s++
		char = #ptr_s

		
	endwhile
	
endfunction


! efface l'ecran
function bios_cls

	ah = 0x06
	al = 0x00
	bh = 0x07
	ch = 0
	cl = 0
	dh = 25
	dl = 80
	_int 0x10

	call bios_move_cursor:0,0
 
endfunction

function bios_move_cursor: short x, short y
	ah = 0x02
	bh = 0x00
	dh = y
	dl = x
	_int 0x10
endfunction

! attend l'appuye sur une touche
function bios_wait_key

	_xor ax, ax
	_int 0x16
	
endfunction

! effectue un reset d'un disque
function bios_reset_drive:short drive
	_xor ax, ax
	dl = drive
	_int 0x13
endfunction

! lit des donn�es d'une disque
/*
function bios_read_disk:short drive, long lba, long size, long seg_buff, long buff
	
	
	! int 0x13 ah=0x42 bios packet
	!						magic			blckcnt		buff		seg_buff     	 	lba
	long read_disk_packet=	{0x0010,		0x0000,		0x0000,		0x0000,	 			0x0000, 0x0000, 0x0000, 0x0000}
	ptr packet_seeker
	packet_seeker = @read_disk_packet

	packet_seeker + 2
	#packet_seeker = size
	
	packet_seeker + 2
	#packet_seeker = buff
	
	packet_seeker + 2
	#packet_seeker = seg_buff
	
	packet_seeker + 2
	#packet_seeker = lba
	

	$ mov si, bios_read_diskread_disk_packet
	ah = 0x42
	dl = drive
	_int 0x13
		
	$ jnc __read_disk_pas_erreur
		return 1
	$ __read_disk_pas_erreur:
	return 0

endfunction
*/