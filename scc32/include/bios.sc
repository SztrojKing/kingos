/*
*******************************************************************************
* Source code wrote in "Sztrojka Code" by LANEZ Ga�l - 2014                   *
* Permission to use, copie and modifie this code.                             *
*******************************************************************************
*/
define DEFAULT_COLOR 0x07

define COLOR_BLACK 0x00
define COLOR_DARK_BLUE 0x01
define COLOR_GREEN 0x02
define COLOR_AQUA 0x03
define COLOR_BROWN 0x04
define COLOR_PURPLE 0x05
define COLOR_KHAKI 0x06
define COLOR_LIGHT_GREY 0x07
define COLOR_GREY 0x08
define COLOR_LIGHT_BLUE 0x09
define COLOR_LIGHT_GREEN 0x0a
define COLOR_CYAN 0x0b
define COLOR_RED 0x0c
define COLOR_PINK 0x0d
define COLOR_YELLOW 0x0e
define COLOR_WHITE 0x0f

include "include\asm_def.sc"
include "include\jumble.sc"



! ****************************** SCREEN ******************************
! affiche une chaine termin� par 0
function bios_prints:uint8_t color, ptr ptr_s

	uint8_t char=0
	char= #ptr_s

	while char =/= 0
	
		! bios printc
		bl = color
		bh = 0
		al = 0
		ah = 0x09
		cx = 1
		_int 0x10
		
		al = char
		ah = 0x0e
		_int 0x10
		
		ptr_s++
		char = #ptr_s

		
	endwhile
	
endfunction

function bios_printc:uint8_t color, uint8_t char
		bl = color
		bh = 0
		al = 0
		ah = 0x09
		cx = 1
		_int 0x10
		
		al = char
		ah = 0x0e
		_int 0x10
endfunction

! efface l'ecran
function bios_cls

	ah = 0x06
	al = 0x00
	bh = 0x07
	ch = 0
	cl = 0
	dh = 25
	dl = 80
	_int 0x10

	call bios_move_cursor:0,0
 
endfunction

! deplace le curseur
function bios_move_cursor: uint8_t x, uint8_t y
	ah = 0x02
	bh = 0x00
	dh = y
	dl = x
	_int 0x10
endfunction

! passe en mode text 80x25
function bios_set_text_mode
	ah = 0x00
	al = 0x03
	_int 0x10
	
	call bios_set_cursor:7
endfunction

function bios_set_cursor:uint8_t cursor
	ch = cursor
	cl = 7
	ah = 1
	_int 0x10
endfunction
! ****************************** KBD ******************************

! attend l'appuye sur une touche
function bios_wait_key

	_xor ax, ax
	_int 0x16
	uint8_t return_value
	return_value = ah
	return return_value
	
endfunction

! ****************************** DISKS ******************************

! effectue un reset d'un disque
function bios_reset_drive:uint8_t drive
	_xor ax, ax
	dl = drive
	_int 0x13
endfunction

! retourne le nombre max de cylindres, secteurs et tetes (ptr to uint8_t)
function bios_get_drive_parameters:uint8_t drive, ptr max_cylinder, ptr max_head, ptr max_sector

	uint8_t my_reg
	
	if drive < 0x80
		my_reg = 18
		#max_sector = my_reg
		my_reg = 80
		#max_cylinder = my_reg
		my_reg = 2
		#max_head = my_reg
	else
		my_reg = 63
		#max_sector = my_reg
		my_reg = 1
		#max_cylinder = my_reg
		my_reg = 16
		#max_head = my_reg
	endif

endfunction
! lit des donn�es d'un disque

function bios_read_hdd:uint8_t drive, uint16_t lba, uint16_t size, uint16_t seg_buff, uint16_t buff
	
	
	! int 0x13 ah=0x42 bios packet
	!						packet size		0			blckcnt		buff			seg_buff     	 		lba
	uint8_t read_disk_packet={0x10,			0x00,		0x00,0x00,	0x00,0x00,		0x00,0x00,	 			0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00}
	ptr packet_seeker
	uint8_t error_code
	packet_seeker = @ read_disk_packet

	packet_seeker=packet_seeker + 2
	#packet_seeker = size
	
	packet_seeker=packet_seeker + 2
	#packet_seeker = buff
	
	packet_seeker=packet_seeker + 2
	#packet_seeker = seg_buff
	
	packet_seeker=packet_seeker + 2
	#packet_seeker = lba
	
	packet_seeker = @read_disk_packet
	
	si = packet_seeker
	ah = 0x42
	dl = drive
	_int 0x13
	

	$ jnc .pas_erreur
		error_code = ah
		if error_code =/= 0
			call print_bios_disk_error:error_code
		endif
		return error_code
	$ .pas_erreur:
	return 0

endfunction

! ecrit des donn�es sur un disque
function bios_write_hdd:uint8_t drive, uint16_t lba, uint16_t size, uint16_t seg_buff, uint16_t buff
	
	
	!int 0x13 ah=0x42 bios packet
	!						packet size		0			blckcnt		buff			seg_buff     	 		lba
	uint8_t write_disk_packet={0x10,			0x00,		0x00,0x00,	0x00,0x00,		0x00,0x00,	 			0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00}
	ptr packet_seeker
	uint8_t error_code
	packet_seeker = @ write_disk_packet

	packet_seeker=packet_seeker + 2
	#packet_seeker = size
	
	packet_seeker=packet_seeker + 2
	#packet_seeker = buff
	
	packet_seeker=packet_seeker + 2
	#packet_seeker = seg_buff
	
	packet_seeker=packet_seeker + 2
	#packet_seeker = lba
	
	packet_seeker = @write_disk_packet
	
	si = packet_seeker
	ah = 0x43
	dl = drive
	_int 0x13
		
	$ jnc .pas_erreur
		error_code = ah
		if error_code =/= 0
			call print_bios_disk_error:error_code
		endif
		return error_code
	$ .pas_erreur:
	return 0

endfunction

! retourne les parametre du bios (INT 13H ah=15H)
function bios_get_drive_type: uint8_t drive
	uint8_t error_code
	ah = 0x15
	dl = drive
	_int 0x13
	error_code = ah
	return error_code
endfunction

!ejecte un cd -> 0 no error
function bios_eject_media:uint8_t drive
	uint8_t error_code
	if drive < 0x80
		ah = 0x46
		al = 0
		dl = drive
		_int 0x13
	endif
	error_code = ah
	return error_code
endfunction

! read sector from disk
function bios_read_disk_chs:uint8_t drive, uint8_t n_sector,uint8_t cylinder, uint8_t head, uint8_t sector, uint16_t seg_buff, uint16_t buff
	uint8_t error_code
	ah = 0x02
	al = n_sector
	! ch = cylinder
	ch = cylinder
	cl = sector
	dh = head
	dl = drive
	bx = buff
	
	$ push es
	es = seg_buff
	$ int 0x13
	$ pop es
	
	error_code = ah
	! on regarde si il y a une erreur
	$ jnc .pas_erreur
		call print_bios_disk_error:error_code
		return 1
	$ .pas_erreur:
	return 0
endfunction

! write sector to disk
function bios_write_disk_chs:uint8_t drive, uint8_t n_sector,uint8_t cylinder, uint8_t head, uint8_t sector, uint16_t seg_buff, uint16_t buff
	uint8_t error_code
	ah = 0x03
	al = n_sector
	ch = cylinder
	cl = sector
	dh = head
	dl = drive
	bx = buff
	$ push es
	es = seg_buff
	$ int 0x13
	$ pop es
	error_code = ah
	$ jnc .pas_erreur
		call print_bios_disk_error:error_code
		return 1
	$ .pas_erreur:
	return 0
endfunction

! read block
function bios_read_disk_block:uint8_t drive, uint16_t lba, uint16_t size, uint16_t seg_buff, uint16_t buff

	uint8_t current_cylinder
	uint8_t current_head
	uint8_t current_sector
	call bios_prints:COLOR_KHAKI, {"*",0}
	if drive < 0x80
		call lba2chs:drive, lba, @current_cylinder, @current_head, @current_sector
		call bios_read_disk_chs:drive , size, current_cylinder, current_head, current_sector, seg_buff, buff
		if getreturn =/= 0
			return 1
		endif
	else
		call bios_read_hdd:drive, lba, size, seg_buff, buff
		if getreturn =/= 0
			return 1
		endif
	endif
	return 0
endfunction


! write block do disk (max size = 50)
function bios_write_disk_block:uint8_t drive, uint16_t lba, uint16_t size, uint16_t seg_buff, uint16_t buff


	uint8_t current_cylinder
	uint8_t current_head
	uint8_t current_sector
	call bios_prints:COLOR_KHAKI, {"*",0}
	if drive < 0x80
		call lba2chs:drive, lba, @current_cylinder, @current_head, @current_sector
		call bios_write_disk_chs:drive , size, current_cylinder, current_head, current_sector, seg_buff, buff
		if getreturn =/= 0
			return 1
		endif
	else
		call bios_write_hdd:drive, lba, size, seg_buff, buff
		if getreturn =/= 0
			return 1
		endif		
	endif
	return 0

endfunction

function read_disk_raw:uint8_t drive, uint16_t lba, uint16_t size, ptr buff

	! read by blocks of 50 sectors
	while size > 50
		call bios_read_disk_block:drive, lba, 50, 0x1000, 0x0
		if getreturn =/= 0
			return 1
		endif
		! 50 * 512 = 0x6400
		call memcpy_16_to_32:buff, 0x1000, 0x0, 0x6400
		size = size - 50
		lba = lba + 50
		buff = buff + 0x6400
	endwhile
	
	! read last sectors ( < 50)
	call bios_read_disk_block:drive, lba, size, 0x1000, 0x0
	if getreturn =/= 0
		return 1
	endif
	! 50 * 512 = 0x6400
	size = size * 512
	call memcpy_16_to_32:buff, 0x1000, 0x0, size
	
	return 0
endfunction

function write_disk_raw:uint8_t drive, uint16_t lba, uint16_t size, ptr buff
	uint16_t buff_size
	! read by blocks of 60 sectors
	while size > 50
		! 50 * 512 = 0x6400
		call memcpy_32_to_16:0x1000, 0x0, buff, 0x6400
		call bios_write_disk_block:drive, lba, 50, 0x1000, 0x0
		if getreturn =/= 0
			return 1
		endif
		size = size - 50
		lba = lba + 50
		buff = buff + 0x6400
	endwhile
	! write last sectors ( < 50)
	buff_size = size * 512
	call memcpy_32_to_16:0x1000, 0x0, buff, buff_size
	call bios_write_disk_block:drive, lba, size, 0x1000, 0x0
	if getreturn =/= 0
		return 1
	endif
	return 0

endfunction

! ****************************** SYSTEM ******************************

! shutdown
function bios_shutdown
		
		! Connect BIOS APM 
		ax = 0x5301
		bx = 0
		_int 0x15
		
		! Engage BIOS APM
		ax = 0x530f
		bx = 1
		cx = 1
		_int 0x15
		
		! Shutdown
		ax = 0x5307
		bx = 1
		cx = 3
		_int 0x15
		
		
endfunction

! reboot
function bios_reboot

		_int 0x19
		
endfunction

function bios_sleep: uint32_t time
	uint16_t cx_tmp
	uint16_t dx_tmp
	
	cx_tmp = (time>>16)
	dx_tmp = (time&0xffff)
	
	ah = 0x86
	cx = cx_tmp
	dx = dx_tmp
	_int 0x15
endfunction
! ****************************** DEBUG *******************************

function print_bios_disk_error:uint16_t error
	call bios_prints:COLOR_RED, {"[DISK ERROR] ",0}
	call print_number:COLOR_WHITE, error
	call bios_prints:COLOR_RED, {": '", 0}
	if error = 0
		call prints:{"successful completion",0}
	endif
	if error = 0x01
		call prints:{"invalid function in AH or invalid parameter",0}
	endif
	if error = 0x02
		call prints:{"address mark not found",0}
	endif
	if error = 0x03
		call prints:{"disk write-protected",0}
	endif
	if error = 0x04
		call prints:{"sector not found/read error",0}
	endif
	if error = 0x05
		call prints:{"reset failed (hard disk)",0}
	endif
	if error = 0x06
		call prints:{"disk changed (floppy)",0}
	endif
	if error = 0x07
		call prints:{"drive parameter activity failed (hard disk)",0}
	endif
	if error = 0x08
		call prints:{"DMA overrun",0}
	endif
	if error = 0x09
		call prints:{"data boundary error (attempted DMA across 64K boundary or >80h sectors)",0}
	endif
	if error = 0x0a
		call prints:{"bad sector detected (hard disk)",0}
	endif
	if error = 0x0b
		call prints:{"bad track detected (hard disk)",0}
	endif
	if error = 0x0c
		call prints:{"unsupported track or invalid media",0}
	endif
	call bios_prints:COLOR_RED, {"'\n", 0}
	return error
endfunction