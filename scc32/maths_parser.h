#ifndef MATHS_PARSER_H_INCLUDED
#define MATHS_PARSER_H_INCLUDED

#include <vector>
#include <string>
#include "structure.h"
#include "var_lister.h"
#include "Struct_lister.h"
enum types { DELIMITER = 1, VARIABLE, NUMBER, HEXADECIMAL, PTR, FUNCTION_CALL, COMPARISON, AND_OR};

class analyzer
{
	private:
	  char *pExpresion;		// Points to the exp<b></b>ression.
	  char *token;		// Holds current token.
	  char tokenType;		// Holds token's type.

	  void recursive0();
	  void recursive1();
	  void recursive2();
	  void recursive3();
	  void recursive4();
	  void recursive5();
	  void bomb();
	  void nextToken();
	  void serror(int error);
	  int isdelim(char c);
		int isStructVar(char *expr);
	  void push_eax();
	  void pop_eax();

	  void push_ebx();
	  void pop_ebx();

	  void push_ecx();
	  void pop_ecx();

	  void push_edx();
	  void pop_edx();

        unsigned int hexstrtoi(std::string str);

	  std::vector<int> *m_stack;
	  unsigned int eax, ebx, ecx, edx;
	  std::string *m_nasm_expr;
        bool m_error;
        std::string *m_error_msg;
        bool optimise; // si pas de variable ni pointeur, on calcul directement
        type_var_size default_operation_size;
        type_var_size operation_size;
        var_lister *m_variables;
        Struct_lister *m_structures;
        type_var_size m_out_size;
        bool m_out_size_changed;
        bool is_cmp;
        int n_and_or;
	public:
	  analyzer(var_lister *variables, Struct_lister *structures, type_var_size op_size);// analyzer constructor.
	  std::string sniff(char *exp);			// analyzer entry point.
	  bool error() {return m_error;};
	  std::string errorStr() {return *m_error_msg;};
	  type_var_size getOutputSize() {return m_out_size;}
	  ~analyzer();						// analyzer destructor.
	  bool isCall(char *expr);
	  int isCmp(char *expr);
	   static int cmp_level;
};
#endif // MATHS_PARSER_H_INCLUDED
