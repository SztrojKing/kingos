#ifndef TRAITEMENT_H_INCLUDED
#define TRAITEMENT_H_INCLUDED

#include <fstream>
#include <vector>
#include "fonction.h"
#include "structure.h"
#define FLAG_DANS_FONCTION 1

#define IF_NO_ELSE 1
#define IF_ELSE 0

#define xor_eax() {out << "xor eax, eax\n";}
#define xor_ebx() {out << "xor ebx, ebx\n";}

#define PIC_LABEL "__SC16_PIC_OFFSET__"

enum erreurs {DEJA_DANS_FONCTION = 1, REDEF, ARG_MANQUANT, ARG_ERREUR, PAS_NOMBRE, MOT_CLEF_NON_RECONNU, VARIABLE_NON_EXISTANTE, PAS_DANS_FONCTION, PAS_DANS_CONDITION, MAUVAIS_TYPE};

int compile(std::stringstream &in, std::ofstream &out, int &_ligne, std::string point_entree, std::string &fichier, std::string fichier_origine, std::string org);


// cut line into words
std::vector<std::string> decoupe(std::string &ligne, int cur_l);


// string exist among vector<string> ?
int vector_string_existe(std::vector<std::string> liste, std::string nom);

// is number ?
bool estNombre(std::string number);


std::string preprocesseur(std::ifstream &in, std::stringstream &out, std::string file_name, std::vector<std::string> &fichiers_inclus, std::vector<std::string> &buf_define,std::vector<std::string> &buf_define_value);

// is valid word ?
bool mot_valide(std::string &mot);

// is registery ?
int estReg(std::string &mot);

// is a valid word?
bool estMot(std::string &mot);


void convert_illegal_char(std::string &str);

// return delimitor type
int isDelimitor(std::string expr);

// convert convert maths to char *expr
int getMathsExpr(std::vector<std::string> line, char *expr, unsigned int start_offset, bool is_cmp);

// convert maths to asm
int doMaths(std::vector<std::string> line, unsigned int start_offset, std::string *out, type_var_size *op_size, bool is_cmp);

// process call
int doCall(std::vector<std::string> line, unsigned int start_offset, unsigned int *stop_offset, std::string *out, bool is_cmp);

// convert {"azerty", 0} to decimal
std::string convertBraceToStr(std::string str);

// get next instruction
std::istream & getNextLine(std::istream &stream, std::string &str);

// check if all parentheses are closed and nicely opened
int checkParentheses();
#endif // TRAITEMENT_H_INCLUDED
