#include "maths_parser.h"
#include <iostream>
#include <cstdlib>
#include <cctype>
#include <cstring>
#include <vector>
#include <string>
using namespace std;

#include <string>
#include <sstream>
#include "var_lister.h"
#include "Struct_lister.h"
#include "traitement.h"

int analyzer::cmp_level=0;
namespace patch
{
    template < typename T > std::string to_string( const T& n )
    {
        std::ostringstream stm ;
        stm << n ;
        return stm.str() ;
    }
}



//************************************
// 13 analyzer constructor.
//************************************
analyzer::analyzer(var_lister *variables, Struct_lister *structures, type_var_size op_size)
{

    m_stack = new vector<int>;
    eax = ebx = ecx = edx = 0;
	pExpresion = NULL;
	m_nasm_expr=new string;
	m_error=false;
	m_error_msg=new string("");
	optimise=true;
	token=new char[2048];
	default_operation_size=op_size;
	operation_size=default_operation_size;
	m_variables=variables;
	m_structures = structures;
	m_out_size=op_size;
	m_out_size_changed=false;
	is_cmp=false;
	n_and_or=0;
	if(m_out_size==VAR_PTR)
        m_out_size=VAR_UINT32_T;


}

//************************************
// 14 analyzer entry point.
//************************************
string analyzer::sniff(char *exp)
{





	pExpresion = exp;

	nextToken();
	if(!*token)
	{
		serror(2);						// No exp<b></b>ression present.
		return "";
	}
	recursive0();
	if(*token)
        serror(0);			// Last token must be null.

	// must have one && or || for each comparison
	if(n_and_or<0||n_and_or>1)
		serror(0);

	if(optimise) // optimisation
	    *m_nasm_expr="mov eax, "+patch::to_string(eax)+"\n";

	// edx contains the cmp value, move it to eax
	//if(is_cmp)
		//*m_nasm_expr+="mov eax, edx\n";
	return *m_nasm_expr;

}
// && or ||
void analyzer::recursive0()
{
	char beer[3]={0};
	recursive1();


	if(!m_error)
	{
		while(tokenType == AND_OR)
		{
			strcpy(beer, token);
			is_cmp=true;
			nextToken();

			push_eax();
			recursive1();

			n_and_or--;

			*m_nasm_expr+="mov ebx, eax\n";
			ebx = eax;
			pop_eax();

			if(strcmp(beer, "&&")==0){
				eax = eax & ebx;
				*m_nasm_expr+="and eax, ebx\n";
			}else if(strcmp(beer, "||")==0){
				eax = eax | ebx;
				*m_nasm_expr+="or eax, ebx\n";
			}
			else{
				cout<<"beer: '"<<beer<<"'"<<endl;
				serror(4);
			}


		}

	}
}

// comparaison
void analyzer::recursive1()
{
	char beer[4]={0};
	recursive2();
	if(!m_error)
	{
		while(tokenType == COMPARISON)
		{

			strcpy(beer, token);
			is_cmp=true;
			nextToken();

			push_eax();
			recursive2();

			n_and_or++;

			*m_nasm_expr+="mov ebx, eax\n";
			ebx = eax;
			// pop eax value into ecx (optimized way)
			pop_ecx();
			*m_nasm_expr+="xor eax, eax\n";
			*m_nasm_expr+="cmp ecx, ebx\n";
			if(strcmp(beer, "==")==0){
				eax = ecx == ebx;
				*m_nasm_expr+="jne";
			}else if(strcmp(beer, ">")==0){
				eax = ecx > ebx;
				*m_nasm_expr+="jbe";
			}else if(strcmp(beer, "<")==0){
				eax = ecx < ebx;
				*m_nasm_expr+="jae";
			}else if(strcmp(beer, ">=")==0){
				eax = ecx >= ebx;
				*m_nasm_expr+="jb";
			}else if(strcmp(beer, "<=")==0){
				eax = ecx <= ebx;
				*m_nasm_expr+="ja";
			}
			else if(strcmp(beer, "=/=")==0){
				eax = ecx != ebx;
				*m_nasm_expr+="je";
			}
			else{
				cout<<"beer: '"<<beer<<"'"<<endl;
				serror(4);
			}
			*m_nasm_expr+=" .__lbl_cmp_"+patch::to_string(cmp_level)+"\n" \
						  "mov eax, 1\n" \
						  ".__lbl_cmp_"+patch::to_string(cmp_level)+":\n";
			cmp_level++;

		}


	}
}

//************************************
// 4 Add or subtract two terms.
//************************************
void analyzer::recursive2()
{


	char beer;



	recursive3();
	if(!m_error)
	{
		while((tokenType == DELIMITER) && ((beer = *token) == '+' || beer == '-'))
		{
			nextToken();

			push_eax();
			recursive3();

			*m_nasm_expr+="mov ebx, eax\n";
			ebx = eax;
			pop_eax();

			switch(beer)
			{
			  case '-':
				eax = eax-ebx;
				*m_nasm_expr+="sub eax, ebx\n";
				break;
			  case '+':
				eax = eax + ebx;
				*m_nasm_expr+="add eax, ebx\n";
				break;
			}
		}


	}
}
//************************************
// 5 Multiply, divide or binary operations between two factors.
//************************************
void analyzer::recursive3()
{




    char beer;

	recursive4();
	if(!m_error)
	{
		while((tokenType == DELIMITER) && ((beer = *token) == '*' || (*token) == '/' || (*token) == '%' || (*token) == '&' || (*token) == '|' || (*token) == '>' || (*token) == '<'))
		{
			nextToken();
			push_eax();
			recursive4();

			*m_nasm_expr+="mov ebx, eax\n";
			ebx = eax;
			pop_eax();
			switch(beer)
			{
			  case '*':

				eax = eax * ebx;

				*m_nasm_expr+="mul ebx\n";
				break;
			  case '/':
			  	if(optimise)
			  		if(ebx!=0)
						eax = eax / ebx;
					else{
						*m_error_msg+="Division par 0\n";
						serror(0);
					}

				*m_nasm_expr+="mov edx, 0\n";
				*m_nasm_expr+="div ebx\n";

				break;
			  case '%':
			  	if(optimise)
					if(ebx!=0)
						eax = (int)eax % (int)ebx;
					else{
						*m_error_msg+="Modulo par 0\n";
						serror(0);
					}

				*m_nasm_expr+="mov edx, 0\n";
				*m_nasm_expr+="div ebx\n";
				*m_nasm_expr+="mov eax, edx\n";

				break;
			  case '&': // binary AND
				eax = eax & ebx;
				*m_nasm_expr+="and eax, ebx\n";
				break;

			  case '|': // binary OR
				eax = eax | ebx;
				*m_nasm_expr+="or eax, ebx\n";
				break;
			  case '>': // binary >>
				  eax = (eax >> ebx);
				  *m_nasm_expr+="mov cl, bl\nshr eax, cl\n";
				break;
			  case '<': // binary <<
				  eax = (eax << ebx);
				  *m_nasm_expr+="mov cl, bl\nshl eax, cl\n";
				break;

			}
		}
	}
}

//************************************
// 7 Evaluate a unary + or - or ~
//************************************
void analyzer::recursive4()
{



	char  beer;

	beer = 0;
	if((*token == '+' || *token == '-' || *token == '~'))
	{
		beer = *token;
		nextToken();
	}

	recursive5();
	if(!m_error && (tokenType == DELIMITER))
	{
		if(beer == '-')
		{

			eax = -eax;
			*m_nasm_expr+="neg eax\n";
		}
		else if(beer=='~') // one's complement
		{
			eax = ~eax;
			*m_nasm_expr+="not eax\n";
		}
		else if(beer=='+')
			; // do nothing
	}
}

//************************************
// 8 Process a parenthesized exp<b></b>ression.
//************************************
void analyzer::recursive5()
{


	if((tokenType == DELIMITER || tokenType==COMPARISON || tokenType==AND_OR) && (*token == '('))
	{
		nextToken();
		recursive0();
		if(!m_error)
		{

			if(*token != ')')
			{
				serror(1);
			}
			else
				nextToken();
		}
	}
	else if(tokenType==PTR)
    {
        nextToken();
        recursive5();
		if(!m_error)
		{
			*m_nasm_expr+="mov ecx, eax\n";

			if(operation_size==VAR_UINT8_T)
				*m_nasm_expr+="xor eax, eax\nmov al, [ecx]\n"; // 8 bits operation
			else if(operation_size==VAR_UINT16_T)
				*m_nasm_expr+="xor eax, eax\nmov ax, [ecx]\n"; // 16 bits operation
			else
				*m_nasm_expr+="mov eax, [ecx]\n"; // 32 bits operation

			optimise=false;
		}
    }
	else
        bomb();



}

//************************************
// 9 Get the value of a number.
//************************************
void analyzer::bomb()
{
	string call_assembly="";
	unsigned int stop_offset=0;
	vector<string> buf_mot_expr;
	string token_str = string(token);
	string main_var;
	string struct_var;

	if(!m_error)
	{
	  switch(tokenType)
	  {
		case NUMBER:
			eax = atoi(token);
			*m_nasm_expr+="mov eax, " +  patch::to_string(atoi(token)) + "\n";

		  break;
		case VARIABLE:



			if(this->isStructVar(token)){
				main_var = token_str.substr(0,token_str.find("."));
				struct_var = token_str.substr(token_str.find(".")+1);
				if(struct_var=="size"){
					if(m_structures->at(main_var)){
						// retrieve the size of the struct
						eax=m_structures->at(main_var)->get_current_offset();
						*m_nasm_expr+="mov eax, " + patch::to_string(m_structures->at(main_var)->get_current_offset())+"\n";
					}
					else
					{
						*m_error_msg=string(main_var) + " : ";
						serror(3);
					}
				}
				else
				{
					optimise=false;
					eax=ebx=1;
					if(m_variables->checkVar(main_var)){
						if(m_variables->at(main_var)->isStruct()){
                            string struct_name = m_variables->at(main_var)->getStructName();
                            if(m_structures->at(struct_name)->at(struct_var)){
								*m_nasm_expr+="mov eax, ["+m_variables->at(main_var)->address()+"]\n";
								*m_nasm_expr+="add eax, "+m_structures->at(struct_name)->at(struct_var)->address()+"\n";
								if(!m_out_size_changed)
									operation_size = m_out_size=m_structures->at(struct_name)->at(struct_var)->size();


                            }
                            else
                            {
								*m_error_msg=string(struct_var) + " : ";
								serror(3);
							}
						}
						else{
							*m_error_msg=string(main_var) + " : ";
							serror(5);
						}
					}
					else{
						*m_error_msg=string(main_var) + " : ";
						serror(3);
					}

				}
			}
			else if(m_variables->checkVar(string(token)))
			{
				switch(m_variables->at(string(token))->size())
				{
					case VAR_UINT8_T:
						*m_nasm_expr+="xor eax, eax\nmov al, [";
					break;
					case VAR_UINT16_T:
						*m_nasm_expr+="xor eax, eax\nmov ax, [";
					break;
					default:
						*m_nasm_expr+="mov eax, [";
				}
				*m_nasm_expr+=m_variables->at(string(token))->address()+"]\n";
				eax=ebx=1;
				optimise=false;
				// update out size if size isnt changed by user
				// if it's not ptr
				if(!m_out_size_changed&&m_variables->at(string(token))->size()!=VAR_PTR)
				{
					m_out_size=m_variables->at(string(token))->size();
				}
			}
			else{
				*m_error_msg=string(token) + " : ";
				serror(3);

			}
			break;


		case HEXADECIMAL:
			eax = hexstrtoi(string(token));
			*m_nasm_expr+="mov eax, " +  patch::to_string(eax) + "\n";
			break;


		case FUNCTION_CALL:
			optimise=false;
			eax=ebx=1;

			// save ebx (might be modified by function call)
			push_ebx();
			buf_mot_expr = decoupe(token_str, 0);
//std::cout<<"token_str="<<token_str<<"\n";
			if(doCall(buf_mot_expr, 0, &stop_offset, &call_assembly, false)<0)
				serror(0);

			*m_nasm_expr += call_assembly;
			pop_ebx();
			break;


		default:
		  serror(0);
	  }
	  nextToken();
	}
}

//************************************
// 11 Display a syntax error.
//************************************
void analyzer::serror(int error)
{


	if(!m_error)
	{
		static char *e[]=
		{
			(char*)"Erreur de syntaxe",
			(char*)"Parenthese ou crochet manquant",
			(char*)"Expression manquante",
			(char*)"Variable inexistante",
			(char*)"Erreur interne",
			(char*)"La variable n'est pas une structure"
		};
		m_error=true;
		*m_error_msg+="PARSER > "+string(e[error])+"\n";
	}
}

//************************************
// 10 Obtain the next token.
//************************************
void analyzer::nextToken()
{
	char lastTokenType=tokenType;
	int tOffset=0;
	if(m_error)
		return;

	char *bucket;

	tokenType = 0;
	if(!token)
    {
        serror(4);
        return;
    }
	bucket = token;
	*bucket = '\0';

	if(!*pExpresion) return; // At end of exp<b></b>ression.

	while(isspace(*pExpresion)) ++pExpresion; // Skip over white space.

    std::string strToken(pExpresion);
    string myOPSizeStr;
    unsigned int i=1;

	if((tOffset=isCmp(pExpresion))>0){
		memcpy(bucket, pExpresion, tOffset);
		bucket+=tOffset;
		pExpresion+=tOffset;
		tokenType = COMPARISON;
	}
	else if((pExpresion[0]=='&'&&pExpresion[1]=='&')||(pExpresion[0]=='|'&&pExpresion[1]=='|')){
		memcpy(bucket, pExpresion, 2);
		bucket+=2;
		pExpresion+=2;
		tokenType = AND_OR;
	}
	else if(strchr("+-*/%()&|~", *pExpresion))
	{
		tokenType = DELIMITER;
		// Advance to next char.
		*bucket++ = *pExpresion++;
	}
    else if(*pExpresion=='>') //
    {
        tokenType = DELIMITER;
        *bucket++ = *pExpresion++;
        if(*pExpresion++!='>')
            serror(0);
    }
    else if(*pExpresion=='<') //
    {
        tokenType = DELIMITER;
        *bucket++ = *pExpresion++;
        if(*pExpresion++!='<')
            serror(0);
    }
    else if(*pExpresion=='#')
    {
        tokenType=PTR;
        *bucket++ = *pExpresion++;
    }
    else if(*pExpresion=='[')
    {

            // get operation size
            for(;strToken.at(i)!=']'&&i<strToken.size();i++)
            {
                myOPSizeStr+=strToken.at(i);
                *bucket++ = strToken.at(i);
            }
            if(i==strToken.size())
                serror(1);
            else
            {
                if(myOPSizeStr=="uint8_t")
                {
                    operation_size=VAR_UINT8_T;
                }
                else if(myOPSizeStr=="uint16_t")
                {
                    operation_size=VAR_UINT16_T;
                }
                else if(myOPSizeStr=="uint32_t")
                {
                    operation_size=VAR_UINT32_T;
                }
                else if(myOPSizeStr=="ptr")
                {
                    operation_size=VAR_UINT32_T;
                }
                else
                    serror(0);
            }
            pExpresion+=myOPSizeStr.size()+2; // skip '[uintx_t]'
            m_out_size_changed=true;
            m_out_size=operation_size;
            *bucket = '\0';
            nextToken();
    }
    // handle char as value
    else if(*pExpresion=='\'')
    {
        if(*(pExpresion+2)!='\'')
            serror(1);
        strToken= patch::to_string((int)(*(pExpresion+1)));
        for(unsigned int i =0;i<strToken.size();i++)
            *bucket++=strToken.at(i);
        tokenType = NUMBER;
        pExpresion+=3;
    }
    // function call
    else if(this->isCall(pExpresion)){
		// copy function name
		while(!isdelim(*pExpresion))
			*bucket++ = *pExpresion++;

		int parenthesisLevel=0;

		do{
			if(*pExpresion=='(')
				parenthesisLevel++;
			else if(*pExpresion==')')
				parenthesisLevel--;

			*bucket++ = *pExpresion++;

		}while(parenthesisLevel>0);

		tokenType = FUNCTION_CALL;
    }
    else if(this->isStructVar(pExpresion)>0){

		memcpy(bucket, pExpresion, this->isStructVar(pExpresion));
		bucket+=this->isStructVar(pExpresion);
		pExpresion+=this->isStructVar(pExpresion);
		tokenType = VARIABLE;
    }
	else if(isalpha(*pExpresion) || pExpresion[i]=='_')
	{
		while(!isdelim(*pExpresion)) *bucket++ = *pExpresion++;
		tokenType = VARIABLE;
	}
	else if(*pExpresion=='0'&&(*(pExpresion+1)=='x'||*(pExpresion+1)=='X')) // hexadecimal number
    {
        pExpresion+=2; // skip 0x
        while(!isdelim(*pExpresion)) *bucket++ = *pExpresion++;
        tokenType = HEXADECIMAL;
    }
	else if(isdigit(*pExpresion)) // decimal number
	{
		while(!isdelim(*pExpresion)) *bucket++ = *pExpresion++;
		tokenType = NUMBER;
	}
	else{

        serror(0);
	}

	*bucket = '\0';
}

//************************************
// 12 Return true if c is a delimiter.
//************************************
int analyzer::isdelim(char c)
{



	if(strchr(" +-*/%()&|><~#[=", c) || c == 9 || c == '\r' || c == 0)
		return 1;
	return 0;
}

// return true if expr is a function call
bool analyzer::isCall(char *expr){
	int i=0;
	bool res=false;
	while(!isdelim(expr[i])){
		i++;
	}
	if(i>0 && expr[i]=='(')
		res=true;
	return res;
}


// return true if expr is a struct var (struct.var)
int analyzer::isStructVar(char *expr){
	int i=0,j=0;
	while(isalpha(expr[i])||isdigit(expr[i])||expr[i]=='_'){
		i++;
	}
	if(expr[i]=='.'){
		i++;
		while(isalpha(expr[i])||isdigit(expr[i])||expr[i]=='_'){
			i++;
			j++;
		}
		if(j==0)
			i=0;
	}
	else
		i=0;

	return (i>=3?i:0);
}

int analyzer::isCmp(char *expr){
	int res=0;
	if(expr[0]=='='){
			if(expr[1]=='=')
				res=2;
			else if(expr[1]=='/' && expr[2]=='=')
				res=3;
	}
	else if(expr[0]=='>' && expr[1]!='>'){
		res=1;
		if(expr[1]=='=')
			res=2;
	}
	else if(expr[0]=='<' && expr[1]!='<'){
		res=1;
		if(expr[1]=='=')
			res=2;
	}
	return res;
}

void analyzer::push_eax()
{
   m_stack->push_back(eax);
   // cout << "PUSH EAX " << eax << endl;
    *m_nasm_expr+="push eax\n";
}

void analyzer::pop_eax()
{
    eax = m_stack->back();
    m_stack->pop_back();
    //cout << "POP EAX " << eax << endl;
    *m_nasm_expr+="pop eax\n";
}

void analyzer::push_ebx()
{
    m_stack->push_back(ebx);
   //cout << "PUSH EBX " << ebx << endl;
    *m_nasm_expr+="push ebx\n";
}

void analyzer::pop_ebx()
{
    ebx = m_stack->back();
    m_stack->pop_back();
   // cout << "POP EBX " << ebx << endl;
    *m_nasm_expr+="pop ebx\n";
}

void analyzer::push_ecx()
{
    m_stack->push_back(ecx);
    *m_nasm_expr+="push ecx\n";
}

void analyzer::pop_ecx()
{
    ecx = m_stack->back();
    m_stack->pop_back();
    *m_nasm_expr+="pop ecx\n";
}

void analyzer::push_edx()
{
    m_stack->push_back(edx);
    *m_nasm_expr+="push edx\n";
}

void analyzer::pop_edx()
{
    edx = m_stack->back();
    m_stack->pop_back();
    *m_nasm_expr+="pop edx\n";
}


unsigned int analyzer::hexstrtoi(string str)
{
    unsigned int buf=0;
    std::stringstream ss;
    ss << std::hex << str;
    ss >> buf;
    return buf;
}
//************************************
// 15 analyzer destructor.
//************************************
analyzer::~analyzer()
{
    delete m_stack;
    delete m_nasm_expr;
    delete token;
}

