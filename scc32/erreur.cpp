#include "erreur.h"

#ifdef WINDOWS
#include <windows.h>
#endif
#include <iostream>
#include <sstream>
using namespace std;

string toAscii(char tab[])
{
#ifdef WINDOWS
char buffer[1024];
CharToOemA(tab, buffer);
string str(tab);
return str;
#else
    return tab;
#endif // WINDOWS
}

int out_str(string msg)
{
    #ifdef WINDOWS
    char buf[1024];
    CharToOemA(msg.c_str(), buf);
    string buf_str(buf);
    cout << buf_str << endl;
    #else
        cout << msg << endl;
    #endif
    return 0;
}

void out_warning(string fichier, int ligne, string mot_erreur, string msg)
{

    stringstream ss;
    string str;
    ss << "[ATTENTION] Fichier '" << fichier << "', ligne " << ligne << ", mot '" << mot_erreur << "':\n"<< msg << endl;
    str=ss.str();
    out_str(str);

}

int out_error(string fichier, int ligne, string mot_erreur, string msg)
{
    stringstream ss;
    string str;
    ss << "[ERREUR] Fichier '" << fichier << "', ligne " << ligne << ", mot '" << mot_erreur << "':\n"<< msg << endl;
    str=ss.str();
    out_str(str);
    return -1;
}

int out_debug(string fichier, int ligne, string mot_erreur, string msg)
{
        stringstream ss;
    string str;
    ss << "[DEBUG] Fichier '" << fichier << "', ligne " << ligne << ", mot '" << mot_erreur << "':\n"<< msg << endl;
    str=ss.str();
    out_str(str);
    return -1;
}

















