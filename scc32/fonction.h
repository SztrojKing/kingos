#ifndef FONCTION_H
#define FONCTION_H
#include <iostream>
#include <vector>
#include "variable.h"

class fonction
{
    public:
        fonction(std::string name);
        fonction(fonction &f);
        virtual ~fonction();
        std::string Getnom() { return *m_nom; }
        void Setnom(std::string val) { *m_nom = val; }
        std::vector<variable> Getvariables() { return *m_variables; }
        void Setvariables(std::vector<variable> val) { *m_variables = val; }
        bool addVariable(const std::string nom, const int type);
        void clear() { m_nom->clear(); m_variables->clear(); }
        bool findVariable(std::string nom);
    protected:
    private:
        std::string *m_nom;
        std::vector<variable> *m_variables;
};

#endif // FONCTION_H
