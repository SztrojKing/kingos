#include "Switch.h"
#include <iostream>
#include <sstream>

int Switch::n_switch = 0;
using namespace std;

namespace patch
{
    template < typename T > std::string to_string( const T& n )
    {
        std::ostringstream stm ;
        stm << n ;
        return stm.str() ;
    }
}

Switch::Switch():isDefaultCase(false), n_case(0)
{
    Switch::n_switch++;
    this->current_switch=Switch::n_switch;
}

Switch::~Switch()
{
    //dtor
}

string Switch::add_case(const string &comp_value){
    string res;
    res = "cmp eax, "+comp_value+"\n"+
        "jne .switch_" + patch::to_string(this->current_switch) + "_case_lbl_"+patch::to_string(this->n_case+1)+"\n";
    this->n_case++;
    return res;
}


string Switch::add_end_case(){
    string res;
    res = "jmp .switch_end_lbl_"+patch::to_string(this->current_switch)+"\n" +
    ".switch_" + patch::to_string(this->current_switch) + "_case_lbl_"+patch::to_string(this->n_case)+":\n";
    return res;
}

string Switch::add_default(){
    this->n_case++;
    return "";
}

string Switch::add_end_switch(){
    string res;
    res =".switch_end_lbl_"+patch::to_string(this->current_switch)+":\n";
    return res;
}
