#include "variable.h"
#include <iostream>
using namespace std;
variable::variable(std::string name, std::string nasm_address, char var_type, type_var_size var_size)
{
    //ctor
    m_Name=new string;
    *m_Name=name;
    m_nasm_address=new string(nasm_address);
    m_default_value=new string;
    m_var_type=var_type;
    m_var_size=var_size;
    m_struct_name=NULL;

}
variable::variable(std::string name, std::string nasm_address, char var_type, std::string struct_name)
{
    //ctor
    m_Name=new string;
    *m_Name=name;
    m_nasm_address=new string(nasm_address);
    m_default_value=new string;
    m_var_type=var_type;
    m_var_size=VAR_PTR;
    m_struct_name=new string(struct_name);
}
variable::variable()
{
    //ctor
    m_Name=new string;
    m_nasm_address=new string;
    m_default_value=new string;
    m_var_type=UNKNOW_VAR;
    m_var_size=VAR_UINT32_T;
        m_struct_name=NULL;

}

variable::~variable()
{
    //dtor
    delete m_Name;
    delete m_nasm_address;
    delete m_default_value;
}

