#include "var_lister.h"
#include "variable.h"


#include "structure.h"
#include <iostream>
#include <vector>
using namespace std;

#include <string>
#include <sstream>



namespace patch
{
    template < typename T > std::string to_string( const T& n )
    {
        std::ostringstream stm ;
        stm << n ;
        return stm.str() ;
    }
}



var_lister::var_lister() //ctor
{

    m_static_local_var_lister=new vector<variable*>;
    m_local_var_lister=new vector<variable*>;
    m_global_var_lister=new vector<variable*>;
    m_static_buffer = new string;
    m_stack_offset = 4;
    m_static_var_counter=0;

}

var_lister::~var_lister() //dtor
{
    delete m_static_local_var_lister;
    delete m_local_var_lister;
    delete m_static_buffer;
    delete m_global_var_lister;
}

// add static var
bool var_lister::addStaticVar(std::string name, std::string default_value, type_var_size var_size)
{
    // cout << "DEBUG: addStaticVar(" << name << ", " << default_value << ", " << var_size << ")\n";
    if(checkVar(name)==UNKNOW_VAR) // if already exists
    {

        variable *var_tmp = new variable(name, STATIC_VAR_PREFIX  + patch::to_string(m_static_var_counter) , STATIC_VAR, var_size);
        *m_static_buffer+="; " + name + "\n" + ALIGNEMENT_MACRO + STATIC_VAR_PREFIX + patch::to_string(m_static_var_counter) + ": ";
        switch(var_size)
        {
            case VAR_UINT8_T:
                *m_static_buffer+="db " + default_value;

            break;
            case VAR_UINT16_T:
                *m_static_buffer+="dw " + default_value;
            break;
            case VAR_UINT32_T:
               *m_static_buffer+="dd " + default_value;
            break;
                case VAR_PTR:
                *m_static_buffer+="dd " + default_value;
            break;
            default:
                *m_static_buffer+="dd " + default_value;
        }
        *m_static_buffer+="\n";
        var_tmp->setDefaultValue(default_value);
        m_static_local_var_lister->push_back(var_tmp);
        m_static_var_counter++;
        // cout << "DEBUG: Added.\n";
        return true;
    }
    // cout << "DEBUG: Not added.\n";
    return false;
}

// add local var
bool var_lister::addLocalVar(std::string name, std::string default_value, type_var_size var_size)
{
    // cout << "DEBUG: addLocalVar(" << name << ", " << default_value << ", " << var_size << ")\n";
    if(checkVar(name)==UNKNOW_VAR) // if already exists
    {

        variable *var_tmp = new variable(name, "ebp-" + patch::to_string(m_stack_offset) , LOCAL_VAR, var_size);
        var_tmp->setDefaultValue(default_value);
        m_local_var_lister->push_back(var_tmp);

        switch(var_size)
        {
            case VAR_UINT8_T:
                m_stack_offset+=4;

            break;
            case VAR_UINT16_T:
                m_stack_offset+=4;
            break;
            case VAR_UINT32_T:
                m_stack_offset+=4;
            break;
            case VAR_PTR:
                m_stack_offset+=4;
            break;
            default:
                m_stack_offset+=4;
        }
        // cout << "DEBUG: Added.\n";
        return true;
    }
    // cout << "DEBUG: Not added.\n";
    return false;
}

// add local var
bool var_lister::addLocalStruct(std::string struct_name, std::string name)
{
    if(checkVar(name)==UNKNOW_VAR) // if already exists
    {

        variable *var_tmp = new variable(name, "ebp-" + patch::to_string(m_stack_offset) , LOCAL_VAR, struct_name);
        var_tmp->setDefaultValue("0");
        m_local_var_lister->push_back(var_tmp);

		m_stack_offset+=4;
        // cout << "DEBUG: Added.\n";
        return true;
    }
    // cout << "DEBUG: Not added.\n";
    return false;
}
// add global var
bool var_lister::addGlobalVar(std::string name, std::string default_value, type_var_size var_size)
{
    // cout << "DEBUG: addStaticVar(" << name << ", " << default_value << ", " << var_size << ")\n";
    if(checkVar(name)==UNKNOW_VAR) // if already exists
    {

        variable *var_tmp = new variable(name, STATIC_VAR_PREFIX  + patch::to_string(m_static_var_counter) , GLOBAL_VAR, var_size);
        *m_static_buffer+="; " + name + "\n" + ALIGNEMENT_MACRO + STATIC_VAR_PREFIX + patch::to_string(m_static_var_counter) + ": ";
        switch(var_size)
        {
            case VAR_UINT8_T:
                *m_static_buffer+="db " + default_value;

            break;
            case VAR_UINT16_T:
                *m_static_buffer+="dw " + default_value;
            break;
            case VAR_UINT32_T:
               *m_static_buffer+="dd " + default_value;
            break;
                case VAR_PTR:
                *m_static_buffer+="dd " + default_value;
            break;
            default:
                *m_static_buffer+="dd " + default_value;
        }
        *m_static_buffer+="\n";
        var_tmp->setDefaultValue(default_value);
        m_global_var_lister->push_back(var_tmp);
        m_static_var_counter++;
        // cout << "DEBUG: Added.\n";
        return true;
    }
    // cout << "DEBUG: Not added.\n";
    return false;
}

// check if var exist and return the type
var_type var_lister::checkVar(std::string name)
{
    // cout << "DEBUG: Checking var '" << name << "'\n";
    std::vector<variable*>::iterator it;
    variable var_tmp;
    // static local

    // cout << "DEBUG: Checking STATIC\n";
    it = m_static_local_var_lister->begin();
    while(it != m_static_local_var_lister->end())
    {

        if((*it)->name()==name)
            return STATIC_VAR;
        ++it;
    }

    // local
    it = m_local_var_lister->begin();
    while(it != m_local_var_lister->end())
    {

        if((*it)->name()==name)
            return LOCAL_VAR;
        ++it;
    }

    // global
    it = m_global_var_lister->begin();
    while(it != m_global_var_lister->end())
    {

        if((*it)->name()==name)
            return GLOBAL_VAR;
        ++it;
    }
    // not found
    // cout << "DEBUG: Not found.\n";
    return UNKNOW_VAR;
}

// return iterator to var with name
variable* var_lister::at(std::string name)
{
    // cout << "DEBUG: At->Checking var'" << name << "'\n";
    std::vector<variable*>::iterator it;
    // static local
    it = m_static_local_var_lister->begin();
    while(it != m_static_local_var_lister->end())
    {

        if((*it)->name()==name)
            return *it;
        ++it;
    }

    // local
    it = m_local_var_lister->begin();
    while(it != m_local_var_lister->end())
    {

        if((*it)->name()==name)
            return *it;
        ++it;
    }

    // global
    it = m_global_var_lister->begin();
    while(it != m_global_var_lister->end())
    {

        if((*it)->name()==name)
            return *it;
        ++it;
    }
     cout << "<<FATAL ERROR>> Variable '" << name << "' do not exist\n";
     while(1);
     return NULL;
}

// return all statics vars with defaults value
std::string var_lister::getStaticVarsBuffer()
{

     return *m_static_buffer;
}
