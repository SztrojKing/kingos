#include "Struct_lister.h"
#include "structure.h"
#include <iostream>

Struct_lister::Struct_lister()
{
	//ctor
	m_struct_lister = new std::vector<Structure*>();
}

Struct_lister::~Struct_lister()
{
	//dtor
	delete m_struct_lister;
}

Structure* Struct_lister::at(std::string name){
	// cout << "DEBUG: At->Checking var'" << name << "'\n";
    std::vector<Structure*>::iterator it;
    // static local
    it = m_struct_lister->begin();
    while(it != m_struct_lister->end())
    {

        if((*it)->name()==name)
            return *it;
        ++it;
    }
	return (Structure*)NULL;
}
