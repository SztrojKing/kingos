#include "structure.h"
#include "variable.h"
#include <iostream>

#include <string>
#include <sstream>

namespace patch
{
    template < typename T > std::string to_string( const T& n )
    {
        std::ostringstream stm ;
        stm << n ;
        return stm.str() ;
    }
}

using namespace std;

Structure::Structure(std::string name, bool isPackedStruct)
{
	//ctor
	m_name = new string(name);
	m_var_lister = new vector<variable*>();
	m_current_offset=0;
	m_is_packed_struct = isPackedStruct;
}

Structure::~Structure()
{
	//dtor
	delete m_name;
}

bool Structure::addVar(std::string name, type_var_size var_size){
	if(!this->at(name)){
		this->m_var_lister->push_back(new variable(name, patch::to_string(m_current_offset), LOCAL_VAR, var_size));
		if(m_is_packed_struct){
			switch(var_size){
				case VAR_UINT8_T:
					m_current_offset++;
				break;
				case VAR_UINT16_T:
					m_current_offset+=2;
				break;
				default:
					m_current_offset+=4;
			}
		}
		else
			m_current_offset+=4;

		return true;
	}
	return false;
};

// return iterator to var with name
variable* Structure::at(std::string name)
{
    // cout << "DEBUG: At->Checking var'" << name << "'\n";
    std::vector<variable*>::iterator it;
    // static local
    it = m_var_lister->begin();
    while(it != m_var_lister->end())
    {

        if((*it)->name()==name)
            return *it;
        ++it;
    }
     return NULL;
}
