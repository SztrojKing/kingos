#include "optimizer.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include "../traitement.h"
#include "../structure.h"
#include <vector>
#include <locale>
#include <string>
#include <cstdlib>
using namespace std;
Optimizer::Optimizer()
{
	//ctor
}

Optimizer::~Optimizer()
{
	//dtor
}
optimizer_registers Optimizer::getRegFromString(string reg){
    if(reg == "eax" || reg=="ax" || reg=="ah" || reg=="al")
        return REG_EAX;
    if(reg == "ebx" || reg=="bx" || reg=="bh" || reg=="bl")
        return REG_EBX;
    if(reg == "ecx" || reg=="cx" || reg=="ch" || reg=="cl")
        return REG_ECX;
    if(reg == "edx" || reg=="dx" || reg=="dh" || reg=="dl")
        return REG_EDX;
    return REG_UNKNOWN;
}

bool Optimizer::optimizeCode(istream &in, ofstream &outStream, bool isPositionIndependentCode){
    string rawline;
    bool lineChanged;
    vector<string> line;
    stringstream out;
	string pic_address="0x110000";
	unsigned int eax, ebx, ecx, edx;
	bool isEaxKnown=false, isEbxKnown = false, isEcxKnown=false, isEdxKnown=false;
	bool isRegValueJustKnown=false;
    if(in.eof() || !outStream.is_open())
    	return false;
    int val;
	 while(std::getline(in, rawline)){
		lineChanged=false;
        isRegValueJustKnown=false;
		line = decoupe_no_point(rawline, 0);

		if(line.size()>0){
			if(line.at(0)==";");
			else{


                // when affecting the value of a register, check if the register don't already contain the same value
                // mov reg, something
                /*
                if(line.size() == 4 && (( line.at(0)=="mov" && estNombre(line.at(3)) ) || line.at(0)=="xor" && getRegFromString(line.at(1))!=REG_UNKNOWN && getRegFromString(line.at(1)) == getRegFromString(line.at(3)))){
                    if(estNombre(line.at(3)) || line.at(0) == "xor"){
                        if(estReg(line.at(1))==VAR_UINT32_T){
                            // what is the value ?
                            if(line.at(0) == "xor" && line.at(1) == line.at(3) )
                                val = 0;
                            else
                                val = atoi(line.at(3).c_str());

                            switch(getRegFromString(line.at(1))){
                            case REG_EAX:
                                // do the register already contains the value ?
                                if(isEaxKnown && eax == val){
                                    lineChanged=true;
                                }
                                else{
                                    isEaxKnown=true;
                                    eax = val;
                                    isRegValueJustKnown=true;
                                }
                                break;
                            }
                        }
                    }
                }

                 // if the value of a register has been changed #label
                 if(line.at(0) == "call" || (line.size() == 2 && estMot(line.at(0)) && line.at(1) == ":") || (line.size() == 3  && line.at(0) == "." &&  line.at(2) == ":")){
                    isEaxKnown=false;
                    isEbxKnown=false;
                    isEcxKnown=false;
                    isEdxKnown=false;
                 }
                // if the value of a register has been changed (if the register is in destination) and if the line has not been changed by previous tests
                // exclude push instruction witch wont modify the register
                if(line.size()>=2 && lineChanged==false && isRegValueJustKnown==false && getRegFromString(line.at(1))!=REG_UNKNOWN&& line.at(0)!="push"){
                            switch(getRegFromString(line.at(1))){
                            case REG_EAX:
                                isEaxKnown=false;
                                break;

                            case REG_EBX:
                                isEbxKnown=false;
                                break;

                            case REG_ECX:
                                isEcxKnown=false;
                                break;

                            case REG_EDX:
                                isEdxKnown=false;
                                break;
                            }
                 }
				// convert mov reg, 0 to xor reg, reg
				if(line.size() == 4 && line.at(0)=="mov" && estReg(line.at(1))!=0 && line.at(3)=="0" && lineChanged==false){
					lineChanged=true;
					out << "xor " << line.at(1) << ", " << line.at(1) << endl;
				}

				// delete useless add/sub reg, 0
				else */ if(line.size() == 4 && (line.at(0)=="sub"||line.at(0)=="add") && estReg(line.at(1))!=0 && line.at(3)=="0"){
					lineChanged=true;
				}
				else if(isPositionIndependentCode){
					/* convert mov reg32, LABEL
						to
						mov reg32, LABEL
						add reg32, [" << pic_address << "]
					*/
					if(line.size()==4&&line.at(0)=="mov" && estReg(line.at(1))==VAR_UINT32_T && estMot(line.at(3)) && estReg(line.at(3))==0 ){
						lineChanged=true;
						out << rawline << endl << "add " << line.at(1) << ", [" << pic_address << "]\n";
					}
					/* convert mov reg, [LABEL]
						to
						mov esi, LABEL
						add esi, [" << pic_address << "]
						mov reg, [esi]
					*/
					else if(line.size()==6&&line.at(0)=="mov" && estReg(line.at(1))!=0 && line.at(3)=="[" && !estReg(line.at(4)) &&  estMot(line.at(4)) ){
						lineChanged=true;
						string reg2use="esi";
						if(line.at(1)=="esi"||line.at(1)=="si")
							reg2use="edi";

						out << "mov " << reg2use << ", " << line.at(4) << "\nadd " << reg2use << ", [" << pic_address << "]\nmov " << line.at(1) << ", [" << reg2use << "]\n";
					}
					/* convert mov [LABEL], reg
						to
						mov edi/esi, LABEL
						add edi/esi, " << pic_address << "
						mov [edi/esi], reg
					*/
					else if(line.size()==6&&line.at(0)=="mov" && line.at(1)=="[" && estReg(line.at(5))!=0 && !estReg(line.at(2)) &&  estMot(line.at(2)) ){
						lineChanged=true;
						if(line.at(5)=="edi"||line.at(5)=="di"){
							out << "mov esi, " << line.at(2) << "\n"
								"add esi, [" << pic_address << "]\n"
								"mov [esi], " << line.at(5) << "\n";
						}
						else
						{
							out << "mov edi, " << line.at(2) << "\n"
									"add edi, [" << pic_address << "]\n"
									"mov [edi], " << line.at(5) << "\n";
						}
					}
					/* convert inc/dec SIZE [LABEL]
						mov edi, LABEL
						add edi, [" << pic_address << "]
						inc SIZE [edi]
					*/
					else if(line.size()==5 && (line.at(0)=="inc"||line.at(0)=="dec") && estMot(line.at(3)) && !estReg(line.at(3))){
						lineChanged=true;
						out << "mov edi, " << line.at(3) << "\nadd edi, [" << pic_address << "]\n" << line.at(0) << " " << line.at(1) << " [edi]\n";
					}
					/* convert push dword LABEL
						to
						mov esi, LABEL
						add esi, [" << pic_address << "]
						push esi
					*/
					else if(line.size()==3 && line.at(0)=="push"  && line.at(1)=="dword" && estMot(line.at(2)) && !estReg(line.at(2))){
						lineChanged=true;
						out << "mov esi, " << line.at(2) << "\nadd esi, [" << pic_address << "]\npush esi\n";
					}
					/* convert lidt/lgdt [LABEL]
						to
						mov esi, LABEL
						add esi, [" << pic_address << "]
						lidt/lgdt [esi]
					*/
					else if(line.size()==4 && (line.at(0)=="lidt"||line.at(0)=="lgdt") && estMot(line.at(2)) && !estReg(line.at(2))){
						lineChanged=true;
						out << "mov esi, " << line.at(2) << "\nadd esi, [" << pic_address << "]\n" << line.at(0) << " [esi]\n";
					}
					/* convert mov dword [ebp-x], LABEL
						to
						mov esi, LABEL
						add esi, [" << pic_address << "]
						mov [ebp-x], esi
					*/
					else if(line.size()==9 && line.at(0)=="mov"&&line.at(3)=="ebp" && estNombre(line.at(5)) && estMot(line.at(8)) && !estReg(line.at(8))){
						lineChanged=true;
						out << "mov esi, " << line.at(8) << "\nadd esi, [" << pic_address << "]\nmov [ebp" << line.at(4) << line.at(5) << "], esi\n";
					}
					/* convert mov dword [LABEL], OTHER_LABEL
						to
						mov edi, LABEL
						add edi, [" << pic_address << "]
						mov esi, OTHER_LABEL
						add esi, [" << pic_address << "]
						mov [edi], esi
					*/
					else if(line.size()==7 && line.at(0)=="mov"&&line.at(2)=="[" && estMot(line.at(3)) && !estReg(line.at(3)) && estMot(line.at(6)) && !estReg(line.at(6))){
						lineChanged=true;
						out << "mov edi, " << line.at(3) << "\nadd edi, [" << pic_address << "]\n" << "mov esi, " << line.at(6) << "\nadd esi, [" << pic_address << "]\nmov [edi], esi\n";
					}
				}
			}
		}

		if(!lineChanged)
			out << rawline << endl;
	 }

	 outStream << out.str();
	 return true;
}


vector<string> Optimizer::decoupe_no_point(string &ligne, int cur_l)
{

        vector<string> argument;
        string tmp;
        tmp.clear();
        for(unsigned int i=0; i<ligne.length();i++)
        {
            if(ligne[i]==' '||ligne[i]==0x09/* TAB */)
            {

                if(!tmp.empty()&&i+1<ligne.length()&&(ligne[i+1]!=' '||ligne[i+1]!=0x09/* TAB */))
                {
                    argument.push_back(tmp);
                    tmp.clear();
                }
            }
            else if(ligne[i]=='"')
            {
            	if(!(argument.size()>=1 && argument.at(0)=="include"))
            		tmp+='{';
                do
                {
                    tmp+=ligne[i];
                    i++;
                } while(i<ligne.length()&&ligne[i]!='"');
                if(tmp[tmp.size()-1]!='"')
                    tmp+='"';
				if(!(argument.size()>=1 && argument.at(0)=="include"))
					tmp+='}';
				if(i>=ligne.length())
                {
                    cout << "Warning: missing '\"' line " << cur_l << endl;
                }
                argument.push_back(tmp);
                tmp.clear();
            }
            else if(ligne[i]=='{')
            {
                do
                {
                    tmp+=ligne[i];
                    i++;
                } while(i<ligne.length()&&ligne[i]!='}');
                if(tmp[tmp.size()-1]!='}')
                {
                    tmp+='}';

                }
                if(i>=ligne.length())
                {
                    cout << "Warning: missing brace '}' line " << cur_l << endl;
                }
                tmp = convertBraceToStr(tmp);
                argument.push_back(tmp);
                tmp.clear();
            }
            else if(ligne[i]==0x27)
            {
                do
                {
                    tmp+=ligne[i];
                    i++;
                } while(i<ligne.length()&&ligne[i]!=0x27);
                if(tmp[tmp.size()-1]!=0x27)
                    tmp+=0x27;
                argument.push_back(tmp);
                tmp.clear();
            }
            else if(ligne[i]==','||ligne[i]=='&'||ligne[i]=='|'||ligne[i]=='~'||ligne[i]=='('||ligne[i]==')'||ligne[i]=='['||ligne[i]==']'||ligne[i]==':' \
                    ||ligne[i]=='#'||ligne[i]=='@'||ligne[i]=='='||ligne[i]=='+'||ligne[i]=='-'||ligne[i]=='*'||ligne[i]=='%'||ligne[i]=='/' \
                    ||ligne[i]=='<'||ligne[i]=='>'||ligne[i]=='!'||(ligne[i]=='\\'&&i==ligne.length()-1))
            {
                if(tmp.empty()==false)
                    argument.push_back(tmp);

                // =/=
                if(ligne[i]=='='&&i+2<ligne.length()&&ligne[i+1]=='/'&&ligne[i+2]=='=')
                {
                    tmp="=/=";
                    i+=2; // skip '/='
                }
				// ==
                else if(ligne[i]=='='&&i+1<ligne.length()&&ligne[i+1]=='=')
                {
                    tmp="==";
                    i++; // skip '='
                }
                // >=
                else if(ligne[i]=='>'&&i+1<ligne.length()&&ligne[i+1]=='=')
                {
                    tmp=">=";
                    i++; // skip '='
                }
                // <=
                else if(ligne[i]=='<'&&i+1<ligne.length()&&ligne[i+1]=='=')
                {
                    tmp="<=";
                    i++;
                }
                // >>
                else if(ligne[i]=='>'&&i+1<ligne.length()&&ligne[i+1]=='>')
                {
                    tmp=">>";
                    i++;
                }
                // <<
                else if(ligne[i]=='<'&&i+1<ligne.length()&&ligne[i+1]=='<')
                {
                    tmp="<<";
                    i++;
                }
                else
                    tmp = ligne[i];
                argument.push_back(tmp);
                tmp.clear();
            }
            else
                tmp+=ligne[i];
        }
        if(!tmp.empty())
            argument.push_back(tmp);
       // for(unsigned int i=0;i<argument.size();i++)
       //     cout << "Ligne #" << cur_l << " Arg #" << i << " = " << argument.at(i) << "\n";
        return argument;
}
