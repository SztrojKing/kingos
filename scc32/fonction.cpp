
#include <iostream>
#include <vector>
#include "variable.h"
#include "fonction.h"

using namespace std;

fonction::fonction(string name)
{
    //ctor
    m_nom = new string(name);
    m_variables = new vector<variable>;
}
fonction::fonction(fonction &f)
{
    m_nom = new string(f.Getnom());
    m_variables = new vector <variable>();
}

fonction::~fonction()
{
    //dtor
    delete m_nom;
    delete m_variables;
}

bool fonction::addVariable(const std::string nom, const int type)
{
    if(this->findVariable(nom))
        return false;
    variable var_tmp(nom, type);
    m_variables->push_back(var_tmp);
    return true;
}

bool fonction::findVariable(std::string nom)
{
    for(unsigned int i=0;i<m_variables->size(); i++)
        if(m_variables->at(i).Getnom()==nom)
            return true;
    return false;
}
