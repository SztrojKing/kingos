#include <iostream>
#include "erreur.h"
#include "traitement.h"
#include <fstream>
#include <sstream>
#include <stdlib.h>
//#include <windows.h>
#include "optimizer.h"
using namespace std;

#ifdef __linux__
    #define EXIT_SUCCESS 0
    #define EXIT_FAILURE 1
    double GetTickCount(void)
    {
      struct timespec now;
      if (clock_gettime(CLOCK_MONOTONIC, &now))
        return 0;
      return now.tv_sec * 1000.0 + now.tv_nsec / 1000000.0;
    }
#endif
int main(int argc, char **argv)
{
    string buf_cmd;
    string entree("entree.sc"), sortie("sortie.bin"), commande_nasm("nasm.exe -f bin sortie.asm -o "), point_entree("main");
    int  ticks = GetTickCount();
    bool auto_compil=true;
    bool boot_sect=false;
    bool pic=false; // position independent code
    bool optmize=true;
    string org("0");
    if(argc <= 1)
        return out_str("Aucun argument sp�cifi�. Utilisez '-aide' pour obtenir la liste des commandes.");

    // Parcours les arguments
    for(int i = 1;i<argc;i++)
    {
        buf_cmd = argv[i];
        if(buf_cmd == "-aide")
            out_str("Liste des commandes:\n\t-aide : Affiche la liste de commandes.\n"\
                    "\t-e 'fichier' : Indique le fichier source en entr�e.\n"\
                    "\t-s 'fichier' : Indique le fichier de sortie.\n"\
                    "\t-p_e 'nom' : Indique le point d'entree du programme (defaut = 'main').\n"\
                    "\t-b : Compilation pour secteur de boot.\n" \
                    "\t-no_auto_compil : Produit uniquement le fichier 'sortie.asm'\n" \
                    "\t-org 'offset' : Offset pour les adresses (defaut = 0)"
                    "\t-pic : Force le code a etre indepent de sa position(position independent code). Note: les 4 premiers octets du fichier binaire contiendront l'offset du code."\
                    "\t-disable_opt : Desactive l'optimization (active par default) (impossible a desactiver avec l'option -pic");
        else if(buf_cmd == "-e")
        {
            if(i++<argc)
            {
                buf_cmd = argv[i];
                entree = buf_cmd;
            }
            else
                return out_str("Nom de fichier manquant.");
        }
        else if(buf_cmd == "-s")
        {
            if(i++<argc)
            {
                buf_cmd = argv[i];
                sortie = buf_cmd;
            }
            else
                return out_str("Nom de fichier manquant.");
        }
        else if(buf_cmd == "-p_e")
        {
            if(i++<argc)
            {
                buf_cmd = argv[i];
                point_entree = buf_cmd;
            }
            else
                return out_str("Nom de fonction manquant.");
        }
        else if(buf_cmd=="-no_auto_compil")
        {
            auto_compil=false;
        }
         else if(buf_cmd == "-b")
        {
            boot_sect=true;
        }
		else if(buf_cmd == "-pic")
        {
            pic=true;
        }
        else if(buf_cmd == "-disable_opt")
        {
            optmize=false;
        }
        else if(buf_cmd == "-org")
        {
            if(i++<argc)
            {
                org = argv[i];
            }
            else
                return out_str("Argument manquant apr�s '-org'.");
        }
        else
            return out_str("Commande non reconnue. Utilisez '-aide' pour obtenir la liste des commandes.");
    }

    // ouvre le fichiers
    ifstream in(entree.c_str(), ios::in);
    ofstream out((pic||optmize)?"rawcode.asm":"sortie.asm", ios::out | ios::trunc);
    stringstream buf_in_out("");
    vector<string> buf_fichiers_inclus, buf_define, buf_define_value;

    int erreur, ligne, res=EXIT_SUCCESS;
    string str_erreur_tmp, str_erreur, buf;
    ostringstream sstream;
    string fichier_en_cours(entree);
    if(!in || !out)
         return out_str("Impossible d'ouvrir le fichier d'entr�e et/ou de sortie.");


    if((buf=preprocesseur(in, buf_in_out, entree.substr(0,entree.find(".")), buf_fichiers_inclus, buf_define, buf_define_value))!="Etape 1: succ�s.")
    {
        out_str(buf);
        return EXIT_FAILURE;
    }
    out_str(buf);

    if((erreur = compile(buf_in_out, out, ligne, point_entree, fichier_en_cours, entree, org))==0) // on commence la compilation
    {

        sstream << "Etape 2: succ�s.\n";

        if(boot_sect)
            out << "; magic code\ntimes 510-($-$$) db 0\ndw 0xAA55";
        in.close();
        out.close();
		if(pic||optmize){

			ifstream optimizerIn("rawcode.asm", ios::in);
			ofstream optimizerOut("sortie.asm", ios::out | ios::trunc);
			Optimizer optimizer;
			sstream << "Etape 3: Optimisation ";
			if(optimizer.optimizeCode(optimizerIn, optimizerOut, pic)){
				sstream << "r�ussie\n";
			}
			else{
				sstream << "erreur.\n";
				res=EXIT_FAILURE;
			}
			optimizerIn.close();
			optimizerOut.close();
		}



		if(res==EXIT_SUCCESS)
			sstream << "La compilation � r�ussi !\n" << ligne << " lignes compil�es en " << GetTickCount()-ticks << " ms.\n";

		out_str(sstream.str());

        commande_nasm+=sortie;
        if(auto_compil)
            return system(commande_nasm.c_str());
        else
            return res;

    }

    else
    {
        // lit le fichier jusqu'a arriver a la ligne correspondante a l'erreur (code d'erreur == numero de ligne de description)
        /*for(int i=0;i<erreur;i++)
            getline(msg_erreur, str_erreur_tmp);
        sstream << "Fichier '"<<fichier_en_cours<<"', ligne " << ligne <<  ": Erreur n� " << erreur << ": " << str_erreur_tmp;
        str_erreur = sstream.str();*/
        out_str("La compilation a �chou�e.\n");
        in.close();
        out.close();
      //  buf_out.close();
      return EXIT_FAILURE;

    }




    return res;
}
