/*
*******************************************************************************
* Source code writen in "Sztrojka Code" by LANEZ Ga�l - 2015                   *
* Permission to use, copie and modifie this code.                             *
*******************************************************************************
*/

include "include\library.sc"

sptr kbdmap=null

function init_keyboard
	kbdmap = call fat12_read_file: {"root0/system/x16/keyboard/azerty.map"}
	if kbdmap = null
		return false
	endif
	
	return true
	
endfunction

! convert qwerty keyboard code into azerty
function convertToAZERTY: uint8_t code
	uint8_t newCode = 0
	if kbdmap = null
		if call init_keyboard = false
			return code
		endif
	endif
	
	newCode = #(kbdmap+code)
	if newCode =/= 0
		return newCode
	else
		return code
	endif
endfunction


function init_mouse
	call mouse_install
endfunction

suint8_t mouse_cycle=0     !unsigned char
suint8_t mouse_byte0=0   ! //signed char
suint8_t mouse_byte1=0
suint8_t mouse_byte2=0
suint32_t mouse_x=0     ! //signed char
suint32_t mouse_y=0        ! //signed char

!//Mouse functions
!void mouse_handler(struct regs *a_r) //struct regs *a_r (not used but just there)
function mouse_handler
 
  if mouse_cycle = 0
      mouse_byte0=call inb:0x60
	  ! check packet alignement
	  if mouse_byte0 & 0x8 = 0
		! wrong alignement, ignore this packet
		mouse_cycle=0
	  else
		mouse_cycle++
	  endif
  else
	if mouse_cycle = 1
      mouse_byte1=call inb:0x60
      mouse_cycle++
    else
      mouse_byte2=call inb:0x60
	  
	  ! x < 0 ?
	  if mouse_byte0 & 0x10 > 0
		if mouse_x > (0xff-mouse_byte1) 
			mouse_x = mouse_x - (0xff-mouse_byte1+1)
		else
			mouse_x=0
		endif
	  else
		mouse_x=mouse_x+mouse_byte1
	  endif
	  
	  ! y < 0 ?
	  if mouse_byte0 & 0x20 > 0
			mouse_y = mouse_y + (0xff-mouse_byte2+1)
	  else
		if mouse_y > mouse_byte2
			mouse_y=mouse_y-mouse_byte2
		else
			mouse_y=0
		endif
	  endif
	  
	  
	  if mouse_x>= 80*10
		mouse_x = 79*10
	  endif
	  
	 if mouse_y>= 25*10
		mouse_y = 24*10
	  endif
      !mouse_y=mouse_y+mouse_byte2
      mouse_cycle=0
	  call coords_color_prints:20, 0, DEFAULT_COLOR, {"###"}
	  call coords_color_prints:25, 0, DEFAULT_COLOR, {"   "}
	  call coords_printn:20, 0, DEFAULT_COLOR, mouse_x
	  call coords_printn:25, 0, DEFAULT_COLOR, mouse_y
	  
	  call coords_color_prints:mouse_x/10, mouse_y/10, COLOR_RED, {"#"}
	endif
  endif
  
 

 
endfunction


function mouse_wait: uint8_t a_type
  uint32_t time_out=100000!; //unsigned int
  uint8_t tmp
  if a_type=0
    while time_out>0 !//Data
		time_out--
		tmp = call inb:0x64
		if(tmp & 1) = 1
			exit
		endif
    endwhile
  else
   while time_out>0 !//signal
		time_out--
		tmp = call inb:0x64
		if(tmp & 2) = 0
			exit
		endif
    endwhile
  endif
endfunction

function mouse_write: uint8_t a_write
  !//Wait to be able to send a command
  call mouse_wait:1
 ! //Tell the mouse we are sending a command
  call outb:0x64, 0xD4
 ! //Wait for the final part
  call mouse_wait:1
 ! //Finally write
  call outb:0x60, a_write
endfunction


function mouse_read
	uint8_t res=0
  !//Get's response from mouse
  call mouse_wait:0
  res = call inb:0x60
  return res
endfunction


function mouse_install
! mouse handler must be installed 
  asm cli
  uint8_t status=0
  !//Enable the auxiliary mouse device
  call mouse_wait:1
  call outb:0x64, 0xA8
  
  !//Enable the interrupts
  call mouse_wait:1
  call outb:0x64, 0x20
  call mouse_wait:0
  status=call inb:0x60
  status = status | 2
  call mouse_wait:1
  call outb:0x64, 0x60
  call mouse_wait:1
  call outb:0x60, status
  
 ! //Tell the mouse to use default settings
  call mouse_write:0xF6
  call mouse_read!();  //Acknowledge
  
  ! unmask all PIC interrupts
  call outb:0x21,0
  call outb:0xa1,0
 
 ! //Enable the mouse
  call mouse_write:0xF4
  call mouse_read!();  //Acknowledge


  asm sti
endfunction

function__no_stack__ int_0x74_handler_asm
	
	! save registers 
	asm pusha
	push_register es
	push_register fs
	push_register gs
	dx = ds
	gs = dx
	! setup segments
	dx = 0x800
	ds = dx
	! we're pushing gs wich is ds
	push_register gs
	call mouse_handler

	! restore registers
	pop_register ds
	
	pop_register gs
	pop_register fs
	pop_register es
	! end of interrupt
	asm mov al,0x20
	asm out 0x20,al
	asm out 0xa0,al
	asm popa
	
	asm iret
endfunction

