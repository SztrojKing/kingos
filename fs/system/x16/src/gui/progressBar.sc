/*
*******************************************************************************
* Source code writen in "Sztrojka Code" by LANEZ Ga�l - 2015                  *
* Permission to use, copie and modifie this code.                             *
*******************************************************************************
*/
include "include\library.sc"
include "include\gui\window.sc"

define PROGRESS_BAR_SIGNATURE 423320943

! create a progressBar struct
function progressBar_create:uint8_t x, uint8_t y, uint8_t size, uint8_t color, uint32_t max_value

	ptr progressBar
	progressBar=call malloc: 512
	
	! progressBar's size cant exceed screen width
	if size > 79-x
		size = 79-x
	endif
	#progressBar=[uint32_t]OBJECT_PROGRESS_BAR
	#(progressBar+4) = x
	#(progressBar+1+4)=y
	#(progressBar+2+4)=size
	#(progressBar+3+4)=color
	#(progressBar+4+4)=max_value
	! current value
	#(progressBar+8+4)=0

	
	return progressBar
endfunction

! draw a progressBar
function progressBar_draw:ptr progressBar, uint32_t value
	if progressBar=null
		exit
	endif
	
	uint8_t x=#(progressBar+4)
	uint8_t y=#(progressBar+1+4)
	uint8_t size=#(progressBar+2+4)
	size=(size-3)
	uint8_t color=#(progressBar+3+4)
	uint32_t max_value=#(progressBar+4+4)
	
	if x = POSITION_RIGHT
		x = 79-(size+3)
	endif
	
	if x = POSITION_CENTER
		x = 37-(size/2)-1
	endif
	x=(x+3)
	! update new value
	call progressBar_setValue:progressBar, value
	
	if max_value < 1000
		max_value=max_value*1000
		value=value*1000
	endif
	
	if value > max_value
		value=max_value
	endif
	
	if value >= (0xffffffff/10000)
		value=value/1000
		max_value=max_value/1000
	endif
	
	uint8_t i=0

	! dont divide by 0
	if (value*1000/max_value) > 0
		! draw until position
		while (i*1000)<((size)*(value*1000/max_value))
			call coords_printc:(i+x+1),y,color, 219
			i++
		endwhile
		! clear rest
		while i<(size)
			call coords_printc:(i+x),y,color, ' '
			i++
		endwhile
	endif
	call coords_printc:x,y,color, 199
	call coords_printc:(x+size),y,color, 182
	call coords_color_prints:(x-3), y, color, {"  %"}
	call coords_printn:(x-3), y, color, (value*100/max_value)
	return 0
endfunction

! redraw a progressBar with its current value
function progressBar_redraw:ptr progressBar
	call progressBar_draw:progressBar, #[uint32_t](progressBar+8+4)
endfunction

! set progressBar's current value
function progressBar_setValue:ptr progressBar, uint32_t value
	#(progressBar+8+4)=value
endfunction
