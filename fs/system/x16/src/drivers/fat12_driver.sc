/*
*******************************************************************************
* Source code writen in "Sztrojka Code" by LANEZ Ga�l - 2015                  *
* Permission to use, copie and modifie this code.                             *
*******************************************************************************
*/
include "include\drivers\bios_driver.sc"
include "include\asm_def.sc"
include "include\malloc.sc"
include "include\string.sc"

define FAT_bpbRootEntries 224
define FILE_NAME_SIZE 11
define PATH_NEXT_ENTRY_EOP 0xff

define FAT12_READ_ONLY 0x01 
define FAT12_HIDDEN 0x02 
define FAT12_SYSTEM 0x04 
define FAT12_VOLUME_ID 0x08 
define FAT12_DIRECTORY 0x10 
define FAT12_ARCHIVE 0x20

enum FAT12_MODE_READ_FILE, FAT12_MODE_GET_SIZE, FAT12_MODE_GET_FIRST_CLUSTER

! fat12 struct
	suint16_t bpbBytesPerSector
	suint8_t bpbSectorsPerCluster
	suint16_t bpbReservedSectors
	suint8_t bpbNumberOfFATs
	suint16_t bpbRootEntries
	suint16_t bpbTotalSectors
	suint8_t bpbMedia
	suint16_t bpbSectorsPerFAT
	! other
	suint16_t root_entry_size
	suint16_t root_entry_start
	
	suint16_t data_region
	
	suint8_t fat12_current_drive
	sptr fat12_current_fat=null
	
	
function fat12_read_file: ptr path
	return call _fat12_read_file: path, FAT12_MODE_READ_FILE
endfunction

function fat12_get_file_size: ptr path
	return call _fat12_read_file: path, FAT12_MODE_GET_SIZE
endfunction

function fat12_write_file: ptr path, ptr data, uint32_t size
	! dont write if file already exists
	if call fat12_get_file_size: path = 0
		return call _fat12_write_file: path, data, size, 0
	endif
	return 0
endfunction

function fat12_create_directory: ptr path
	ptr data=null
	uint32_t size=0
	if call fat12_get_file_size: path = 0
		size = (bpbRootEntries*32)
		data = call malloc: size
		size = call _fat12_write_file: path, data, size, FAT12_DIRECTORY
		call free: data
		return size
	endif
	return 0
endfunction

	
function fat12_read_file_from_directory:ptr file_name, ptr dir_buffer, ptr file_size_param, ptr starting_cluster

	bool found=false
	uint16_t entry_left
	ptr file_buffer
	#file_size_param = [uint32_t](0)
	
	
	! search file
	found = false
	entry_left = bpbRootEntries
	while found =/= true
		
		if call memcmp:dir_buffer, file_name, FILE_NAME_SIZE = 0
			found = true
		else
			dir_buffer = (dir_buffer+32)
			entry_left--
		endif
		! end of root entry ?
		if entry_left = 0
			!call cls
			!call prints:file_name
			!call prints:{"\nFichier non trouve.\n"}
			!call pause
			return null
		endif
	endwhile
	
	! we found the file

	! calc fat properties
	uint16_t cluster
	uint16_t fat_size
	uint16_t lba_to_read
	uint32_t file_size=#(dir_buffer+0x1c)
	uint8_t attributes=#(dir_buffer+0x0b)
	
	! dont align the file size param
	#file_size_param = file_size
	
	! align to 512 b (sector size)
	if file_size%bpbBytesPerSector =/= 0
		file_size = file_size + (bpbBytesPerSector - (file_size % bpbBytesPerSector))
	endif
	! check directory
	if attributes&FAT12_DIRECTORY =/= 0

		file_size=root_entry_size*bpbBytesPerSector
		#file_size_param = file_size
		!call prints:{"Directory\nSize="}
		!call printn:DEFAULT_COLOR, file_size
		!call pause
	endif
	
	! if file is empty
	if file_size = 0
		!call prints:{"Empty file\n"}
		return null
	endif
		
	file_buffer=call malloc:(file_size)
	
	ptr file_buffer_offseter=file_buffer
	
	cluster = #(dir_buffer+0x1a)
	#starting_cluster = cluster
	

	uint32_t size_read = 0
	! read clusters until the end cluster (0x0ff0)
	while cluster < 0x0FF0 and size_read < file_size
		lba_to_read = call cluster2lba:cluster
		
		! read current data
		call read_disk_raw:fat12_current_drive, (lba_to_read), bpbSectorsPerCluster, file_buffer_offseter
		
		cluster = call get_next_cluster: cluster
			
		file_buffer_offseter = file_buffer_offseter +(bpbSectorsPerCluster*bpbBytesPerSector)
		size_read = size_read + bpbSectorsPerCluster
	endwhile
	
	return file_buffer
endfunction
! 
function get_fat
	
	ptr fat_buffer
	fat_buffer = call malloc:((bpbNumberOfFATs * bpbSectorsPerFAT)*bpbBytesPerSector)
	
	! load fat
	if call read_disk_raw:fat12_current_drive, bpbReservedSectors, (bpbNumberOfFATs * bpbSectorsPerFAT), fat_buffer =/= 0
		return null
	endif
	fat12_current_fat = fat_buffer
	return fat_buffer
endfunction

function save_fat
	! save fat
	call write_disk_raw:fat12_current_drive, bpbReservedSectors, (bpbNumberOfFATs * bpbSectorsPerFAT), fat12_current_fat

endfunction

! return the value of the cluster
function get_next_cluster:  uint16_t cluster
	if (cluster%2) = 1
		return [uint16_t](#(fat12_current_fat+((cluster/2)+cluster)) >> 0x04)
	endif
	
	return [uint16_t](#(fat12_current_fat+((cluster/2)+cluster)) & 0x0FFF)

endfunction

! set the cluster to value
function set_cluster:  uint16_t cluster, uint16_t value
	if (cluster%2) = 1
		#(fat12_current_fat+((cluster/2)+cluster)) = [uint16_t](((#(fat12_current_fat+((cluster/2)+cluster)))&0x000f) |  (value << 0x04))
	else
		#(fat12_current_fat+((cluster/2)+cluster)) = [uint16_t]((#(fat12_current_fat+((cluster/2)+cluster)) & 0xF000) | value)
	endif
	
endfunction

! read root directory
function fat12_read_root_directory: uint8_t drive
	ptr buffer
	uint8_t previous_drive=fat12_current_drive
	
	fat12_current_drive = drive
	! header
	buffer = call malloc:512

	
	
	if call read_disk_raw:drive, 0, 1, buffer =/= 0
		call free: buffer
		return null
	endif
	
	! check if not FAT 12
	if call memcmp: (buffer+54), {"FAT12   "}, 8 =/= 0
		call free: buffer
		return null
	endif
	
	! read fat properties from boot loader
	bpbBytesPerSector = #(buffer+0x0b)
	bpbSectorsPerCluster = #(buffer+0x0d)
	bpbReservedSectors = #(buffer+0x0e)
	bpbNumberOfFATs = #(buffer+0x10)
	bpbRootEntries = #(buffer+0x11)
	bpbTotalSectors = #(buffer+0x13)
	bpbMedia = #(buffer+0x15)
	bpbSectorsPerFAT = #(buffer+0x16)
	
	call free: buffer
	
	! calc root entry properties
	root_entry_size = (32*bpbRootEntries)/bpbBytesPerSector
	root_entry_start = (bpbNumberOfFATs*bpbSectorsPerFAT)+bpbReservedSectors
	data_region = (root_entry_start + root_entry_size)
	buffer=call malloc: (root_entry_size*bpbBytesPerSector)
	! read root entry
	
	if call read_disk_raw:drive, root_entry_start, root_entry_size, buffer =/= 0
		call free: buffer
		return null
	endif
	
	! refresh fat ?
	if previous_drive =/= drive
		call free: fat12_current_fat
		call get_fat
	endif
	
	if fat12_current_fat = null
		call get_fat
	endif
	
	return buffer
endfunction
!
function cluster2lba:uint16_t cluster
	return ((cluster-2)*bpbSectorsPerCluster) + data_region
endfunction

! return 0xff on error
function get_drive_from_name: ptr name
	! convert entry_name
	ptr entry
	entry = call malloc:FILE_NAME_SIZE
	call strcpy:entry, name
	call toUpper:entry
	call convert_path_entry_name_to_legacy:entry
	
	if call memcmp: entry, {"ROOT1      "}, FILE_NAME_SIZE = 0
		call free:entry
		return 0x00
	endif
	if call memcmp: entry, {"ROOT2      "}, FILE_NAME_SIZE = 0
		call free:entry
		return 0x01
	endif
	if call memcmp: entry, {"ROOT3      "}, FILE_NAME_SIZE = 0
		call free:entry
		return 0x80
	endif
	if call memcmp: entry, {"ROOT4      "}, FILE_NAME_SIZE = 0
		call free:entry
		return 0x81
	endif
	
	if call memcmp: entry, {"ROOT0      "}, FILE_NAME_SIZE = 0
		call free:entry
		return bootdrive
	endif
	call free:entry
	! default: bootdrive
	return 0xff
endfunction

! read file or file info (depends on the mode parameter)
function _fat12_read_file: ptr file_path, uint8_t mode
	

	ptr entry_name=null
	ptr current_dir=null
	ptr current_data=null
	uint8_t drive=0
	uint32_t offset=0
	uint32_t returned_value=1
	uint32_t file_size = 0
	ptr param_buff
	uint16_t cluster
	ptr path
	uint32_t len
	bool isRootPathOnly=true

	if file_path =/= null
	
		len = call strlen: file_path
		path = call malloc: 512
		call memcpy: path, file_path, len
		

		! check if we only want the root entry
		while offset<len-1
			if  [uint8_t](#(path+offset)) = [uint8_t]'/'
				isRootPathOnly=false
			endif
			offset++
		endwhile

		call toUpper:path
		
		
		entry_name=call malloc:512
		call memset:entry_name, 0, 512
		offset=call path_get_next_entry: path, entry_name
		call convert_path_entry_name_to_legacy:entry_name
		
		drive= call get_drive_from_name: entry_name
		if drive = 0xff
			call free: path
			call free: entry_name
			call prints:{"_fat12_read_file: erreur dans le nom de la racine du fichier"}
			call pause
			return 0
		endif
		

	
		current_dir = call fat12_read_root_directory:drive
		
		! if we dont only want root entry
		if isRootPathOnly=false
			param_buff = call malloc: 6
			! can read root entry
			if current_dir =/= null
				/* call memset:entry_name, 0, 512
				returned_value=call path_get_next_entry: (path+offset), entry_name
				offset=offset+returned_value
				*/
				while returned_value =/= 0
					call memset:entry_name, 0, 512
					returned_value=call path_get_next_entry: (path+offset), entry_name
					offset=offset+returned_value
					
					if returned_value = PATH_NEXT_ENTRY_EOP
						returned_value=0
					endif

					call convert_path_entry_name_to_legacy:entry_name
					
					current_data = call fat12_read_file_from_directory: entry_name, current_dir, param_buff, (param_buff+4)
					file_size = [uint32_t]#(param_buff)
					cluster = [uint16_t]#(param_buff+4)
					! file not found -> exit loop
					if current_data = null
						returned_value=0
						file_size=0
						call free: current_data
						call free:current_dir
						call free:param_buff
						call free:path
						call free: entry_name
						/*
						call prints:{"_fat12_read_file: file not found: "}
						call prints:entry_name
						call pause*/
						return 0
					endif
					call free:current_dir
					current_dir = (current_data)
				endwhile
			endif
			call free:path
			call free:param_buff
		! if we only want root entry
		else
			call free: entry_name
			call free:path
			if mode = FAT12_MODE_READ_FILE
				return current_dir
			else
				call free:current_dir
				if mode = FAT12_MODE_GET_FIRST_CLUSTER
					return 0xfff
				else
					return (32*bpbRootEntries)/bpbBytesPerSector
				endif
			endif
		endif
		if returned_value =/= 0
			call free:current_dir
		endif
		call free: entry_name
	endif
	if mode = FAT12_MODE_READ_FILE
		return current_data
	else
		call free:current_dir
		if mode = FAT12_MODE_GET_FIRST_CLUSTER
			return cluster
		else
			return file_size
		endif
	endif
endfunction


function _fat12_write_file: ptr file_path, ptr data, uint32_t size, uint8_t attributes
	uint32_t result=0
	uint16_t first_cluster
	ptr current_dir
	ptr file_entry_in_dir
	ptr entry_name
	ptr fat
	uint8_t drive = bootdrive
	ptr path
	uint32_t len
	uint16_t dir_cluster
	len = call strlen: file_path
	path = call malloc: len
	call strcpy: path, file_path
	uint32_t file_name_offset=len
	call toUpper:path
		
		
	while [uint8_t](#(path+file_name_offset)) =/= '/' and file_name_offset > 0
		file_name_offset--
	endwhile
	
	if file_name_offset = 0
		call prints:{"FAT12_WRITE: format de chemin incorrect\n"}
		call pause
		return 0
	endif
	
	#(path+file_name_offset) = [uint8_t](0)
	
	
	entry_name = call malloc:512
	
	current_dir = call fat12_read_file:path
	

	call path_get_next_entry: (path+file_name_offset+1), entry_name
	call convert_path_entry_name_to_legacy:entry_name
	
	! if we're creating a directory
	if [uint8_t](attributes&FAT12_DIRECTORY) > 0
		! add '.' and '..' entries
		call strcpy: (data), {".          "}
		#(data+11) = attributes
		bool previousIsRoot=true
		file_name_offset=0
		
		while [uint8_t]#(path+file_name_offset) =/= 0
			if [uint8_t]#(path+file_name_offset) = '/'
				previousIsRoot=false
			endif
			file_name_offset++
		endwhile
	
		if previousIsRoot = false
		! if previous dir isn't the root
			call memcpy:(data+32), current_dir, 32
			#(data+33)=[uint8_t]'.'
		else
			call strcpy: (data+32), {"..         "}
			#(data+43) = attributes
		endif
		
	endif
	
	
	file_entry_in_dir = call fat12_find_free_file_entry_in_dir: current_dir
	
	! found ?
	if file_entry_in_dir =/= null
		! yes, find free cluster chain
		first_cluster = call fat12_alloc_cluster_chain:  data, size
		if first_cluster =/= 0
			! fill file entry
			! name
			call memcpy: file_entry_in_dir, entry_name, FILE_NAME_SIZE
			! others (set to 0)
			call memset: (file_entry_in_dir+11), 0, (26-11)
			#(file_entry_in_dir+0x0b) = attributes
			! first cluster
			#(file_entry_in_dir+26) = first_cluster
			! file size
			#(file_entry_in_dir+28) = size
			
			dir_cluster = call _fat12_read_file:path, FAT12_MODE_GET_FIRST_CLUSTER
			if dir_cluster > 0 and dir_cluster < 0xff0
				! update our directory
				call fat12_replace_content: dir_cluster, current_dir
				
			else
				call write_disk_raw:fat12_current_drive, root_entry_start, root_entry_size, current_dir
			endif
			! save the directory
		!	if call write_disk_raw:fat12_current_drive, root_entry_start, root_entry_size, current_dir = 0
				! save the fat
			!	call save_fat: fat
			
			result = 1
			!endif

		else
			call prints:{"FAT12_WRITE: Disk is full.\n"}
			call pause
		endif
	else
		! no, error (directory is full)
		call prints:{"FAT12_WRITE: Directory is full.\n"}
		call pause
	endif
	
	call free: current_dir
	call free:entry_name
	call free:path
	return result
endfunction
! replace the content of a file/dir (it has to have the same size)
function fat12_replace_content: uint16_t starting_cluster, ptr data
	uint16_t cluster=0
	uint32_t offset = 0
	uint32_t lba=0
	while cluster < 0xff0
		if cluster = 0
			cluster = starting_cluster
		endif
		lba = call cluster2lba: cluster
		call write_disk_raw:fat12_current_drive, lba, bpbSectorsPerCluster, (data+offset)
		offset = offset + (bpbBytesPerSector*bpbSectorsPerCluster)
		cluster = call get_next_cluster: cluster
	endwhile

endfunction

! return a ptr to a free file entry in the given directory
function fat12_find_free_file_entry_in_dir: ptr dir
	bool continue = true
	bool entry_found = false
	uint32_t offset = 0
	
	while continue = true and offset<(bpbRootEntries*32)
			! entry is free
			if [uint8_t](#(dir+offset)) = 0x00
				entry_found = true
				continue = false
			else
			! entry is free
				if [uint8_t](#(dir+offset)) = 0xE5
					entry_found = true
					continue = false
				else
					! find next
					offset = offset + 32
				endif
			endif
	endwhile
	
	if entry_found = true
		return (dir+offset)
	else
		return null
	endif
endfunction

! return the first cluster of a chain of cluser
function fat12_alloc_cluster_chain:  ptr data, uint32_t size
	uint16_t previous_cluster = 0
	uint16_t first_cluster = 0
	uint16_t current_cluster = 2
	uint16_t tmp_cluster = 0
	uint32_t clusters_left
	uint32_t fat_size = ((bpbNumberOfFATs*bpbSectorsPerFAT)+((bpbNumberOfFATs*bpbSectorsPerFAT)/2))*512
	uint32_t offset = 0
	uint32_t lba=0
	bool continue = true
	bool retry = true
	! adjust size
	if size % (bpbSectorsPerCluster*bpbBytesPerSector) =/= 0
		size = size + ((bpbSectorsPerCluster*bpbBytesPerSector)-(size % (bpbSectorsPerCluster*bpbBytesPerSector)))
	endif
	
	! size correspond now to the number of clusters to find
	size = size / (bpbSectorsPerCluster*bpbBytesPerSector)
	!call prints:{"fat12_alloc_cluster_chain: "}
	!call printn:DEFAULT_COLOR, size
	!call prints: {" clusters to find\n"}
	! find all the clusters we need
	while size > 0
		continue = true
		
		! find a valid cluster
		while continue = true
			tmp_cluster = call get_next_cluster:  current_cluster
			! valid ?
			if tmp_cluster = 0
				!call prints:{"Cluster found\n"}
				! set to eof in fat
				call set_cluster:  current_cluster, 0xfff
				continue = false
				
				! write data
				lba = call cluster2lba:current_cluster
				call write_disk_raw:fat12_current_drive, lba, bpbSectorsPerCluster, (data+(bpbSectorsPerCluster*offset*bpbBytesPerSector))
				offset++
			else
				current_cluster++
			endif
			
			! not enough space in fat ?
			if current_cluster >= fat_size
				size = 1
				continue = false
				first_cluster = 0
			endif
			
		endwhile
		
		! set previous cluster with current cluster value
		if previous_cluster >= 2
			call set_cluster:  previous_cluster, current_cluster
		else
			first_cluster = current_cluster
		endif
		previous_cluster = current_cluster
		current_cluster++
		size--
	endwhile
	call save_fat
	return first_cluster
endfunction

! convert entry name to legacy. Ex: "test.txt" -> "test    txt"
function convert_path_entry_name_to_legacy:ptr path
	ptr ext=null
	uint32_t pathLen
	pathLen = call strlen: path
	ext=call malloc:4
	ptr v_ext=ext

	ptr path_offset=path
	call memset:ext, ' ', 3
	
	bool continue=true
	bool is_ext=0
	uint32_t i=0
	
	
	while continue=true
		if [uint8_t]#(path_offset)=0
			continue=false
		else
			! found extension ?
			if [uint8_t]#(path_offset)='.'
				#path_offset=[uint8_t]' '
				is_ext=1
			else
				if is_ext > 0
					if is_ext>4
						continue=false
					else
						#v_ext = [uint8_t]#(path_offset)
						v_ext++
						is_ext++
						#path_offset=[uint8_t]' '
					endif
				endif
			endif
			path_offset++
			i++
			if i > FILE_NAME_SIZE
				continue = false
			endif
		endif

	endwhile
	if is_ext = 0 and pathLen = FILE_NAME_SIZE
	
	else
		while i<8
			#path_offset=[uint8_t]' '
			path_offset++
			i++
		endwhile
	
	uint8_t j=0
	path_offset=(path+8)
	v_ext=ext
	while j<3
		#path_offset=[uint8_t]#v_ext
		path_offset++
		v_ext++
		j++
	endwhile
	endif
	
	
	call free:ext
endfunction


function path_get_next_entry: ptr path, ptr out_entry_name
	bool continue=true
	uint32_t entry_name_size=0
	ptr entry_name=null
	if [uint8_t]#path=0
		return 0
	endif
	
	while continue=true
		if [uint8_t]#path=0
			! we reached the last char of the path
			continue=false
		else
			if [uint8_t]#path = '/'
				! we reached the end of the entry name
				continue=false
			else
				#out_entry_name=[uint8_t]#path
				out_entry_name++
				path++
				entry_name_size++
			endif
		endif
	endwhile
	
	if entry_name_size > FILE_NAME_SIZE
		call prints:{"<file name too long>"}
		call pause
		return 0
	endif
	if [uint8_t]#path=0
		!call prints:{"<EOP>"}
		return PATH_NEXT_ENTRY_EOP
	endif
	return entry_name_size+1
endfunction

! count how many clusters are used
function fat12_count_used_clusters: uint8_t drive
	ptr dir=null
	uint16_t cluster
	uint16_t used_clusters=0
	dir = call fat12_read_root_directory:drive
	if dir =/= null
		while cluster < bpbTotalSectors
			if call get_next_cluster: cluster =/= 0
				used_clusters++
			endif
			cluster++
		endwhile
		call free:dir
	endif
	return used_clusters
endfunction