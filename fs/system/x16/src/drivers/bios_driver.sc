/*
*******************************************************************************
* Source code wrote in "Sztrojka Code" by LANEZ Ga�l - 2014                   *
* Permission to use, copie and modifie this code.                             *
*******************************************************************************
*/

include "include\asm_def.sc"
include "include\jumble.sc"
include "include\screen.sc"

suint8_t writeDiskAccess = false
suint8_t isBiosHooked = false

! deplace le curseur
function bios_move_cursor: uint8_t x, uint8_t y
	ah = 0x02
	bh = 0x00
	dh = y
	dl = x
	_int 0x10
endfunction

! passe en mode text 80x25
function bios_set_text_mode
	ah = 0x00
	al = 0x03
	_int 0x10
	
	call bios_set_cursor:7
endfunction

function bios_set_cursor:uint8_t cursor
	ch = cursor
	cl = 7
	ah = 1
	_int 0x10
endfunction

function bios_write_character: uint8_t char, uint8_t color
	ah = 0x09
	al = char
	bh = 0
	bl = color
	cx = 1
	_int 0x10
endfunction
! ****************************** KBD ******************************

! attend l'appuye sur une touche
function bios_wait_key

	_xor ax, ax
	_int 0x16
	uint8_t return_value
	return_value = ah
	return return_value
	
endfunction

function bios_wait_key_ascii

	_xor ax, ax
	_int 0x16
	uint8_t return_value
	return_value = al
	return_value = call convertToAZERTY: return_value
	return return_value
	
endfunction
function bios_read_key

	ah = 0x01
	_int 0x16
	asm jz .noKey
	
	! return key in buffer
	return call bios_wait_key
	
	! no key available
	asm .noKey:
	return 0x00

endfunction

function bios_read_key_code_ascii: ptr code, ptr ascii
	uint8_t tmp_code
	uint8_t tmp_ascii
	_xor ax, ax
	_int 0x16
	
	tmp_code = ah
	tmp_ascii = al
	tmp_ascii = call convertToAZERTY: tmp_ascii
	
	#code = tmp_code
	#ascii = tmp_ascii
endfunction

! ****************************** DISKS ******************************
! do a disk syscall
function bios_disk_syscall: uint8_t drive, uint8_t reg_ah, uint8_t reg_al
	uint8_t error_code
	ah = reg_ah
	al = reg_al
	dl = drive
	_int 0x13
	error_code = ah
	return error_code
endfunction

! effectue un reset d'un disque
function bios_reset_drive:uint8_t drive
	_xor ax, ax
	dl = drive
	_int 0x13
endfunction

! retourne le nombre max de cylindres, secteurs et tetes (ptr to uint8_t)
function bios_get_drive_parameters:uint8_t drive, ptr max_cylinder, ptr max_head, ptr max_sector
	uint16_t c=0
	uint8_t s=0
	uint8_t h = 0
	uint16_t reg_cx=0
	/*
	if drive < 0x80
		#max_cylinder = [uint16_t]80
		#max_sector = [uint8_t]18
		#max_head = [uint8_t]2
	else
		#max_cylinder = [uint16_t]1023
		#max_sector = [uint8_t]63
		#max_head = [uint8_t]16
	endif*/
	! es:di = 0
	ax = 0
	push_register es
	es = ax
	di = 0
	
	ah = 0x8
	dl = drive

	asm int 0x13
	pop_register es
	reg_cx = cx
	h = dh
	h++
	c = ((reg_cx >> 8) & 0xff) | ((reg_cx & 0xc0) << 8)
	c++
	s= [uint8_t](reg_cx & 0x3f)
	
	if c > 0 and s > 0 and h > 0
		#max_cylinder = c
		#max_sector = s
		#max_head = h
	else
		if drive < 0x80
			#max_cylinder = [uint16_t]80
			#max_sector = [uint8_t]18
			#max_head = [uint8_t]2
		else
			#max_cylinder = [uint16_t]1023
			#max_sector = [uint8_t]63
			#max_head = [uint8_t]16
		endif
	endif
	/*call prints:{"\nmax_cylinder "}
	call printn:DEFAULT_COLOR, c
	call prints:{"\nmax_sector "}
	call printn:DEFAULT_COLOR, s
	call prints:{"\nmax_head "}
	call printn:DEFAULT_COLOR, h
	call pause*/
endfunction


! retourne les parametre du bios (INT 13H ah=15H)
function bios_get_drive_type: uint8_t drive
	return call bios_disk_syscall: drive, 0x15, 0
endfunction


! lock/unlock drive
!	unlock = true -> unlock
!	unlock = false -> lock
function bios_lock_unlock_drive: uint8_t drive, bool unlock
	return call bios_disk_syscall: drive, 0x45, unlock
endfunction

!ejecte un cd
function bios_eject_media:uint8_t drive
	call bios_lock_unlock_drive: drive, true
	return call bios_disk_syscall: drive, 0x46, 0
endfunction

! read sector from disk
function bios_read_disk_raw:uint8_t drive, uint32_t lba, uint16_t size, uint16_t seg_buff, uint16_t buff
	uint8_t error_code=0
	uint8_t n_try=2
	! int 0x13 ah=0x42 bios packet
	!						packet size		0			blckcnt		buff			seg_buff     	 		lba
	suint8_t read_disk_packet={0x10,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}

	ptr packet_seeker
	uint16_t cylinder=0
	uint8_t head=0
	uint8_t sector=0
	uint8_t size_8_bits=[uint8_t]size
	uint16_t reg_cx=0
	
	! floppy
	if drive < 0x80
		ptr chs_array
		chs_array = call malloc:4
		! call lba2chs:drive, [uint16_t]lba, @cylinder, @head, @sector
		call lba2chs:drive, [uint16_t]lba, [ptr](chs_array), [ptr](chs_array+2), [ptr](chs_array+3)
		cylinder = [uint16_t]#chs_array
		head = [uint8_t]#(chs_array+2)
		sector = [uint8_t]#(chs_array+3)
		call free:chs_array
		reg_cx=[uint16_t]((cylinder&255)<<8) | ( (cylinder&768)>>2) | sector
		/*
		
		call prints:{"c="}
		call printn:DEFAULT_COLOR, cylinder
		call prints:{" h="}
		call printn:DEFAULT_COLOR, head
		call prints:{" s="}
		call printn:DEFAULT_COLOR, sector
		call prints:{" size="}
		call printn:DEFAULT_COLOR, size_8_bits
		call prints:{"\n"}
		call print_bios_disk_error:0, drive, lba, size
		*/
		while n_try =/= 0

			ah = 0x02
			al = size_8_bits
			! ch = cylinder
			!ch = cylinder
			!cl = sector
			cx=reg_cx
			dh = head
			dl = drive
			bx = buff
			
			asm push es
			es = seg_buff
			asm int 0x13
			asm pop es
			
			error_code = ah
			if error_code =/= 0
				call bios_reset_drive:drive
				call print_bios_disk_error:error_code, drive, lba, size
				n_try--
			else

				return 0
			endif
			
		endwhile
	! hdd
	else
		while n_try =/= 0
			packet_seeker = @read_disk_packet
			! clear previous data
			call memset:(read_disk_packet+2),0,14
			
			#(packet_seeker+2) = [uint16_t]size
			
			#(packet_seeker+4) = [uint16_t]buff
			

			#(packet_seeker+6) = [uint16_t]seg_buff
			

			#(packet_seeker+8) = [uint32_t]lba
			

			
			si = packet_seeker
			ah = 0x42
			dl = drive
			_int 0x13
			
			error_code = ah
			
			! unsupported mode
			
			/*
			if error_code = 0x01
				! check normal mode
				call prints:{"Read use default\n"}
				call pause
				drive = drive - 0x80
				call bios_read_disk_raw: drive, lba, size, seg_buff, buff
			endif
			*/
			! check error
			if error_code =/= 0
				call bios_reset_drive:drive
				call print_bios_disk_error:error_code, drive, lba, size
				n_try--
			else
				return 0
			endif
			
		endwhile
	endif
	return error_code
	
endfunction

! write sector to disk
function bios_write_disk_raw:uint8_t drive, uint32_t lba, uint16_t size, uint16_t seg_buff, uint16_t buff

	! read-only mode
	if writeDiskAccess =/= true
		call print_bios_disk_error:0x03, drive, lba, size
		return 0x03
	endif

	
	if size <= 0
		return 0
	endif
	
	uint8_t error_code=0
	uint8_t n_try=3
	!int 0x13 ah=0x43 bios packet
	!						packet size		0			blckcnt		buff			seg_buff     	 		lba
	suint8_t write_disk_packet={0x10,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}

	ptr packet_seeker
	uint16_t cylinder=0
	uint8_t head=0
	uint8_t sector=0
	uint8_t size_8_bits=size
	uint16_t reg_cx

	! floppy
	if drive < 0x80
		ptr chs_array
		chs_array = call malloc:4
		! call lba2chs:drive, [uint16_t]lba, @cylinder, @head, @sector
		call lba2chs:drive, [uint16_t]lba, [ptr](chs_array), [ptr](chs_array+2), [ptr](chs_array+3)
		cylinder = [uint16_t]#chs_array
		head = [uint8_t]#(chs_array+2)
		sector = [uint8_t]#(chs_array+3)
		call free:chs_array
		reg_cx=((cylinder&255)<<8) | ( (cylinder&768)>>2) | sector
		
		while n_try =/= 0
			ah = 0x03
			al = size_8_bits
			!ch = cylinder
			!cl = sector
			cx=reg_cx
			dh = head
			dl = drive
			bx = buff
			asm push es
			es = seg_buff
			asm int 0x13
			asm pop es
		
			error_code = ah
			if error_code =/= 0
				call bios_reset_drive:drive
				call print_bios_disk_error:error_code, drive, lba, size
				n_try--
			else
				return 0
			endif
			
		endwhile
	! hdd
	else
		while n_try > 0
			packet_seeker = @write_disk_packet
			! clear previous data
			call memset:(write_disk_packet+2),0,14
			
			#(packet_seeker+2) = [uint16_t]size
			
			#(packet_seeker+4) = [uint16_t]buff
			

			#(packet_seeker+6) = [uint16_t]seg_buff
			

			#(packet_seeker+8) = [uint32_t]lba
			
			packet_seeker = @write_disk_packet
			
			si = packet_seeker
			ah = 0x43
			dl = drive
			_int 0x13
			
			error_code = ah
			
			! unsupported mode
			/*
			if error_code = 0x01
				! check normal mode
				drive = drive - 0x80
				call bios_write_disk_raw: drive, lba, size, seg_buff, buff
			endif
			*/
			if error_code =/= 0
				call bios_reset_drive:drive
				call print_bios_disk_error:error_code, drive, lba, size
				n_try--
			else
				return 0
			endif
			
		endwhile
	endif
	return error_code
endfunction

! read block
function bios_read_disk_block:uint8_t drive, uint32_t lba, uint16_t size, uint16_t seg_buff, uint16_t buff

	if call bios_read_disk_raw:drive , lba, size, seg_buff, buff =/= 0
		return 1
	endif
		
	!call sleep:1
	!halt
	return 0
endfunction


! write block do disk (max size = 50)
function bios_write_disk_block:uint8_t drive, uint32_t lba, uint16_t size, uint16_t seg_buff, uint16_t buff

	if call bios_write_disk_raw:drive , lba, size, seg_buff, buff =/= 0
		return 1
	endif
	!call sleep:1
	!halt
	return 0

endfunction


! ****************************** SYSTEM ******************************

! shutdown
function bios_shutdown

	/* copy/cut from osdev */
		
	asm ;perform an installation check
	asm mov ah,53h            ;this is an APM command
	asm mov al,00h            ;installation check command
	asm xor bx,bx             ;device id (0 = APM BIOS)
	asm int 15h               ;call the BIOS function through interrupt 15h

	asm                       ;the function was successful
	asm                       ;AX = APM version number
	asm                           ;AH = Major revision number (in BCD format)
	asm                           ;AL = Minor revision number (also BCD format)
	asm                       ;BX = ASCII characters "P" (in BH) and "M" (in BL)
	asm                       ;CX = APM flags (see the official documentation for more details)

	asm ;disconnect from any APM interface
	asm mov ah,53h               ;this is an APM command
	asm mov al,04h               ;interface disconnect command
	asm xor bx,bx                ;device id (0 = APM BIOS)
	asm int 15h                  ;call the BIOS function through interrupt 15h

	 
	asm ;connect to an APM interface
	asm mov ah,53h               ;this is an APM command
	asm mov al,0x01 ;real mode interface
	asm xor bx,bx                ;device id (0 = APM BIOS)
	asm int 15h                  ;call the BIOS function through interrupt 15h
	asm                          ;otherwise, the function was successful
	asm                          ;The return values are different for each interface.
	asm                          ;The Real Mode Interface returns nothing.
	asm  
	asm                          ;16-Bit Protected Mode Interface
	asm                          ;AX = Code Segment
	asm                          ;BX = Entry Point (Offset)
	asm                          ;CX = Data Segment
	asm                          ;SI = Code Segment Length
	asm                          ;DI = Data Segment Length
	asm  
	asm                          ;32-Bit Protected Mode Interface
	asm                          ;AX = 32-Bit Code Segment
	asm                          ;EBX = 32-Bit Entry Point (Offset)
	asm                          ;CX = 16-Bit Code Segment
	asm                          ;DX = Data Segment
	asm                          ;ESI 0:15 = 32-Bit Code Segment Length
	asm                          ;ESI 16:31 = 16-Bit Code Segment Length
	asm                          ;DI = Data Segment Length


	asm ;Enable power management for all devices
	asm mov ah,53h              ;this is an APM command
	asm mov al,08h              ;Change the state of power management...
	asm mov bx,0001h            ;...on all devices to...
	asm mov cx,0001h            ;...power management on.
	asm int 15h                 ;call the BIOS function through interrupt 15h

	asm ;Set the power state for all devices
	asm mov ah,53h              ;this is an APM command
	asm mov al,07h              ;Set the power state...
	asm mov bx,0001h            ;...on all devices to...
	asm mov cx,0x03    ;power off
	asm int 15h                 ;call the BIOS function through interrupt 15h
	
	! at this point, the pc should be off, display an error message if not
	ptr label

	label = call label_create:POSITION_CENTER,12,COLOR_GREEN, {"Veuillez eteindre manuellement l'ordinateur..."}
	call raw_cls
	call label_draw:label
	
	asm __shutdown_halt:
	asm cli
	asm halt
	asm jmp __shutdown_halt
	
endfunction

! reboot
function bios_reboot

		_int 0x19
		
endfunction

! wait
function bios_sleep: uint32_t time
	uint16_t cx_tmp
	uint16_t dx_tmp
	
	cx_tmp = (time>>16)
	dx_tmp = (time&0xffff)
	
	ah = 0x86
	cx = cx_tmp
	dx = dx_tmp
	_int 0x15
endfunction

! return memory size in bytes
function bios_getMemorySize
	uint16_t register=0
	
	cx = 0
	dx = 0
	ax = 0xe801
	_int 0x15
	
	! check error
	asm jnc .pas_erreur
		! we got an error, we'll assume we got 64 mb ram
		call prints:{"WARNING: memory size not found, we assume that there is 64 MB of RAM\n"}
		return (64*1024*1024)
	asm .pas_erreur:
	! save our register
	push_register bx
	register = dx
	! maybe we have to use bx insted of dx
	if register = 0
		pop_register bx
		register = bx
	else
		pop_register bx
	endif
	
	return [uint32_t](register*64*1024)+(16*1024*1024)
	
endfunction
! ****************************** DEBUG *******************************

function print_bios_disk_error:uint16_t error, uint8_t drive, uint32_t lba, uint16_t size
	/*
	call color_prints:COLOR_RED, {"[DISK ERROR] drive="}
	call printn:COLOR_WHITE, drive
	call color_prints:COLOR_WHITE, {" lba="}
	call printn:COLOR_WHITE, lba
	call color_prints:COLOR_WHITE, {" size="}
	call printn:COLOR_WHITE, size
	call color_prints:COLOR_WHITE, {" error "}
	call printn:COLOR_WHITE, error
	call color_prints:COLOR_RED, {" => '"}
	
	if error = 0
		call prints:{"successful completion"}
	endif
	if error = 0x01
		call prints:{"invalid function in AH or invalid parameter"}
	endif
	if error = 0x02
		call prints:{"address mark not found"}
	endif
	if error = 0x03
		call prints:{"disk write-protected"}
	endif
	if error = 0x04
		call prints:{"sector not found/read error"}
	endif
	if error = 0x05
		call prints:{"reset failed (hard disk)"}
	endif
	if error = 0x06
		call prints:{"disk changed (floppy)"}
	endif
	if error = 0x07
		call prints:{"drive parameter activity failed (hard disk)"}
	endif
	if error = 0x08
		call prints:{"DMA overrun"}
	endif
	if error = 0x09
		call prints:{"data boundary error (attempted DMA across 64K boundary or >80h sectors)"}
	endif
	if error = 0x0a
		call prints:{"bad sector detected (hard disk)"}
	endif
	if error = 0x0b
		call prints:{"bad track detected (hard disk)"}
	endif
	if error = 0x0c
		call prints:{"unsupported track or invalid media"}
	endif
	if error = 0x0D
		call prints:{"Invalid number of sectors on format (hard disk)"}
	endif
	if error = 0x0E
		call prints:{"Control data address mark detected (hard disk)"}
	endif
	if error = 0x0F
		call prints:{"DMA arbitration level out of range (hard error - retry failed)"}
	endif
	if error = 0x10
		call prints:{"Uncorrectable CRC or ECC data error (hard error - retry failed)"}
	endif
	if error = 0x11
		call prints:{"ECC corrected data error (soft error - retried OK ) (hard disk)"}
	endif
	if error = 0x20
		call prints:{"Controller failure"}
	endif
	if error = 0x40
		call prints:{"Seek failure"}
	endif
	if error = 0x80
		call prints:{"Disk timout (failed to respond)"}
	endif
	if error = 0xAA
		call prints:{"Drive not ready (hard disk)"}
	endif
	if error = 0xBB
		call prints:{"Undefined error (hard disk)"}
	endif
	if error = 0xCC
		call prints:{"Write fault (hard disk)"}
	endif
	if error = 0xE0
		call prints:{"Statur register error (hard disk)"}
	endif
	if error = 0xFF
		call prints:{"Sense operation failed (hard disk)"}
	endif
	call color_prints:COLOR_RED, {"'\n", 0}
	call pause
	return error
	*/
endfunction


! ****************************** TIME *******************************

! get time
function bios_get_time:ptr ptrHour, ptr ptrMin, ptr ptrSec

	uint8_t hour=0
	uint8_t min=0
	uint8_t sec=0
	uint8_t returnValue = 0
	ah = 0x02
	_int 0x1a
	
	returnValue = ah
	
	hour = ch
	min = cl
	sec = dh
	
	! correct time (don't know why but it seems to be bugged)
	sec = sec - (sec/0x10*6)
	min = min - (min/0x10*6)
	hour = hour - (hour/16*6)
		
	#ptrSec = [uint8_t](sec)
	#ptrMin = [uint8_t](min)
	#ptrHour = [uint8_t](hour)
	
	return returnValue
endfunction

function bios_get_ticks
	uint16_t regCX
	uint16_t regDX
	ax = 0
	_int 0x1a
	regCX = cx
	regDX = dx
	
	return [uint32_t] ((regCX<<16) | regDX)
endfunction
