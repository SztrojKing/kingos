/*
*******************************************************************************
* Source code writen in "Sztrojka Code" by LANEZ Ga�l - 2016                  *
* Permission to use, copie and modifie this code.                             *
*******************************************************************************
*/

define READ_DISK_RAW_BLOCK_SIZE 8
define READ_WRITE_DISK_BUFFER_ADDRESS 0x5000 

include "include\library.sc"
! raw read from disk (always use this function)
function read_disk_raw:uint8_t drive, uint32_t lba, uint32_t size, ptr buff

	ptr progressBar=null
	uint32_t blocksWritten=0
	
	if size = 0
		call prints:{"ERROR: Trying to read disk with size = 0\n"}
		call pause
		return 1
	endif
	progressBar=call progressBar_create:1,24,20, COLOR_GREEN, size
	
	call progressBar_draw:progressBar, 0
	
	! read by blocks of READ_DISK_RAW_BLOCK_SIZE sectors
	while size > READ_DISK_RAW_BLOCK_SIZE
		
		if call bios_read_disk_block:drive, lba, READ_DISK_RAW_BLOCK_SIZE, 0, READ_WRITE_DISK_BUFFER_ADDRESS =/= 0
			call free: progressBar
			return 1
		endif

		call memcpy_16_to_32:buff, 0, READ_WRITE_DISK_BUFFER_ADDRESS, (READ_DISK_RAW_BLOCK_SIZE*512)
		size = size - READ_DISK_RAW_BLOCK_SIZE
		lba = lba + READ_DISK_RAW_BLOCK_SIZE
		buff = buff + (READ_DISK_RAW_BLOCK_SIZE*512)
		
		blocksWritten=blocksWritten+READ_DISK_RAW_BLOCK_SIZE
		call progressBar_draw:progressBar, blocksWritten

	endwhile
	
	! read last sectors ( < READ_DISK_RAW_BLOCK_SIZE)
	if call bios_read_disk_block:drive, lba, size,0, READ_WRITE_DISK_BUFFER_ADDRESS =/= 0
		call free: progressBar
		return 1
	endif
	
	blocksWritten=blocksWritten+size
	call progressBar_draw:progressBar, blocksWritten
	

	
	call memcpy_16_to_32:buff, 0, READ_WRITE_DISK_BUFFER_ADDRESS, (size * 512)

	call free: progressBar
	return 0
endfunction


! raw write to disk (always use this function)
function write_disk_raw:uint8_t drive, uint32_t lba, uint32_t size, ptr buff
	ptr progressBar=null
	uint32_t blocksWritten=0
	
	if size = 0
		call prints:{"ERROR: Trying to read disk with size = 0\n"}
		call pause
		return 1
	endif
	
	progressBar=call progressBar_create:1,24,20, COLOR_RED, size
	
	call progressBar_draw:progressBar, 0
	
	
	uint16_t buff_size
	! read by blocks of 50 sectors
	while size > READ_DISK_RAW_BLOCK_SIZE
		! READ_DISK_RAW_BLOCK_SIZE * 512 = 0x6400
		call memcpy_32_to_16:0, READ_WRITE_DISK_BUFFER_ADDRESS, buff, (READ_DISK_RAW_BLOCK_SIZE*512)
		if call bios_write_disk_block:drive, lba, READ_DISK_RAW_BLOCK_SIZE, 0, READ_WRITE_DISK_BUFFER_ADDRESS =/= 0
			call free: progressBar
			return 1
		endif
		size = size - READ_DISK_RAW_BLOCK_SIZE
		lba = lba + READ_DISK_RAW_BLOCK_SIZE
		buff = buff + (READ_DISK_RAW_BLOCK_SIZE*512)
		
				blocksWritten=blocksWritten+READ_DISK_RAW_BLOCK_SIZE
		call progressBar_draw:progressBar, blocksWritten
	endwhile
	blocksWritten=blocksWritten+size
	call progressBar_draw:progressBar, blocksWritten

	
	! write last sectors ( < READ_DISK_RAW_BLOCK_SIZE)
	buff_size = size * 512
	call memcpy_32_to_16:0, READ_WRITE_DISK_BUFFER_ADDRESS, buff, buff_size
	if call bios_write_disk_block:drive, lba, size, 0, READ_WRITE_DISK_BUFFER_ADDRESS =/= 0
		call free: progressBar
		return 1
	endif

	call free: progressBar
	return 0
endfunction


! return a ptr to a struct witch contains drive info
!		returns null if drive doesn't exists
function get_drive_info: uint8_t drive
	ptr drive_info = null
	uint8_t error_code
	uint16_t used_clusters
	ptr buff
	buff = call malloc:512
	
	if call read_disk_raw:drive, 0, 1, buff = 0
		drive_info = call malloc: 128
		call memset: drive_info, 0, 128
		! cd/usb or hdd ?
		if drive < 0x80
			#(drive_info+GET_DRIVE_INFO_TYPE_OFFSET) = [uint8_t]GET_DRIVE_INFO_TYPE_CD_USB
		else
			#(drive_info+GET_DRIVE_INFO_TYPE_OFFSET) = [uint8_t]GET_DRIVE_INFO_TYPE_HDD
		endif
		
		! bootdrive ?
		if drive = bootdrive
			#(drive_info+GET_DRIVE_INFO_IS_BOOTDRIVE_OFFSET) = [uint8_t](true)
		else
			#(drive_info+GET_DRIVE_INFO_IS_BOOTDRIVE_OFFSET) = [uint8_t](false)
		endif
		
		! drive label
		
		call memcpy:(drive_info+GET_DRIVE_INFO_LABEL), (buff+43), 11
		! filesystem label
		call memcpy:(drive_info+GET_DRIVE_INFO_FILESYSTEM), (buff+54), 8
		
		
		if call memcmp:(buff+54), {"FAT12   "}, 8 = 0
			! used clusters
			used_clusters = call fat12_count_used_clusters: drive
			#(drive_info+GET_DRIVE_INFO_USED_CLUSTERS) = [uint16_t](used_clusters)
			
			! clusters
			#(drive_info+GET_DRIVE_INFO_TOTAL_CLUSTERS) = [uint16_t](#(buff+19))
		else
			#(drive_info+GET_DRIVE_INFO_USED_CLUSTERS) = [uint16_t]1
			
			! clusters
			#(drive_info+GET_DRIVE_INFO_TOTAL_CLUSTERS) = [uint16_t]1
		endif
		

	endif 
	call free:buff
	return drive_info
endfunction

! convert lba to chs
function lba2chs:uint8_t drive, uint16_t lba, ptr sptr_cylinder, ptr sptr_head, ptr sptr_sector
	uint8_t heads_per_cylinder
	uint8_t sectors_per_track
	uint16_t cylinders_per_disk
	
	ptr chs_array
	chs_array = call malloc:4
	
	uint16_t temp=0
	
	! call bios_get_drive_parameters:drive, @cylinders_per_disk, @heads_per_cylinder, @sectors_per_track
	call bios_get_drive_parameters:drive, chs_array, (chs_array+2), (chs_array+3)
	cylinders_per_disk = #chs_array 
	heads_per_cylinder = #(chs_array+2)
	sectors_per_track = #(chs_array+3)
	call free:chs_array
	
	temp = [uint16_t] lba % (heads_per_cylinder * sectors_per_track)


	#sptr_cylinder = [uint16_t](lba / (heads_per_cylinder * sectors_per_track))
	
	#sptr_head = [uint8_t](temp / sectors_per_track)

	#sptr_sector = [uint8_t]((temp % sectors_per_track) + 1)
	
	
endfunction
