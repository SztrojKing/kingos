/*
*******************************************************************************
* Source code writen in "Sztrojka Code" by LANEZ Ga�l - 2015                  *
* Permission to use, copie and modifie this code.                             *
*******************************************************************************
*/

include "include\library.sc"
!include "include\interrupts.sc"

suint32_t time_ticks=0


! sleep (in millisec)
function sleep: uint32_t time
	time = time*1000
	call bios_sleep: time
	
	/*
	uint32_t initialTimeTicks = time_ticks
	time = time / 18
	if time = 0
		time = 1
	endif

	while time_ticks - initialTimeTicks < time
		halt
	endwhile
	*/
endfunction


function draw_time

	uint8_t hour
	uint8_t min
	uint8_t sec
	ptr hms_array
	hms_array = call malloc:16
	!call bios_get_time: @hour, @min, @sec
	call bios_get_time: [ptr](hms_array),[ptr](hms_array+1), [ptr](hms_array+2)
	hour = [uint8_t](#hms_array)
	min = [uint8_t](#(hms_array+1))
	sec = [uint8_t](#(hms_array+2))
	call free:hms_array
	
	if hour = 0 and min = 0 and sec = 0
		call coords_color_prints:0,0,COLOR_WHITE, {"??:??:?? "}
	else
		call coords_color_prints:0,0,COLOR_WHITE, {"00:00:00 "}
	
		! print time
		if hour < 10
			call coords_printn:1,0,COLOR_WHITE, hour
		else
			call coords_printn:0,0,COLOR_WHITE, hour
		endif
		
		if min < 10
			call coords_printn:4,0,COLOR_WHITE, min
		else
			call coords_printn:3,0,COLOR_WHITE, min
		endif
		if sec < 10
			call coords_printn:7,0,COLOR_WHITE, sec
		else
			call coords_printn:6,0,COLOR_WHITE, sec
		endif
	endif
	
endfunction


function time_get_ticks
	if isBiosHooked = true
		return time_ticks
	else
		return call bios_get_ticks
	endif
endfunction



