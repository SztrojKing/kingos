/*
 *****************************************************************************
 Source code wrote in "Sztrojka Code" by LANEZ Gaël - 2014                   *
 Permission to use, copie and modifie this code.                             *
 ***************************************************************************** 
*/

define _int {asm int}
define _xor {asm xor}
define _pop {asm pop}
define push_register {asm push}
define pop_register {asm pop}
define push_all_registers {asm pusha}
define pop_all_registers {asm popa}
define _goto {asm jmp}
define halt {asm hlt}

define true 1
define false 0

define null 0

define bool uint8_t

define SIZEOF_UINT8_T 1
define SIZEOF_UINT16_T 2
define SIZEOF_UINT32_T 4
define SIZEOF_PTR 4