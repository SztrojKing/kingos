/*
*******************************************************************************
* Source code writen in "Sztrojka Code" by LANEZ Ga�l - 2015                  *
* Permission to use, copie and modifie this code.                             *
******************************************************************************* 
*/

include "include\library.sc"
include "include\drivers\syscalls_driver.sc"

suint8_t enableIVTHook = true
suint8_t isBiosHooked = false
suint8_t stop_output=false

function setup_bios_ivt
	isBiosHooked = false
	! timer
	if call hook_bios_interrupt:0x1c, function int_0x1c_handler_asm =/= true
		return false
	endif
	
	! reboot
	if call hook_bios_interrupt:0x19, function int_0x19_handler_asm =/= true
		return false
	endif
	
	! OS syscall handler
	if call hook_bios_interrupt:0x78, function int_0x78_handler_asm =/= true
		return false
	endif
	
	! kbd
	!if call hook_bios_interrupt:0x09, function int_0x09_handler_asm =/= true
	!	return false
	!endif
	
	! mouse
	if call hook_bios_interrupt:0x74, function int_0x74_handler_asm =/= true
		return false
	endif
	
	isBiosHooked = true
	return true
endfunction

function__no_stack__ int_0x09_handler_asm
	
	! save registers 
	asm pusha
	push_register es
	push_register fs
	push_register gs
	dx = ds
	gs = dx
	! setup segments
	dx = 0x800
	ds = dx
	! we're pushing gs wich is ds
	push_register gs
	call kbd_driver

	! restore registers
	pop_register ds
	
	pop_register gs
	pop_register fs
	pop_register es
	! end of interrupt
	asm mov al,0x20
	asm out 0x20,al
	asm out 0xa0,al
	asm popa
	
	asm iret
endfunction

function kbd_driver
	uint8_t i=0
    while (i & 0x01) = 0
          i = call inb: 0x64
    endwhile

    i = call inb:0x60
    i--

	suint32_t count=0
	call coords_printn:10,0,DEFAULT_COLOR, count
	count++
	
	
endfunction


! install handler for specified interrupt
function hook_bios_interrupt:uint16_t interrupt, uint16_t handler

	! check if we can hook our bios
	if enableIVTHook = false
		return false
	endif
	
	
	interrupt = (interrupt*4)
	suint16_t reg=0
	
	! disable interrupts
	asm cli
	push_register es
	asm xor ax, ax
	asm mov es, ax
	
	! ** save old offset **
	/*
	bx = interrupt
	asm mov word ax, [es:bx]
	reg = ax
	#old_off= [uint16_t]reg
	*/
	
	! ** update new offset **
	! skip nasm ORG 0x8000
	!handler = (handler-0x8000)
	ax = handler
	bx = interrupt
	asm mov word [es:bx], ax
	
	interrupt = interrupt+2
	
	! ** save old seg **
	/*
	bx = interrupt
	asm mov word ax, [es:bx]
	reg = ax
	#old_seg = [uint16_t]reg
	*/
	
	! ** update new seg **
	 
	! ax = cs
	bx = interrupt
	push_register cs
	pop_register ax
	asm mov word [es:bx], ax
	
	pop_register es
	asm sti
	
	return true
endfunction


! *********************************************************************************************************************


! clock interrupt (18.2 ticks/sec)

function__no_stack__ int_0x1c_handler_asm
	! save registers 
	asm pusha
	push_register es
	push_register fs
	push_register gs
	dx = ds
	gs = dx
	!dx = ds
	! setup segments
	dx = 0x800
	ds = dx
	! we're pushing gs wich is ds
	push_register gs
	!es = dx
	!fs = dx
	!gs = dx
	
	/* TODO: use kernel stack */
	!ss = dx
	
	! do our stuff
	/* 	eax = command id
		ebx = pointer to struct */
		
	/* push_register ax
	asm mov al, 0x20
	asm out 0x20, al
	pop_register ax
	*/
	
	! enable interrupts so we can use bios functions
	!asm sti

	! do our stuff
	call int_0x1c_handler
	! restore registers
	pop_register ds
	
	pop_register gs
	pop_register fs
	pop_register es
	asm popa
	! we jump to old bios code
	! jump [es:di]
	
	!es = int_0x1c_old_seg
	!di = int_0x1c_old_off
	!push_register es
	!push_register di
	!es = reg_es
	!di = reg_di
	! asm retf
	
	! end of interrupt
	asm mov al, 0x20
	asm out 0x20, al
	asm out 0xa0,al
	asm iret
	
	asm iret
endfunction



function int_0x1c_handler
	
	time_ticks++
	
	if stop_output=false
		! update time every sec
		if time_ticks%18 = 0
			call draw_time
			
			
			!uint16_t CS=0
			!CS = cs
			!uint16_t IP=0
			!IP = ip

			!call coords_color_prints:10, 0, DEFAULT_COLOR, {"cs="}
			!call coords_printn:14, 0, DEFAULT_COLOR, CS
			!call coords_color_prints:50, 0, DEFAULT_COLOR, {"IP="}
			!call coords_printn:54, 0, DEFAULT_COLOR, IP
			!
		endif
	endif
endfunction

! reboot interrupt 

function__no_stack__ int_0x19_handler_asm
	! save registers 
	asm pusha
	push_register es
	push_register fs
	push_register gs
	dx = ds
	gs = dx
	! setup segments
	dx = 0x800
	ds = dx
	! we're pushing gs wich is ds
	push_register gs

	call int_0x19_handler
	! restore registers
	pop_register ds
	
	pop_register gs
	pop_register fs
	pop_register es

	asm popa
	asm iret
endfunction



function int_0x19_handler
	call cls
	call prints:{"Redemarrage..\n"}
	call sleep:1000
	! reboot with CPU reset line 
    asm cli
	asm mov     al,0xfe
    asm out     0x64,al 
	asm __halt: 
	asm hlt
    asm jmp __halt

endfunction

! OS syscall handler
function__no_stack__ int_0x78_handler_asm
	
	! save registers 
	push_register es
	push_register fs
	push_register gs
	dx = ds
	gs = dx
	! setup segments
	dx = 0x800
	ds = dx
	! we're pushing gs wich is ds
	push_register gs

	! call handler
	call syscall_handler:eax, ebx, ecx, es, esi, edi
	! restore registers
	pop_register ds
	
	pop_register gs
	pop_register fs
	pop_register es
	
	asm iret
endfunction
