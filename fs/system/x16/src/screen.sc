/*
*******************************************************************************
* Source code writen in "Sztrojka Code" by LANEZ Ga�l - 2015                  *
* Permission to use, copie and modifie this code.                             *
*******************************************************************************
*/

! Basic screen operations

include "include\common\colors.sc"
include "include\string.sc"

sptr VIDEO_MEM = {(0xb0000)} /*{[ptr](0xb0000)} */
define VIDEO_MEM_SIZE 0xfa0

! video coords
suint32_t video_x=0
suint32_t video_y=0


function init_video

	uint16_t reg
	ax = ds
	reg = ax
	VIDEO_MEM = (0xb8000) - (reg*0x10)

endfunction


! color_prints with default color
function prints:ptr str
	! call color_prints:DEFAULT_COLOR, str
	call color_prints:DEFAULT_COLOR, str
endfunction

! prints with color
function color_prints:uint8_t color, ptr str
	if str = null
		exit
	endif
	ptr video=[ptr](VIDEO_MEM+((video_y*80+video_x)*2))
	while [uint8_t]#str =/= 0
		! LF
		if [uint8_t]#str = 10
			video_y++
			video=[ptr](VIDEO_MEM+((video_y*80+video_x)*2))
			if video_y>=24

				call scrollup:1
				video=[ptr](VIDEO_MEM+((video_y*80+video_x)*2))
			endif
		endif
		! CR
		if [uint8_t]#str = 13
			video_x=0
			video=[ptr](VIDEO_MEM+((video_y*80+video_x)*2))
		endif
		
		! tab
		if  [uint8_t]#str = 9
			video_x=video_x+4
			video=[ptr](VIDEO_MEM+((video_y*80+video_x)*2))
		endif
		
		! normal char
		if [uint8_t]#str >= ' '
			#video=[uint8_t]#str
			video++
			#video=color
			video++
						
			video_x++

		endif
		
		if video_x>=80
			video_x=video_x-80
			video_y++
			video=[ptr](VIDEO_MEM+((video_y*80+video_x)*2))
		endif
		if video_y>=24
			call scrollup:1
			video=[ptr](VIDEO_MEM+((video_y*80+video_x)*2))
		endif
		
		str++	
	
	endwhile
	
endfunction

! coords + color prints
function coords_color_prints:uint8_t x, uint8_t y, uint8_t color, ptr str
	if str = null
		exit
	endif
	if x>=80
		exit
	endif
	if y>=25
		exit
	endif
	ptr video=[ptr](VIDEO_MEM+((y*80+x)*2))
	while [uint8_t]#str =/= 0
		! LF
		if [uint8_t]#str = 10
			y++
			video=[ptr](VIDEO_MEM+((y*80+x)*2))
		endif
		! CR
		if [uint8_t]#str = 13
			x=0
			video=[ptr](VIDEO_MEM+((y*80+x)*2))
		endif
		
		! tab
		if  [uint8_t]#str = 9
			x=x+4
			video=[ptr](VIDEO_MEM+((y*80+x)*2))
		endif
		! normal char
		if [uint8_t]#str >= ' '
			#video=[uint8_t]#str
			video++
			#video=color
			video++
						
						
			x++

		endif
		if x>=80
			x=x-80
			y++
			video=[ptr](VIDEO_MEM+((y*80+x)*2))
		endif
		str++	
	
	endwhile
endfunction

! print one char
function coords_printc:uint8_t x, uint8_t y, uint8_t color, uint8_t char
	ptr video=[ptr](VIDEO_MEM+((y*80+x)*2))
	#video=char
	video++
	#video=color
endfunction


! move writing position
function move_cursor: uint8_t x, uint8_t y
	video_x=x
	video_y=y
endfunction

! raw clear screen
function raw_cls
	call move_cursor:0,0
	ptr video=VIDEO_MEM
	uint32_t i=0
	while video<VIDEO_MEM+VIDEO_MEM_SIZE
		#video=[uint32_t]0
		video=video+SIZEOF_UINT32_T
	endwhile
endfunction

! print [OK]
function printOk
	call coords_color_prints:72, video_y, COLOR_GREEN, {"[  OK  ]"}
endfunction

! print [ERREUR]
function printError
	call coords_color_prints:72, video_y, COLOR_RED, {"[ERRE16]"}
endfunction

! printn & coords_printn internal buffer
suint8_t _printn_buffer={"                     "}

function printn:uint8_t color, uint32_t number
	call memset:@_printn_buffer, 0, 21
	call itoa:@_printn_buffer, number
	call color_prints:color, @_printn_buffer
endfunction

function coords_printn:uint8_t x, uint8_t y, uint8_t color, uint32_t number
	call memset:@_printn_buffer, 0, 21
	call itoa:@_printn_buffer, number
	call coords_color_prints:x, y, color, @_printn_buffer
endfunction

function scrollup:uint32_t nlines
	if video_y>nlines
		video_y=video_y-nlines
		call memcpy:VIDEO_MEM+160, VIDEO_MEM+(160*(nlines+1)), VIDEO_MEM_SIZE-(160*(nlines+1))
		call memset:VIDEO_MEM+((video_y)*160), 0, (160*nlines)
	endif
endfunction