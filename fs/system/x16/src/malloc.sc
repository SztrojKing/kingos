/*
*******************************************************************************
* Source code writen in "Sztrojka Code" by LANEZ Ga�l - 2015                  *
* Permission to use, copie and modifie this code.                             *
*******************************************************************************
*/

/*
malloc function will allocate 1024b blocks
	4 first bytes are reserved for header

*/ 
include "include\mem.sc"
include "include\system_exceptions.sc"
include "include\library.sc"
include "include\drivers\bios_driver.sc"
!include "include\drivers\syscalls_driver.sc"
include "include\screen.sc"



define MALLOC_BLOCK_SIZE {([uint32_t](64))}
define MALLOC_MAGIC {([uint32_t](0xcb94E3a7))}

suint32_t MALLOC_BITMAP_ADDRESS = {(0x20000-0x8000)}
suint32_t MALLOC_DATA_ADDRESS = 0
suint32_t mallocBitmapSize=0
suint32_t ramSize=0
suint32_t malloc_starting_offset = 0


!suint32_t ramUsed=MALLOC_BITMAP_ADDRESS
suint32_t ramUsed=0
sptr ram_progressBar=null

function init_malloc
	
	! 400 kb free memory
	ramSize=400*1024

	MALLOC_DATA_ADDRESS = (MALLOC_BITMAP_ADDRESS + (ramSize/MALLOC_BLOCK_SIZE/8))
	! align with 0x10
	MALLOC_DATA_ADDRESS = MALLOC_DATA_ADDRESS + (0x10 - (MALLOC_DATA_ADDRESS%0x10))
	mallocBitmapSize=((ramSize)/MALLOC_BLOCK_SIZE/8)
	ramUsed = MALLOC_DATA_ADDRESS-MALLOC_BITMAP_ADDRESS
		
	call prints:{"RAM: "}
	call printn:DEFAULT_COLOR, (ramSize/1024)
	call prints:{"kb"}
	call memset:MALLOC_BITMAP_ADDRESS, 0, mallocBitmapSize
	
	! create ram_progressBar
	ram_progressBar=call progressBar_create:65,0,14,(COLOR_KHAKI), (ramSize)
	call progressBar_draw:ram_progressBar, (ramUsed)
endfunction

function malloc: uint32_t size
	
	uint32_t realSize=0
	ptr address=0
	
	if size=0
		call fatal_exception:EXCEPTION_MALLOC_SIZE_0
	endif
	
	! if malloc has not been initialised yet
	if MALLOC_DATA_ADDRESS = 0
		call fatal_exception:EXCEPTION_NOT_INITIALISED
	endif
	! add header's size
	size=size+0x10+512
	! round size
	if size%MALLOC_BLOCK_SIZE=0
		realSize=(size/MALLOC_BLOCK_SIZE)
	else
		realSize=(size/MALLOC_BLOCK_SIZE)+1
	endif
	
	! find the address
	address=call malloc_findLocation:realSize
	! check error
	if address<MALLOC_DATA_ADDRESS
		! generate fatal exception
		call fatal_exception: EXCEPTION_MEMORY
	endif
	! reserve space
	call malloc_setBitMap: address, realSize, true
	! write veryfying bytes
	#address=MALLOC_MAGIC

	! write header
	#(address+4)=realSize
	
	! skip header
	address=(address+0x10)
	
	! refresh progressBar
	ramUsed=(ramUsed+(realSize*MALLOC_BLOCK_SIZE))
	call progressBar_draw:ram_progressBar, (ramUsed)

	! fill with 0's
	call memset:address, 0, size-0x10
	
	return address
	
endfunction

function free: ptr address
	
	if address=/=null
		address = address - 8
		if #[uint32_t](address-8)=/=MALLOC_MAGIC
			call fatal_exception:EXCEPTION_PTR
		endif
		! refresh progressBar
		ramUsed=ramUsed-((#[uint32_t](address-4))*MALLOC_BLOCK_SIZE)
		call progressBar_draw:ram_progressBar, (ramUsed)
		
		! clear our bitmap
		call malloc_setBitMap: (address-8),  #[uint32_t](address-4), false
		! erase data
		call memset:(address-8), 0,  ((#[uint32_t](address-4))*MALLOC_BLOCK_SIZE)
	endif
	return null

endfunction

! real address, length in blocks
function malloc_setBitMap: uint32_t address, uint32_t length, bool bit_state

	uint32_t i=0
	ptr bitmap_bit_address
	uint8_t bitmap_bit_offset
	uint8_t bitmap_bit_value
	! convert address to bitmap address
	
	while i<length
		bitmap_bit_address=(((address+i*MALLOC_BLOCK_SIZE)-MALLOC_DATA_ADDRESS)/MALLOC_BLOCK_SIZE/8)+MALLOC_BITMAP_ADDRESS
		bitmap_bit_offset=((address+i*MALLOC_BLOCK_SIZE)-MALLOC_DATA_ADDRESS)/MALLOC_BLOCK_SIZE%8
		
		bitmap_bit_value=call setBit:[uint8_t](#bitmap_bit_address), bitmap_bit_offset, bit_state
		#bitmap_bit_address=bitmap_bit_value
		
		i++
	endwhile
	
endfunction

! length in blocks
! return address
function malloc_findLocation: uint32_t length
	uint32_t i=(malloc_starting_offset/MALLOC_BLOCK_SIZE/8)+1
	uint8_t j=((malloc_starting_offset/MALLOC_BLOCK_SIZE)%8)
	uint32_t continuousBlocks=0
	
	while i<(mallocBitmapSize)

		while j<8
			! check current bit
			if call checkBit:#(i+MALLOC_BITMAP_ADDRESS), j =/= 0
				continuousBlocks=0
			else
				continuousBlocks++
			endif
			
			! we found enough space
			if continuousBlocks>=length
				! return address
				return ((((i*8)+j+1)-continuousBlocks)*MALLOC_BLOCK_SIZE)+MALLOC_DATA_ADDRESS
			endif
			j++
		endwhile
		
		j=0
		i++
	endwhile
	return null
endfunction

