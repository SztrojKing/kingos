/*
*******************************************************************************
* Source code writen in "Sztrojka Code" by LANEZ Ga�l - 2016                  *
* Permission to use, copie and modifie this code.                             *
*******************************************************************************
*/

function inb: uint16_t port
	dx = port
	asm in al, dx
	return al
endfunction

function outb: uint16_t port, uint8_t data
	dx = port
	al = data
	asm out dx, al
endfunction