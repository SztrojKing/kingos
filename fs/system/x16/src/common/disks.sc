/*
*******************************************************************************
* Source code writen in "Sztrojka Code" by LANEZ Ga�l - 2016                  *
* Permission to use, copie and modifie this code.                             *
*******************************************************************************
*/


define GET_DRIVE_INFO_TYPE_OFFSET 0
enum GET_DRIVE_INFO_TYPE_CD_USB, GET_DRIVE_INFO_TYPE_HDD
define GET_DRIVE_INFO_IS_BOOTDRIVE_OFFSET 1
define GET_DRIVE_INFO_LABEL 2
define GET_DRIVE_INFO_FILESYSTEM 14
define GET_DRIVE_INFO_TOTAL_CLUSTERS 23
define GET_DRIVE_INFO_USED_CLUSTERS 25