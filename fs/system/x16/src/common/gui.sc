/*
*******************************************************************************
* Source code writen in "Sztrojka Code" by LANEZ Ga�l - 2015                  *
* Permission to use, copie and modifie this code.                             *
*******************************************************************************
*/

enum OBJECT_NULL, OBJECT_PROGRESS_BAR, OBJECT_LABEL, OBJECT_BUTTON, OBJECT_TEXTBOX

define OBJECT_VISIBLE 0x01
define OBJECT_SELECTED 0x02

define POSITION_LEFT 0
define POSITION_RIGHT 0x0fe
define POSITION_CENTER 0xff
define POSITION_TOP 1
define POSITION_BOT 23

enum TEXTBOX_TEXT, TEXTBOX_PASSWORD