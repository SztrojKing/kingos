include "..\..\include\common\syscall_header.sc"
define DO_SYSCALL {asm int 0x78}

function get_bootdrive
	eax = SYSCALL_GET_BOOTDRIVE
	DO_SYSCALL
endfunction
function cls
	eax = SYSCALL_CLS
	ebx = 0
	DO_SYSCALL
endfunction

function prints:ptr str
	ebx = str
	eax = SYSCALL_PRINTS
	DO_SYSCALL
endfunction

function printn:uint32_t number
	ebx = number
	eax = SYSCALL_PRINTN
	DO_SYSCALL
endfunction

function sleep: uint32_t time
	ebx = time
	eax = SYSCALL_SLEEP
	DO_SYSCALL
endfunction

function shutdown
	ebx = 0
	eax = SYSCALL_SHUTDOWN
	DO_SYSCALL
endfunction

function reboot
	ebx = 0
	eax = SYSCALL_REBOOT
	DO_SYSCALL
endfunction

function read_disk_raw:uint8_t drive, uint32_t lba, uint32_t size, ptr buff
	ptr args
	args = call malloc: 13
	#(args) = drive
	#(args+1) = lba
	#(args+5) = size
	#(args+9) = buff
	ebx = args
	eax = SYSCALL_READ_DISK_RAW
	DO_SYSCALL
	push_register eax
	call free: args
	pop_register eax
endfunction

function write_disk_raw:uint8_t drive, uint32_t lba, uint32_t size, ptr buff
	ptr args
	args = call malloc: 13
	#(args) = drive
	#(args+1) = lba
	#(args+5) = size
	#(args+9) = buff
	ebx = args
	eax = SYSCALL_WRITE_DISK_RAW
	DO_SYSCALL
	push_register eax
	call free: args
	pop_register eax
endfunction

function malloc: uint32_t size
	ebx = size
	eax = SYSCALL_MALLOC
	DO_SYSCALL
endfunction

function free: ptr address
	ebx = address
	eax = SYSCALL_FREE
	DO_SYSCALL
endfunction

function read_file: ptr path
	ebx = path
	eax = SYSCALL_FAT12_READ_FILE
	DO_SYSCALL
endfunction

function get_file_size: ptr path
	ebx = path
	eax = SYSCALL_FAT12_GET_FILE_SIZE
	DO_SYSCALL
endfunction

function write_file: ptr path, ptr data, uint32_t size
	ptr args
	args = call malloc: 12
	#(args) = path
	#(args+4) = data
	#(args+8) = size
	ebx = args
	eax = SYSCALL_WRITE_FILE
	DO_SYSCALL
	push_register eax
	call free: args
	pop_register eax
endfunction

function create_directory: ptr path
	ebx = path
	eax = SYSCALL_CREATE_DIRECTORY
	DO_SYSCALL
endfunction

function file_browser
	eax = SYSCALL_FILE_BROWSER
	DO_SYSCALL
endfunction

function exec: ptr path, ptr args
	ecx = args
	ebx = path
	eax = SYSCALL_EXEC
	DO_SYSCALL
endfunction

function pause
	eax = SYSCALL_PAUSE
	DO_SYSCALL
endfunction

function printc: uint8_t x, uint8_t y, uint8_t color, uint8_t char
	ptr args
	args = call malloc: 4*SIZEOF_UINT8_T
	#(args) = x
	#(args+1) = y
	#(args+2) = color
	#(args+3) = char
	ebx = args
	eax = SYSCALL_PRINTC
	DO_SYSCALL
	call free: args
endfunction

/* ********************************* GUI ********************************* */
/* |||||||||| WINDOW |||||||||| */
function window_create
	eax = SYSCALL_WINDOW_CREATE
	DO_SYSCALL
endfunction

function window_free: ptr window
	ebx = window
	eax = SYSCALL_WINDOW_FREE
	DO_SYSCALL
endfunction

function window_addItem: ptr window, uint32_t itemType, ptr item
	ptr args
	args = call malloc:12
	#(args) = window
	#(args+4) = itemType
	#(args+8) = item
	ebx = args
	eax = SYSCALL_WINDOW_ADDITEM
	DO_SYSCALL
	! save the returned value
	push_register eax
	call free: args
	pop_register eax
endfunction

function window_showItem: ptr window, ptr item, bool visible
	ptr args
	args = call malloc:9
	#(args) = window
	#(args+4) = item
	#(args+8) = visible
	ebx = args
	eax = SYSCALL_WINDOW_SHOWITEM
	DO_SYSCALL
	! save the returned value
	push_register eax
	call free: args
	pop_register eax
endfunction


function window_exec: ptr window
	ebx = window
	eax = SYSCALL_WINDOW_EXEC
	DO_SYSCALL
endfunction

/* |||||||||| PROGRESSBAR |||||||||| */
function progressBar_create: uint8_t x, uint8_t y, uint8_t size, uint8_t color, uint32_t max_value
	ptr args
	args = call malloc:8
	#(args) = x
	#(args+1) = y
	#(args+2) = size
	#(args+3) = color
	#(args+4) = max_value
	ebx = args
	eax = SYSCALL_PROGRESSBAR_CREATE
	DO_SYSCALL
	! save the returned value
	push_register eax
	call free: args
	pop_register eax
endfunction


function progressBar_setValue: ptr progressBar, uint32_t value
	ebx = progressBar
	ecx = value
	eax = SYSCALL_PROGRESSBAR_SET_VALUE
	DO_SYSCALL
endfunction

/* |||||||||| LABEL |||||||||| */
function label_create: uint8_t x, uint8_t y, uint8_t color, ptr str
	ptr args
	args = call malloc:7
	#(args) = x
	#(args+1) = y
	#(args+2) = color
	#(args+3) = str
	ebx = args
	eax = SYSCALL_LABEL_CREATE
	DO_SYSCALL
	! save the returned value
	push_register eax
	call free: args
	pop_register eax
endfunction

function label_set_text:ptr label, ptr str
	ebx = label
	ecx = str
	eax = SYSCALL_LABEL_SET_TEXT
	DO_SYSCALL
endfunction

function label_add_text:ptr label, ptr str
	ebx = label
	ecx = str
	eax = SYSCALL_LABEL_ADD_TEXT
	DO_SYSCALL
endfunction

function label_add_number:ptr label, uint32_t number
	ebx = label
	ecx = number
	eax = SYSCALL_LABEL_ADD_NUMBER
	DO_SYSCALL
endfunction

/* |||||||||| BUTTON |||||||||| */
function button_create:uint8_t x, uint8_t y, uint8_t color, ptr str
	ptr args
	args = call malloc:7
	#(args) = x
	#(args+1) = y
	#(args+2) = color
	#(args+3) = str
	ebx = args
	eax = SYSCALL_BUTTON_CREATE
	DO_SYSCALL
	! save the returned value
	push_register eax
	call free: args
	pop_register eax
endfunction

function button_set_text:ptr button, ptr str
	ebx = button
	ecx = str
	eax = SYSCALL_BUTTON_SET_TEXT
	DO_SYSCALL
endfunction

function button_get_text:ptr button
	ebx = button
	eax = SYSCALL_BUTTON_GET_TEXT
	DO_SYSCALL
endfunction

/* |||||||||| TEXTBOX |||||||||| */
function textbox_create: uint8_t x, uint8_t y, uint8_t size, uint8_t color, ptr default_text
	ptr args
	args = call malloc:8
	#(args) = x
	#(args+1) = y
	#(args+2) = size
	#(args+3) = color
	#(args+4) = default_text
	ebx = args
	eax = SYSCALL_TEXTBOX_CREATE
	DO_SYSCALL
	! save the returned value
	push_register eax
	call free: args
	pop_register eax
endfunction

function textbox_set_text:ptr textbox, ptr str
	ebx = textbox
	ecx = str
	eax = SYSCALL_TEXTBOX_SET_TEXT
	DO_SYSCALL
endfunction

function textbox_get_text:ptr textbox
	ebx = textbox
	eax = SYSCALL_TEXTBOX_GET_TEXT
	DO_SYSCALL
endfunction

function textbox_set_type:ptr textbox, uint8_t type
	ebx = textbox
	cl = type
	eax = SYSCALL_TEXTBOX_SET_TYPE
	DO_SYSCALL
endfunction

/* |||||||||| KERNEL |||||||||| */
! must be called if the main function returns a ptr
function convert_ptr:ptr address
	ebx = address
	eax = SYSCALL_CONVERT_PTR
	DO_SYSCALL
endfunction


function shutdown_menu
	eax = SYSCALL_SHUTDOWN_MENU
	DO_SYSCALL
endfunction

function get_drive_info:ptr root
	ebx=root
	eax = SYSCALL_GET_DRIVE_INFO
	DO_SYSCALL
endfunction

function get_arg: ptr cmd, uint32_t n
	ecx = n
	ebx = cmd
	eax = SYSCALL_GET_ARG
	DO_SYSCALL
endfunction

/* |||||||||| I/O |||||||||| */
function wait_key
	eax = SYSCALL_WAIT_KEY
	DO_SYSCALL
endfunction

function wait_key_ascii
	eax = SYSCALL_WAIT_KEY_ASCII
	DO_SYSCALL
endfunction

function bios_read_key_code_ascii: ptr code, ptr ascii
	ecx = ascii
	ebx = code
	eax = SYSCALL_WAIT_KEY_CODE_ASCII
	DO_SYSCALL
endfunction

function get_drive_from_name: ptr name
	ebx = name
	eax = SYSCALL_GET_DRIVE_FROM_NAME
	DO_SYSCALL
endfunction

function stop_output
	eax = SYSCALL_STOP_OUTPUT
	DO_SYSCALL
endfunction
