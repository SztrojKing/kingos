/*********************************
* This file is a part of King OS *
*  written by LANEZ Gaël - 2017  *
*********************************/

include "include/king_includes.sc"

defstruct _loaderinfo
	u8 bootdrive
	u16 res_x
	u16 res_y
	u16 bpp
	ptr vram
	u32 vmode
	u32 memsize
	u8 enable_sse
	u8 disk_only_use_bios
	u8 readOnly
endstruct

function main
	asm cli
	struct _loaderinfo bootinfo=0x6c00
	
	! get kernel entry  address (___entry_address is defined by scc32)
	asm mov eax, ___entry_address
	kernel_address=eax

	asm mov eax, esp
	kernel_stack_address = eax
	
	bootdrive = dl
	cls()
	log("Initialisation..")
	
	log("\nGDT")
	init_gdt()
	console_printOk()

	log("\nPaging")
	init_paging()
	console_printOk()
	
	klog("\nMemoire\n")
	if init_malloc(#bootinfo.memsize)
		console_printOk()
	else
		console_printError()
		fatal_error(" ")
	endif
	bootinfo = malloc(_loaderinfo.size)
	memcpy(bootinfo, 0x6c00, _loaderinfo.size)
	

	disk_only_use_bios = #bootinfo.disk_only_use_bios
	readOnly = #bootinfo.readOnly
	

	log("\nDemarrage depuis ")
	if bootdrive < 0x80
		log("CD/USB")
	else
		log("HDD ")
	endif
	logn(bootdrive)
	if readOnly==true
		color_log(CONSOLE_COLOR_RED, " [READ ONLY]")
	else
		color_log(CONSOLE_COLOR_GREEN, " [READ/WRITE]")
	endif
	log(" (root0) ")

	log("\nKernel @ 0x")
	logx(kernel_address)

	log("\nIDT")
	init_idt()
	console_printOk()
	
	log("\nPIC")
	init_pic()
	console_printOk()

	log("\nPIT (1000hz)")
	! set timer to 1000 ticks/sec
	init_pit(1000)
	console_printOk()

	log("\nRTC date ")
	ptr time_str = get_date_time_str()
	log(time_str)
	free(time_str)

	console_printOk()
	
	log("\nActivation des interruptions")
	asm sti
	console_printOk()
	
	!log("\nReset des disques")
	!init_drives()
	!console_printOk()

	log("\nSSE2")
	if #(bootinfo.enable_sse) == true
		if sse2_init() == true
			console_printOk()
		else
			console_printError()
		endif
	else
		console_printError()
	endif
	
	log("\nRandom")
	init_rand()
	console_printOk()
	
	log("\nClavier")
	if keyboard_init() == true
		console_printOk()
	else
		console_printError()
	endif
		
	log("\nGUI")
	! must be called before enabling interrupts
	init_gui()
	console_printOk()
	
	log("\nMultitache")
	! must be called before enabling interrupts
	start_multitasking()
	console_printOk()
	
!	do_tests()

	log("\nMode graphique")
	sleep(50)
	if init_graphical_mode(#bootinfo.res_x, #bootinfo.res_y, #bootinfo.bpp, #bootinfo.vram, #bootinfo.vmode) == true
		console_printOk()
	else
		console_printError()
	endif
	sleep(50)
	log(" (")
	logn(#bootinfo.res_x)
	log("x")
	logn(#bootinfo.res_y)
	log("x")
	logn(#bootinfo.bpp)
	log(" @ 0x")
	logx(#bootinfo.vram)
	log(" mode=0x")
	logx(#bootinfo.vmode)
	log(" taille=")
	logn([u32](vres_x*vres_y*vbytepp)/1024)
	log(" ko)")
	!klog("Test\n")
	
	ptr screen_args = object_list_create(8)
	! set max fps to 30
	object_list_add(screen_args, 30)
	
	! will have pid 1
	multitask_exec("root0/system/x86/apps/idle.app", null)
	
	! will have pid 2
	multitask_exec("root0/system/x86/apps/screen.app", screen_args)

	
	klog("\nDesktop")
	init_desktop()
	console_printOk()
	
	klog("\nSouris [")
	if mouse_init() == true
		klog("]")
		console_printOk()
	else
		klog("]")
		console_printError()
	endif
	
	klog("\n--------------------------------------------------------------------------------\n")
	
	enable_multitasking()

	!sleep(100000)
	!int16(0x10, 3, 0, 0, 0, 0, 0, 0)
	/*do_tests()
	klog("\nFin\n")
	pause()*/
	!window_hide(multitask_get_console(1))
	sleep(100)
/*	multitask_exec("root0/system/x86/apps/a.app")
	multitask_exec("root0/system/x86/apps/a.app")
	multitask_exec("root0/system/x86/apps/a.app")
	multitask_exec("root0/system/x86/apps/a.app")
	multitask_exec("root0/system/x86/apps/a.app")*/
	!multitask_exec("root0/system/x86/apps/a.app")
	
	!multitask_exec("root0/system/x86/apps/b.app")
	!multitask_exec("root0/system/x86/apps/c.app")
!multitask_exec("root0/system/x86/apps/crash.app")
	!sleep(1000)
	!sleep(5000)
	!ultitask_kill(4)


	!windows_manager_add_window(window_create_console())
	!multitask_kill(4)
	!multitask_exec("root0/system/x86/apps/guitest.app", null)
	!do_tests()
	!exec("root0/system/x86/apps/flbrwsr.app", null)
!	gui_tests()
	!ptr vm_args = object_list_create(8)
	!object_list_add(vm_args, "root0/system/x86/apps/testapp.vma")
	!multitask_exec("root0/system/x86/apps/vm.app", vm_args)
	
	!ptr vm_args2 = object_list_create(8)
	!object_list_add(vm_args2, "root0/system/x86/apps/flbrwsr.app")
	!multitask_exec("root0/system/x86/apps/vm.app", vm_args2)
	!multitask_exec("root0/system/x86/apps/b.app", null)
	klog("Linux Compilation 2 !\n")
! do_tests()
!	do_tests()
!	gui_tests()

	multitask_kill(0)
	loop
endfunction


function var_dump: ptr name, u32 value
	klog(name)
	klog(":  ")
	klogn(value)
	klog("\n")
endfunction


function gui_tests
	ptr win = window_create(500, 500, "My Window")
	ptr vscroll = vscroll_create(win, 100, 300, 800, 500)
	ptr vscrollContainer = container_get_container(vscroll)
	ptr buttonInContainer = button_create(vscrollContainer, 300,500,400, "In Container")
	ptr textboxInContainer = [ptr](textbox_create(vscrollContainer, 0,2000,1000, "Entrer du texte", 32))
	ptr buttonInContainer2 = button_create(vscrollContainer, 300,1200,400, "In Container 2")
	vscroll_add_widget(vscroll, buttonInContainer)

	vscroll_add_widget(vscroll, buttonInContainer2)
		vscroll_add_widget(vscroll, textboxInContainer)
	
	ptr button = button_create(win, 400,250,200, "Click me !")
	ptr buttonNewWindow = button_create(win, 800,50,200, "New window")
	ptr buttonStop = button_create(win, 800,0,200, "Close window")
	ptr event
	ptr textbox = [ptr](textbox_create(win, 300,100,400, "Entrer du texte", 16))
	ptr label_random_number = label_create(win, 400, 900, 1000, 1000, null)
	

	windows_manager_add_window(win, 0)
	window_add_widget(win, label_create(win, 0,0,1000, 100, "Hello GUI world !"))
	window_add_widget(win, button)
	window_add_widget(win, buttonStop)
	window_add_widget(win, buttonNewWindow)
	window_add_widget(win, textbox)
	window_add_widget(win, vscroll)
	window_attach_pid(win, MAX_TASKS)
	bool continue = true
	ptr random_number_str_label = malloc(128)
	ptr random_number_str = malloc(32)
	ptr textbox_text = null
	window_add_widget(win, label_random_number)
	/*window_add_widget(win, label_create(0,100,"Hello GUI world !"))
	window_add_widget(win, label_create(0,200,"Hello GUI world !"))
	window_add_widget(win, label_create(0,300,"Hello GUI world !"))
	window_add_widget(win, label_create(0,400,"Hello GUI world !"))
	window_add_widget(win, label_create(0,500,"Hello GUI world !"))
	window_add_widget(win, label_create(0,600,"Hello GUI world !"))
	window_add_widget(win, label_create(0,700,"Hello GUI world !"))*/
	
	! kill kernel task ( console will not die)
	!multitask_kill(0)
	ptr label = null
	while continue
		inSyscall=true
		event = window_get_last_event(win)
		if event =/= null
			if window_event_get_widget(event) == [ptr](button)
				if label =/= null
					window_remove_widget(win, label)
					widget_free(label)
				endif
				#random_number_str_label = 0
				
				strcat(random_number_str_label, "Random number: ")
				strcatn(random_number_str_label, rand(0, 999))
				label_set_text(label_random_number, random_number_str_label)
				textbox_text = textbox_get_text(textbox)
				label = label_create(win, 0,100, 1000, 1000, textbox_text)
				free(textbox_text)
				window_add_widget(win, label)
				
			endif
			if window_event_get_widget(event) == [ptr](buttonStop) || window_event_get_cat(event) == SIGNAL_EVENT_KILL
				continue = false
			endif
			if window_event_get_widget(event) == [ptr](buttonNewWindow)
				window_lock(win, true)
				!gui_tests()
				inSyscall=true
				!label_set_text(label_random_number, do_tests())
				do_tests()
				inSyscall=false
				window_lock(win, false)
				
			endif
			window_delete_last_event(win)
		endif
		inSyscall=false
		sleep(50)
	endwhile
	free(random_number_str_label)
	free(random_number_str)
	windows_manager_close_window(win)
	window_free(win)
endfunction

function do_tests
	inSyscall=true
	!ptr file=null
	ptr file=fat12_read_file("root0/system/yolo.txt")
!	ptr write_tmp=malloc(512)
	if file=/=null
		klog("\nContenu de root0/system/yolo.txt(")
		klogn(fat12_get_file_size("root0/system/yolo.txt"))
		klog(" octet(s)):\n")
		klog(file)

	else
		klog("Read file error")
	endif
	klog("\nWrite file result: ")
	!klogn(fat12_write_file("root0/yolo2.txt", "yolo", 5))
	klog("\n")
	free(file)
	klog("inSyscall : ")
	klogn(inSyscall)
	/*
	klog("\nEntrez un mot:\n")
	scans(write_tmp, 512, "\n ")
	klog("\nVous avez ecrit le mot '")
	klog(write_tmp)
	klog("'\n")*/
	!free(write_tmp)
	inSyscall=false
	!return file
endfunction

sbool in_exception=false

function fatal_error: ptr desc
	! Only halt the system if the error is generated by kernel (pid=0)
	! Otherwise, just kill the faulty application and resume
	multitaskingPaused=true

	! if it's not the kernel that generated the fault
	if current_pid>1 && in_exception==false
		ptr errorMsg = malloc(512)
		strcat(errorMsg, "L'application [PID = ")
		strcatn(errorMsg, current_pid)
		strcat(errorMsg, "] s'est arretee: ")
		strcat(errorMsg, desc)
		msgbox("Erreur", errorMsg)
		klog("\n")
		klog(errorMsg)
		klog("\n")
		free(errorMsg)
		

		multitaskingPaused=false
		
		multitask_kill(current_pid)
		inSyscall=false

	else
		
		switch_to_text_mode()

		!console_printError()
		klog(desc)
		klog("\nErreur interne (PID=")
		klogn(current_pid)
		klog(").\nImpossible de continuer..")
		asm cli
		
	endif

	if in_exception == true
		in_exception = false
		! properly end exception with iret to an infinite loop
	endif
	

	while 1
		asm hlt
	endwhile


endfunction

asm infinite_loop:
asm jmp $


function msgbox: ptr title, ptr text
	ptr args = object_list_create(3)
	object_list_add(args, clone_str(title))
	object_list_add(args, clone_str(text))
	multitask_exec("root0/system/x86/apps/msgbox.app", args)
endfunction
