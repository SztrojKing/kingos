#!/bin/sh






sudo rm -r /mnt/floppyKing
sudo mkdir /mnt/floppyKing/
RED="\e[31m"
GREEN="\e[32m"
NORMAL="\e[0m"
BLUE="\e[34m"
pause()
{
	printf "Appuyez sur entrée pour continuer..\n" 
	read mot
}


	
while true; do
	clear
	mount -o loop bin/disk.img /mnt/floppyKing/
	printf "*******************************************************************************\n"
	printf "*                                                                             *\n"
	printf "*                        $BLUE Building KING OS for FLOPPY$NORMAL                         *\n"
	printf "*                                                                             *\n"
	printf "*******************************************************************************\n"





	printf "									$GREEN[OK]$NORMAL\n"


	printf "*********************** Compilation du kernel ************************\n"
	./scc32_linux -no_auto_compil -e kernel.sc -org 0x8300000

	if test "$?" = "0"
	then
		nasm -O0 -f bin sortie.asm -o /mnt/floppyKing/system/x86/kernel/king.knl
		if test "$?" = "0"
		then
			printf "									$GREEN[OK]$NORMAL\n"
			printf "********************* Copie et creation du disque *******************\n"
			sudo umount /mnt/floppyKing
			printf "									$GREEN[OK]$NORMAL\n"
			printf "Test (qemu)"
			
			qemu-system-i386 -d guest_errors -m 256 -drive file="bin/disk.img",index=0,if=ide,format=raw -localtime
		else
			printf "$RED							            [ERREUR]$NORMAL\n"
		fi
	else
		printf "$RED							            [ERREUR]$NORMAL\n"
	fi
#	sudo umount /mnt/floppyKing
	pause

done



