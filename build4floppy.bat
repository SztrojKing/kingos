@echo off
setlocal enabledelayedexpansion
title Building KING OS for FLOPPY
set runCount=0
cd /d %~dp0
:start
color 09
rem cls
echo *******************************************************************************
echo *                                                                             *
echo *                        Building KING OS for FLOPPY                          *
echo *                                                                             *
echo *******************************************************************************
echo.

color 07


echo 									[OK]
echo.

echo *********************** Compilation du kernel ************************
scc32.exe -no_auto_compil -e kernel.sc -s bin\kernel.bin -org 0x8300000
if ERRORLEVEL 1 ( goto exit_fail )
Stupid_Loader\tools\nasm.exe -O0 -f bin sortie.asm -o bin\kernel.bin
if ERRORLEVEL 1 ( goto exit_fail )
del sortie.asm
rem pause
echo 									[OK]
echo.

echo ********************* Copie et creation du disque *******************

copy /b bin\kernel.bin fs\system\x86\kernel\king.knl
rem xcopy /S /y apps fs\system\x86\src\apps
rem xcopy /S /y include fs\system\x86\src\kernel
rem copy /b kernel.sc fs\system\x86\src\kernel.sc
Stupid_Loader\tools\winimage\winimage.exe bin\disk.img fs /I /H /Q
del bin\*.bin
:end
set runCount=1
goto eof


:exit_fail
color 0c
echo 									[ERREUR]
pause
goto start
:eof