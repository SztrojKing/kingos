@echo off
f:
cd %CD%
set cpath=%CD%

:start
for /d %%b in (*) do (
    call "%CD%\%%b\build.bat" 1
	cd %cpath%
)
color 0a
pause
goto start