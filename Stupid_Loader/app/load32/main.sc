include "..\..\include\common\app_kit.sc"
include "..\_lib\stdlib.sc"
! stack at 0x820000 (130 Mo)
! os at 0x8300000 (131Mo)
define OS 0x8300000

define MODULE_INT16_ADDRESS 0x7000

function main: ptr args
	
	ptr videoStruct = 0x100000
	uint16_t decallage=0
	uint32_t memsize = 0
	uint32_t errorCode=0
	
	asm mov ax, ds
	decallage = ax
	call cls
	
	memsize = call exec:{"root0/system/x16/apps/memdet.bin"}, null
	
	! if their is an error (not enough memory to load the kernel code)
	if memsize =/= 1
		! did the syscall failed ?
		if memsize = 0
			if call continuerPrompt:{"[ATTENTION] Impossible de detecter la memoire. Continuer quand meme ? [o/n]"} = 1
				! let's assume we got at least 256MB
				memsize = 256*1024*1024
			endif
		endif	
		! check we have enough memory
		if memsize >= 192*1024*1024
			! load video
			videoStruct = call exec:{"root0/system/x16/apps/video.bin"}, null
			if videoStruct =/= null
				
				if [uint32_t](videoStruct) > ((decallage - 0x800) * 0x10)
					
					videoStruct = videoStruct - ((decallage - 0x800) * 0x10)
					
					call prints:{"Chargement du module d'interruptions 16 bits\n"}
					if call load_int16 = true
						call prints:{"Chargement du module 32 bits\n"}
						call stop_output
						call load_module_32:{"root0/system/x86/kernel/king.knl"}, videoStruct, memsize
						errorCode = 6
					else
						errorCode = 5
					endif
				else
					errorCode = 4
				endif
			else
				errorCode = 3
			endif
			! jump back to text mode
			ah = 0x00
			al = 0x03
			asm int 0x10
			call cls
		else
			errorCode = 2
			call prints:{"Pas assez de memoire disponible\n"}
		endif
	else
		errorCode = 1
		call prints:{"Erreur lors de la detection de la memoire\n"}
	endif
	! if we're their, the function must have failed
	call prints:{"Une erreur est survenue !\nCode d'erreur: "}
	call printn:errorCode
	call prints:{"\n"}
	call pause
endfunction

function load_int16
	
	ptr module_data
	uint32_t module_size=0
	module_size = call get_file_size:{"root0/system/x16/apps/int16.bin"}
	if module_size>0 and module_size<0xffff
		module_data = call read_file:{"root0/system/x16/apps/int16.bin"}
		if module_data =/= null
			call memcpy_32_to_16:MODULE_INT16_ADDRESS/0x10, 0, module_data, module_size
			return true
		endif
	endif
	return false

endfunction

function load_module_32: ptr module_path, ptr videoStruct, uint32_t memsize
	ptr file=null
	uint32_t size=0
	uint16_t decallage=0
	uint8_t bootdrive=0
	uint16_t tmp16
	uint32_t tmp32
	bootdrive = call get_bootdrive
	size = call get_file_size:module_path
	if size > 0
		file = call read_file:module_path
		if file =/= null
			! copy kernel info to 0x7c0:0x0
			push_register es
			ax = 0x6c0
			es = ax
			! bootdrive
			al = bootdrive
			asm mov [es:0], al
			! res_x
			tmp16 = [uint16_t]#(videoStruct)
			ax = tmp16
			asm mov [es:4], ax
			!res_y
			tmp16 = [uint16_t]#(videoStruct+2)
			ax = tmp16
			asm mov [es:8], ax
			! bpp
			tmp16 = [uint16_t]#(videoStruct+4)
			ax = tmp16
			asm mov [es:12], ax
			! video address
			tmp32 = [uint32_t]#(videoStruct+6)
			eax = tmp32
			asm mov [es:16], eax
			! video mode
			tmp32 = [uint32_t]#(videoStruct+10)
			eax = tmp32
			asm mov [es:20], eax
			
			! video mode
			eax = memsize
			asm mov [es:24], eax
			
			pop_register es
			! convert virtual address of module in the current process to real address with segment = 0
			asm mov ax, ds
			decallage  = ax
			call jump_to_module_32:[uint32_t](file + (decallage*0x10)), size
			
		endif
	endif
	return false
endfunction

function jump_to_module_32: ptr code_data_real_address, uint32_t len
	uint8_t bootdrive
	bootdrive = call get_bootdrive
	! jump to protected mode

	asm ; initialisation du pointeur sur la GDT
    asm mov ax, gdtend    ; calcule la limite de GDT
    asm mov bx, gdt
    asm sub ax, bx
    asm mov word [gdtptr], ax
	asm 
	asm     xor eax, eax      ; calcule ladresse lineaire de GDT
	asm 	xor ebx, ebx
	asm     mov ax, ds
	asm     mov ecx, eax
	asm     shl ecx, 4
	asm     mov bx, gdt
	asm     add ecx, ebx
	asm     mov dword [gdtptr+2], ecx
	
	call prints:{"OK\n"}
	! save our variables
	ebx = code_data_real_address
	ecx = len
	dl = bootdrive
	
	asm cli
	
	asm lgdt [gdtptr]    ; charge la gdt
	asm mov eax, cr0
    asm or  ax, 1
    asm mov cr0, eax        ; PE mis a 1 (CR0)

	asm     jmp .next
	asm .next:
	! set new segment registers
	! **** DONT USE SC NOW ****
	! GDT data segment
	asm     mov ax, 0x10
	asm     mov ds, ax
	asm     mov fs, ax
	asm     mov gs, ax
	asm     mov es, ax
	asm     mov ss, ax
	! set stack to 10 Mo
	asm     mov ebp, 0x820000
	asm     mov esp, 0x820000

	! copy code in low memory to new kernel address
	asm mov esi, ebx
	asm mov edi, OS
	asm cld
	! ecx already contains len
	! copy using 32 bit instruction (a32) processor rep 
	asm a32 rep movsb
	
	! everything should be setup now, we can go on
	! set segment to GDT code segment
	asm     jmp dword 0x08:OS


endfunction

function continuerPrompt: ptr str
	ptr myWindow
	ptr myLabel
	ptr buttonYes
	ptr buttonNo
	!ptr buttonExec
	ptr selectedChoice
	uint32_t choix = 10
	
	
	myWindow=call window_create

	myLabel=call label_create:POSITION_CENTER,POSITION_CENTER,COLOR_RED, str
	
	buttonYes=call button_create:POSITION_LEFT,20,(DEFAULT_COLOR), {"Oui"}
	!buttonExec=call button_create:POSITION_CENTER,21,(DEFAULT_COLOR), {"Execute"}
	buttonNo=call button_create:POSITION_RIGHT,20,(DEFAULT_COLOR), {"Non"}
	
	call window_addItem:myWindow, OBJECT_LABEL, myLabel
	call window_addItem:myWindow, OBJECT_BUTTON, buttonNo
	call window_addItem:myWindow, OBJECT_BUTTON, buttonYes
	
	while 1=1
		selectedChoice = call window_exec:myWindow
		if selectedChoice = buttonNo
			choix = 0
		endif
		if selectedChoice = buttonYes
			choix = 1
		endif
	
		if choix<2
			call window_free:myWindow
			return choix
		endif
	endwhile

endfunction

asm gdt:
asm     db 0, 0, 0, 0, 0, 0, 0, 0
asm gdt_cs:
asm     db 0xFF, 0xFF, 0x0, 0x0, 0x0, 10011011b, 11011111b, 0x0
asm gdt_ds:
asm     db 0xFF, 0xFF, 0x0, 0x0, 0x0, 10010011b, 11011111b, 0x0
asm gdtend:
asm ;--------------------------------------------------------------------
asm gdtptr:
asm     dw 0  ; limite
asm     dd 0  ; base
asm ;--------------------------------------------------------------------