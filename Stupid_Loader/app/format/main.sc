include "..\..\include\common\app_kit.sc"
include "..\_lib\stdlib.sc"
function main: ptr args

	uint32_t drive=0
	bool isError = false
	ptr boot_sector
	drive = call atoi:args
	boot_sector = call read_file: {"root0/system/x16/boot/fat12.bin"}
	if boot_sector =/= null
		uint32_t i=0
		ptr blank_space
		blank_space = call malloc: 1024
		call memset:blank_space, 0, 1024
		
		/* erase all data */
		while i<360
			if call write_disk_raw: drive, i*2, 2, blank_space =/= 0
				call prints:{"Disk access error (erase)\n"}
				isError = true
			endif
			i++
		endwhile
		
		! first clusters are used
		#blank_space = [uint32_t]0x00FFFFF0
		if call write_disk_raw: drive, 1, 1, blank_space =/= 0
				call prints:{"Disk access error (clusters)\n"}
				isError = true
			endif
		call free:blank_space
		
		! write new boot sector
		if call write_disk_raw: drive, 0, 1, boot_sector =/= 0
			call prints:{"Disk access error(copy)\n"}
			isError = true
		endif
		call free:boot_sector
	else
		call prints:{"File read error\n"}
		isError = true
	endif
	if isError = true
		call pause
	endif
	return 0
	
endfunction


