include "..\..\include\common\app_kit.sc"
include "..\_lib\stdlib.sc"
function main: ptr args
	uint8_t drive
	ptr drive_str
	ptr root
	ptr cmd
	
	cmd = call malloc:512

	
	root = call malloc:16
	drive_str = call malloc: 32
	
	drive = call choose_drive:root
	if drive < 0xff
		call itoa:drive_str, drive
		call prints:{"Formatage..\n"}
		call exec:{"root0/system/x16/apps/format.bin"}, drive_str
		call strcpy:cmd, {"root0|"}
		call strcat:cmd, root
		call exec:{"root0/system/x16/apps/copy.bin"}, cmd
		call prints:{"\nInstallation terminee !\n\n"}
		call pause
	else
		call prints:{"Aucun disque trouve.\n"}
		call pause
	endif
	call free:drive_str
	call free:root
	call free:cmd
endfunction


function choose_drive: ptr root_name
		ptr window
	ptr object
	ptr choosen_root
	ptr info
	ptr current_name
	uint32_t nFound=0
	uint8_t res=0xff
	uint8_t color
	uint8_t boot_drive
	uint8_t x=0
	uint8_t root_number='1'
	ptr button_name
	window = call window_create
	object = call label_create:POSITION_CENTER,POSITION_TOP,COLOR_LIGHT_BLUE, {"Selectionnez le disque sur lequel installer l'OS"}
	call window_addItem:window, OBJECT_LABEL, object
	current_name=call malloc:8
	button_name=call malloc:32
	call strcpy: current_name, {"root?"}
	! create buttons
	while root_number <= '4'
		color=DEFAULT_COLOR
		#(current_name+4)=root_number

		info = call get_drive_info:current_name
		! if drive exists
		if info =/= null
			if [uint8_t]#(info+GET_DRIVE_INFO_IS_BOOTDRIVE_OFFSET) = [uint8_t]false
				call strcpy:button_name, {"      "}
				call strcat:button_name, current_name
				call strcat:button_name, {"      "}
				object = call button_create:x,5,color, button_name
				call window_addItem:window, OBJECT_BUTTON, object
				
				if [uint8_t]#(info+GET_DRIVE_INFO_TYPE_OFFSET) = [uint8_t]GET_DRIVE_INFO_TYPE_CD_USB
					object = call label_create:x,6,DEFAULT_COLOR, {"CD/USB"}
				else
					object = call label_create:x,6,DEFAULT_COLOR, {"HDD"}
				endif
				
				call window_addItem:window, OBJECT_LABEL, object
				object = call label_create:x,7,DEFAULT_COLOR, {"Espace occupe:"}
				call window_addItem:window, OBJECT_LABEL, object
				
				object = call progressBar_create:x,8,18,(COLOR_LIGHT_BLUE), [uint16_t]#(info+GET_DRIVE_INFO_TOTAL_CLUSTERS)
				call progressBar_setValue:object, [uint16_t]#(info+GET_DRIVE_INFO_USED_CLUSTERS)
				call window_addItem:window, OBJECT_PROGRESS_BAR, object
				
				object = call label_create:x,9,DEFAULT_COLOR, (info+GET_DRIVE_INFO_LABEL)
				call window_addItem:window, OBJECT_LABEL, object
				
				object = call label_create:x,10,DEFAULT_COLOR, (info+GET_DRIVE_INFO_FILESYSTEM)
				call window_addItem:window, OBJECT_LABEL, object
				x = x + 20
				nFound++
			endif
			call free:info
		endif
		root_number++
	endwhile
	call free:current_name
	call free:button_name
	if nFound>0
		object = call window_exec:window
		if object =/= null
			choosen_root = call button_get_text:object
			res = call get_drive_from_name:(choosen_root+6)
			call memcpy:root_name, (choosen_root+6), 5
		endif
	endif
	call window_free:window
	return res
endfunction
