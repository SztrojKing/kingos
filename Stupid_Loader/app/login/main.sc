include "..\..\include\common\app_kit.sc"
include "..\..\include\string.sc"
include "..\..\include\mem.sc"

function hash: ptr str
	uint32_t hash = 246984
	uint32_t i=65
	while [uint8_t]#(str) =/= 0
		uint8_t char = [uint8_t]#(str)
		hash = hash + (((char << (char%2))) + (i*char))
		str++
		i++
	endwhile
	return hash
endfunction

function main: ptr args
	ptr myWindow
	ptr labelLogin
	ptr labelPass
	ptr labelTry
	ptr buttonOk
	ptr login
	ptr pass
	ptr selectedChoice
	uint8_t n_try=3
	
	myWindow=call window_create
	
	
	labelLogin=call label_create:POSITION_CENTER,8,COLOR_LIGHT_BLUE, {"Identifiant"}
	labelPass=call label_create:POSITION_CENTER,10,COLOR_LIGHT_BLUE, {"Mot de passe"}
	
	labelTry=call label_create:POSITION_CENTER,15,COLOR_RED, {" "}
	
	buttonOk=call button_create:POSITION_CENTER,13,COLOR_GREEN, {"Valider"}

	
	login = call textbox_create:POSITION_CENTER, 9, 20, DEFAULT_COLOR, {"SztrojKing"}
	pass = call textbox_create:POSITION_CENTER, 11, 20, DEFAULT_COLOR, null
	call textbox_set_type:pass, TEXTBOX_PASSWORD
	
	
	call window_addItem:myWindow, OBJECT_LABEL, labelLogin
	call window_addItem:myWindow, OBJECT_LABEL, labelPass

	call window_addItem:myWindow, OBJECT_TEXTBOX, login
	call window_addItem:myWindow, OBJECT_TEXTBOX, pass
	call window_addItem:myWindow, OBJECT_BUTTON, buttonOk
	
	call window_addItem:myWindow, OBJECT_LABEL, labelTry
	call window_showItem:myWindow, labelTry, false
	
	while n_try>0
		selectedChoice = call window_exec:myWindow
		if selectedChoice = buttonOk
			ptr loginStr
			uint32_t hash
			ptr passStr
			passStr = call textbox_get_text:pass
			hash = call hash:passStr
			loginStr = call textbox_get_text:login
			if call strcmp:loginStr, {"SztrojKing"} = 0 and hash = 304816
				call window_free:myWindow
				return 1
			else
				call textbox_set_text:pass, null
			endif
			
			n_try--
			call label_set_text:labelTry, null
			call label_add_number:labelTry, n_try
			if n_try > 1
				call label_add_text:labelTry, {" essais restants."}
			else
				call label_add_text:labelTry, {" essai restant."}
			endif
			call window_showItem:myWindow, labelTry, true
		endif
	endwhile
	call window_free:myWindow
	while 1=1
		call shutdown_menu
	endwhile
endfunction

