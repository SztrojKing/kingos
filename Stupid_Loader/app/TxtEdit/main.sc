include "..\..\include\common\app_kit.sc"
/*
function main
	ptr myAddress = 0
	call prints:{"Hello world !\n"}
	call cls
	
	ptr file_buffer
	file_buffer = call read_file: {"root0/boot/img/logo.txt"}

	call cls
	call prints:{"\n\n\n\n\n\n\n\n"}
	
	if file_buffer =/= null
		call prints:file_buffer
		call free:file_buffer
		call sleep:500
	endif
	call cls
	call prints:{"Sleep test..\n"}
	call sleepTest
	call prints:{"\nMalloc.."}
	myAddress = call malloc: 256*1024
	call sleep:250
	if myAddress = null
		call prints:{" Error !\n"}
	else
		call prints:{" OK\nFree..  "}
		call sleep:250
		call free:myAddress
		call prints:{" OK\nExiting!\n"}
	endif
	call sleep:500
	return 1
endfunction

function sleepTest
	uint32_t i = 4
	while i > 0
		call printn:i
		call prints:{" "}
		call sleep:250
		i--
	endwhile
endfunction
*/


function main: ptr args
	ptr file_path
	ptr data
	uint32_t current_line=0
	call cls
	! file_path = call file_browser
	! data = call read_file: file_path
	! call free: file_path
	
	data = call read_file:{"root0/system/x16/src/bios.sc"}
	if data = null
		call prints:{"data = null"}
	else
		while current_line=/=0xffffffff
			current_line = call custom_prints:current_line, data
		endwhile
	endif

	!call prints:data
	!call free:data

	call pause
	call cls
	call free:data
endfunction

function custom_prints: uint32_t start_line, ptr str
	uint8_t x=0
	uint8_t current_line = 0
	uint8_t y = 3
	uint8_t color = DEFAULT_COLOR
	if str = null
		exit
	endif

	while [uint8_t]#str =/= 0 and y<24
		! LF
		if [uint8_t]#str = 10
			if current_line>=start_line
				y++
			endif
			current_line++
		endif
		! CR
		if [uint8_t]#str = 13
			x=0
		endif
		
		! tab
		if  [uint8_t]#str = 9
			x=x+4
		endif
		! normal char
		if [uint8_t]#str >= ' '
			if current_line>=start_line
				call printc:x, y, color, [uint8_t]#str		
			endif
			x++
		endif
		
		if x>=80
			x=x-80
			if current_line>=start_line
				y++
			endif
			current_line++
		endif
		str++	
	
	endwhile
	return current_line

endfunction
