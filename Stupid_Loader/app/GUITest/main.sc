include "..\..\include\common\app_kit.sc"

function main: ptr args
	ptr myWindow
	ptr myProgressBar
	ptr myLabel
	ptr buttonUp
	ptr buttonDown
	!ptr buttonExec
	ptr textbox
	ptr selectedChoice
	
	uint32_t myProgressBarCurrentValue=50
	uint32_t nTextChange = 1
	
	myWindow=call window_create
	
	myProgressBar=call progressBar_create:POSITION_RIGHT,POSITION_BOT,20,COLOR_WHITE,100
	call progressBar_setValue:myProgressBar, myProgressBarCurrentValue
	
	myLabel=call label_create:POSITION_CENTER,POSITION_CENTER,(COLOR_GREEN<<4)|(COLOR_GREEN>>4), {"Label au centre"}
	
	buttonDown=call button_create:POSITION_LEFT,21,(COLOR_RED), {"Progress bar -"}
	!buttonExec=call button_create:POSITION_CENTER,21,(DEFAULT_COLOR), {"Execute"}
	buttonUp=call button_create:POSITION_RIGHT,21,(COLOR_RED), {"Progress bar +"}

	
	textbox = call textbox_create:POSITION_CENTER, 5, 20, COLOR_WHITE, {"Textbox sur 1 ligne"}
	
	call window_addItem:myWindow, OBJECT_PROGRESS_BAR, myProgressBar
	call window_addItem:myWindow, OBJECT_LABEL, myLabel
	call window_addItem:myWindow, OBJECT_BUTTON, buttonDown
	!call window_addItem:myWindow, OBJECT_BUTTON, buttonExec
	call window_addItem:myWindow, OBJECT_BUTTON, buttonUp
	call window_addItem:myWindow, OBJECT_TEXTBOX, textbox
	
	while 1=1
		selectedChoice = call window_exec:myWindow
		if selectedChoice = null
			call window_free:myWindow
			exit
		endif
		if selectedChoice = buttonUp
			if myProgressBarCurrentValue < 100
				myProgressBarCurrentValue++
				call progressBar_setValue:myProgressBar, myProgressBarCurrentValue
			endif
		endif
		if selectedChoice = buttonDown
			if myProgressBarCurrentValue > 0
				myProgressBarCurrentValue--
				call progressBar_setValue:myProgressBar, myProgressBarCurrentValue
			endif
		endif
		
		if selectedChoice = textbox
			ptr valeur_textbox
			valeur_textbox = call textbox_get_text:textbox
			
			call label_set_text:myLabel, {"Texte "}
			call label_add_number:myLabel, nTextChange
			call label_add_text:myLabel, {" : '"}
			call label_add_text:myLabel, valeur_textbox
			call label_add_text:myLabel, {"'"}
			
			nTextChange++
		endif
	endwhile

endfunction

