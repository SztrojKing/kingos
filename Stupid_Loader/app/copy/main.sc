include "..\..\include\common\app_kit.sc"
include "..\_lib\stdlib.sc"
define fat12_read_file read_file

define bpbRootEntries 224
define FILE_NAME_SIZE 11
define PATH_NEXT_ENTRY_EOP 0xff

define FAT12_READ_ONLY 0x01 
define FAT12_HIDDEN 0x02 
define FAT12_SYSTEM 0x04 
define FAT12_VOLUME_ID 0x08 
define FAT12_DIRECTORY 0x10 
define FAT12_ARCHIVE 0x20
function main: ptr args
	ptr arg0
	ptr arg1
	arg0 = call get_arg: args, 0
	arg1 = call get_arg: args, 1
	if arg0 =/= null and arg1 =/= null
		call copy_directory: arg0, arg1
	endif
	call free:arg0
	call free:arg1
endfunction


function copy_file: ptr srcPath, ptr destPath
	
	ptr data=null
	uint32_t size
	data = call read_file: srcPath
	if data =/= null
		size = call get_file_size:srcPath
		call write_file:destPath, data, size
		call free:data
	endif
endfunction

function copy_directory: ptr srcPath, ptr destPath

	ptr currentDirBuffer=null
	ptr currentFileName=null
	ptr tmpPath=null
	ptr tmpDestPath=null
	ptr tmpDestPathShort=null
	
	tmpPath= call malloc:512
	tmpDestPath = call malloc:512
	tmpDestPathShort = call malloc:512
	
	! start by reading root entry
	currentDirBuffer = call fat12_read_file:srcPath
	currentFileName = call malloc:FILE_NAME_SIZE+3

	uint32_t fileListOffset

		
		
	uint16_t entry_left = 0
	uint32_t offset=0
	uint32_t entry_found = 0
	fileListOffset = 0
	
		
	if currentDirBuffer =/= null
		! read every entry
		while entry_left < bpbRootEntries
			
			! copy current file name
			call memcpy:currentFileName, currentDirBuffer+offset, FILE_NAME_SIZE
			
			! first byte = 0 -> no more files
			if [uint8_t]#(currentFileName) = 0
				entry_left = bpbRootEntries
			else
				! first byte = 0xE5 -> file has been deleted and not long file name entry ?
				if [uint8_t]#(currentFileName) =/= 0xE5 and [uint8_t]#(currentDirBuffer+offset+11) =/= 0x0f
					! dont copy "." and ".." directories
					if call memcmp: currentFileName, {".          "}, FILE_NAME_SIZE =/= 0 and call memcmp: currentFileName, {"..         "}, FILE_NAME_SIZE =/= 0
						call memset: tmpPath, 0, 512
						call memset: tmpDestPath, 0, 512
						call strcpy: tmpPath, srcPath
						call strcpy: tmpDestPath, destPath
						call strcat: tmpDestPath, {"/"}
						call strcat: tmpPath, {"/"}
						call strcat: tmpDestPath, currentFileName
						call strcat: tmpPath, currentFileName
						call toUpper:tmpDestPath
						call strcpy:tmpDestPathShort, tmpDestPath
						call remove_useless_spaces:tmpDestPathShort
						
						! check directory
						if #(currentDirBuffer+offset+0x0b)&FAT12_DIRECTORY =/= 0
							call prints:{"Repertoire '"}
							call prints:tmpDestPathShort
							call prints:{"'\n"}
							call create_directory:tmpDestPath
							call copy_directory: tmpPath, tmpDestPath
						else
							call prints:{"Fichier '"}
							call prints:tmpDestPathShort
							call prints:{"'\n"}
							call copy_file: tmpPath, tmpDestPath
						endif
					
						entry_found++
						!call pause
					endif
				endif
				offset = offset + 32
				entry_left++
			endif
		endwhile
	endif
	call free:currentDirBuffer
	call free:tmpPath
	call free:tmpDestPath
	call free:currentFileName
	call free:tmpDestPathShort

endfunction

function remove_useless_spaces: ptr path
	uint32_t i
	i = call strlen:path
	if i>10 and [uint8_t]#(path+i-4)=[uint8_t]' ' and [uint8_t]#(path+i-3) =/= ' '
		#(path+i-4)=[uint8_t]('.')
	endif

	i=0
	! remove spaces
	while #[uint8_t](path+i) =/= 0
		if #[uint8_t](path+i) = ' '
			call strcpy: (path+i), (path+i+1)
			i--
		endif
		i++
	endwhile
endfunction
