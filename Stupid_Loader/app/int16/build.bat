@echo off

cd /d %~dp0
for %%a in (.) do set appName=%%~na
title Building Application %appName%
color 09
Set argC=0
for %%x in (%*) do Set /A argC+=1

:start

echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
if %argC% == 0 (
	cls
)
echo *******************************************************************************
echo.
echo                               Building %appName%
echo.
echo *******************************************************************************
echo.


rem ..\..\tools\scc16.exe -no_auto_compil -e main.sc -s %appName%.bin -org 0x7000 -p_e main
rem if ERRORLEVEL 1 ( goto exit_fail )
..\..\tools\nasm.exe -f bin main.asm -o %appName%.bin
if ERRORLEVEL 1 ( goto exit_fail )
copy /b %appName%.bin "..\..\..\fs\system\x16\apps\%appName%.bin"
if ERRORLEVEL 1 ( goto exit_fail )
del %appName%.bin


if %argC% == 0 (
	pause
	goto start
)

goto :eof

:exit_fail
color 0c
echo 									[ERREUR]
pause
goto start


