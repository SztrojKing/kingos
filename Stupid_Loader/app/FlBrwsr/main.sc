include "..\..\include\common\app_kit.sc"
include "..\_lib\stdlib.sc"
define fat12_read_file read_file

define bpbRootEntries 224
define FILE_NAME_SIZE 11
define PATH_NEXT_ENTRY_EOP 0xff

define FAT12_READ_ONLY 0x01 
define FAT12_HIDDEN 0x02 
define FAT12_SYSTEM 0x04 
define FAT12_VOLUME_ID 0x08 
define FAT12_DIRECTORY 0x10 
define FAT12_ARCHIVE 0x20


function main: ptr args
	ptr window
	ptr object
	ptr choosen_root
	ptr info=null
	ptr current_name
	ptr res=null
	uint8_t color
	uint8_t x=0
	uint8_t root_number='1'
	ptr button_name

	
	window = call window_create
	object = call label_create:POSITION_CENTER,POSITION_TOP,COLOR_LIGHT_BLUE, {"Navigateur de fichiers"}
	call window_addItem:window, OBJECT_LABEL, object
	current_name=call malloc:32
	button_name=call malloc:64
	call strcpy: current_name, {"root?"}
	! create buttons
	while root_number <= '4'
		color=DEFAULT_COLOR
		#(current_name+4)=root_number

		info = call get_drive_info:current_name
		! if drive exists
		if info =/= null
			if [uint8_t]#(info+GET_DRIVE_INFO_IS_BOOTDRIVE_OFFSET) = [uint8_t]true
				color=COLOR_GREEN
			endif
			call strcpy:button_name, {"      "}
			call strcat:button_name, current_name
			call strcat:button_name, {"      "}
			object = call button_create:x,5,color, button_name
			call window_addItem:window, OBJECT_BUTTON, object
			
			if [uint8_t]#(info+GET_DRIVE_INFO_TYPE_OFFSET) = [uint8_t]GET_DRIVE_INFO_TYPE_CD_USB
				object = call label_create:x,6,DEFAULT_COLOR, {"CD/USB"}
			else
				object = call label_create:x,6,DEFAULT_COLOR, {"HDD"}
			endif
				
			call window_addItem:window, OBJECT_LABEL, object
			
			
			object = call label_create:x,7,DEFAULT_COLOR, {"Espace occupe:"}
			call window_addItem:window, OBJECT_LABEL, object
			
			object = call progressBar_create:x,8,18,(COLOR_LIGHT_BLUE), [uint16_t]#(info+GET_DRIVE_INFO_TOTAL_CLUSTERS)
			call progressBar_setValue:object, [uint16_t]#(info+GET_DRIVE_INFO_USED_CLUSTERS)
			call window_addItem:window, OBJECT_PROGRESS_BAR, object
			
			
			object = call label_create:x,9,DEFAULT_COLOR, (info+GET_DRIVE_INFO_LABEL)
			call window_addItem:window, OBJECT_LABEL, object
			
			object = call label_create:x,10,DEFAULT_COLOR, (info+GET_DRIVE_INFO_FILESYSTEM)
			call window_addItem:window, OBJECT_LABEL, object
			
			x = x + 20
			call free:info
		endif
		root_number++
	endwhile
	call free:current_name
	call free:button_name
	object = call window_exec:window
	if object =/= null
		choosen_root = call button_get_text:object
		res = call browse: (choosen_root+6)
	endif

	call window_free:window

	return res
endfunction

function browse: ptr root
	ptr currentPath
	ptr currentPathShort
	ptr mainWindow
	
	ptr object
	
	ptr currentDirBuffer
	ptr currentFileName
	uint32_t currentPathSize
	ptr root_resized
	root_resized = call malloc:FILE_NAME_SIZE
	call memcpy:root_resized, {"ROOT?      "}, FILE_NAME_SIZE
	#(root_resized+4) = [uint8_t]#(root+4)
	
	! start by reading root entry
	currentDirBuffer = call fat12_read_file:root_resized
	currentFileName = call malloc:FILE_NAME_SIZE+3
	! absolute path
	currentPath = call malloc:(FILE_NAME_SIZE+1)*32
	call memcpy:currentPath, root_resized, FILE_NAME_SIZE
	
	currentPathShort = call malloc:(FILE_NAME_SIZE+1)*32

	
	ptr fileList
	uint32_t fileListOffset
	fileList = call malloc:(FILE_NAME_SIZE+2)*bpbRootEntries
	
	while 1=1
		
		
		uint16_t entry_left = bpbRootEntries
		uint32_t y = 4
		uint32_t x = 0
		uint32_t offset=0
		uint32_t entry_found = 0
		fileListOffset = 0
		
		if currentDirBuffer = null
			return null
		endif
			
		!gui
		mainWindow = call window_create
		object = call label_create:POSITION_CENTER,POSITION_TOP,COLOR_LIGHT_BLUE, {"Navigateur de fichiers"}
		call window_addItem:mainWindow, OBJECT_LABEL, object
		object = call label_create:POSITION_LEFT,2, DEFAULT_COLOR, {"Chemin: "}
		call window_addItem:mainWindow, OBJECT_LABEL, object
		
		call strcpy:currentPathShort, currentPath
		call remove_useless_spaces: currentPathShort
		object = call label_create:8,2,COLOR_PURPLE, currentPathShort
		call window_addItem:mainWindow, OBJECT_LABEL, object
		
		! read every entry
		while entry_left > 0
			
			! copy current file name
			call memcpy:currentFileName+1, currentDirBuffer+offset, FILE_NAME_SIZE
			
			! first byte = 0 -> no more files
			if [uint8_t]#(currentFileName+1) = 0
				entry_left = 0
			else
				! first byte = 0xE5 -> file has been deleted and not long file name entry ?
				if [uint8_t]#(currentFileName+1) =/= 0xE5 and [uint8_t]#(currentDirBuffer+offset+11) =/= 0x0f
						! add to gui
						
						! check directory
						if #(currentDirBuffer+offset+0x0b)&FAT12_DIRECTORY =/= 0
							! we print dir's first
							! dir
							#(currentFileName)=[uint8_t]'='
							object=call button_create:x,y,COLOR_AQUA, currentFileName
							call window_addItem:mainWindow, OBJECT_BUTTON, object
							y++
							if y >= 24
								x = x + 16
								y = 4
							endif
						else
							! save file name to display it after
							#(currentFileName)=[uint8_t]'-'
							call strcat:fileList, currentFileName
						endif

						entry_found++
					
				endif
				offset = offset + 32
				entry_left--
			endif
		endwhile
		
		! print file list
		while [uint8_t]#(fileList+fileListOffset) =/= 0
		
			call memcpy: currentFileName, fileList+fileListOffset, FILE_NAME_SIZE+1
			object=call button_create:x,y,DEFAULT_COLOR, currentFileName
			call window_addItem:mainWindow, OBJECT_BUTTON, object
			y++
			if y >= 24
					x = x + 16
					y = 4
			endif
			fileListOffset = fileListOffset + FILE_NAME_SIZE+1
		endwhile
		
		!clear file list
		call memset: fileList, 0, (FILE_NAME_SIZE+1)*bpbRootEntries
		
		
		! if no entry found and not in root dir, redirect to root directory
		if entry_found = 0 and call strcmp: root_resized,currentPath =/= 0 
			call free: currentDirBuffer
			call window_free:mainWindow
			currentDirBuffer = call fat12_read_file:root
			if currentDirBuffer = null
				call window_free:mainWindow
				call free:currentFileName
				call free:fileList
				call free:currentPath
				call free:currentPathShort
				return null
			endif
		else
			! if we're in an empty root directory, add the "." (home) directory link
			if call strcmp: root_resized,currentPath = 0 and entry_found = 0
				object = call button_create:0,4,COLOR_AQUA, {"=.          "}
				call window_addItem:mainWindow, OBJECT_BUTTON, object
			endif
			! go to selected choice
			ptr selectedChoice
			ptr selectedChoiceText
			
			uint8_t continue = true
			
			while continue = true
				! exec our ugi
				selectedChoice = call window_exec: mainWindow
				
				
				! exit !
				if selectedChoice = null
					call window_free:mainWindow
					call free:currentDirBuffer
					call free:currentFileName
					call free:fileList
					call free:currentPath
					call free:currentPathShort
					return null
				endif
				
				! get selected button text
				selectedChoiceText = call button_get_text:selectedChoice
				
				! if it's a file, return it
				if [uint8_t]#(selectedChoiceText) = '-'
				
					
					call strcat:currentPath, {"/"}
					call strcat:currentPath, selectedChoiceText+1

					! call exec:currentPath
					
					call window_free:mainWindow
					call free:currentDirBuffer
					call free:currentFileName
					call free:fileList
					call free:currentPathShort
					!call free:currentPath
					return call convert_ptr:currentPath
					/*
					
					currentPathSize = call strlen:currentPath
					call memset:(currentPath+currentPathSize-(FILE_NAME_SIZE+1)), 0, (FILE_NAME_SIZE+1)
					*/
				else
					! it's a dir, go to it
					continue = false
				endif
				
			endwhile
			call free: currentDirBuffer
			
				! if "." is selected (go to root)
				if call memcmp:selectedChoiceText+1, {".          "}, FILE_NAME_SIZE = 0
					currentDirBuffer = call fat12_read_file:root
					call memset:currentPath,0, (FILE_NAME_SIZE+1)*32
					call memcpy:currentPath,root_resized, FILE_NAME_SIZE
				else
				
					! if ".." is selected (go back)
					if call memcmp:selectedChoiceText+1, {"..         "}, FILE_NAME_SIZE = 0
						currentPathSize = call strlen:currentPath
						! fill last entry with 0
						call memset:(currentPath+currentPathSize-(FILE_NAME_SIZE+1)), 0, FILE_NAME_SIZE+1

					else
						! normal directory
						call strcat:currentPath, {"/"}
						call strcat:currentPath, selectedChoiceText+1
				
					endif

					! read entry
					currentDirBuffer = call fat12_read_file: currentPath

					! if error
					if currentDirBuffer = null
						currentDirBuffer = call fat12_read_file:root
						call memset:currentPath,0, (FILE_NAME_SIZE+1)*32
						call memcpy:currentPath,root_resized, FILE_NAME_SIZE
					endif
				endif	

			call window_free:mainWindow
		endif
		
	endwhile

	
endfunction

function remove_useless_spaces: ptr path
	uint32_t i
	i = call strlen:path
	if i>6 and [uint8_t]#(path+i-4)=[uint8_t]' ' and [uint8_t]#(path+i-3) =/= ' '
		#(path+i-4)=[uint8_t]('.')
	endif
	call prints:path
	call prints:{"\n"}
	i=0
	! remove spaces
	while #[uint8_t](path+i) =/= 0
		if #[uint8_t](path+i) = ' '
			call strcpy: (path+i), (path+i+1)
			i--
		endif
		i++
	endwhile
endfunction