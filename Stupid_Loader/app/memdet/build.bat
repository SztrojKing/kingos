@echo off

cd /d %~dp0
for %%a in (.) do set appName=%%~na
title Building Application %appName%
color 09
Set argC=0
for %%x in (%*) do Set /A argC+=1

:start

echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
if %argC% == 0 (
	cls
)
echo *******************************************************************************
echo.
echo                               Building %appName%
echo.
echo *******************************************************************************
echo.


..\..\tools\scc16.exe -no_auto_compil -e main.sc -s %appName%.bin -org 0 -p_e entry
if ERRORLEVEL 1 ( goto exit_fail )
..\..\tools\nasm.exe -f bin sortie.asm -o %appName%.bin
if ERRORLEVEL 1 ( goto exit_fail )
rem del sortie.asm
copy /b %appName%.bin "..\..\..\fs\system\x16\apps\%appName%.bin"
if ERRORLEVEL 1 ( goto exit_fail )
del %appName%.bin


if %argC% == 0 (
	pause
	goto start
)

goto :eof

:exit_fail
color 0c
echo 									[ERREUR]
pause
goto start


