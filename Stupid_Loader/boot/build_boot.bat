@echo off
set mingw_path="C:\Program Files (x86)\CodeBlocks\MinGW"
setlocal enabledelayedexpansion
title Building JUMBLE for FLOPPY


:start
color 09
cls
echo *******************************************************************************
echo *                                                                             *
echo *                 Building STUPID OS (bootloader) for FLOPPY                  *
echo *                                                                             *
echo *******************************************************************************
echo.
rem ping 1.1.1.1 -n 1 -w 1000 > nul
color 07


echo Nettoyage
del fat12.bin

echo 									[OK]
echo.


echo ***************** Compilation du boot loader asm (fat12)*****************
..\tools\nasm.exe -f bin fat12_loader.asm -o fat12.bin
if ERRORLEVEL 1 ( goto exit_fail )
echo 									[OK]

echo Succes !
color 0a


pause
goto start


:exit_fail
color 0c
echo 									[ERREUR]
pause
goto start
