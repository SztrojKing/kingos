/*
*******************************************************************************
* Source code writen in "Sztrojka Code by LANEZ" Ga�l - 2015                  *
* Permission to use, copie and modifie this code.                             *
*******************************************************************************
*/


define WINDOW_ITEM_NUMBER 128

include "include\common\gui.sc"
include "include\library.sc"
include "include\gui\progressBar.sc"
include "include\gui\button.sc"
include "include\gui\label.sc"
include "include\gui\textbox.sc"


! create a window struct
function window_create
	ptr window_ptr
	! WINDOW_ITEM_NUMBER items max
	window_ptr=call malloc:(WINDOW_ITEM_NUMBER*12)+32
	call memset:window_ptr,0,(WINDOW_ITEM_NUMBER*12)+32
	! window header : nothingSelected = true
	#(window_ptr+8)=[uint8_t]true
	return window_ptr
endfunction

!free a window struct with all its children
function window_free:ptr window
	ptr windowBegin=window
	! window header
	window=window+32
	uint32_t i=0
	call cls
	while i < WINDOW_ITEM_NUMBER
		! if there is an item
		if #[uint32_t]window =/= 0
			/*
			call prints:{"Deleting item "}
			if #[uint32_t]window = OBJECT_PROGRESS_BAR
				call prints:{"OBJECT_PROGRESS_BAR\n"}
			endif
			if #[uint32_t]window = OBJECT_LABEL
				call prints:{"OBJECT_LABEL\n"}
			endif
			if #[uint32_t]window = OBJECT_BUTTON
				call prints:{"OBJECT_BUTTON\n"}
			endif
			if #[uint32_t]window = OBJECT_TEXTBOX
				call prints:{"OBJECT_TEXTBOX\n"}
			endif
			call pause
			*/
			! free it
			call free:[ptr]#(window+8)
			#window = [uint32_t]null
			#(window+4) = [uint32_t]0
		endif
		window=window+12
		i++
	endwhile

	call free:windowBegin
endfunction

! add an item to the window
function window_addItem:ptr window, uint32_t itemType, ptr item
	bool continue=true
	uint32_t i=0
	! window header
	window=window+32
	while continue=true
		
		! max item
		if i > WINDOW_ITEM_NUMBER
			continue = false
			call fatal_exception:EXCEPTION_OBJECT
		endif

		! if there is an item
		if #[uint32_t]window = 0
			#window = itemType
			! flags
			#(window+4)=(OBJECT_VISIBLE)
			#(window+8)=item
			continue = false
		endif
		window = window+12
		i++
	endwhile
endfunction

! show or hide an iteam
function window_showItem: ptr window, ptr item, bool visible
	bool continue=true
	uint32_t i=0
	! window header
	window=window+32
	while continue=true
		
		! max item
		if i > WINDOW_ITEM_NUMBER
			continue = false
			call fatal_exception:EXCEPTION_OBJECT
		endif

		! if there is an item
		if #[uint32_t]window =/= 0
			! if we found the item we searched
			if #[uint32_t](window+8)=item
				! clear flag bit
				if visible = true
					#(window+4)=(OBJECT_VISIBLE)
				else
					#(window+4)=0
				endif
				continue = false
			endif
		endif
		window = window+12
		i++
	endwhile
endfunction

! set the select state of an item
function window_set_item_select_state: ptr item, bool isSelected
	if [uint32_t]#item = [uint32_t]OBJECT_BUTTON
		call button_set_select_state: item, isSelected
	else
		
		if [uint32_t]#item = [uint32_t]OBJECT_TEXTBOX
			call textbox_set_select_state: item, isSelected
		else
			call cls
			call prints:{"Error in function 'window_set_item_select_state': unknown item\n"}
			call pause
		endif
	endif
endfunction
! redraw the window's children
function window_redraw:ptr window
	call cls
	bool continue=true
	uint32_t i=0
	! window header
	window=window+32
	while continue=true
		! max item
		if i > WINDOW_ITEM_NUMBER
			continue = false
		endif
	
		! if there is an item
		if #window[uint32_t] =/= 0
			! if object is visible
			if  (#[uint32_t](window+4))&OBJECT_VISIBLE =/= 0
				! progressBar
				if #[uint32_t]window = OBJECT_PROGRESS_BAR
					call progressBar_redraw:#([uint32_t]window+8)
				endif
				
				! label
				if #[uint32_t]window = OBJECT_LABEL
					call label_draw:#([uint32_t]window+8)
				endif
				
				! button
				if #[uint32_t]window = OBJECT_BUTTON
					call button_draw:#([uint32_t]window+8)
				endif
				
				! textbox
				if #[uint32_t]window = OBJECT_TEXTBOX
					call textbox_draw:#([uint32_t]window+8)
				endif
			endif
		endif
		window=window+12
		i++
	endwhile
endfunction

! execute a window
function window_exec:ptr window
	bool continue
	uint8_t key=0
	uint32_t currentOffset=#(window+4)
	uint32_t previousOffset=#window
	bool nothingSelected = #(window+8)
	bool generateCall

	! select first item
	if nothingSelected = true
		key = 80
	endif
	
	! refresh screen
	call window_redraw:window
	
 	while 1=1
		!call coords_printn:0,5,DEFAULT_COLOR, key

		! escape key
		if key=0x01
			!call window_set_item_select_state:#[uint32_t](window+previousOffset+8+32), false
			!call window_set_item_select_state:#[uint32_t](window+currentOffset+8+32), true
			return null
		endif
		
		! down key -> next item
		if key = 80
			continue=true
			while continue = true
				currentOffset=currentOffset+12
				if currentOffset>WINDOW_ITEM_NUMBER*12
					currentOffset=0
				endif
				if currentOffset=previousOffset
					continue = false
				endif

				! if object is visible
				if  (#[uint32_t](window+currentOffset+4+32))&OBJECT_VISIBLE =/= 0
					
					! next object is a button
					if [uint32_t]#(window+currentOffset+32) = OBJECT_BUTTON
						continue=false
					endif
					
					! next object is a textbox
					if [uint32_t]#(window+currentOffset+32) = OBJECT_TEXTBOX
						continue=false
					endif
					
					! refresh the screen if a valid object has been selected
					if continue = false
						if nothingSelected = false
							! unselect button
							call window_set_item_select_state:#[uint32_t](window+previousOffset+8+32), false
						else
							nothingSelected = false
						endif
						! select button
						call window_set_item_select_state:#[uint32_t](window+currentOffset+8+32), true

						previousOffset=currentOffset
					endif
				endif
			endwhile
		else
			! up key
			if key = 72
				continue=true
				while continue = true
				
					if currentOffset=0
						currentOffset=WINDOW_ITEM_NUMBER*12
					endif
					
					if currentOffset>0
						currentOffset=currentOffset-12
					endif

					if currentOffset=previousOffset
						continue = false
					endif
						! if object is visible
					if  (#[uint32_t](window+currentOffset+4+32))&OBJECT_VISIBLE =/= 0
						
						! next object is a button
						if [uint32_t]#(window+currentOffset+32) = OBJECT_BUTTON
							continue=false
						endif
						
						! next object is a textbox
						if [uint32_t]#(window+currentOffset+32) = OBJECT_TEXTBOX
							continue=false
						endif
						
						! refresh the screen if a valid object has been selected
						if continue = false
							if nothingSelected = false
								! unselect button
								call window_set_item_select_state:#[uint32_t](window+previousOffset+8+32), false
							else
								nothingSelected = false
							endif
							! select button
							call window_set_item_select_state:#[uint32_t](window+currentOffset+8+32), true

							previousOffset=currentOffset
						endif
					endif
				endwhile
			endif
			! enter key -> return object ptr
			if key = 28
				! something must have been selected
				if nothingSelected = false
					generateCall = true
					#window=previousOffset
					#(window+4)=currentOffset
					#(window+8) = nothingSelected
					
					if [uint32_t]#(#(window+currentOffset+8+32)) = OBJECT_TEXTBOX
						! unselect item
						call window_set_item_select_state:#[uint32_t](window+currentOffset+8+32), false
						
						! exec it
						if call textbox_exec:#[uint32_t](window+previousOffset+8+32) = false
							generateCall = false
						endif
						! select it again
						call window_set_item_select_state:#[uint32_t](window+currentOffset+8+32), true
					endif
					
					if generateCall = true
						! return wich item has been clicked
						return [uint32_t]#(window+currentOffset+8+32)
					endif
				endif
			endif
		endif
		
		key=call bios_wait_key
		
	endwhile
endfunction
