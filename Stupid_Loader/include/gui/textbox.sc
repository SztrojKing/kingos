/******************************************************************************
* Source code writen in "Sztrojka Code" by LANEZ Ga�l - 2015                  *
* Permission to use, copie and modifie this code.                             *
******************************************************************************/

include "include\library.sc"
include "include\gui\window.sc"
define TEXTBOX_SIGNATURE 386829731

function textbox_create: uint8_t x, uint8_t y, uint8_t size, uint8_t color, ptr default_text
	ptr textbox
	uint32_t strSize
	strSize=call strlen:default_text
	textbox=call malloc:512
	
	if y = POSITION_CENTER
		y = 12
	endif
	#textbox=[uint32_t]OBJECT_TEXTBOX
	#(textbox+4)=x
	#(textbox+5)=y
	#(textbox+6)=color
	! not selected
	#(textbox+7)=false
	
	if x =/= POSITION_CENTER and x+2+size > 79
		size = 79-x-1
	endif
	#(textbox+8)=size
	
	! normal textbox by default
	#(textbox+9)=[uint8_t](TEXTBOX_TEXT)
	! print default text
	if default_text =/= null
		call memcpy:(textbox+10), default_text, strSize
	else
		! or nothing
		#(textbox+10) = 0
	endif
	
	return textbox

endfunction

function textbox_set_select_state: ptr textbox, bool isSelected
	#(textbox+7) = isSelected
	
	! refresh element
	call textbox_draw: textbox
endfunction

! return real x coord even if it is defined position
function textbox_get_real_x: ptr textbox

	uint8_t x = #(textbox+4)
	uint8_t size = [uint8_t]#(textbox+8)
	
	if x = POSITION_CENTER
		x = 39-(size/2)
	endif
	
	if x = POSITION_RIGHT
		x = 78-size
	endif
	
	return x
	
endfunction

function textbox_draw:ptr textbox
	uint8_t x = #(textbox+4)
	uint8_t color = #(textbox+6)
	uint8_t type = #(textbox+9)
	uint8_t i=0
	uint8_t j=0
	uint8_t size = [uint8_t]#(textbox+8)


	x = call textbox_get_real_x: textbox
	
	! selected ?
	if [uint8_t]#(textbox+7) = true
		color = (color<<4)|(color>>4)
	endif
	
	! print text
	call coords_color_prints:x, #(textbox+1+4), color, {"["}
	
	i = call strlen: (textbox+10)
	
	if type = TEXTBOX_PASSWORD
		while j < i
			call coords_color_prints:x+1+j, #(textbox+1+4),color, {"*"}
			j++
		endwhile
	else
		call coords_color_prints:x+1, #(textbox+1+4),color, (textbox+10)
	endif
	

	
	while i < size
		call coords_color_prints:x+1+i, #(textbox+1+4),color, {" "}
		i++
	endwhile
	
	call coords_color_prints:x+size+1, #(textbox+1+4), color, {"]"}
	
endfunction

function textbox_set_text:ptr textbox, ptr str
	if str = null
		#(textbox+10)=[uint8_t](0)
	else
		call strcpy: (textbox+10), str
	endif
endfunction

function textbox_get_text:ptr textbox
	return (textbox+10)
endfunction

! set type (text or password)
function textbox_set_type:ptr textbox, uint8_t type
	#(textbox+9) = type
endfunction

! return true if modified
function textbox_exec: ptr textbox
	bool continue = true
	bool modified = true
	uint32_t offset = 0
	uint8_t key = 0
	uint8_t ascii = 0
	uint8_t size = [uint8_t]#(textbox+8)
	uint32_t strSize=0
	uint8_t y = #(textbox+1+4)
	uint8_t x
	ptr keyBuffer_array
	keyBuffer_array = call malloc:4
	ptr text
	ptr initialText
	text = call textbox_get_text: textbox
	initialText = call malloc:size+1
	call strcpy:initialText, text
	
	x = call textbox_get_real_x: textbox
	
	call memset: (textbox+10), 0, size
	
	while continue = true
		
		call textbox_draw: textbox
		call bios_move_cursor: x+offset+1, y
		call bios_read_key_code_ascii: keyBuffer_array, (keyBuffer_array+2)
		key = #keyBuffer_array
		ascii = #(keyBuffer_array+2)
		! normal key
		if ascii >= 32 and ascii <= 125
			! if we have enough space to write
			if strSize < size
				if call string_append_at_offset:text, ascii, offset = true
					offset++
					strSize++
				endif
			endif
		! control key
		else
			! enter ?
			if key = 28
				continue = false
			endif
			
			! escape
			if key = 1
				continue = false
				modified = false
				call textbox_set_text:textbox, initialText
			endif
			
			! backspace
			if key = 14 and offset>0
				call memcpy:(text+offset-1), (text+offset), strSize-offset
				#(text+strSize-1)=0
				offset--
				strSize--
			endif

			! suppr
			if key = 83 and offset<strSize
				call memcpy:(text+offset), (text+offset+1), strSize-offset
				#(text+strSize-1)=0
				strSize--
			endif
			! left
			if key = 75
				if offset > 0
					offset--
				endif
			endif
			
			! right
			if key = 77 and offset < strSize and offset < size
				offset++
			endif
		endif

	endwhile
	
	call bios_move_cursor: 0xff, 0xff
	call free: initialText
	call free: keyBuffer_array
	return modified
endfunction
