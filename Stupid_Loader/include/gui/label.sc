/*
*******************************************************************************
* Source code writen in "Sztrojka Code" by LANEZ Ga�l - 2015                  *
* Permission to use, copie and modifie this code.                             *
*******************************************************************************
*/

include "include\library.sc"
include "include\gui\window.sc"

define LABEL_SIGNATURE 380819767

! create a label
function label_create:uint8_t x, uint8_t y, uint8_t color, ptr str
	ptr label
	uint32_t strSize
	strSize=call strlen:str
	label=call malloc:512
	
	if y = POSITION_CENTER
		y = 12
	endif
	#label=[uint32_t]OBJECT_LABEL
	#(label+4)=x
	#(label+1+4)=y
	#(label+2+4)=color
	call memcpy:(label+3+4), str, strSize
	return label
endfunction

! draw a label
function label_draw:ptr label
	uint8_t x = #(label+4)
	uint32_t strSize
			
	if x = POSITION_CENTER
		strSize=call strlen:(label+3+4)
		x = 39-(strSize/2)
	endif
	
	if x = POSITION_RIGHT
		strSize=call strlen:(label+3+4)
		x = 79-strSize
	endif
	
	call coords_color_prints:x, #(label+1+4), #(label+2+4), (label+3+4)
endfunction

! set label text
function label_set_text:ptr label, ptr str
	uint32_t strSize
	if str = null
		#(label+3+4)=0
	else
		strSize=call strlen:str
		call memcpy:(label+3+4), str, strSize+1
	endif
endfunction

! add text to label
function label_add_text:ptr label, ptr str
	call strcat:(label+7), str
endfunction

! add number to label
function label_add_number:ptr label, uint32_t number
	ptr str
	str = call malloc:64
	call itoa: str, number
	call strcat:(label+7), str
	call free: str
endfunction
