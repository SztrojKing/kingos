/*
*******************************************************************************
* Source code writen in "Sztrojka Code" by LANEZ Ga�l - 2015                  *
* Permission to use, copie and modifie this code.                             *
*******************************************************************************
*/
include "include\library.sc"
include "include\gui\window.sc"

define BUTTON_SIGNATURE 353638906

function button_create:uint8_t x, uint8_t y, uint8_t color, ptr str
	ptr button
	uint32_t strSize
	strSize=call strlen:str
	button=call malloc:256
	
	if y = POSITION_CENTER
		y = 12
	endif
	
	
	#button=[uint32_t]OBJECT_BUTTON
	#(button+4)=x
	#(button+5)=y
	#(button+6)=color
	! not selected
	#(button+7)=0
	! str size
	#(button+8)=strSize
	call memcpy:(button+12), str, strSize+1
	
	! #(button+12)=str
	return button

endfunction

function button_set_select_state:ptr button, bool selected
	#(button+7)=selected
	call button_draw: button
endfunction

function button_set_text:ptr button, ptr str
	uint32_t strSize
	strSize=call strlen:str
	#(button+8)=strSize
	call memcpy:(button+12), str, strSize+1

endfunction

function button_get_text:ptr button
	return (button+12)
endfunction

function button_draw: ptr button

	uint8_t x=#(button+4)
	uint8_t y=#(button+5)
	uint8_t color=#(button+6)
	uint8_t selected=#(button+7)
	uint32_t strSize=#(button+8)
	ptr str=(button+12)
	
	! selected -> reverse colors
	if selected = true
		color = (color<<4)|(color>>4)
	endif

	if x = POSITION_CENTER
		x = 38-(strSize/2)
	endif
	
	if x = POSITION_RIGHT
		x = 78-strSize
	endif
	
	! draw button
	call coords_color_prints:x, y, color, {"<"}
	call coords_color_prints:x+1, y, color, str
	call coords_color_prints:(x+strSize+1), y, color, {">"}
endfunction
