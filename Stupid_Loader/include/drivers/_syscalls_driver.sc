/*
*******************************************************************************
* Source code writen in ""Sztrojka Code" by LANEZ Ga�l - 2016                 *
* Permission to use, copie and modifie this code.                             *
*******************************************************************************
*/

include "include\library.sc"
include "include\common\syscall_header.sc"

! ******************** KERNEL SYSCALL HANDLER ********************
! Will be automatically called on every syscall interrupt
function syscall_handler: uint32_t regEAX, uint32_t regEBX, uint32_t regECX, uint16_t regES, uint32_t regESI, uint32_t regEDI


	uint16_t regFS
	uint8_t syscall_done = false
	uint32_t returnValue = 0
	ptr tmpP0 = null
	ptr tmpP1 = null
	ptr tmpP2 = null
	uint8_t tmp_uint8
	ax = fs
	regFS = ax
	
	! force malloc to allocate memory after app code. Otherwise pointer with negative value might be requiered to access data
	if regFS >= 0x800 and ((regFS - 0x800) * 0x10) >= MALLOC_DATA_ADDRESS
		malloc_starting_offset =  ((regFS - 0x800) * 0x10) - MALLOC_DATA_ADDRESS
	else
		malloc_starting_offset = 0
		call fatal_exception:EXCEPTION_SYSCALL
	endif
	! convert address (must not be null)
	
	! get bootdrive
	if syscall_done = false and regEAX = SYSCALL_GET_BOOTDRIVE
		returnValue = bootdrive
		syscall_done = true
	endif
	
	!cls
	if syscall_done = false and regEAX = SYSCALL_CLS
		call cls
		syscall_done = true
	endif
	
	! prints
	if syscall_done = false and regEAX = SYSCALL_PRINTS
		regEBX = call convertSegAddress:0x800, regFS, regEBX
		call prints:regEBX
		syscall_done = true
	endif
	
	! printn
	if syscall_done = false and regEAX = SYSCALL_PRINTN
		call printn:DEFAULT_COLOR, regEBX
		syscall_done = true
	endif
	
	! sleep
	if syscall_done = false and regEAX = SYSCALL_SLEEP
		call sleep:regEBX
		syscall_done = true
	endif
	
	! shutdown
	if syscall_done = false and regEAX = SYSCALL_SHUTDOWN
		call bios_shutdown
		syscall_done = true
	endif
	
	! reboot
	if syscall_done = false and regEAX = SYSCALL_REBOOT
		call bios_reboot
		syscall_done = true
	endif
	
	! malloc
	if syscall_done = false and regEAX = SYSCALL_MALLOC
		returnValue = call malloc: regEBX
		returnValue = call convertSegAddress:regFS, 0x800, returnValue
		syscall_done = true
	endif
	
	! read disk raw
	if syscall_done = false and regEAX = SYSCALL_READ_DISK_RAW
		regEBX = call convertSegAddress:0x800, regFS, regEBX
		tmpP0 = call convertSegAddress:0x800, regFS, [ptr]#(regEBX+9)
		returnValue = call read_disk_raw: [uint8_t]#regEBX, [uint32_t]#(regEBX+1), [uint8_t]#(regEBX+5), tmpP0
		syscall_done = true
	endif
	
	! write disk raw
	if syscall_done = false and regEAX = SYSCALL_WRITE_DISK_RAW
		regEBX = call convertSegAddress:0x800, regFS, regEBX
		tmpP0 = call convertSegAddress:0x800, regFS, [ptr]#(regEBX+9)
		returnValue = call write_disk_raw: [uint8_t]#regEBX, [uint32_t]#(regEBX+1), [uint8_t]#(regEBX+5), tmpP0
		syscall_done = true
	endif
	
	! free
	if syscall_done = false and regEAX = SYSCALL_FREE
		regEBX = call convertSegAddress:0x800, regFS, regEBX
		call free: regEBX
		syscall_done = true
	endif
	
	! read file
	if syscall_done = false and regEAX = SYSCALL_FAT12_READ_FILE
		regEBX = call convertSegAddress:0x800, regFS, regEBX
		returnValue = call fat12_read_file: regEBX
		returnValue = call convertSegAddress:regFS, 0x800, returnValue
		syscall_done = true
	endif
	
	! read file size
	if syscall_done = false and regEAX = SYSCALL_FAT12_GET_FILE_SIZE
		regEBX = call convertSegAddress:0x800, regFS, regEBX
		returnValue = call fat12_get_file_size: regEBX
		syscall_done = true
	endif
	
	! write file
	if syscall_done = false and regEAX = SYSCALL_WRITE_FILE
		regEBX = call convertSegAddress:0x800, regFS, regEBX
		tmpP0 = call convertSegAddress:0x800, regFS, [ptr]#(regEBX)
		tmpP1 = call convertSegAddress:0x800, regFS, [ptr]#(regEBX+4)
		returnValue = call fat12_write_file: tmpP0, tmpP1, [uint32_t]#(regEBX+8)
		syscall_done = true
	endif
	
	! create directory
	if syscall_done = false and regEAX = SYSCALL_CREATE_DIRECTORY
		regEBX = call convertSegAddress:0x800, regFS, regEBX
		returnValue = call fat12_create_directory: regEBX
		syscall_done = true
	endif
	
	! file browser
	if syscall_done = false and regEAX = SYSCALL_FILE_BROWSER
		returnValue = call fileBrowser
		returnValue = call convertSegAddress:regFS, 0x800, returnValue
		syscall_done = true
	endif
	
	! exec
	if syscall_done = false and regEAX = SYSCALL_EXEC
		regEBX = call convertSegAddress:0x800, regFS, regEBX
		regECX = call convertSegAddress:0x800, regFS, regECX
		returnValue = call exec: regEBX, regECX
		syscall_done = true
	endif
	
	! pause
	if syscall_done = false and regEAX = SYSCALL_PAUSE
		call pause
		syscall_done = true
	endif
	
	! printc
	if syscall_done = false and regEAX = SYSCALL_PRINTC
		regEBX = call convertSegAddress:0x800, regFS, regEBX
		call coords_printc: [uint8_t]#regEBX, [uint8_t]#(regEBX+1), [uint8_t]#(regEBX+2), [uint8_t]#(regEBX+3)
		syscall_done = true
	endif
	
	
	! window_create
	if syscall_done = false and regEAX = SYSCALL_WINDOW_CREATE
		returnValue = call window_create
		returnValue = call convertSegAddress:regFS, 0x800, returnValue
		syscall_done = true
	endif
	
	! window_free
	if syscall_done = false and regEAX = SYSCALL_WINDOW_FREE
		regEBX = call convertSegAddress:0x800, regFS, regEBX
		returnValue = call window_free: regEBX
		syscall_done = true
	endif
	
	! window_addItem
	if syscall_done = false and regEAX = SYSCALL_WINDOW_ADDITEM
		regEBX = call convertSegAddress:0x800, regFS, regEBX
		tmpP0 = call convertSegAddress:0x800, regFS, [ptr]#(regEBX)
		tmpP1 = call convertSegAddress:0x800, regFS, [ptr]#(regEBX+8)
		returnValue = call window_addItem: tmpP0, [uint32_t]#(regEBX+4), tmpP1
		syscall_done = true
	endif
	
	! window_showItem
	if syscall_done = false and regEAX = SYSCALL_WINDOW_SHOWITEM	
		regEBX = call convertSegAddress:0x800, regFS, regEBX
		tmpP0 = call convertSegAddress:0x800, regFS, [ptr]#(regEBX)
		tmpP1 = call convertSegAddress:0x800, regFS, [ptr]#(regEBX+4)
		returnValue = call window_showItem: tmpP0, tmpP1, [bool]#(regEBX+8)
		syscall_done = true
	endif
	
	! window_exec
	if syscall_done = false and regEAX = SYSCALL_WINDOW_EXEC
		regEBX = call convertSegAddress:0x800, regFS, regEBX
		returnValue = call window_exec: regEBX
		returnValue = call convertSegAddress:regFS, 0x800, returnValue
		syscall_done = true
	endif
	
	! progressBar_create
	if syscall_done = false and regEAX = SYSCALL_PROGRESSBAR_CREATE
		regEBX = call convertSegAddress:0x800, regFS, regEBX
		returnValue = call progressBar_create: #(regEBX), #(regEBX+1), #(regEBX+2), #(regEBX+3), #(regEBX+4)
		returnValue = call convertSegAddress:regFS, 0x800, returnValue
		syscall_done = true
	endif
	
	! progressBar_setValue
		if syscall_done = false and regEAX = SYSCALL_PROGRESSBAR_SET_VALUE
		regEBX = call convertSegAddress:0x800, regFS, regEBX
		returnValue = call progressBar_setValue: regEBX, regECX
		syscall_done = true
	endif
	
	! label_create
	if syscall_done = false and regEAX = SYSCALL_LABEL_CREATE
		regEBX = call convertSegAddress:0x800, regFS, regEBX
		tmpP0 = call convertSegAddress:0x800, regFS, [uint32_t]#(regEBX+3)
		returnValue = call label_create: #regEBX, #(regEBX+1), #(regEBX+2), tmpP0
		returnValue = call convertSegAddress:regFS, 0x800, returnValue
		syscall_done = true
	endif
	
	! label_set_text
	if syscall_done = false and regEAX = SYSCALL_LABEL_SET_TEXT
		regEBX = call convertSegAddress:0x800, regFS, regEBX
		regECX = call convertSegAddress:0x800, regFS, regECX
		returnValue = call label_set_text: regEBX, regECX
		syscall_done = true
	endif
	
	! label_add_text
	if syscall_done = false and regEAX = SYSCALL_LABEL_ADD_TEXT
		regEBX = call convertSegAddress:0x800, regFS, regEBX
		regECX = call convertSegAddress:0x800, regFS, regECX
		returnValue = call label_add_text: regEBX, regECX
		syscall_done = true
	endif
	
	! label_add_number
	if syscall_done = false and regEAX = SYSCALL_LABEL_ADD_NUMBER
		regEBX = call convertSegAddress:0x800, regFS, regEBX
		returnValue = call label_add_number: regEBX, regECX
		syscall_done = true
	endif
	
	! button_create
	if syscall_done = false and regEAX = SYSCALL_BUTTON_CREATE
		regEBX = call convertSegAddress:0x800, regFS, regEBX
		tmpP0 = call convertSegAddress:0x800, regFS, [uint32_t]#(regEBX+3)
		returnValue = call button_create: #regEBX, #(regEBX+1), #(regEBX+2), tmpP0
		returnValue = call convertSegAddress:regFS, 0x800, returnValue
		syscall_done = true
	endif
	
	! button_set_text
	if syscall_done = false and regEAX = SYSCALL_BUTTON_SET_TEXT
		regEBX = call convertSegAddress:0x800, regFS, regEBX
		regECX = call convertSegAddress:0x800, regFS, regECX
		returnValue = call button_set_text: regEBX, regECX
		syscall_done = true
	endif
	
	! button_get_text
	if syscall_done = false and regEAX = SYSCALL_BUTTON_GET_TEXT
		regEBX = call convertSegAddress:0x800, regFS, regEBX
		returnValue = call button_get_text: regEBX
		returnValue = call convertSegAddress:regFS, 0x800, returnValue
		syscall_done = true
	endif
	
	! textbox_create
	if syscall_done = false and regEAX = SYSCALL_TEXTBOX_CREATE
		regEBX = call convertSegAddress:0x800, regFS, regEBX
		tmpP0 = call convertSegAddress:0x800, regFS, [uint32_t]#(regEBX+4)
		returnValue = call textbox_create: #regEBX, #(regEBX+1), #(regEBX+2), #(regEBX+3), tmpP0
		returnValue = call convertSegAddress:regFS, 0x800, returnValue
		syscall_done = true
	endif
	
	! textbox_set_text
	if syscall_done = false and regEAX = SYSCALL_TEXTBOX_SET_TEXT
		regEBX = call convertSegAddress:0x800, regFS, regEBX
		regECX = call convertSegAddress:0x800, regFS, regECX
		returnValue = call textbox_set_text: regEBX, regECX
		syscall_done = true
	endif
	
	! textbox_get_text
	if syscall_done = false and regEAX = SYSCALL_TEXTBOX_GET_TEXT
		regEBX = call convertSegAddress:0x800, regFS, regEBX
		returnValue = call textbox_get_text: regEBX
		returnValue = call convertSegAddress:regFS, 0x800, returnValue
		syscall_done = true
	endif
	
	! textbox_set_type
	if syscall_done = false and regEAX = SYSCALL_TEXTBOX_SET_TYPE
		regEBX = call convertSegAddress:0x800, regFS, regEBX
		returnValue = call textbox_set_type: regEBX, regECX
		syscall_done = true
	endif
	
	! convert_ptr
	if syscall_done = false and regEAX = SYSCALL_CONVERT_PTR
		returnValue = call convertSegAddress:0x800, regFS, regEBX
		syscall_done = true
	endif
	
	! shutdown_menu
	if syscall_done = false and regEAX = SYSCALL_SHUTDOWN_MENU
		call shutdown_menu
		syscall_done = true
	endif
	
	! get_drive_info
	if syscall_done = false and regEAX = SYSCALL_GET_DRIVE_INFO
		regEBX = call convertSegAddress:0x800, regFS, regEBX
		tmp_uint8 = call get_drive_from_name:regEBX
		returnValue = call get_drive_info: tmp_uint8
		returnValue = call convertSegAddress:regFS, 0x800, returnValue
		syscall_done = true
	endif
	
	! get arg
	if syscall_done = false and regEAX = SYSCALL_GET_ARG
		regEBX = call convertSegAddress:0x800, regFS, regEBX
		returnValue = call get_arg: regEBX, regECX
		returnValue = call convertSegAddress:regFS, 0x800, returnValue
		syscall_done = true
	endif
	
	! read key
	if syscall_done = false and regEAX = SYSCALL_WAIT_KEY
		returnValue = call bios_wait_key
		syscall_done = true
	endif
	
	! read key ascii
	if syscall_done = false and regEAX = SYSCALL_WAIT_KEY_ASCII
		returnValue = call bios_wait_key_ascii
		syscall_done = true
	endif
	
	! read key code ascii
	if syscall_done = false and regEAX = SYSCALL_WAIT_KEY_CODE_ASCII
		regECX = call convertSegAddress:0x800, regFS, regECX
		regEBX = call convertSegAddress:0x800, regFS, regEBX
		call bios_read_key_code_ascii: regEBX, regECX
		syscall_done = true
	endif
	
	! read key code ascii
	if syscall_done = false and regEAX = SYSCALL_GET_DRIVE_FROM_NAME
		regEBX = call convertSegAddress:0x800, regFS, regEBX
		returnValue = call get_drive_from_name: regEBX
		syscall_done = true
	endif
	
	! read key code ascii
	if syscall_done = false and regEAX = SYSCALL_STOP_OUTPUT
		stop_output=true
		syscall_done = true
	endif
	
	! dump registers
	if syscall_done = false and regEAX = 0xff
		!call cls
		call prints:{"REGDUMP:\nEAX="}
		call printn:DEFAULT_COLOR, regEAX
		call prints:{"\nEBX="}
		call printn:DEFAULT_COLOR, regEBX
		call prints:{"\nECX="}
		call printn:DEFAULT_COLOR, regECX
		call prints:{"\nES="}
		call printn:DEFAULT_COLOR, regES
		call prints:{"\nESI="}
		call printn:DEFAULT_COLOR, regESI
		call prints:{"\nEDI="}
		call printn:DEFAULT_COLOR, regEDI
		!call pause
		returnValue = 0
		syscall_done = true
	endif
	
	malloc_starting_offset = 0
	
	if syscall_done = false
		call prints:{"\nSyscall interrupt called with unknown parameter.\n"}
		call pause
		call fatal_exception:EXCEPTION_SYSCALL
	endif
	
	return returnValue
endfunction

