/*
*******************************************************************************
* Source code writen in "Sztrojka Code" by LANEZ Ga�l - 2015                  *
* Permission to use, copie and modifie this code.                             *
*******************************************************************************
*/

include "include\jumble.sc"
include "include\drivers\bios_driver.sc"
include "include\screen.sc"

enum EXCEPTION_UNKNOWN, EXCEPTION_MEMORY, EXCEPTION_PTR, EXCEPTION_STACK, EXCEPTION_OBJECT, EXCEPTION_MALLOC_SIZE_0, \
	EXCEPTION_SYSCALL, EXCEPTION_NOT_INITIALISED

function fatal_exception: uint32_t error_code
	call raw_cls
	call prints:{"\nUne erreur est survenue, le systeme va redemarrer.\nCode d'erreur: "}
	call printn:COLOR_RED, error_code
	call prints:{" ( "}
	call print_fatal_exception_error_str:error_code
	call prints:{" )\n"}
	call pause
	call bios_reboot
	loop

endfunction

function print_fatal_exception_error_str: uint32_t error_code
	if error_code=EXCEPTION_MEMORY
		call prints:{"Plus de memoire disponible"}
	endif
	if error_code=EXCEPTION_PTR
		call prints:{"Pointeur corrompu"}
	endif
	if error_code=EXCEPTION_STACK
		call prints:{"Kernel corrompu par la pile"}
	endif
	if error_code=EXCEPTION_OBJECT
		call prints:{"Erreur d'objet"}
	endif
	
	if error_code=EXCEPTION_MALLOC_SIZE_0
		call prints:{"Allocation de 0 octet de memoire"}
	endif
	
	if error_code=EXCEPTION_SYSCALL
		call prints:{"Erreur de syscall"}
	endif
	
	if error_code=EXCEPTION_NOT_INITIALISED
		call prints:{"Fonctionnalite non initialise"}
	endif
	
endfunction
