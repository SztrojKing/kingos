/*
*******************************************************************************
* Source code writen in "Sztrojka Code" by LANEZ Ga�l - 2015                  *
* Permission to use, copie and modifie this code.                             *
*******************************************************************************
*/
include "include\common\colors.sc"
include "include\common\gui.sc"
include "include\common\colors.sc"
include "include\common\disks.sc"
include "include\io.sc"
include "include\asm_def.sc"
include "include\jumble.sc"
include "include\string.sc"
include "include\mem.sc"
include "include\malloc.sc"
include "include\screen.sc"
!include "include\apps.sc"
include "include\system_exceptions.sc"
include "include\gui\window.sc"
include "include\keyboard.sc"
!include "include\interrupts.sc"
include "include\time.sc"

include "include\drivers\bios_driver.sc"
include "include\drivers\disk_driver.sc"
include "include\drivers\fat12_driver.sc"