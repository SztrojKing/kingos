/*
*******************************************************************************
* Source code writen in "Sztrojka Code" by LANEZ Ga�l - 2015                  *
* Permission to use, copie and modifie this code.                             *
*******************************************************************************
*/

include "include\library.sc"
include "include\common\syscall_header.sc"

define EXEC_STACK_SIZE 4096

function exec:ptr path, ptr args
	uint32_t res=0
	uint32_t file_size=0
	uint16_t file_size_16=0
	uint32_t args_size=0
	uint32_t old_malloc_starting_offset = malloc_starting_offset
	uint8_t args_offset
	ptr new_args=null
	args_size = call strlen: args
	
	if path =/= null and isBiosHooked = true
		uint16_t bufferSegment = 0
		uint16_t regDS = 0
		uint32_t regEAX = 0
		
		
		ptr tempFileBuffer
		tempFileBuffer = call fat12_read_file:path
		ptr myBuffer
		
		if tempFileBuffer = null
			call cls
			call prints:{"Erreur de lecture"}
			call pause
			return 0
		endif
		
		
		file_size = call fat12_get_file_size:path
		
		file_size = (file_size + EXEC_STACK_SIZE)
		!file_size=0xfff0
		!file_size = 0xfff0
		if file_size > 0xfffe
			call cls
			call prints:{"EXEC: Fichier trop grand."}
			call pause
			call free:myBuffer
			return 0
		endif
		
		! allocate max space
		myBuffer = call malloc:file_size+4
		! check if buffer is aligned with 0x10 (malloc should've done the job)
		if myBuffer%0x10 =/= 0
			call cls
			call prints:{"EXEC: Memoire non alignee."}
			call pause
			call free:myBuffer
			return 0
		endif
		
		call memcpy:myBuffer, tempFileBuffer, file_size-EXEC_STACK_SIZE
		call free:tempFileBuffer
		
		if [uint8_t]#myBuffer = 0xeb and [uint32_t]#(myBuffer+2) = APPLICATION_MAGICAL_NUMBER
			! small app
			!call prints:{"Application valide (small)\n"}
			!call pause
			!call sleep:1000
			args_offset=2
		else
			if [uint8_t]#myBuffer = 0xe9 and [uint32_t]#(myBuffer+3) = APPLICATION_MAGICAL_NUMBER
				! big app
				!call prints:{"Application valide (big)\n"}
				!call pause
				!call sleep:1000
				args_offset=3
			else
				! not app
				call cls
				call prints:{"Application non valide.\n"}
				call pause
				call free:myBuffer
				return 0
			endif
		endif
		
		!call cls


		! we have to set ds, ss and cs with the right segment and set offset to 0
		ax = ds
		regDS = ax
		bufferSegment = (myBuffer/0x10) + regDS
		if bufferSegment = 2048	
			call prints:{"SS invalide\n"}
			call pause
		endif
		
			
		! copy args to where the app can read
		malloc_starting_offset =  ((bufferSegment - 0x800) * 0x10) - MALLOC_DATA_ADDRESS
		new_args = call malloc: args_size+1
		call strcpy:new_args, args
		args = call convertSegAddress: bufferSegment,0x800, new_args
		#(myBuffer+args_offset) = args
		malloc_starting_offset = old_malloc_starting_offset
		
		! debug informations
		/*
		call prints:{"file_size="}
		call printn:COLOR_YELLOW, file_size
		
		call prints:{"\nmyBuffer="}
		call printn:COLOR_YELLOW, myBuffer
		
		call prints:{"\nbufferSegment="}
		call printn:COLOR_YELLOW, bufferSegment
		
		call prints:{"\nds="}
		call printn:COLOR_YELLOW, regDS
		call prints:{"\n"}
		call pause
		call prints:{"Jumping..\n"}
		*/
		file_size_16 = [uint16_t](file_size)
		
		! save stack address (16 bits) in high part of edx reg
		dx = file_size_16
		asm rol edx, 16
		
		! save OS segments
		dx = ds
		
		! set application segments
		ax = bufferSegment
		ds = ax
		es = ax
		fs = ax
		gs = ax

		! save OS stack
		bx = ss
		cx = bp
		asm rol ecx, 16
		cx = sp

		! set application stack
		ss = ax

			! swap parts of registers
			asm rol edx, 16
			! dx is now equal to file_size wich was in its higher part
			
		! set stack top address with file_size value
		bp = dx
		!asm push esp
		sp = dx
		

			! restore registers value
			! rollback edx
			asm rol edx, 16
		
		! save OS segments in application stack
		!push_register bx /* push ss */
		!push_register ecx /* push ebp */
		!push_register dx /* push ds */
		asm push ebx
		asm push ecx
		asm push edx
		
		
		! push cs:ip so application will be able to return to kernel
		asm push cs
		asm push word .returnOffset
		
		! jump into cs:ip
		asm push ax
		asm push word 0
		asm retf
		
		! application return point
		asm .returnOffset:
		asm pop edx
		asm pop ecx
		asm pop ebx
		! retreive OS segments
		!pop_register dx
		!pop_register ecx
		!pop_register bx
		
		!asm pop esp
		! restore OS stack
		!ebp = ecx
		!ss = bx

		! restore OS segments
		sp = cx
		asm rol ecx, 16
		bp = cx
		ss = bx
		!loop
		!loop
		dx = 0x800
		ds = dx
		es = dx
		fs = dx
		gs = dx
		
		! get return value
		regEAX = eax
		
		! free args
		call free: new_args
		!call cls
		/*
		call prints:{"Fin de l'application '"}
		call prints:path
		call prints:{"' avec la valeur "}
		call printn:DEFAULT_COLOR, regEAX
		call prints:{".\n"}
		call pause
		*/
		res = regEAX
		call free:myBuffer
	endif
	return res
endfunction


/*
	example:
		0x900:0x1
		0x800:0x1001

*/
function convertSegAddress: uint16_t destReg, uint16_t srcReg, uint32_t address
	if address =/= null
		return address + ((srcReg - destReg) * 0x10)
	endif
	return null
endfunction

function get_arg:ptr cmd, uint32_t n
	uint32_t len
	uint32_t count=0
	uint32_t i=0
	uint32_t j=0
	ptr res=null
	len = call strlen:cmd
	res = call malloc:len+1
	while count < n
		if #[uint8_t](cmd+i) = '|'
			count++
		else
			! n is out of range
			if #[uint8_t](cmd+i) = 0
				return null
			endif
		endif
		i++
	endwhile
	
	while #[uint8_t](cmd+i+j) =/= '|'
		#(res+j) = #[uint8_t](cmd+i+j)
		if #[uint8_t](cmd+i+j) = 0
			return res
		endif
		j++
	endwhile
	return res
endfunction