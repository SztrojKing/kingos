/*
*******************************************************************************
* Source code writen in "Sztrojka Code by LANEZ Ga�l" - 2015                  *
* Permission to use, copie and modifie this code.                             *
*******************************************************************************
*/

! Basic IO operation


function memcpy_16_to_32:ptr dest, uint16_t src_seg, uint16_t src_off, uint32_t size
	uint8_t b
	asm push es
	ax = src_seg
	es = ax
	while size > 0
		bx = src_off
		asm mov cl, [es:bx]
		b = cl
		#dest = b
		size--
		dest++
		src_off++
	endwhile
	asm pop es
endfunction

function memcpy_32_to_16:uint16_t dest_seg, uint16_t dest_off, ptr src, uint32_t size

	uint8_t buf
	asm push es
	ax = dest_seg
	es = ax
	while size > 0
		
		buf = #src
		cl = buf
		bx = dest_off
		asm mov [es:bx], cl
		size--
		dest_off++
		src++
	endwhile
	asm pop es
endfunction

function memcpy:ptr dest, ptr src, uint32_t size
	uint32_t prog=0
	while prog<size
		#dest = [uint8_t]#src
		prog++
		src++
		dest++
	endwhile
endfunction

function memcpy_from_end: ptr dest, ptr src, uint32_t size
		while size > 0
		#(dest+size-1) = [uint8_t]#(src+size-1)
		size--
	endwhile
endfunction

function memcmp:ptr src1, ptr src2, uint32_t size
	
	while size > 0
		if #src1[uint8_t] =/= #src2[uint8_t]
			return 1
		endif
		src1++
		src2++
		size--
	endwhile
	return 0
endfunction

function memset:ptr dest, uint8_t value, uint32_t length
	uint32_t prog=0
	while prog<length
		#dest=[uint8_t]value
		dest++
		prog++
	endwhile
endfunction

function memset32:ptr dest, uint32_t value, uint32_t length
	uint32_t prog=0
	while prog<length
		#dest=[uint32_t]value
		dest = dest + 4
		prog++
	endwhile
endfunction
function setBit:uint8_t var, uint8_t offset, bool bit_state
	if bit_state=0
		var = var & ~(1<<offset)
	else
		var = var | (1<<offset)
	endif
	return var
endfunction

function checkBit: uint8_t var, uint8_t offset
	return ((var) & (1<<(offset)))
endfunction
! reverse buffer
! "abc" => "cba"
function memreverse: ptr array, uint32_t length
	uint32_t offset=0
	uint32_t max_progress=length / 2
	uint8_t tmp=0
	length--
	while offset<max_progress
		tmp = #(array+offset)
		#(array+offset) = [uint8_t]#(array+(length-offset))
		#(array+(length-offset)) = tmp
		offset++
	endwhile
endfunction