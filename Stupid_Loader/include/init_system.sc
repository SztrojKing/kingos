/*
*******************************************************************************
* Source code writen in ""Sztrojka Code" by LANEZ Ga�l - 2015                 *
* Permission to use, copie and modifie this code.                             *
*******************************************************************************
*/
function init_system



endfunction

/*
function extendMemory
asm
asm Set4Gig:
asm mov eax, Mem32_GDT
asm mov [GDTtable_asb_addr], eax

asm mov eax, GDTptr
asm sub eax, 1
asm sub eax, Mem32_GDT
asm mov [GDTptr], eax
asm 
asm
asm         mov     eax,cr0         ; check for V86 mode
asm         ror     eax,1
asm         jc      leave4gb        ; exit routine if V86 mode is set.
asm 
asm         mov     ax,cs           ; set up GDT for this code segment
asm         mov     ds,ax
asm         movzx   eax,ax
asm         shl     eax,4
asm         add     [ds:GDTptr+2],eax
asm         lgdt    [ds:GDTptr]
asm 
asm         mov     ax,cs
asm         and     eax,65535
asm         shl     eax,4

! asm         mov     word ptr ds:Mem32_GDT[Code16GDT+2],ax
asm mov ebx, Mem32_GDT
asm add ebx, [Code16GDT]
asm add ebx, 2
asm mov [ebx], ax

!asm         mov     word ptr ds:Mem32_GDT[Data16GDT+2],ax
asm mov ebx, Mem32_GDT
asm add ebx, [Data16GDT]
asm add ebx, 2
asm mov [ebx], ax

asm         ror     eax,16

!asm         mov     byte ptr ds:Mem32_GDT[Code16GDT+4],al
asm mov ebx, Mem32_GDT
asm add ebx, [Code16GDT]
asm add ebx, 4
asm mov [ebx], al

!asm         mov     byte ptr ds:Mem32_GDT[Data16GDT+4],al
asm mov ebx, Mem32_GDT
asm add ebx, [Data16GDT]
asm add ebx, 4
asm mov [ebx], al

! asm         mov     byte ptr ds:Mem32_GDT[Code16GDT+7],ah
asm mov ebx, Mem32_GDT
asm add ebx, [Code16GDT]
asm add ebx, 7
asm mov [ebx], ah

!asm         mov     byte ptr ds:Mem32_GDT[Data16GDT+7],ah
asm mov ebx, Mem32_GDT
asm add ebx, [Data16GDT]
asm add ebx, 7
asm mov [ebx], ah
asm 

asm         cli                     ; no interrupts
asm 
asm         mov     eax,cr0         ; set protected mode
asm         or      al,1
asm         mov     cr0,eax
asm 

!asm         db      0eah            ; far jump to pmode label
!asm         dw      offset pmode
!asm         dw      Code16GDT
asm jmp 08:pmode
asm 
asm pmode:  mov     ax,[Data32GDT]    ; now we are in protected mode
asm         mov     ds,ax           ; set all selector limits to 4 GB
asm         mov     es,ax
asm         mov     fs,ax
asm         mov     gs,ax
asm 

asm         mov     eax,cr0         ; restore real mode
asm         and     al,0feh
asm         mov     cr0,eax
asm 
!asm         db      0eah            ; far jump to rmode label
!asm         dw      offset rmode
!asm         dw      Code
asm jmp 08:rmode
asm 
asm rmode:  clc                     ; now we are back in real mode, zero carry
asm         sti                     ; to indicate ok and enable interrupts
asm                 
asm leave4gb:
asm         jmp extendMemory_end
asm 		
asm Code16GDT: dw 8
asm Data16GDT: dw 16
asm Data32GDT: dw 24
asm 
asm Mem32_GDT:
asm			dw      0,0,0,0
asm 
asm         dw      0ffffh,0ffh,9a00h,0
asm         dw      0ffffh,0ffh,9200h,0
asm         dw      0ffffh,0ffh,9200h,8fh
asm 
asm GDTptr:
asm         dw      0 ;GDTptr-1- Mem32_GDT
asm GDTtable_asb_addr:        dd      0 ; =Mem32_GDT Absolute adress GDTtable
asm         dw      0

asm extendMemory_end:
endfunction
*/