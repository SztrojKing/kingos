/*
*******************************************************************************
* Source code writen in "Sztrojka Code" by LANEZ Ga�l - 2016                  *
* Permission to use, copie and modifie this code.                             *
*******************************************************************************
*/

! enable a20 gate
! return 1 on succes, 0 on error
function init_a20
	uint32_t loops=0
	call bios_move_cursor:0, 24
	call bios_write_character:'A', DEFAULT_COLOR
	call bios_move_cursor:1, 24
	call bios_write_character:'2', DEFAULT_COLOR
	call bios_move_cursor:2, 24
	call bios_write_character:'0', DEFAULT_COLOR
	call bios_move_cursor:4, 24
	! try 255 times
	while loops<255
		if call check_a20 = 1
			return 1
		endif
		
		call enable_a20_bios
		
		if call check_a20 = 1
			return 1
		endif
		
		call enable_a20_keyboard
		
		if call check_a20 = 1
			return 1
		endif
		
		call enable_a20_fast
		
		if call check_a20 = 1
			return 1
		endif
		loops++
	endwhile
	call bios_write_character:'E', COLOR_RED
	call bios_move_cursor:5, 24
	call bios_write_character:'R', COLOR_RED
	call bios_move_cursor:6, 24
	call bios_write_character:'R', COLOR_RED
	call bios_move_cursor:7, 24
	call bios_write_character:'O', COLOR_RED
	call bios_move_cursor:8, 24
	call bios_write_character:'R', COLOR_RED
	call pause
	return 0
endfunction


function enable_a20_bios
	asm	mov ax, 0x2401
	asm int 0x15
endfunction

function enable_a20_keyboard
		asm cli
        call    a20wait
        asm mov     al,0xAD
        asm out     0x64,al
 
        call    a20wait
        asm mov     al,0xD0
        asm out     0x64,al
 
        call    a20wait2
        asm  in      al,0x60
        asm  push    eax
 
        call    a20wait
        asm mov     al,0xD1
        asm out     0x64,al
 
        call    a20wait
        asm pop     eax
        asm or      al,2
        asm out     0x60,al
 
        call    a20wait
        asm mov     al,0xAE
        asm out     0x64,al
 
        call    a20wait
        asm sti
endfunction
 
function a20wait
		asm .begin:
        asm in      al,0x64
        asm test    al,2
        asm jnz     .begin
endfunction
 
function a20wait2
		asm .begin:
        asm in      al,0x64
        asm test    al,1
        asm jz      .begin
endfunction

function enable_a20_fast
	
	asm in al, 0x92
	asm test al, 2
	asm jnz .after
	asm or al, 2
	asm and al, 0xFE
	asm out 0x92, al
	asm .after:
endfunction

function check_a20
asm ; The following code is public domain licensed
asm  
asm  
asm ; Function: check_a20
asm ;
asm ; Purpose: to check the status of the a20 line in a completely self-contained state-preserving way.
asm ;          The function can be modified as necessary by removing push's at the beginning and their
asm ;          respective pop's at the end if complete self-containment is not required.
asm ;
asm ; Returns: 0 in ax if the a20 line is disabled (memory wraps around)
asm ;          1 in ax if the a20 line is enabled (memory does not wrap around)
asm  

asm     pushf
asm     push ds
asm     push es
asm     push di
asm     push si
asm  
asm     cli
asm  
asm     xor ax, ax ; ax = 0
asm     mov es, ax
asm  
asm     not ax ; ax = 0xFFFF
asm     mov ds, ax
asm  
asm     mov di, 0x0500
asm     mov si, 0x0510
asm  
asm     mov al, byte [es:di]
asm     push ax
asm  
asm     mov al, byte [ds:si]
asm     push ax
asm  
asm     mov byte [es:di], 0x00
asm     mov byte [ds:si], 0xFF
asm  
asm     cmp byte [es:di], 0xFF
asm  
asm     pop ax
asm     mov byte [ds:si], al
asm  
asm     pop ax
asm     mov byte [es:di], al
asm  
asm     mov ax, 0
asm     je check_a20__exit
asm  
asm     mov ax, 1
asm  
asm check_a20__exit:
asm     pop si
asm     pop di
asm     pop es
asm     pop ds
asm     popf
asm sti
	return ax
 
endfunction
		