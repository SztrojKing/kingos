define u32 uint32_t

function memdet
	ptr entries_buffer=null
	u32 entries_buffer_tmp=0
	u32 memtype=0
	asm xor eax, eax
	asm mov ax, mem_list
	entries_buffer_tmp =  eax
	entries_buffer = entries_buffer_tmp
	
	
	u32 entries_found=0
	entries_found = call get_memory_map
	u32 i=0
	u32 max_free_base=0
	u32 max_free_length=0
	
	u32 current_base=0
	u32 current_length=0
	

	if entries_found>0
		while i<entries_found
			current_base = [u32]#(entries_buffer+(24*i))
			current_length = [u32]#(entries_buffer+(24*i)+8)
			memtype = [u32]#(entries_buffer+(24*i)+16)
			call prints:{"\nbase: "}
			call printn:DEFAULT_COLOR, current_base
			call prints:{" | length: "}
			call printn:DEFAULT_COLOR, current_length
			call prints:{" | type: "}
			call printn:DEFAULT_COLOR, memtype
			
			if memtype = 1
				call prints:{" (Free Memory)"}
			else
				if memtype = 2
					call prints:{" (Reserved Memory)"}
				else
					if memtype = 3
						call prints:{" (ACPI reclaimable memory)"}
					else
						if memtype = 4
							call prints:{" (ACPI NVS memory)"}
						else
							call prints:{" (Area containing bad memory)"}
						endif
					endif
				endif
			endif
			
			if memtype = 1 ! free memory
				! 64 bit memory is should not be detected
				if [u32]#(entries_buffer+(24*i)+4) = 0
					if current_base<=0x1200000
						if current_length>max_free_length
							if current_base>max_free_base
								max_free_base=current_base
								max_free_length=current_length
							endif
						endif
					endif
				else
					call prints:{" [64 bit]"}
				endif 
			endif
			i++
		endwhile
		call prints:{"\n"}
		call printn:DEFAULT_COLOR,  entries_found
		call prints:{" entrees trouves\n"}
		
		if max_free_length>0
			call prints:{"Base selectionne: "}
			call printn:DEFAULT_COLOR, max_free_base/1024/1024
			call prints:{" mb\nTaille: "}
			call printn:DEFAULT_COLOR, max_free_length/1024
			call prints:{" ko\n"}
			return (max_free_base+max_free_length)
		else
			return 1
		endif
	endif
	call printn:DEFAULT_COLOR,  entries_found
	call prints:{" entrees trouves\n"}
	return 0
endfunction


! return the number of entries found
function get_memory_map
	u32 entries_count=0
	asm mov ax, ds
	asm mov es, ax
	asm mov di, mem_list
	asm ; use the INT 0x15, eax= 0xE820 BIOS function to get a memory map
	asm ; inputs: es:di -> destination buffer for 24 byte entries
	asm ; outputs: bp = entry count, trashes all registers except esi
	asm do_e820:
	asm 	xor ebx, ebx		; ebx must be 0 to start
	!asm 	xor bp, bp		; keep an entry count in bp
	asm 	mov edx, 0x0534D4150	; Place "SMAP" into edx
	asm 	mov eax, 0xe820
	asm 	mov [es:di + 20], dword 1	; force a valid ACPI 3.X entry
	asm 	mov ecx, 24		; ask for 24 bytes
	asm 	int 0x15
	asm 	jc short .failed	; carry set on first call means "unsupported function"
	asm 	mov edx, 0x0534D4150	; Some BIOSes apparently trash this register?
	asm 	cmp eax, edx		; on success, eax must have been reset to "SMAP"
	asm 	jne short .failed
	asm 	test ebx, ebx		; ebx = 0 implies list is only 1 entry long (worthless)
	asm 	je short .failed
	asm 	jmp short .jmpin
	asm .e820lp:
	asm 	mov eax, 0xe820		; eax, ecx get trashed on every int 0x15 call
	asm 	mov [es:di + 20], dword 1	; force a valid ACPI 3.X entry
	asm 	mov ecx, 24		; ask for 24 bytes again
	asm 	int 0x15
	asm 	jc short .e820f		; carry set means "end of list already reached"
	asm 	mov edx, 0x0534D4150	; repair potentially trashed register
	asm .jmpin:
	asm 	jcxz .skipent		; skip any 0 length entries
	asm 	cmp cl, 20		; got a 24 byte ACPI 3.X response?
	asm 	jbe short .notext
	asm 	test byte [es:di + 20], 1	; if so: is the "ignore this data" bit clear?
	asm 	je short .skipent
	asm .notext:
	asm 	mov ecx, [es:di + 8]	; get lower uint32_t of memory region length
	asm 	or ecx, [es:di + 12]	; "or" it with upper uint32_t to test for zero
	asm 	jz .skipent		; if length uint64_t is 0, skip entry
	!asm 	inc bp			; got a good entry: ++count, move to next storage spot
			entries_count++
	asm 	add di, 24
	asm .skipent:
	asm 	test ebx, ebx		; if ebx resets to 0, list is complete
	asm 	jne short .e820lp
	asm .e820f:
	!asm 	mov [mmap_ent], bp	; store the entry count
!	asm 	xor eax, eax
!	asm 	mov ax, bp
	asm 	clc			; there is "jc" on end of list to this point, so the carry must be cleared
			return entries_count
	asm .failed:
	asm 	stc			; "function unsupported" error exit
			return 0
endfunction

asm mem_list: resb 128*24