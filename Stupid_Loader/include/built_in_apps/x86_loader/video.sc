
suint32_t videoMode=0

function video
	uint32_t i=0
	uint16_t tmp=0
	uint16_t tmp2=0
	uint16_t decallage=0
	uint32_t mode=0
	ptr video_address=null
	ptr physical_video_address=null
	ptr modeInfoAddress=null
	ptr in_process_modeInfoAddress=null
	ptr videoStruct
	uint16_t res_x=0
	uint16_t res_y=0
	uint16_t bpp=0

	! try to read user cfg
	videoStruct = call fat12_read_file:{"root0/system/x16/video/video.cfg"}
	
	! if it failed, prompt user
	if videoStruct = null
		videoStruct = call chooseGraphicModes32
		if videoStruct = null
			call cls
			call prints:{"Video mode error\n"}
			call pause
			return 0
		endif

		! save choice for future boots
		call fat12_write_file:{"root0/system/x16/video/video.cfg"}, videoStruct, 6
	endif
	
	res_x = #(videoStruct)
	res_y = #(videoStruct+2)
	bpp = #(videoStruct+4)
	call free:videoStruct
	!call pause
	!call prints:{"Switching video mode"}
	!call pause
	! jump to special video mode
	physical_video_address = call switch_to_video_res:res_x, res_y, bpp
	
	call sleep:1000
	call bios_set_text_mode
	
	if physical_video_address = 0

		call cls
		call prints:{"physical_video_address = 0\n"}
		call pause
		return 0
	endif
	videoStruct = call malloc:14
	#(videoStruct) = res_x
	#(videoStruct+2) = res_y
	#(videoStruct+4) = bpp
	#(videoStruct+6) = [uint32_t](physical_video_address)
	#(videoStruct+10) = [uint32_t](videoMode)
	! jump back to text mode
	ah = 0x00
	al = 0x03
	_int 0x10
	!call cls
	!call prints:{"Address = "}
	!call printn:video_address
	!call pause
	
	! return a copy of real modeInfo
	! ******* hard but good code, don't modify it *******
	!asm mov ax, _modeinfo
	!tmp = ax
	!asm mov ax, ds
	!decallage  = ax
	
	!in_process_modeInfoAddress = [ptr]tmp
	!modeInfoAddress = call malloc:256
	!call memcpy:modeInfoAddress, in_process_modeInfoAddress, 256
	
	! return the physical address
	!return (modeInfoAddress - (decallage*0x10))
	!asm mov ax, ds
	!decallage  = ax
	return videoStruct
endfunction



function test_video: ptr video
	uint32_t color = 0x00ffffff
	uint32_t r=0
	uint32_t g=0
	uint32_t b = 0
	while color > 0x0000000
		call clear_screen:video, color
		color = color - 0x00111111
	endwhile
	while r < 0xfe
		r = r + 2
		call clear_screen:video, [uint32_t]((r<<16) | (g<<8) | b)
	endwhile
	while g < 0xfe
		g = g + 2
		call clear_screen:video, [uint32_t]((r<<16) | (g<<8) | b)
	endwhile
	
	while r > 2
		r = r - 2
		call clear_screen:video, [uint32_t]((r<<16) | (g<<8) | b)
	endwhile
	
	while b < 0xfe
		b = b + 2
		call clear_screen:video, [uint32_t]((r<<16) | (g<<8) | b)
	endwhile
	
	while g > 2
		g = g - 2
		call clear_screen:video, [uint32_t]((r<<16) | (g<<8) | b)
	endwhile
	
endfunction

function get_real_video_address: ptr video
	uint16_t decallage=0
	asm mov ax, ds
	decallage  = ax
	return video-(decallage*0x10)
endfunction

function clear_screen: ptr video, uint32_t color

	call memset32:video, color, 1024*768
endfunction

asm _findGraphicMode:
asm    ;; The VESA 2.0 specification states that they will no longer
asm    ;; create standard video mode numbers, and video card vendors are
asm    ;; no longer required to support the old numbers.  This routine
asm    ;; will dynamically find a supported mode number from the VIDEO BIOS,
asm    ;; according to the desired resolutions, etc.
asm    
asm    ;; The function takes parameters that describe the desired graphic
asm    ;; mode, (X resolution, Y resolution, and Bits Per Pixel) and returns
asm    ; the VESA mode number in EAX, if found.  Returns 0 on error.
asm    
asm    ;; The C prototype for this function would look like the following:
asm    ;; int findGraphicMode(short int x_res, short int y_res, 
asm    ;;                     short int bpp);
asm    ;; (Push bpp, then y_res, then x_res onto the 16-bit stack before
asm    ;; calling.  Caller pops them off again after the call.)
asm 
asm    ;; Save space on the stack for the mode number we're returning
asm    sub SP, 2
asm 
asm    ;; Save regs
asm    pusha
asm 
asm    ;; Save the stack pointer
asm    mov BP, SP
asm 
asm    ;; By default, return the value 0 (failure)
asm    mov word [SS:(BP + 16)], 0
asm 
asm    ;; Get the VESA info block.  Save ES, since this call could
asm    ;; destroy it
asm         push ES
asm         mov DI, VESAINFO
asm         mov AX, 4F00h
asm         int 10h
asm         ;; Restore ES
asm         pop ES
asm 
asm         cmp AX, 004Fh
asm    ;; Fail
asm         jne .done
asm    
asm    ;; We need to get a far pointer to a list of graphics mode numbers
asm    ;; from the VESA info block we just retrieved.  Get the offset now,
asm    ;; and the segment inside the loop.
asm    mov SI, [VESAINFO + 0Eh]
asm 
asm    ;; Do a loop through the supported modes
asm    .modeLoop:
asm    
asm    ;; Save ES
asm    push ES
asm 
asm    ;; Now get the segment of the far pointer, as mentioned above
asm    mov AX, [VESAINFO + 10h]
asm    mov ES, AX
asm 
asm    ;; ES:SI is now a pointer to the next supported mode.  The list
asm    ;; terminates with the value FFFFh
asm 
asm    ;; Get the first/next mode number
asm    mov DX, word [ES:SI]
asm 
asm    ;; Restore ES
asm    pop ES
asm 
asm    ;; Is it the end of the mode number list?
asm    cmp DX, 0FFFFh
asm    je near .done
asm 
asm    ;; Increment the pointer for the next loop
asm    add SI, 2
asm 
asm    ;; We have a mode number.  Now we need to do a VBE call to
asm    ;; determine whether this mode number suits our needs.
asm    ;; This call will put a bunch of info in the buffer pointed to
asm    ;; by ES:DI
asm 
asm    mov CX, DX
asm    mov AX, 4F01h
asm    mov DI, _modeinfo
asm    int 10h
asm 
asm    ;; Make sure the function call is supported
asm    cmp AL, 4Fh
asm    ;; Fail
asm    jne near .done
asm    
asm    ;; Is the mode supported by this call? (sometimes, they're not)
asm    cmp AH, 00h
asm    jne .modeLoop
asm 
asm    ;; We need to look for a few features to determine whether this
asm    ;; is the mode we want.  First, it needs to be supported, and it
asm    ;; needs to be a graphics mode.  Next, it needs to match the
asm    ;; requested attributes of resolution and BPP
asm 
asm    ;; Get the first word of the buffer
asm    mov AX, word [_modeinfo]
asm    
asm    ;; Is the mode supported?
asm    bt AX, 0
asm    jnc .modeLoop
asm 
asm    ;; Is this mode a graphics mode?
asm    bt AX, 4
asm    jnc .modeLoop
asm 
!asm    %if REQUIRELFB
asm    ;; Does this mode support a linear frame buffer?
asm    bt AX, 7
asm    jnc .modeLoop
!asm    %endif
asm 
asm    ;; Does the horizontal resolution of this mode match the requested
asm    ;; number?
asm    mov AX, word [_modeinfo + 12h]
asm    cmp AX, word [SS:(BP + 20)]
asm    jne near .modeLoop
asm 
asm    ;; Does the vertical resolution of this mode match the requested
asm    ;; number?
asm    mov AX, word [_modeinfo + 14h]
asm    cmp AX, word [SS:(BP + 22)]
asm    jne near .modeLoop
asm 
asm    ;; Do the Bits Per Pixel of this mode match the requested number?
asm    xor AX, AX
asm    mov AL, byte [_modeinfo + 19h]
asm    cmp AX, word [SS:(BP + 24)]
asm    jne near .modeLoop
asm 
asm    ;; If we fall through to here, this is the mode we want.
asm    mov word [SS:(BP + 16)], DX
asm    
asm    .done:
asm    popa
asm    ;; Return the mode number
asm    xor EAX, EAX
asm    pop AX
asm    ret
asm 
asm 
asm ;;
asm ;; The data segment
asm ;;
asm 
asm    SEGMENT .data
asm    ALIGN 4
asm 
asm VESAINFO     db 'VBE2'      ;; Space for info ret by vid BIOS
asm       times 508  db 0
asm global      _modeinfo
asm _modeinfo   times 256 db 0
asm 

asm 
asm _mode_no dd 0

function switch_to_video_res: uint16_t x_res, uint16_t y_res, uint16_t bpp 
		uint32_t mode=0
		ax = bpp
		bx = y_res
		cx = x_res
asm    push word ax ;! bpp
asm    push word bx ;!y_res
asm    push word cx ;!x_res
asm    call _findGraphicMode
	! I want to use LFB.
	asm or ax, word 0x4000 
	mode = eax
	videoMode=mode
	return call switch_to_video_mode: mode
endfunction


function get_video_mode_from_res: uint16_t x_res, uint16_t y_res, uint16_t bpp 
		uint32_t mode=0
		ax = bpp
		bx = y_res
		cx = x_res
asm    push word ax ;! bpp
asm    push word bx ;!y_res
asm    push word cx ;!x_res
asm    call _findGraphicMode
	! I want to use LFB.
	asm or ax, word 0x4000 
	mode = eax
	videoMode=mode
	return mode
endfunction
function switch_to_video_mode: uint32_t mode
	if mode = 0
		return null
	endif
	eax = mode
asm    mov [_mode_no], eax
                      
asm    mov ax, 0x4F02
asm    mov bx, [_mode_no]
asm   int 0x10
asm mov eax, [_modeinfo+40]
asm 
	return eax
endfunction

enum DISPLAY_MODE_CHOICE_PREVIOUS, DISPLAY_MODE_CHOICE_OK, DISPLAY_MODE_CHOICE_NEXT


! print all 32 bits modes
function chooseGraphicModes32
	! must be local so bios can read/write it
	uint32_t choice=DISPLAY_MODE_CHOICE_NEXT
	ptr vesainfoblock=0
	uint32_t n_modes=0
	uint32_t virtual_n_modes=0
	uint16_t current_mode=0
	uint16_t res_x
	uint16_t res_y
	uint16_t bpp
	ptr mode_info_tmp
	ptr modes_list
	uint32_t offset = 0
	ptr auto_selected_mode
	uint32_t current_mode_res=0
	
	mode_info_tmp  = call malloc:6
	auto_selected_mode=call malloc:6
	modes_list = call get_vesa_modes_list
	
	while 1=1
		current_mode = [uint16_t]#(modes_list+offset)
		
		! did we reached end of the list
		if current_mode = 0xffff
			if n_modes < 2
				call free:modes_list
				call free:mode_info_tmp
				return 0
			endif
			
			! return best mode found
			if auto_choose_res = true
				call free:modes_list
				return auto_selected_mode
			endif
			n_modes--
			offset = offset - 2
			choice = DISPLAY_MODE_CHOICE_PREVIOUS
		else
			if choice = DISPLAY_MODE_CHOICE_PREVIOUS and n_modes<2
				choice = DISPLAY_MODE_CHOICE_NEXT
			else
				if call vesa_get_mode_info_32: current_mode, mode_info_tmp, mode_info_tmp+2, mode_info_tmp+4 = true
					res_x = #(mode_info_tmp)
					res_y = #(mode_info_tmp+2)
					bpp = #(mode_info_tmp+4)
					
					! auto select mode ?
					if auto_choose_res = true
						if res_x*res_y > current_mode_res
							current_mode_res = res_x*res_y
							call memcpy:auto_selected_mode,mode_info_tmp,6
						endif
					else
						choice = call display_mode:choice, res_x, res_y, bpp
					endif
					virtual_n_modes++
					! choose this mode
					if choice = DISPLAY_MODE_CHOICE_OK
						call free:modes_list
						return mode_info_tmp
					endif
				endif
			endif
			if choice = DISPLAY_MODE_CHOICE_PREVIOUS
				if offset >= 2
					offset = offset - 2
					n_modes--
				endif
			else
				offset = offset + 2
				n_modes++
			endif
		endif

	endwhile
endfunction

function display_mode: uint8_t last_choice, uint16_t res_x, uint16_t res_y, uint16_t bpp
	uint32_t final_choice=0xff
	ptr myWindow
	ptr resolution
	ptr buttonPrevious
	ptr buttonNext
	ptr buttonOk
	ptr selectedChoice
	ptr titre
	
	uint32_t myProgressBarCurrentValue=50
	uint32_t nTextChange = 1
	
	myWindow=call window_create
	
	titre=call label_create:POSITION_CENTER,POSITION_TOP,COLOR_LIGHT_BLUE, {"Veuillez choisir la resolution d'ecran a utiliser"}
	resolution=call label_create:POSITION_CENTER,8,DEFAULT_COLOR, {""}

	call label_add_number: resolution, res_x
	call label_add_text: resolution, {"x"}
	call label_add_number: resolution, res_y
	call label_add_text: resolution, {"x"}
	call label_add_number: resolution, bpp
	
	buttonPrevious=call button_create:POSITION_LEFT,POSITION_CENTER,(DEFAULT_COLOR), {"Precedent"}
	buttonOk=call button_create:POSITION_CENTER,POSITION_CENTER,(DEFAULT_COLOR), {"Choisir"}
	buttonNext=call button_create:POSITION_RIGHT,POSITION_CENTER,(DEFAULT_COLOR), {"Suivant"}

	
	call window_addItem:myWindow, OBJECT_LABEL, resolution
	call window_addItem:myWindow, OBJECT_LABEL, titre
	
	if last_choice = DISPLAY_MODE_CHOICE_PREVIOUS
		call window_addItem:myWindow, OBJECT_BUTTON, buttonPrevious
		call window_addItem:myWindow, OBJECT_BUTTON, buttonOk
		call window_addItem:myWindow, OBJECT_BUTTON, buttonNext
	else
		call window_addItem:myWindow, OBJECT_BUTTON, buttonNext	
		call window_addItem:myWindow, OBJECT_BUTTON, buttonOk
		call window_addItem:myWindow, OBJECT_BUTTON, buttonPrevious
	endif
	while final_choice=0xff
		selectedChoice = call window_exec:myWindow
		if selectedChoice = buttonPrevious
			final_choice = DISPLAY_MODE_CHOICE_PREVIOUS
		endif
		if selectedChoice = buttonOk
			final_choice = DISPLAY_MODE_CHOICE_OK
		endif
		if selectedChoice = buttonNext
			final_choice = DISPLAY_MODE_CHOICE_NEXT
		endif
		
	endwhile
	call window_free:myWindow
	return final_choice
endfunction

function get_vesa_modes_list
	ptr res
	uint32_t offset=0
	! 256 modes max
	uint32_t modes_left_available=256
	res = call malloc:modes_left_available*2
	
	uint16_t current_mode=0

	push_register es
	
	! get vesa info block
	asm mov di, VESAINFO
	ax = 0x4F00 
	asm int 0x10

	asm cmp AX, 004Fh
	asm jne .end

	! segment
	asm mov ax, [VESAINFO + 10h]
	es = ax
	! offset
	asm mov ax, [VESAINFO + 0Eh]
	si = ax
	push_register es
	push_register si
	while current_mode=/=0xffff and modes_left_available > 0
		pop_register si
		pop_register es
		asm mov ax, [es:si]
		asm add si, 2
		push_register es
		push_register si
		current_mode = ax
		#(res+offset) = [uint16_t](current_mode)
		offset = offset + 2
		modes_left_available--
	endwhile
	
	pop_register si
	pop_register es
	
	asm .end:
	! add terminating mode
	#(res+offset) = [uint16_t]0xffff
	
	pop_register es
	return res
endfunction

function vesa_get_mode_info_32: uint16_t mode, ptr res_x, ptr res_y, ptr bpp
	uint16_t t_res_x=0
	uint16_t t_res_y=0
	uint16_t t_bpp = 0
	! get mode info into _modeinfo
	cx=mode
	ax = 0x4F01
	asm    mov DI, _modeinfo
	asm    int 10h
	
	! Make sure the function call is supported
	asm    cmp AL, 4Fh
	! Fail
	asm    jne .erreur
	! Is the mode supported by this call? (sometimes, they're not)
	asm    cmp AH, 00h
	asm    jne .erreur
	! Get the first word of the buffer
	asm    mov AX, word [_modeinfo]
	asm    
	! Is the mode supported?
	asm    bt AX, 0
	asm    jnc .erreur
	asm 
	! Is this mode a graphics mode?
	asm    bt AX, 4
	asm    jnc .erreur
	asm 
	! Does this mode support a linear frame buffer?
	asm    bt AX, 7
	asm    jnc .erreur
	
	
	asm mov AX, word [_modeinfo + 12h]
	t_res_x = ax
	
	asm mov AX, word [_modeinfo + 14h]
	t_res_y = ax
	
	ax = 0
	asm mov AL, byte [_modeinfo + 19h]
	t_bpp = ax
	
	
	if t_bpp = 32
		asm jmp .ok
	endif
	
	if t_bpp = 24
		asm jmp .ok
	endif
	asm jmp .erreur
	
	
	asm .ok:
	if t_res_x<720
		asm jmp .erreur
	endif
	
	if t_res_y<480
		asm jmp .erreur
	endif
	#res_x = t_res_x
	#res_y = t_res_y
	#bpp = t_bpp
	
	return true
	
	asm .erreur:
	return false
endfunction