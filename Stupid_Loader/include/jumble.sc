/*
*******************************************************************************
* Source code wrote in "Sztrojka Code" by LANEZ Ga�l - 2014                   *
* Permission to use, copie and modifie this code.                             *
*******************************************************************************
*/
include "include\asm_def.sc"
include "include\drivers\bios_driver.sc"
include "include\string.sc"
include "include\library.sc"
include "include\screen.sc"

include "include\built_in_apps\fileBrowser.sc"

! clear screen then print the title
function cls
 	call raw_cls
	call color_prints:COLOR_GREY, {"################################"}
	call color_prints:COLOR_GREEN, {" STUPID LOADER "}
	call color_prints:COLOR_GREY, {"################################"}

	call coords_color_prints:0,24,COLOR_GREY, {"##############################################################################"}
	call move_cursor:0,2
	
	call progressBar_draw:ram_progressBar, (ramUsed)
	
	call draw_time
	
	! bios hook state
	if isBiosHooked = true

		call coords_color_prints:79-5,24,COLOR_GREEN, {" I"}
	else
		call coords_color_prints:79-5,24,COLOR_RED, {" I"}
	endif
	
	! write disk access
	call coords_color_prints:79-3,24,COLOR_GREEN, {" R"}
	if writeDiskAccess = true
		call coords_color_prints:79-1,24,COLOR_GREEN, {"W"}
	else
		call coords_color_prints:79-1,24,COLOR_RED, {"W"}
	endif
	
	! boot drive
	if bootdrive < 0x80
		call coords_color_prints:65,24,COLOR_YELLOW, {" CD/USB "}
	else
		call coords_color_prints:68,24,COLOR_YELLOW, {" HDD "}
	endif
		
endfunction



! wait for a key
function pause
	call prints:{"Appuyez sur une touche pour continuer...\n", 0}
	call bios_wait_key
endfunction

! color_printn with default color
!function printn:uint32_t number
!	call color_printn:DEFAULT_COLOR, number
!endfunction

! shutdown or reboot menu
function shutdown_menu
	ptr myWindow
	ptr myLabel
	ptr buttonCancel
	ptr buttonShutdown
	ptr buttonRestart
	
	ptr selectedChoice
	
	myWindow=call window_create
	
	
	myLabel=call label_create:0,1,DEFAULT_COLOR, {"Selectionnez l'action a suivre:"}
	
	buttonCancel=call button_create:0,3,DEFAULT_COLOR, {"Annuler"}
	buttonShutdown=call button_create:0,4,DEFAULT_COLOR, {"Arreter"}

	buttonRestart=call button_create:0,5,DEFAULT_COLOR, {"Redemarrer"}
	
	call window_addItem:myWindow, OBJECT_LABEL, myLabel
	call window_addItem:myWindow, OBJECT_BUTTON, buttonCancel
	call window_addItem:myWindow, OBJECT_BUTTON, buttonShutdown
	call window_addItem:myWindow, OBJECT_BUTTON, buttonRestart
	

	
	selectedChoice = call window_exec:myWindow
	
	if selectedChoice = buttonShutdown
		call bios_shutdown
	endif
	
	if selectedChoice = buttonRestart
		call bios_reboot
	endif
	call window_free:myWindow

endfunction

function main_menu
	call prints:{"Unhandled function 'main_menu' in 'jumble.sc'\n"}
	call pause
	
	/*
	ptr myWindow
	
	ptr myLabel
	ptr buttonFileBrowser
	ptr buttonGui
	ptr buttonInstall
	ptr buttonShutdown
	ptr buttonKeyCode
	ptr selectedChoice
	ptr button32Bits
	ptr buttonMem
	myWindow=call window_create
	
	
	myLabel=call label_create:POSITION_CENTER,POSITION_TOP,COLOR_LIGHT_BLUE, {"Menu principal"}
	
	buttonFileBrowser=call button_create:POSITION_LEFT,3,DEFAULT_COLOR, {"Navigateur de fichiers"}
	buttonGui=call button_create:POSITION_LEFT,4,DEFAULT_COLOR, {"Test GUI"}
	buttonInstall=call button_create:POSITION_LEFT,5,DEFAULT_COLOR, {"Installer"}
	buttonKeyCode=call button_create:POSITION_LEFT,6,DEFAULT_COLOR, {"Scancode clavier"}
	buttonMem=call button_create:POSITION_LEFT,7,DEFAULT_COLOR, {"Detection de la memoire"}
	button32Bits=call button_create:POSITION_LEFT,8,DEFAULT_COLOR, {"Demarrer le noyau 32 bits"}
	buttonShutdown=call button_create:POSITION_LEFT,9,DEFAULT_COLOR, {"Arreter"}

	
	call window_addItem:myWindow, OBJECT_LABEL, myLabel
	call window_addItem:myWindow, OBJECT_BUTTON, buttonFileBrowser
	call window_addItem:myWindow, OBJECT_BUTTON, buttonGui
	call window_addItem:myWindow, OBJECT_BUTTON, buttonInstall
	call window_addItem:myWindow, OBJECT_BUTTON, buttonKeyCode
	call window_addItem:myWindow, OBJECT_BUTTON, buttonMem
	call window_addItem:myWindow, OBJECT_BUTTON, button32Bits
	call window_addItem:myWindow, OBJECT_BUTTON, buttonShutdown


	while 1 = 1
		selectedChoice = call window_exec:myWindow
		
		if selectedChoice = buttonFileBrowser
			ptr file_path
			file_path = call fileBrowser
			if file_path =/= null
				call exec: file_path, null
				call free: file_path
			endif
		endif
		if selectedChoice = buttonGui
			call exec:{"root0/system/x16/apps/GUITest.bin"}, null
		endif
		
		if selectedChoice = buttonInstall
			call install
		endif
		
		if selectedChoice = buttonKeyCode
			call scancodeApp
		endif
		
		if selectedChoice = buttonShutdown
			call shutdown_menu
		endif
		
		if selectedChoice = buttonMem
			call cls
			uint32_t total_mem=0
			total_mem = call exec:{"root0/system/x16/apps/memdet.bin"}, null
			call prints:{"\n\nMemoire disponible pour le noyau 32 bits: "}
			call printn:DEFAULT_COLOR, total_mem/1024
			call prints:{"ko\n"}
			call pause
		endif
		
		if selectedChoice = button32Bits
			call exec:{"root0/system/x16/apps/load32.bin"}, null
		endif

	endwhile
*/

endfunction

/*
function scancodeApp

	ptr code_ascii_array
	code_ascii_array = call malloc:4
	
	call cls
	call pause
	
	! escape
	while [uint16_t]#(code_ascii_array) =/= 0x1
		call cls
		call prints:{"\n\nScancode: "}
		call printn:COLOR_RED, [uint16_t]#code_ascii_array
		call prints:{"\nASCII: "}
		call printn:COLOR_GREEN, [uint16_t]#(code_ascii_array+2)
		call prints:{" '"}
		
		! cr
		if [uint16_t]#(code_ascii_array+2) = 13
			call color_prints:COLOR_CYAN, {"[CR]"}
		else
			if [uint16_t]#(code_ascii_array+2) = 0
				call color_prints:COLOR_CYAN, {"[NULL]"}
			else
				call color_prints:COLOR_CYAN, (code_ascii_array+2)
			endif
		endif
		
		call prints:{"'\n"}
		call bios_read_key_code_ascii: code_ascii_array, (code_ascii_array+2)
	endwhile
	
	call free: code_ascii_array
	call pause
	
endfunction
function install
	!call exec:{"root0/system/x16/apps/install.bin"}, null
	call prints:{"Unhandler function 'install' in 'jumble.sc'\n"}
	call pause
endfunction
*/
/*
define OS_LBA 0
define OS_SIZE 2880
function install:uint8_t boot_drive

		uint8_t install_drive = 0
		uint8_t i
		call cls
		call color_prints:DEFAULT_COLOR, {"Disque de demarrage: ", 0} 
		call printn:0x0d, boot_drive
		
		
		call color_prints:DEFAULT_COLOR, {" - Disque d'installation: ", 0} 

		! choose on wich drive we will install
		if boot_drive =/= 0x80
			install_drive = 0x80
			
			! is disk found ?
			if call bios_get_drive_type: install_drive =/= 0x03
				! no
				install_drive = 0x81
				
				! is disk found ?
				if call bios_get_drive_type: install_drive =/= 0x03
					! no, cancel installation
					call color_prints:COLOR_RED, {"<non trouve>\nArret.\n", 0} 
					call bios_eject_media: boot_drive
					! shutdown
					call pause
					exit
				endif
			endif
			call color_prints:COLOR_GREEN, {"<trouve> ", 0} 
		endif
		if boot_drive = 0x80
			install_drive = 0x81
			
			! is disk found ?
			if call bios_get_drive_type: install_drive =/= 0x03
				! no, cancel installation
				call color_prints:COLOR_RED, {"<non trouve>\nArret.\n", 0} 
				call bios_eject_media: boot_drive
				! shutdown
				call pause
				exit
			endif
			call color_prints:COLOR_GREEN, {"<trouve> ", 0} 
		endif
		
		! print selected drive
		

		call printn:0x0d, install_drive
		
		! ask the user to press enter
		call color_prints:DEFAULT_COLOR, {"\nAppuyez sur [",0}
		call color_prints:COLOR_LIGHT_BLUE, {"ENTER",0}
		call color_prints:DEFAULT_COLOR, {"] pour installer, ou [",0}
		call color_prints:COLOR_LIGHT_BLUE, {"ESCAPE",0}
		call color_prints:DEFAULT_COLOR, {"] pour abandonner.\n", 0}
		uint8_t key = 0
		
		key = call bios_wait_key
		! while not press ENTER
		while key =/= 0x1c
			! ESCAPE pressed: cancel installation
			if key = 0x01
				call color_prints:COLOR_RED, {"Arret.\n", 0}
				call bios_eject_media: boot_drive
				! shutdown
				call pause
				exit
			endif
			
			key = call bios_wait_key
		endwhile
		! allocate memory
		ptr buffer
		
		i = 0
		define INSTALL_N_STEPS 4
		! read in 5 steps (because ram isnt enough)
		while i<INSTALL_N_STEPS
			buffer = call malloc:(OS_SIZE/INSTALL_N_STEPS*512)
			
			call color_prints:DEFAULT_COLOR, {"\nLecture depuis le disque de demarrage..", 0}
			
			! error ?
			if call read_disk_raw:boot_drive, OS_LBA+(i*OS_SIZE/INSTALL_N_STEPS), OS_SIZE/INSTALL_N_STEPS, buffer =/= 0
				
					! fatal error
					call color_prints:COLOR_RED, {"    [ERREUR]\nArret.\n", 0}
					call bios_eject_media: boot_drive
					call pause
					call free:buffer
					exit
			endif
			
			! done, write data now
			call color_prints:COLOR_GREEN, {"    [OK]", 0}
			call color_prints:DEFAULT_COLOR, {"\nEcriture sur le disque..", 0}

			! error ?
			if 	call write_disk_raw:install_drive, (i*OS_SIZE/INSTALL_N_STEPS), OS_SIZE/INSTALL_N_STEPS, buffer =/= 0
				call color_prints:COLOR_RED, {"    [ERREUR]\nArret.\n", 0}
				call bios_eject_media: boot_drive
				call pause
				call free: buffer
				exit
			endif
			
			call color_prints:COLOR_GREEN, {"    [OK]", 0}
			call free:buffer
			i++
		endwhile
		
		call color_prints:DEFAULT_COLOR, {" \n", 0}
		call color_prints:COLOR_GREEN, {"\nInstallation reussi !\n",0}
		
		if call bios_eject_media: boot_drive =/= 0
			call color_prints:COLOR_RED, {"Veuillez retirer le peripherique d'installation.\n"}
		endif
		call prints:{"Le systeme va redemarrer.\n",0}
		call pause
		call bios_reboot
endfunction

*/