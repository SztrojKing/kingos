/*
*******************************************************************************
* Source code wrote in "Sztrojka Code" by LANEZ Ga�l - 2014                   *
* Permission to use, copie and modifie this code.                             *
*******************************************************************************
*/


define 16_BITS_MAX_DIGITS 0
define INT_DIGITS 19



! compare 2 chaines de caractere et retourne 0 si elles sont �gales
function strcmp:ptr str_1, ptr str_2
	while [uint8_t]#str_1 =/= 0
		if  [uint8_t]#str_1 =/= [uint8_t]#str_2
			return 1
		endif
		str_1++
		str_2++
	endwhile
	
	if  [uint8_t]#str_1 =/= [uint8_t]#str_2
		return 1
	endif
		
	return 0
endfunction

function strlen: ptr str
	uint32_t len=0
	if str =/= null
		while [uint8_t]#(str+len)=/=0
			len++
		endwhile
	endif
	return len
endfunction

function strcat: ptr dest, ptr src
	uint32_t destLen
	uint32_t srcLen
	destLen = call strlen:dest
	srcLen = call strlen:src
	
	call memcpy:(dest+destLen), src, (srcLen+1)
	return dest
endfunction

function strcpy: ptr dest, ptr src
	uint32_t srcLen=0
	srcLen = call strlen:src
	
	call memcpy:dest, src, (srcLen+1)
endfunction

function itoa: ptr dest, uint32_t number
	uint32_t digits=0
	#dest = [uint8_t]'0'
	while number =/=0
		#(dest+digits) = [uint8_t]('0'+(number%10))
		number=number/10
		digits++
	endwhile
	call memreverse:dest, digits
endfunction

function atoi:ptr str
	uint32_t res=0
	while [uint8_t]#(str) >= '0' and [uint8_t]#(str) <= '9'
		res = (res * 10) + ([uint8_t]#(str)-'0')
		str++
	endwhile
	return res
endfunction

function toUpper: ptr str
	while [uint8_t]#str =/= 0
		if [uint8_t](#str) >= [uint8_t]'a' and [uint8_t](#str) <= [uint8_t]'z'
			#str=[uint8_t](#str+('A'-'a'))
		endif
		str++
	endwhile

endfunction

! put a char at a specified position in a str
function string_append_at_offset: ptr str, uint8_t char, uint32_t offset
	uint32_t strSize=0
	strSize = call strlen: str

	if offset > strSize
		return false
	endif
	
	call memcpy_from_end: (str+offset+1), (str+offset), (strSize-offset)
	#(str+offset) = char
	
	return true
endfunction
