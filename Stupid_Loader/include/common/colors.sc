/*
*******************************************************************************
* Source code writen in "Sztrojka Code" by LANEZ Ga�l - 2015                  *
* Permission to use, copie and modifie this code.                             *
*******************************************************************************
*/
define DEFAULT_COLOR 0x07

define COLOR_BLACK 0x00
define COLOR_DARK_BLUE 0x01
define COLOR_GREEN 0x02
define COLOR_AQUA 0x03
define COLOR_BROWN 0x04
define COLOR_PURPLE 0x05
define COLOR_KHAKI 0x06
define COLOR_LIGHT_GREY 0x07
define COLOR_GREY 0x08
define COLOR_LIGHT_BLUE 0x09
define COLOR_LIGHT_GREEN 0x0a
define COLOR_CYAN 0x0b
define COLOR_RED 0x0c
define COLOR_PINK 0x0d
define COLOR_YELLOW 0x0e
define COLOR_WHITE {([uint8_t]0x0f)}

