/*
*******************************************************************************
* Source code writen in "Sztrojka Code" by LANEZ Ga�l - 2015                  *
* Permission to use, copie and modifie this code.                             *
*******************************************************************************
*/

/*
	This file should be introduced in first of the main file of an app
	It will do all the nasty stuff and will call the main function
*/

include "..\..\include\common\syscall_header.sc"

DEFINE_AS_APPLICATION

include "..\..\include\asm_def.sc"
include "..\..\include\common\syscall.sc"

include "..\..\include\common\colors.sc"
include "..\..\include\common\gui.sc"
include "..\..\include\common\disks.sc"
function__no_stack__ entry
	! get args ptr
	asm mov eax, [MAGICAL_NUMBER]
	! call main function
	call main:eax
	asm retf
endfunction


