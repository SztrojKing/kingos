@echo off
setlocal enabledelayedexpansion
set runCount=0
cd /d %~dp0
:start
color 09
echo *******************************************************************************
echo *                                                                             *
echo *                          Building STUPID LOADER                             *
echo *                                                                             *
echo *******************************************************************************
echo.
rem ping 1.1.1.1 -n 1 -w 1000 > nul
color 07
set listfile=obj\kernel.c.o

echo Nettoyage

del ..\bin\disk.img
del ..\bin\hdd.img
del ..\bin\*.bin

echo 									[OK]
echo.

echo *********************** Compilation du kernel ************************
tools\scc16.exe -no_auto_compil -e kernel.sc -s ..\bin\kernel.bin -org 0
if ERRORLEVEL 1 ( goto exit_fail )
tools\nasm.exe -f bin sortie.asm -o ..\bin\kernel.bin
if ERRORLEVEL 1 ( goto exit_fail )
del sortie.asm
rem pause
echo 									[OK]
echo.

echo ********************* Copie des fichiers *******************


copy /b ..\bin\disk.vfd ..\bin\disk.img
del ..\bin\hdd.img
copy /b ..\bin\hdd_src.vfd ..\bin\hdd.img
xcopy /f /y ..\bin\kernel.bin ..\fs\stupid.knl
xcopy /S /y system ..\fs\system\x16\
xcopy /S /y boot\fat12.bin ..\fs\system\x16\boot\fat12.bin
xcopy /S /y include ..\fs\system\x16\src\
xcopy /S /y boot ..\fs\system\x16\src\boot\



echo Succes !
color 0a


:end
set runCount=1
goto eof


:exit_fail
color 0c
echo 									[ERREUR]
pause
goto start
:eof