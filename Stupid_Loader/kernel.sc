/*
*******************************************************************************
* Source code wrote in "Sztrojka Code" by LANEZ Gaël - 2016                   *
* Permission to use, copie and modifie this code.                             *
*******************************************************************************
*/

!include "include\interrupts.sc"
include "include\library.sc"
include "include\init_system.sc"
include "include\drivers\fat12_driver.sc"
include "include\a20.sc"
include "include\built_in_apps\x86_loader\load32.sc"

include "include\time.sc"

function__no_stack__ main 

	asm cli
	! segments
	ax = 0x800
	ds = ax
	es = ax
	fs = ax
	gs = ax
	ss = ax
	
	esp = 0xffff

	! go to flat real mode (unreal mode)
	! ************* stack located from 0x8000 to 0x700 (about 30 kb) ************* 
	
	asm   mov esp, 0xffff      ; 30 kb stack
	asm   push ds                ; save real mode
	asm 
	asm   lgdt [gdtinfo]         ; load gdt register
	asm 
	asm   mov  eax, cr0          ; switch to pmode by
	asm   or al,1                ; set pmode bit
	asm   mov  cr0, eax
	asm 
	asm   jmp $+2                ; tell 386/486 to not crash
	asm 
	asm   mov  bx, 0x8          ; select descriptor 1
	asm   mov  ds, bx            ; 8h = 1000b
	asm 
	asm   and al,0xFE            ; back to realmode
	asm   mov  cr0, eax          ; by toggling bit again
	asm 
	asm   pop ds                 ; get back old segment
	asm   sti

	call kmain
	loop
	
	
	asm gdtinfo:
	asm   dw gdt_end - gdt - 1   ;last byte in table
	asm   dd gdt + 0x8000               ;start of table
	asm 
	asm gdt         dd 0,0        ; entry 0 is always unused
	asm flatdesc    db 0xff, 0xff, 0, 0, 0, 10010010b, 11001111b, 0
	asm gdt_end:
	

endfunction



suint8_t bootdrive=0
! shall we enable sse for faster memory access ?
suint8_t enable_sse=true
! shall we automatically choose screen res ?
suint8_t auto_choose_res = false
! shall we only use bios int to access disks ?
suint8_t diskOnlyUseBios = false
! enable read only for main kernel ?
suint8_t readOnly=false



function kmain

	bootdrive = dl
	if call init_a20 =/= 1
		loop
	endif
	call init_video
	call bios_set_text_mode
	call bios_move_cursor:0xff, 0xff
	call raw_cls
	call color_prints:COLOR_PURPLE, {"\nInitialisation...\n"}
	bool continue=true
	bool boot16bits=false
	uint32_t ticks
	!call extendMemory
	call prints:{"\nBooting from "}
	if bootdrive < 0x80
		call color_prints:COLOR_GREEN, {"CD/USB (root0)"}
	else
		call color_prints:COLOR_GREEN, {"HDD (root0)"}
	endif
	
		
	! malloc
	call prints:{"\nAllocation de memoire\n"}
	call init_malloc
	
	! clavier azerty
	call prints:{"\nClavier "}
	if call init_keyboard = true
		call printOk
	else
		call printError
	endif
	
	! souris
	!call prints:{"\nSouris(Alpha) "}
	!call init_mouse
	!call printOk


	
	
	call color_prints:COLOR_AQUA, {"\nF1 pour acceder aux reglages de demarrage\n"}
	
	
	! reglages de demarrage
	ticks = call time_get_ticks
	while continue = true
		if call time_get_ticks > ticks + 18
			continue=false
		endif
			
		if call bios_read_key = 59
			call bootSettingsWindow
			call cls
			continue = false
		endif
	endwhile
	
	/*
	call prints:{"IVT hook..."}
	if call setup_bios_ivt = true
		call printOk
	else
		call printError
	endif
	*/
	
	!call sleep:2000
	/*
	call cls
	
	
	
	if call exec: {"root0/system/x16/apps/login.bin"}, null = 1
		call main_menu
	endif
	
	! if we're there, then exec application didnt started
	call cls
	call prints:{"Demarrage impossible.\n"}
	call pause
	
	while 1=1
		call shutdown_menu
	endwhile
	*/
	/*
	if boot16bits = true
		call main_menu
	else
		call exec:{"root0/system/x16/apps/load32.bin"}, null
	endif
	*/
	
	call load32
	while 1=1
		call shutdown_menu
	endwhile
	loop
	
endfunction



function bootSettingsWindow
	ptr myWindow
	ptr myLabel
	ptr buttonEnableSSE
	ptr buttonEnableReadOnly
	ptr button16BitsBoot
	ptr buttonAutoChooseRes
	ptr buttonDiskOnlyUseBios
	ptr selectedChoice
	
	myWindow=call window_create
	
	myLabel=call label_create:0,3,DEFAULT_COLOR, {"Reglages de demarrage du mode 16 bits (experts uniquement)"}
	
	buttonEnableSSE=call button_create:0,5,(COLOR_RED), null
	buttonEnableReadOnly=call button_create:0,6,(COLOR_RED), null
	buttonAutoChooseRes=call button_create:0,7,(COLOR_RED), null
	buttonDiskOnlyUseBios=call button_create:0,8,(COLOR_RED), null
	button16BitsBoot=call button_create:POSITION_CENTER,POSITION_CENTER,(COLOR_GREEN), {"OK"}
	
	call window_addItem:myWindow, OBJECT_LABEL, myLabel
	call window_addItem:myWindow, OBJECT_BUTTON, buttonEnableSSE
	call window_addItem:myWindow, OBJECT_BUTTON, buttonEnableReadOnly
	call window_addItem:myWindow, OBJECT_BUTTON, buttonAutoChooseRes
	call window_addItem:myWindow, OBJECT_BUTTON, buttonDiskOnlyUseBios
	call window_addItem:myWindow, OBJECT_BUTTON, button16BitsBoot
	while 1=1
		if enable_sse = true
			call button_set_text:buttonEnableSSE, {"Desactiver SSE2 [actuellement ACTIVE]"}
		else
			call button_set_text:buttonEnableSSE, {"Activer SSE2 [actuellement DESACTIVE]"}
		endif
		
		if readOnly = false
			call button_set_text:buttonEnableReadOnly, {"Desactiver l'ecriture des disques [actuellement ACTIVE]"}
		else
			call button_set_text:buttonEnableReadOnly, {"Activer l'ecriture des disques [actuellement DESACTIVE]"}
		endif
		
		if auto_choose_res = true
			call button_set_text:buttonAutoChooseRes, {"Selectionner manuellement la resolution  d'ecran  [actuellement AUTO]"}
		else
			call button_set_text:buttonAutoChooseRes, {"Selectionner automatiquement la resolution  d'ecran [actuellement MANUEL]"}
		endif
		
		if diskOnlyUseBios = true
			call button_set_text:buttonDiskOnlyUseBios, {"Utiliser les driver kernel pour acceder aux disques [actuellement BIOS]"}
		else
			call button_set_text:buttonDiskOnlyUseBios, {"Utiliser uniquement le bios pour acceder aux disques  [actuellement KERNEL]"}
		endif
		
		selectedChoice = call window_exec:myWindow
		if selectedChoice = null
			call window_free:myWindow
			return 0
		endif
		if selectedChoice = buttonEnableSSE
		
			if enable_sse = true
				enable_sse = false
			else
				enable_sse = true
			endif
		endif
		
		if selectedChoice = buttonEnableReadOnly
		
			if readOnly = true
				readOnly = false
			else
				readOnly = true
			endif
		endif
		
		if selectedChoice = buttonAutoChooseRes
		
			if auto_choose_res = true
				auto_choose_res = false
			else
				auto_choose_res = true
			endif
		endif
		
		if selectedChoice = buttonDiskOnlyUseBios
		
			if diskOnlyUseBios = true
				diskOnlyUseBios = false
			else
				diskOnlyUseBios = true
			endif
		endif
		
		if selectedChoice = button16BitsBoot
			call window_free:myWindow
			return 1
		endif
	endwhile
endfunction

