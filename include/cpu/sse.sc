/*********************************
* This file is a part of King OS *
*  written by LANEZ Gaël - 2017  *
*********************************/

su8 sse_enabled=false

! check if sse2 feature is supported on this cpu
function is_sse2_supported
	eax = 0x1
	asm cpuid
	asm test edx, 1<<25
	asm jz .noSSE2
	! sse2 supported
	return true
	asm .noSSE2:
	return false
endfunction

! init 
function sse2_init
	bool res=false
	
	! allocate memory for X_aligned_memset32_sse2
	memset_sse2_buffer = malloc(128)
	
	if is_sse2_supported() == true
		klog(" [ SUPPORTE")
		sse2_setup()
		klog(" | ACTIVE ]")
		sse_enabled=true
		res = true
	endif	
	return res
	
endfunction

! enable sse2
function sse2_setup
	asm mov eax, cr0
	asm and ax, 0xFFFB		;clear coprocessor emulation CR0.EM
	asm or ax, 0x2			;set coprocessor monitoring  CR0.MP
	asm mov cr0, eax
	asm mov eax, cr4
	asm or ax, 3 << 9		;set CR4.OSFXSR and CR4.OSXMMEXCPT at the same time
	asm mov cr4, eax
endfunction
