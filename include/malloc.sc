/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/


define MALLOC_BLOCK_SIZE 4096
define MALLOC_MAGIC {([uint32_t](0xcb94E3a7))}
define MALLOC_ALIGN 4096
define MALLOC_HEADER_SIZE 0x10

suint32_t MALLOC_BITMAP_ADDRESS = 0
suint32_t MALLOC_DATA_ADDRESS = 0
suint32_t mallocBitmapSize=0
suint32_t ramSize=0
suint32_t totalRamSize=0


!suint32_t ramUsed=MALLOC_BITMAP_ADDRESS
suint32_t ramUsed=0
sptr ram_progressBar=null

function init_malloc: u32 memsize
	
	! if bootloader didn't passed memsize
	if memsize == 0
		klog("\tErreur de detection\n")
		! buggy function: can't detect more than 64 MB
		outb(0x70, 0x31)
		totalRamSize = ((inb(0x71)  << 8)*1024) + 0x100000
	else
		totalRamSize=memsize
	endif
	if totalRamSize<192*0x100000
		klog("RAM insuffisante/non detectee\nTest sur le loader 16 bits recommande\n")
		return 0
		!klog("\tErreur de ram ?\n")
		!totalRamSize=64*1024*1024
	endif
	
	! bitmap start 1MB after kernel
	MALLOC_BITMAP_ADDRESS = kernel_page_directory + PAGING_PAGES_SIZE + 1*1024*1024
	
	ramSize = totalRamSize - MALLOC_BITMAP_ADDRESS
	
	mallocBitmapSize=(ramSize/MALLOC_BLOCK_SIZE)
	
	MALLOC_DATA_ADDRESS = (MALLOC_BITMAP_ADDRESS + mallocBitmapSize)
	! align with MALLOC_ALIGN
	MALLOC_DATA_ADDRESS = MALLOC_DATA_ADDRESS + (MALLOC_ALIGN - (MALLOC_DATA_ADDRESS%MALLOC_ALIGN))

	ramUsed = MALLOC_DATA_ADDRESS-MALLOC_BITMAP_ADDRESS
		
	klog("\tRAM: ")
	klogn((totalRamSize/0x100000))
	klog("MB (RESERVE: ")
	klogn(MALLOC_BITMAP_ADDRESS/0x100000)
	klog("MB | BITMAP: ")
	klogn(mallocBitmapSize/1024/1024)
	klog("MB | DISPONIBLE: ")
	klogn((mallocBitmapSize*MALLOC_BLOCK_SIZE)/0x100000)
	klog("MB [")
	klogn(mallocBitmapSize/1000)
	klog("k blocs])\n")
	memset(MALLOC_BITMAP_ADDRESS, 0, mallocBitmapSize)
	return true
endfunction

function malloc: uint32_t size
	
	uint32_t realSize=0
	ptr address=0
	
	if size==0
		!call fatal_exception:EXCEPTION_MALLOC_SIZE_0
		fatal_error("malloc: EXCEPTION_MALLOC_SIZE_0")
	endif
	
	! if malloc has not been initialised yet
	if MALLOC_DATA_ADDRESS == 0
		!call fatal_exception:EXCEPTION_NOT_INITIALISED
		fatal_error("malloc: EXCEPTION_NOT_INITIALISED")
	endif
	! add header's size
	size=size+MALLOC_HEADER_SIZE
	! round size
	if size%MALLOC_BLOCK_SIZE==0
		realSize=(size/MALLOC_BLOCK_SIZE)
	else
		realSize=(size/MALLOC_BLOCK_SIZE)+1
	endif
	
	! find the address
	address=malloc_findLocation(realSize)
	! check error
	if address<MALLOC_DATA_ADDRESS
		! generate fatal exception
		!call fatal_exception: EXCEPTION_MEMORY
		fatal_error("malloc: EXCEPTION_MEMORY")
	endif
	! reserve space
	malloc_setBitMap(address, realSize, true)
	! write veryfying bytes
	#address=[u32]MALLOC_MAGIC

	! write header
	#(address+4)=realSize
	
	! allow access to data
	paging_map_area(current_page_directory, address, address, PAGING_FLAG_PRESENT, realSize*MALLOC_BLOCK_SIZE)
	!paging_flush_table()
	! skip header
	address=(address+MALLOC_HEADER_SIZE)
	
	! refresh progressBar
	ramUsed=(ramUsed+(realSize*MALLOC_BLOCK_SIZE))
	!call progressBar_draw:ram_progressBar, (ramUsed)

	! fill with 0's
	memset(address, 0, (realSize*MALLOC_BLOCK_SIZE)-MALLOC_HEADER_SIZE)
	
	return address
	
endfunction

function realloc: ptr old_address, u32 new_size
	ptr header_address = [ptr](old_address-0x10)
	u32 magic = [u32]#(header_address)
	u32 old_size=[u32](#(header_address+4)) * MALLOC_BLOCK_SIZE - 0x10
	
	ptr new_address = old_address
	
	if magic=/=MALLOC_MAGIC
		!call fatal_exception:EXCEPTION_PTR
		fatal_error("realloc: EXCEPTION_PTR")
	endif
	
	! dont do anything if we're trying to realloc with a smaller size
	if old_size < new_size
		new_address = malloc(new_size)
		! copy old data
		memcpy(new_address, old_address, old_size)
		
		free(old_address)
	endif

	return new_address
endfunction

function free: ptr virtual_address
	
	if virtual_address=/=null
		ptr header_address = [ptr](virtual_address-0x10)
		u32 magic = [u32]#(header_address)
		u32 size=[u32]#(header_address+4)

		if magic=/=MALLOC_MAGIC
			!call fatal_exception:EXCEPTION_PTR
			fatal_error("free: EXCEPTION_PTR")
		endif
		! refresh progressBar
		ramUsed=ramUsed-(size*MALLOC_BLOCK_SIZE)
		!call progressBar_draw:ram_progressBar, (ramUsed)
		
		! clear our bitmap
		malloc_setBitMap( header_address,  size, false)
		! erase data
		!memset(header_address, 0,  size*MALLOC_BLOCK_SIZE)

		! forbide access to data
		!paging_map_area(current_page_directory, header_address, header_address, PAGING_FLAG_PRESENT, size*MALLOC_BLOCK_SIZE)
		!paging_flush_table()
	endif
	return null

endfunction

! real address, length in blocks

function malloc_setBitMap: uint32_t address, uint32_t length, bool bit_state

	uint32_t i=0
	ptr bitmap_bit_address
	uint32_t tmp=0
	! convert address to bitmap address
	
	while i<length
		! optimization using tmp value
		tmp=(((address+(i*MALLOC_BLOCK_SIZE))-MALLOC_DATA_ADDRESS)/MALLOC_BLOCK_SIZE)
		
		#(tmp+MALLOC_BITMAP_ADDRESS) = [u8](bit_state)
		!#bitmap_bit_address=[u8](setBit([uint8_t](#bitmap_bit_address), tmp%8, bit_state))

		
		i++
	endwhile
	
endfunction
/*
function malloc_setBitMap2: uint32_t address, uint32_t length, bool bit_state

	uint32_t i=0
	ptr bitmap_bit_address
	uint8_t bitmap_bit_offset
	u32 address_offset=0
	address_offset = malloc_addressToBitmapOffset(address)
	! convert address to bitmap address
	
	while i<length
		bitmap_bit_address=address_offset/8
		bitmap_bit_offset=address_offset%8
		
		#bitmap_bit_address=[u8](setBit((#(bitmap_bit_address+MALLOC_BITMAP_ADDRESS)), bitmap_bit_offset, bit_state))

		
		i++
	endwhile
	
endfunction*/
/*
function malloc_addressToBitmapOffset: u32 address
	
	return [u32](address-MALLOC_DATA_ADDRESS)/MALLOC_BLOCK_SIZE
endfunction
*/

! length in blocks
! return address
function malloc_findLocation: uint32_t length
	uint32_t i=0
	uint8_t j=0
	uint32_t continuousBlocks=0
	
	while i<mallocBitmapSize
		/*
		while j<8
			! check current bit
			if (([u8]#(i+MALLOC_BITMAP_ADDRESS)) & (1<<(j))) =/= 0
				continuousBlocks=0
			else
				continuousBlocks++
			endif
			
			! we found enough space
			if continuousBlocks>=length
				! return address
				return ((((i*8)+j+1)-continuousBlocks)*MALLOC_BLOCK_SIZE)+MALLOC_DATA_ADDRESS
			endif
			j++
		endwhile
		*/
		if [u8]#(i+MALLOC_BITMAP_ADDRESS) =/= 0
			continuousBlocks=0
		else
			continuousBlocks++
		endif
		
		i++
		
		if continuousBlocks>=length
			return (i-continuousBlocks)*MALLOC_BLOCK_SIZE+MALLOC_DATA_ADDRESS
		endif
		
	endwhile
	return null
endfunction

! returns the allocated size
function malloc_get_size: ptr virtual_address
	u32 res = 0

	if virtual_address=/=null
		ptr header_address = [ptr](virtual_address-0x10)
		u32 magic = [u32]#(header_address)
		if magic=/=MALLOC_MAGIC
			!call fatal_exception:EXCEPTION_PTR
			fatal_error("malloc_get_size: EXCEPTION_PTR")
		endif

		res=[u32]#(header_address+4)

	endif
	return res * MALLOC_BLOCK_SIZE
endfunction
