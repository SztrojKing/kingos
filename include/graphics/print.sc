/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/

define CHAR_X_SIZE 8
define CHAR_Y_SIZE 10
define TAB_LENGTH 4
function prints_rect: u32 x1, u32 y1, u32 x2, u32 y2, u32 color, ptr str
	prints_rect_buffer(vram, vres_x, vres_y, x1, y1, x2, y2, color, str)
endfunction

function prints_rect_buffer: ptr buffer, u32 res_x, u32 res_y, u32 x1, u32 y1, u32 x2, u32 y2, u32 color, ptr str
	if str == null
		exit
	endif
	u32 current_x=x1
	u32 current_y=y1
	while [u8]#str =/= 0
	
		! LF CR
		if [u8]#str == 10
			current_x=x1
			current_y+=CHAR_Y_SIZE
		endif
		
		! tab
		if  [u8]#str == 9
			current_x+=TAB_LENGTH*CHAR_X_SIZE
		endif
		
		if current_x>x2-CHAR_X_SIZE
			current_x=x1
			current_y+=CHAR_Y_SIZE
		endif
		if current_y>y2-CHAR_Y_SIZE
			exit
		endif
		if [u8]#str >= ' '
			plot_char_buffer(buffer, res_x, res_y, current_x, current_y, color, [u8]#str)
		endif
		str++
		current_x+=CHAR_X_SIZE
	endwhile
endfunction


function print_console_buffer: ptr buffer, u32 res_x, u32 res_y, u32 x1, u32 y1, u32 x2, u32 y2, ptr console_buffer
	
	x1++
	y1++
	
	u32 current_x=x1
	u32 current_y=y1
	u32 char_x=0
	u32 char_y=0
	u32 i=0
	u32 color=COLOR_BLACK
	u8 previous_console_attr = 0
	!raw_cls()
	while i < CONSOLE_VIDEO_MEM_SIZE
		if previous_console_attr =/= [u8]#(console_buffer+1)
			previous_console_attr = [u8]#(console_buffer+1)
			color = get_24bit_color_from_console(previous_console_attr)
		endif
		
		if current_x>x2-CHAR_X_SIZE+1 || char_x>=80
			current_x=x1
			char_x=0
			current_y+=CHAR_Y_SIZE
			char_y++
		endif
		if current_y>y2-CHAR_Y_SIZE || char_y>=25
			exit
		endif
		if [u8]#console_buffer > ' '
			plot_char_buffer(buffer, res_x, res_y, current_x, current_y, color, [u8]#console_buffer)


		endif
		current_x+=CHAR_X_SIZE
		char_x++
		console_buffer+=2
		i+=2

	endwhile
endfunction

! return the coord to center a string
function get_centered_text_coord: ptr str, u32 coord
	return coord-(strlen(str)*CHAR_X_SIZE/2)
endfunction

! return the coord to left align a string
function get_left_aligned_text_coord: ptr str, u32 coord
	return coord-(strlen(str)*CHAR_X_SIZE)
endfunction

function str_get_size_px: ptr str
	return strlen(str)*CHAR_X_SIZE
endfunction
