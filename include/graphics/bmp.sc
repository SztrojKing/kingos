/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/

defpackedstruct bmpFileHeader
	u16 type
	u32 _size
	u16 reserved1
	u16 reserved2
	u32 offset
endstruct

defpackedstruct bmpInfoHeader
	u32 infoSize
	u32 width
	!u32 width2 ! high 64 bit width
	u32 height
	!u32 height2
	u16 planes
	u16 bpp
	u32 compression
	u32 imageSize
	! total size of this struct is 40 b
endstruct

function bmp_read: ptr path
	ptr file = fat12_read_file(path)
	ptr res = null
	if file =/= null
		struct bmpFileHeader fileHeader = file
		! check it's a bmp
		if #(fileHeader.type) == 0x4d42
			res = bmp_convert_to_bpp(file, vbpp)
			!free(file)
		else
			fatal_error("bmp_read(): trying to read non bmp file")
		endif
	endif
	return res
endfunction

function bmp_draw: ptr bmp, u32 x, u32 y
	struct bmpInfoHeader bmpInfo = bmp+14
	u32 current_height=0
	u32 width = [u32](#bmpInfo.width)
	u32 height = [u32](#bmpInfo.height)
	ptr vram_ptr = vram + (x+(y+height-1)*vres_x)*vbytepp
	u32 bmp_line_size = width*vbytepp
	ptr bmp_px = bmpInfo + 40
	u32 vram_offset = vres_x*vbytepp
	if bmp == null || x + width >= vres_x || y + height >= vres_y || width*[u16]#(bmpInfo.bpp)/4 % vbytepp =/= 0
		fatal_error("BMP_DRAW")
	endif
	while current_height<height
		memcpy(vram_ptr, bmp_px, width*vbytepp)
		vram_ptr -= vram_offset
		bmp_px += bmp_line_size
		current_height++
	endwhile
endfunction

function bmp_convert_to_bpp: ptr bmp, u16 bpp
	struct bmpInfoHeader bmpInfo = bmp+14
	u32 width = [u32](#bmpInfo.width)
	u32 height = [u32](#bmpInfo.height)
	u32 bmp_line_size = width*vbytepp
	ptr bmp_px = [ptr](bmpInfo + 40)
	
	ptr res_bmp = malloc(54+width*height*bpp/4)

	memcpy(res_bmp, bmp, 54) ! copy header
	struct bmpInfoHeader res_bmpInfo = res_bmp+14
	ptr res_bmp_px =  res_bmpInfo+40
	#(res_bmpInfo.bpp) = [u16](bpp)

	u32 i = 0

	! if images have same bpp
	if [u16]#(bmpInfo.bpp) == bpp
		memcpy(res_bmp_px, bmp_px, width*height*bpp/4)
	else
		ptr res_offset = res_bmp_px
		ptr src_offset = bmp_px
		
		! convert 24 to 32
		if bpp == 32
			while i < width*height

				
				#(res_offset) = [u8]#(src_offset)
				#(res_offset+1) = [u8]#(src_offset+1)
				#(res_offset+2) = [u8]#(src_offset+2)
				#(res_offset+3) = [u8](0)
				
				res_offset+=4
				src_offset+=3
				i++
			endwhile
		
		else
			!  32 to 24
			if bpp == 24
				while i < width*height
					
					#(res_offset) = [u8]#(src_offset)
					#(res_offset+1) = [u8]#(src_offset+1)
					#(res_offset+2) = [u8]#(src_offset+2)
					
					res_offset+=3
					src_offset+=4
				
					i++
				endwhile
			else
				fatal_error("bmp_convert_to_bpp(): cannot convert to requested bpp")
			endif
		endif
	endif
	

	
	return res_bmp
endfunction
