/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/



function plot_char: u32 x, u32 y, u32 color, u8 char
	plot_char_buffer(vram, vres_x, vres_y, x, y, color, char)
endfunction

function plot_char_buffer: ptr buffer, u32 res_x, u32 res_y, u32 x, u32 y, u32 color, u8 char
	u32 current_y=0
	u32 current_x
	u8 i
	
	if x+8>res_x || y+8>res_y
		exit
	endif
	
	struct _pixel24 pix24=[ptr](buffer+((((current_y+y)*res_x)+(x))*vbytepp))
	if char>' '
		while current_y < 8
			current_x=7
			i=0
			u8 current_line_bitmap = [u8]#(current_font+(char*8)+current_y)
			while i < 8
				if (current_line_bitmap >> current_x) & 0x01 == 1
					#pix24.red=[u8]color
					#pix24.green=[u8](color>>8)
					#pix24.blue=[u8](color>>16)
					
				endif
				pix24 += vbytepp
				current_x--
				i++
			endwhile
			pix24+=(res_x-8)*vbytepp
			current_y++
		endwhile
	endif
endfunction

function plot_pixel: u32 x, u32 y, u32 color
	if x>vres_x || y>vres_y
		exit
	endif
	struct _pixel24 pix24=[ptr](vram+(((y*vres_x)+x)*vbytepp))
	#pix24.red=[u8]color
	#pix24.green=[u8](color>>8)
	#pix24.blue=[u8](color>>16)
endfunction

function plot_pixel_buffer: ptr buffer, u32 res_x, u32 res_y, u32 x, u32 y, u32 color
	if x>res_x || y>res_y
		exit
	endif
	struct _pixel24 pix24=[ptr](buffer+(((y*res_x)+x)*vbytepp))
	#pix24.red=[u8]color
	#pix24.green=[u8](color>>8)
	#pix24.blue=[u8](color>>16)
endfunction
