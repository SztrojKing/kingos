/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/
function draw_horizontal_line: u32 x1, u32 y, u32 x2, u32 color
	u32 tmp=0
	struct _pixel32 pix32
	if x1 > x2
		tmp = x1
		x1 = x2
		x2 = tmp
	endif
	if x2>=vres_x || y >= vres_y
		exit
	endif
	
	pix32=[ptr](vram+(((y*vres_x)+x1)*vbytepp))
	
	if vbpp == 32
		cpu_memset32(pix32, color, x2-x1)
	else
		u8 red = [u8]color
		u8 green=[u8](color>>8)
		u8 blue=[u8](color>>16)
		
		while x1<=x2
			#pix32.red=[u8]red
			#pix32.green=[u8]green
			#pix32.blue=[u8]blue
			x1++
			pix32+=vbytepp
		endwhile
	endif
endfunction

function draw_horizontal_line_buffer: ptr buffer, u32 res_x, u32 res_y, u32 x1, u32 y, u32 x2, u32 color
	u32 tmp=0
	struct _pixel32 pix32
	if x1 > x2
		tmp = x1
		x1 = x2
		x2 = tmp
	endif
	if x2>res_x || y >= res_y
		exit
	endif
	
	pix32=[ptr](buffer+(((y*res_x)+x1)*vbytepp))
	
	if vbpp == 32
		cpu_memset32(pix32, color, x2-x1)
	else
		u8 red = [u8]color
		u8 green=[u8](color>>8)
		u8 blue=[u8](color>>16)
		
		while x1<=x2
			#pix32.red=[u8]red
			#pix32.green=[u8]green
			#pix32.blue=[u8]blue
			x1++
			pix32+=vbytepp
		endwhile
	endif
endfunction


function draw_vertical_line: u32 x, u32 y1, u32 y2, u32 color
	u32 tmp=0
	struct _pixel32 pix32
	if y1 > y2
		tmp = y1
		y1 = y2
		y2 = tmp
	endif
	if x>=vres_x || y2 >= vres_y
		exit
	endif
	
	pix32=[ptr](vram+(((y1*vres_x)+x)*vbytepp))
	tmp = vres_x*vbytepp
	
		
	u8 red = [u8]color
	u8 green=[u8](color>>8)
	u8 blue=[u8](color>>16)
		
	while y1<y2
			#pix32.red=[u8]red
			#pix32.green=[u8]green
			#pix32.blue=[u8]blue
		y1++
		pix32+=tmp
	endwhile
endfunction

function draw_vertical_line_buffer: ptr buffer, u32 res_x, u32 res_y, u32 x, u32 y1, u32 y2, u32 color
	u32 tmp=0
	struct _pixel32 pix32
	if y1 > y2
		tmp = y1
		y1 = y2
		y2 = tmp
	endif
	if x>=res_x || y2 >= res_y
		exit
	endif
	
	pix32=[ptr](buffer+(((y1*res_x)+x)*vbytepp))
	tmp = res_x*vbytepp
	
		
	u8 red = [u8]color
	u8 green=[u8](color>>8)
	u8 blue=[u8](color>>16)
		
	while y1<y2
			#pix32.red=[u8]red
			#pix32.green=[u8]green
			#pix32.blue=[u8]blue
		y1++
		pix32+=tmp
	endwhile
endfunction

function draw_rect: u32 x1, u32 y1, u32 x2, u32 height, u32 color
	u32 tmp=0
	if x1 > x2
		tmp = x1
		x1 = x2
		x2 = tmp
	endif
	
	! optimize for 32 bit color or not gray scale 24 bit color
	if vbpp == 32 || ([u8](color&0xff) =/= [u8]((color>>8)&0xff) || [u8](color&0xff) =/= [u8]((color>>16)&0xff)) || x2-x1<height
		! optimize
		if x2-x1>=height
			! rectangle like this
			/*	*****************
				*****************
				***************** */
			tmp = 0
			while tmp<height
				draw_horizontal_line(x1, y1+tmp, x2, color)
				tmp++
			endwhile
		else
			! rectangle like this
			/*	***
				***
				***
				***
				*** */
			while x1<x2
				draw_vertical_line(x1, y1,y1+height, color)
				x1++
			endwhile
		endif
	! optimize for 24 bit gray scale
	else
	! optimize
			! rectangle like this
			/*	*****************
				*****************
				***************** */
			tmp = 0
			u32 gray_color = [u32]((color<<8) | color)
			ptr vram_ptr = vram+(x1+y1*vres_x)*3
			u32 res_x_offset = vres_x*3
			u32 size = (x2-x1)*3
			
			while tmp<height
				!draw_horizontal_line(x1, y1+tmp, x2, color)
				memset(vram_ptr, gray_color, size)
				vram_ptr+=res_x_offset
				tmp++
			endwhile
	endif
endfunction

function draw_rect_buffer: ptr buffer, u32 res_x, u32 res_y, u32 x1, u32 y1, u32 x2, u32 height, u32 color
	u32 tmp=0
	if x1 > x2
		tmp = x1
		x1 = x2
		x2 = tmp
	endif
	
	! optimize for 32 bit color or not gray scale 24 bit color
	if vbpp == 32 || ([u8](color&0xff) =/= [u8]((color>>8)&0xff) || [u8](color&0xff) =/= [u8]((color>>16)&0xff)) || x2-x1<height
		! optimize
		if x2-x1>=height
			! rectangle like this
			/*	*****************
				*****************
				***************** */
			tmp = 0
			while tmp<height
				draw_horizontal_line_buffer(buffer, res_x, res_y, x1, y1+tmp, x2, color)
				tmp++
			endwhile
		else
			! rectangle like this
			/*	***
				***
				***
				***
				*** */
			while x1<x2
				draw_vertical_line_buffer(buffer, res_x, res_y, x1, y1,y1+height, color)
				x1++
			endwhile
		endif
	! optimize for 24 bit gray scale
	else
	! optimize
			! rectangle like this
			/*	*****************
				*****************
				***************** */
			tmp = 0
			u32 gray_color = [u32]((color<<8) | color)
			ptr vram_ptr = buffer+(x1+y1*res_x)*3
			u32 res_x_offset = res_x*3
			u32 size = (x2-x1)*3
			
			while tmp<height
				!draw_horizontal_line(x1, y1+tmp, x2, color)
				memset(vram_ptr, gray_color, size)
				vram_ptr+=res_x_offset
				tmp++
			endwhile
	endif
endfunction

function draw_rect_direct_draw: u32 x1, u32 y, u32 x2, u32 height, u32 color
	u32 tmp=0

	while tmp<height
		draw_horizontal_line_buffer(hardware_vram, vres_x, vres_y, x1, y+tmp, x2, color)
		tmp++
	endwhile
endfunction

function draw_filled_circle: u32 origin_x, u32 origin_y, u32 radius, u32 color
	u32 x=0
	u32 y=0
	u32 double_radius = 2 * radius
	u32 radiusXradius = radius*radius
	while y<=double_radius
		x=0
		while x<double_radius

				if (x)*(x) + (y-radius)*(y-radius) <= radiusXradius
					plot_pixel(origin_x-x, origin_y+y-radius, color)
				endif
				if (x-radius)*(x-radius) + (y-radius)*(y-radius) <= radiusXradius
					plot_pixel(origin_x+x-radius, origin_y+y-radius, color)
				endif
				if (x-radius)*(x-radius) + (y-radius)*(y-radius) <= radiusXradius
					plot_pixel(origin_x+x-radius, origin_y+y-radius, color)
				endif
				if (x-radius)*(x-radius) + (y-radius)*(y-radius) <= radiusXradius
					plot_pixel(origin_x+x-radius, origin_y+y-radius, color)
				endif
			x++
		endwhile
		y++
	endwhile
endfunction

! draw unfilled rectangle
function draw_rect_border: u32 x1, u32 y1, u32 x2, u32 y2, u32 border_size, u32 color

	if border_size>0
		draw_horizontal_line(x1, y1, x2, color)
		draw_horizontal_line(x1, y2, x2, color)
		draw_vertical_line(x1, y1, y2, color)
		draw_vertical_line(x2, y1, y2, color)
		
		draw_rect_border(x1+1, y1+1, x2-1, y2-1, border_size-1, color)
	endif
endfunction

! draw unfilled rectangle
function draw_rect_border_buffer: ptr buffer, u32 res_x, u32 res_y, u32 x1, u32 y1, u32 x2, u32 y2, u32 border_size, u32 color

	if border_size>0
		draw_horizontal_line_buffer(buffer, res_x, res_y, x1, y1, x2, color)
		draw_horizontal_line_buffer(buffer, res_x, res_y, x1, y2, x2, color)
		draw_vertical_line_buffer(buffer, res_x, res_y, x1, y1, y2, color)
		draw_vertical_line_buffer(buffer, res_x, res_y, x2, y1, y2, color)
		
		draw_rect_border_buffer(buffer, res_x, res_y, x1+1, y1+1, x2-1, y2-1, border_size-1, color)
	endif
endfunction

! fill a buffer background
function fill_buffer: ptr buffer, u32 res_x, u32 res_y, u32 color
	if vbpp == 32
		memset(buffer, color, res_x*res_y*vbytepp)
	else
		u32 i=0
		while i<res_y
			draw_horizontal_line_buffer(buffer, res_x, res_y, 0, i, res_x-1, color)
			i++
		endwhile
	endif
endfunction

