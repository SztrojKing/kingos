/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/

enum VIDEO_GRAPHICAL, VIDEO_TEXT

sptr current_font=null
sptr vram=null
sptr previous_frame_vram=null
sptr previous_frame_vram_static=null
sptr hardware_vram=null
su32 vres_x=0
su32 vres_y=0
su32 vbpp=32
su32 vbytepp=4
su32 vram_size=0
su32 virtual_vres_x=0
su32 virtual_vres_y=0
su32 next_redraw_type=NEXT_REDRAW_TYPE_REFRESH_ALL
su32 fps=0
su32 video_mode=VIDEO_TEXT


function get_video_info
	struct video_info_struct info = malloc(video_info_struct.size)
	#(info.res_x) = [u32](vres_x)
	#(info.res_y) = [u32](vres_y)
	#(info.bpp) = [u32](vbpp)
	#(info.bytepp) = [u32](vbytepp)
	#(info.vram) = [u32](vram)
	#(info.hardware_vram) = [u32](hardware_vram)
	#(info.console_buffer) = [ptr](CONSOLE_VIDEO_MEM)
	#(info.current_font) = [ptr](current_font)
	return info
endfunction

function switch_to_video_mode: u32 mode
	
	if mode == 0
		return false
	endif
	int16(0x10, 0x4F02, mode, 0, 0, 0, 0, 0)
	video_mode=VIDEO_GRAPHICAL
	paging_map_area(kernel_page_directory, hardware_vram, hardware_vram, 3, vram_size)
	return true
endfunction

function switch_to_text_mode
	if video_mode =/= VIDEO_TEXT
		video_mode=VIDEO_TEXT
		int16(0x10, 0x0003, 0, 0, 0, 0, 0, 0)
		
		if kernel_console =/= null
			memcpy(CONSOLE_VIDEO_MEM, console_get_displayed_buffer(kernel_console), CONSOLE_VIDEO_MEM_SIZE)
			kernel_video_x = console_get_x(kernel_console)
			kernel_video_y = console_get_y(kernel_console)
			kernel_console=CONSOLE_VIDEO_MEM
		endif
	endif
endfunction

function init_graphical_mode: u16 res_x, u16 res_y, u16 bpp, ptr vram_address, u32 mode
	if vram_address == null
		return false
	endif
	vres_x=res_x
	vres_y=res_y
	virtual_vres_x=vres_x
	virtual_vres_y=vres_y
	vbpp = bpp
	vbytepp = bpp/8
	vram_size = (vres_x*vres_y*vbytepp)
	
	hardware_vram = vram_address
	!CONSOLE_VIDEO_MEM = malloc(CONSOLE_VIDEO_MEM_SIZE)
	! copy current console to allocated console
	!memcpy(CONSOLE_VIDEO_MEM, 0xb8000,CONSOLE_VIDEO_MEM_SIZE)
	current_font=fat12_read_file("root0/system/x86/font/default.fnt")
	if current_font == null
		return false
	endif
	
	
	switch_to_video_mode(mode)
	vram=malloc(vram_size)
	previous_frame_vram=malloc(vram_size)
	previous_frame_vram_static = previous_frame_vram
	return true
endfunction


! repaint the screen (repaint depends of next_redraw_type)
function refresh_screen
	if video_mode==VIDEO_GRAPHICAL
		su32 refresh_count=0
		su32 frame_count = 0
		su32 frame_count_ticks = 0
		!print_console_buffer(((vres_x/2)-320),vres_y/2-130,((vres_x/2)+319), (vres_y/2+120), CONSOLE_VIDEO_MEM)
		!
		! get the focused window
		struct window_struct top_win = [ptr]#(windows_stack)

		! refresh the entire screen every 30 frame
		if refresh_count%5 == 0
			next_redraw_type=NEXT_REDRAW_TYPE_REFRESH_ALL
		endif
		
		if keyboard_overflow==true
			u16 tmp = mouse_read(false)
			if tmp < 0xffff
				keyboard_add_input(tmp - 1)
			else
				keyboard_overflow=false
			endif
		endif
		
		windows_manager_mouse_parse_events()
		windows_manager_parse_keyboard_events()
		
		
		
		if next_redraw_type == NEXT_REDRAW_TYPE_NORMAL
			previous_frame_vram=vram
			! only refresh the content of the focused window
			windows_manager_draw()
		
			draw_cursor(hardware_vram) 
			
			/*
			if sse_enabled
				u32 offset = [u32]#(top_win.coord_y) * vres_x * vbytepp
				memcpy(hardware_vram+offset, vram+offset, vram_size-offset-((vres_y-([u32]#(top_win.coord_y)+[u32]#(top_win.height))) * vres_x * vbytepp))
			else
				refresh_hardware_screen([u32]#(top_win.coord_x), [u32]#(top_win.coord_y), [u32]#(top_win.width), [u32]#(top_win.height))
			endif
			*/
			
		else
		
			previous_frame_vram=vram
			vram=previous_frame_vram_static
			previous_frame_vram_static = previous_frame_vram
			
			! refresh everything
			raw_cls()
			log_system_stats()
			windows_manager_draw_all()
			draw_cursor(vram) 
			memcpy(hardware_vram, vram, vram_size)
			refresh_count=0
			
		endif
		
		!draw_cursor()
		
		refresh_count++
		frame_count++
		

		if time_ticks - frame_count_ticks >= 1000
			fps = frame_count
			frame_count=0
			frame_count_ticks = time_ticks
		endif
		next_redraw_type=NEXT_REDRAW_TYPE_NORMAL
	endif
endfunction

function refresh_hardware_screen: u32 x, u32 y, u32 width, u32 height

	height+=y
	if vbytepp == 4
		width=(width*4)
		while y<height
			memcpy(hardware_vram+(x+(y*vres_x))*4, vram+(x+(y*vres_x))*4, width)
			y++
		endwhile
	else
		width=(width*3)
		while y<height
			! video mem size is always multiple of 4, so their won't be buffer overflow
			memcpy(hardware_vram+(x+(y*vres_x))*3, vram+(x+(y*vres_x))*3, width)
			y++
		endwhile
	endif
endfunction



function save_rect_to_buffer: u32 x1, u32 y, u32 x2, u32 height, ptr buffer
	u32 tmp
	ptr address=null
	if x1 > x2
		tmp = x1
		x1 = x2
		x2 = tmp
	endif
	
	if x2>=vres_x || y+height >= vres_y
		exit
	endif
	while x1<x2
		tmp = 0
		while tmp<height
			if vbytepp == 3
				address=[ptr](previous_frame_vram+((((y+tmp)*vres_x)+x1)*vbytepp))
				#buffer=[u8]#(address)
				#(buffer+1)=[u8]#(address+1)
				#(buffer+2)=[u8]#(address+2)
				
			else
				#buffer=[u32]#(previous_frame_vram+((((y+tmp)*vres_x)+x1)*vbytepp))
			endif
			tmp++
			buffer+=vbytepp
		endwhile
		x1++
	endwhile
endfunction

function draw_rect_from_buffer: u32 x1, u32 y, u32 x2, u32 height, ptr buffer
	u32 tmp
	ptr address=null
	if x1 > x2
		tmp = x1
		x1 = x2
		x2 = tmp
	endif
	
	if x2>=vres_x || y+height >= vres_y
		exit
	endif
	while x1<x2
		tmp = 0
		while tmp<height
			if vbytepp == 3
				address=[ptr](vram+((((y+tmp)*vres_x)+x1)*vbytepp))
				#(address)=[u8]#buffer
				#(address+1)=[u8]#(buffer+1)
				#(address+2)=[u8]#(buffer+2)
				
			else
				#(vram+((((y+tmp)*vres_x)+x1)*vbytepp))=[u32]#buffer
			endif
			tmp++
			buffer+=vbytepp
		endwhile
		x1++
	endwhile
endfunction

function draw_rect_from_buffer_direct_draw: u32 x1, u32 y, u32 x2, u32 height, ptr buffer
	u32 tmp
	ptr address=null
	if x1 > x2
		tmp = x1
		x1 = x2
		x2 = tmp
	endif
	
	if x2>=vres_x || y+height >= vres_y
		exit
	endif
	while x1<x2
		tmp = 0
		while tmp<height
			if vbytepp == 3
				address=[ptr](hardware_vram+((((y+tmp)*vres_x)+x1)*vbytepp))
				#(address)=[u8]#buffer
				#(address+1)=[u8]#(buffer+1)
				#(address+2)=[u8]#(buffer+2)
				
			else
				#(hardware_vram+((((y+tmp)*vres_x)+x1)*vbytepp))=[u32]#buffer
			endif
			tmp++
			buffer+=vbytepp
		endwhile
		x1++
	endwhile
endfunction
