/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/

defpackedstruct _pixel24
	u8 red
	u8 green
	u8 blue
endstruct

defpackedstruct _pixel32
	u8 red
	u8 green
	u8 blue
	u8 alpha
endstruct

defstruct video_info_struct
	u32 res_x
	u32 res_y
	u32 bpp
	u32 bytepp
	u32 vram
	u32 hardware_vram
	ptr console_buffer
	ptr current_font
endstruct
