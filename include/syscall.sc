/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/

su32 inSyscall=false


function do_syscall: u32 syscall_id, u32 reg_ebx, u32 reg_ecx, u32 reg_edx
	if inSyscall == true
		fatal_error("DO_SYSCALL() : already in syscall\n")
	endif
	inSyscall=true

	! save current_page_directory
	ptr current_task_paging_directory = current_page_directory

	! set kernel directory in order to access everything (don't update current_page_directory field)
	paging_set_tables_raw(kernel_page_directory)

	u32 res = 0
	
	switch syscall_id
		case SYSCALL_MEMSET
			memset(reg_ebx, reg_ecx, reg_edx)
		endcase
	
		case SYSCALL_MEMCPY
			memcpy(reg_ebx, reg_ecx, reg_edx)
		endcase
		
		case SYSCALL_CLS
			cls()
		endcase
		
		case SYSCALL_LOG
			res = log(reg_ebx)
		endcase
		
		case SYSCALL_LOGN
			res = logn(reg_ebx)
		endcase
			
		case SYSCALL_LOGX
			res = logx(reg_ebx)
		endcase
		
		case SYSCALL_TASK_KILL
			res = multitask_kill(reg_ebx)
		endcase
		
		case SYSCALL_EXEC
			res = exec(reg_ebx, reg_ecx)
		endcase
		
		case SYSCALL_GET_TIME_TICKS
			res=[u32](time_ticks)
		endcase
		
		case SYSCALL_SHEDULE_LATER
			multitask_shedule_later()
		endcase
		
		case SYSCALL_REFRESH_SCREEN 
			refresh_screen()
		endcase
		
		case SYSCALL_GET_VIDEO_INFO
			res = [ptr](get_video_info())
		endcase
		
		case SYSCALL_FREE
			free(reg_ebx)
		endcase
		
		case SYSCALL_MALLOC
			res = malloc(reg_ebx)
		endcase
		
		case SYSCALL_WINDOW_CREATE
			res = window_create(reg_ebx, reg_ecx, reg_edx)
		endcase
		
		case SYSCALL_RAND
			res = rand(reg_ebx, reg_ecx)
		endcase
		
		case SYSCALL_BUTTON_CREATE
			res = button_create(object_list_get_at(reg_ebx, 0), object_list_get_at(reg_ebx, 1), object_list_get_at(reg_ebx, 2), object_list_get_at(reg_ebx, 3), object_list_get_at(reg_ebx, 4))
		endcase
		
		case SYSCALL_TEXTBOX_CREATE
			res = textbox_create(object_list_get_at(reg_ebx, 0), object_list_get_at(reg_ebx, 1), object_list_get_at(reg_ebx, 2), object_list_get_at(reg_ebx, 3), object_list_get_at(reg_ebx, 4), object_list_get_at(reg_ebx, 5))
		endcase
		
		case SYSCALL_LABEL_CREATE
			res = label_create(object_list_get_at(reg_ebx, 0), object_list_get_at(reg_ebx, 1), object_list_get_at(reg_ebx, 2), object_list_get_at(reg_ebx, 3), object_list_get_at(reg_ebx, 4), object_list_get_at(reg_ebx, 5))
		endcase

		case SYSCALL_LABEL_SET_TEXT
			res = label_set_text(reg_ebx, reg_ecx)
		endcase
		
		case SYSCALL_WINDOW_LOCK
			res = window_lock(reg_ebx, reg_ecx)
		endcase
		
		case SYSCALL_WINDOWS_MANAGER_ADD_WINDOW

			res = windows_manager_add_window(reg_ebx, current_pid)
		endcase
		
		case SYSCALL_WINDOWS_MANAGER_SET_FOCUS
			res = windows_manager_set_focus(reg_ebx)
		endcase
		
		case SYSCALL_WINDOW_GET_LAST_EVENT
			res = window_get_last_event(reg_ebx)
		endcase
		
		case SYSCALL_WINDOW_REMOVE_WIDGET
			res = window_remove_widget(reg_ebx, reg_ecx)
		endcase
		
		case SYSCALL_WIDGET_FREE
			res = widget_free(reg_ebx)
		endcase
		
		case SYSCALL_WINDOW_DELETE_LAST_EVENT
			res = window_delete_last_event(reg_ebx)
		endcase
		
		case SYSCALL_WINDOWS_MANAGER_CLOSE_WINDOW
			res = windows_manager_close_window(reg_ebx)
		endcase
		case SYSCALL_WINDOW_FREE
			res = window_free(reg_ebx)
		endcase
		case SYSCALL_TEXTBOX_GET_TEXT
			res = textbox_get_text(reg_ebx)
		endcase
		case SYSCALL_WINDOW_EVENT_GET_WIDGET
			res = window_event_get_widget(reg_ebx)
		endcase
		case SYSCALL_WINDOW_EVENT_GET_CAT
			res = window_event_get_cat(reg_ebx)
		endcase
		case SYSCALL_WINDOW_ATTACH_PID
			res = window_attach_pid(reg_ebx, reg_ecx)
		endcase
		case SYSCALL_WINDOW_HIDE
			res = window_hide(reg_ebx)
		endcase
		case SYSCALL_GET_CONSOLE_FROM_PID
			res = multitask_get_console(reg_ebx)
		endcase
		case SYSCALL_FATAL_ERROR
			res = fatal_error(reg_ebx)
		endcase

		case SYSCALL_READ_FILE
			res = fat12_read_file(reg_ebx)
		endcase
		
		case SYSCALL_GET_FILE_SIZE
			res = fat12_get_file_size(reg_ebx)
		endcase
		case SYSCALL_WRITE_FILE
			res = fat12_write_file(reg_ebx, reg_ecx, reg_edx)
		endcase
		
		case SYSCALL_CREATE_DIRECTORY
			res = fat12_create_directory(reg_ebx)
		endcase
		
		case SYSCALL_READ_DISK_RAW
			res = read_disk_raw(object_list_get_at(reg_ebx, 0), object_list_get_at(reg_ebx, 1), object_list_get_at(reg_ebx, 2), object_list_get_at(reg_ebx, 3))
		endcase
		case SYSCALL_WRITE_DISK_RAW
			res = write_disk_raw(object_list_get_at(reg_ebx, 0), object_list_get_at(reg_ebx, 1), object_list_get_at(reg_ebx, 2), object_list_get_at(reg_ebx, 3))
		endcase
		
		case SYSCALL_TEXTBOX_SET_TEXT
			res = textbox_set_text(reg_ebx, reg_ecx)
		endcase
		
		case SYSCALL_WINDOW_FREE_CONTENT
			res = window_free_content(reg_ebx)
		endcase
		
		case SYSCALL_TEXTBOX_SET_COLOR
			res = textbox_set_color(reg_ebx, reg_ecx)
		endcase
		
		case SYSCALL_BUTTON_SET_COLOR
			res = button_set_color(reg_ebx, reg_ecx)
		endcase
		
		case SYSCALL_BUTTON_SET_BACKGROUND_COLOR
			res = button_set_background_color(reg_ebx, reg_ecx)
		endcase
		
		case SYSCALL_VSCROLL_CREATE
			res = vscroll_create(object_list_get_at(reg_ebx, 0), object_list_get_at(reg_ebx, 1), object_list_get_at(reg_ebx, 2), object_list_get_at(reg_ebx, 3), object_list_get_at(reg_ebx, 4))
		endcase
		
		case SYSCALL_CONTAINER_GET_CONTAINER
			res = container_get_container(reg_ebx)
		endcase
		
		case SYSCALL_SLEEP
			multitask_sleep(current_pid, reg_ebx)
		endcase
		
		case SYSCALL_REBOOT
			reboot()
		endcase
		
		case SYSCALL_SHUTDOWN
			shutdown()
		endcase
	
		case default
			fatal_error("Unknown syscall")
		endcase
	endswitch


	paging_set_tables(current_task_paging_directory)

	inSyscall=false
	return res
endfunction
