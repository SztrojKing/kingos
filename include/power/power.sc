/*********************************
* This file is a part of King OS *
*  written by LANEZ Gaël - 2018  *
*********************************/

! TODO: Implement shutdown
function shutdown
	current_pid=0
	fatal_error("Veuillez eteindre le PC manuellement\n")
endfunction


function reboot

    u8 good = 0x02
    while good & 0x02 > 0
        good = inb(0x64)
	endwhile
	
	while 1
		outb(0x64, 0xFE)

		asm hlt
	endwhile
	
endfunction
