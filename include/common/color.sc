/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/

define COLOR_WHITE 0xffffff
define COLOR_BLACK 0
define COLOR_LIGHT_GREY 0xaaaaaa
define COLOR_RED 0xff0000
define COLOR_GREEN 0x00ff00
define COLOR_BLUE 0x0000ff
define COLOR_DARK_GREY 0x555555