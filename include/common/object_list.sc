/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/

define OBJECT_LIST_MAGIC {[uint32_t](0x589fe90d)}
defstruct object_list_struct
	u32 magic
	u32 max_size
	u32 current_size
	ptr data
endstruct

function object_list_create: u32 max_size
	struct object_list_struct list = malloc(object_list_struct.size)
	#list.magic=[u32](OBJECT_LIST_MAGIC)
	#list.max_size=max_size
	#list.data = [ptr](malloc(max_size*SIZEOF_PTR))
	return list
endfunction

! return the reference to the object
function object_list_get_at: ptr hlist, u32 pos
	struct object_list_struct list=hlist

	object_list_test_integrity(hlist)
	
	if pos>=#list.max_size
		fatal_error("object_list_get_at() : id>=#list.max_size")
	endif
	
	return [ptr]#((#list.data)+pos*SIZEOF_PTR)
endfunction

! add an object at the end of list, return its position
function object_list_add: ptr hlist, ptr obj
	struct object_list_struct list=hlist
	u32 current_size
	
	object_list_test_integrity(hlist)
	
	current_size = [u32](#list.current_size)
	if current_size>=[u32](#list.max_size)
		fatal_error("object_list_add() : current_size>=#list.max_size")
	endif

	#((#list.data)+current_size*SIZEOF_PTR) = [ptr](obj)

	#list.current_size = [u32](current_size+1)
	
	return current_size
endfunction

! add an object in the list at the specified position, return its position
function object_list_add_at: ptr hlist, ptr obj, ptr pos
	struct object_list_struct list=hlist
	u32 current_size = #list.current_size
	
	object_list_test_integrity(hlist)
	
	if current_size>=[u32](#list.max_size)
		fatal_error("object_list_add_at() : current_size>=#list.max_size")
	endif
	
	if pos>=#list.max_size
		fatal_error("object_list_add_at() : id>=#list.max_size")
	endif
	
	if pos < #list.current_size
		memcpy_from_end(#list.data + pos*SIZEOF_PTR + SIZEOF_PTR, #list.data + pos*SIZEOF_PTR , (#list.max_size-pos-1)*SIZEOF_PTR)
	endif
	
	#((#list.data)+pos*SIZEOF_PTR) = [ptr](obj)
	#list.current_size = [u32](current_size+1)
	
	return current_size
endfunction

! retrieve the position of the object in the list, return #list.max_size on error
function object_list_get_position: ptr hlist, ptr obj
	u32 i = 0
	struct object_list_struct list = hlist
	object_list_test_integrity(hlist)
	while i < #list.max_size
		if object_list_get_at(list, i) == obj
			return i
		endif
		i++
	endwhile
	return i
endfunction

! remove an object from the list
function object_list_remove_at: ptr hlist, ptr pos
	struct object_list_struct list = hlist
	object_list_test_integrity(hlist)
		
	if pos>=#list.max_size
		fatal_error("object_list_remove_at() : id>=#list.current")
	endif
	
	if pos<=#list.max_size
		memcpy(#list.data + pos*SIZEOF_PTR, #list.data + pos*SIZEOF_PTR + SIZEOF_PTR, (#list.max_size-pos-1)*SIZEOF_PTR)

		#list.current_size = [u32](#list.current_size-1)
		#(#list.data + (#list.current_size*SIZEOF_PTR)) = [u32](null)
	endif
endfunction

! remove all occurences of an object in the list
function object_list_remove_object: ptr hlist, ptr obj
	u32 pos = object_list_get_position(hlist, obj)
	struct object_list_struct list = hlist
	
	! remove until the object can't be found
	while pos < #list.max_size
		object_list_remove_at(list, pos)
		pos = object_list_get_position(hlist, obj)
	endwhile
	
endfunction


function object_list_get_size: ptr hlist
	struct object_list_struct list = hlist
	object_list_test_integrity(hlist)
	return [u32]#list.current_size
endfunction

! test the magic number of a list
function object_list_test_integrity: ptr hlist
	struct object_list_struct list=hlist
	if #list.magic =/= OBJECT_LIST_MAGIC
		fatal_error("object_list_test_integrity() : #list.magic =/= OBJECT_LIST_MAGIC")
	endif
endfunction