/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/



define bpbRootEntries 224
define FILE_NAME_SIZE 11
define PATH_NEXT_ENTRY_EOP 0xff

define FAT12_READ_ONLY 0x01 
define FAT12_HIDDEN 0x02 
define FAT12_SYSTEM 0x04 
define FAT12_VOLUME_ID 0x08 
define FAT12_DIRECTORY 0x10 
define FAT12_ARCHIVE 0x20

defstruct filedescriptor_struct
	u8 attributes
	ptr name
	u32 file_size
endstruct

! returns an object_list of filedescriptor_struct containing all the files of a directory
function directory_list: ptr path
	ptr currentDirBuffer = read_file(path)
	ptr res_list = null
	u32 entry_left = bpbRootEntries
	struct filedescriptor_struct file_desc
	u32 offset=0
	
	if currentDirBuffer =/= null
		res_list = object_list_create(bpbRootEntries)
	
		! read every entry
		while entry_left > 0
			
			! first byte = 0 -> no more files
			if [uint8_t]#(currentDirBuffer+offset+1) == 0
				entry_left = 0
			else
				! first byte = 0xE5 -> file has been deleted and not long file name entry ?
				if [uint8_t]#(currentDirBuffer+offset+1) =/= 0xE5 && [uint8_t]#(currentDirBuffer+offset+11) =/= 0x0f
						! add to gui
						
						file_desc = malloc(filedescriptor_struct.size)
						#file_desc.name = [ptr](malloc(FILE_NAME_SIZE+1))
						
						! copy current file info
						memcpy(#file_desc.name, currentDirBuffer+offset, FILE_NAME_SIZE)
						#file_desc.attributes = [u8](#(currentDirBuffer+offset+0x0b))
						#file_desc.file_size = [u32](#(currentDirBuffer+offset+0x1c))
						! add the file to the list
						object_list_add(res_list, file_desc)
					
				endif
				offset = offset + 32
				entry_left--
			endif
		endwhile
	endif
	return res_list
endfunction