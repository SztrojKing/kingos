/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/

! shared defines between kernel and apps
enum MOUSE_EVENT, KEYBOARD_EVENT, SIGNAL_EVENT_KILL

define MAX_TASKS 32