/*********************************
* This file is a part of King OS *
*  written by LANEZ Gaël - 2018  *
*********************************/

! compute cpu usage for all processes (automatically called every second)
function cpu_usage_second_elapsed
	struct _task task = [u32]#(tasks)
	u32 pid=0
	
	while pid<MAX_TASKS
	
		task = [u32]#(tasks+pid*SIZEOF_PTR)
		
		if task =/= null
			#task.last_cpu_percentage = [u32](#task.cpu_ticks)/10
			#task.cpu_ticks = [u32](0)
		endif
		pid++
	endwhile
endfunction

! return cpu usage
function get_cpu_usage
	! idle process have pid 1
	struct _task idle_task = #(tasks+SIZEOF_PTR)
	return 100-([u32]#idle_task.last_cpu_percentage)
endfunction
