/*********************************
* This file is a part of King OS *
*  written by LANEZ Gaël - 2018  *
*********************************/

su32 seed=1234567

function rand:u32 min, u32 max
	seed = 1103515245 * seed + 12345
	return seed % max + min
endfunction

function init_rand
	struct time_struct time = time_get_time()
	seed = #time.sec + (#time.min)*0xff  + (#time.min)*0xfff
	free(time)
endfunction
