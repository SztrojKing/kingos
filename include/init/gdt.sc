/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/

/* addr. physique ou doit resider la gdt */
define GDT_BASE 0xf800
/* nombre max. de descripteurs dans la table */
define GDT_SIZE 0xFF

define GDT_DESC_SIZE 8

define CODE32_SEGMENT 0x08
define DATA32_SEGMENT 0x10
define CODE16_SEGMENT 0x18
define DATA16_SEGMENT 0x20

sptr gdt_address

function init_gdt_desc:u32 base, u32 limite, u8 acces, u8 other, ptr desc
	#desc = [u16](limite & 0xffff)
	#(desc+2) = [u16](base & 0xffff)
	#(desc+4) = [u8]((base & 0xff0000) >> 16)
	#(desc+5) = [u8]acces
	#(desc+6) = [u8]((((limite >> 16))&0xf) | ((other<<4) & 0xf0))
	#(desc+7) = [u8]((base & 0xff000000) >> 24)
endfunction

function init_gdt
	gdt_address = GDT_BASE
	
	init_gdt_desc(0x0, 0x0, 0x0, 0x0, gdt_address)
	init_gdt_desc(0x0, 0xFFFFF, 0x9B, 0x0D, (gdt_address+1*GDT_DESC_SIZE))	/* 32 bit code */
	init_gdt_desc(0x0, 0xFFFFF, 0x93, 0x0D, (gdt_address+2*GDT_DESC_SIZE))	/* 32 bit data */
	init_gdt_desc(0x0, 0xFFFF, 0x9B, 0x01, (gdt_address+3*GDT_DESC_SIZE))	/* 16 bit code */
	init_gdt_desc(0x0, 0xFFFF, 0x93, 0x01, (gdt_address+4*GDT_DESC_SIZE))	/* 16 bit data */
	!init_gdt_desc(0x0, 0x0, 0x97, 0x0D, (gdt_address+3*GDT_DESC_SIZE))		/* stack */
	
	asm lgdt [kgdtr]
	
	ax = DATA32_SEGMENT	
    ds = ax
	es = ax
	fs = ax
	gs = ax
	asm jmp dword CODE32_SEGMENT:.next
	asm .next:
endfunction

asm kgdtr: 
asm		.kgdtr_limite: dw GDT_SIZE*GDT_DESC_SIZE
	! base (u32)
asm		.kgdtr_base: dd GDT_BASE
