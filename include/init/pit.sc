/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/

function init_pit:u32 hz
	
!u32 divisor = 1193180 / hz   /* Calculate our divisor */
	u32 divisor = [u32](1193182/hz)
    outb(0x43, 0x34)            /* Set our command byte 0x34 */
	wait_a_bit()
    outb(0x40, [u8](divisor & 0xFF))   /* Set low byte of divisor */
	wait_a_bit()
    outb(0x40, [u8](divisor >> 8))     /* Set high byte of divisor */

endfunction

function wait_a_bit
	u32 i=0
	while i<0xfffff
		i++
	endwhile
endfunction