/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/

/* addr. physique ou doit resider la IDT */
define IDTBASE 0x900000
/* nombre max. de descripteurs dans la table */
define IDTSIZE 0xFF
/* utilise pour gerer les interruptions */
define INTGATE  0x8E00
/* utilise pour faire des appels systemes */	
define TRAPGATE 0xEF00

sptr idt_address=0

function init_idt_desc: u16 select, u32 offset, u16 type, ptr desc
	offset = offset + kernel_address
	eax = offset
	asm sub eax, $$
	offset = eax
	#(desc) = [u16](offset & 0xffff)
	#(desc+2) = [u16]select
	#(desc+4) = [u16]type
	#(desc+6) = [u16]((offset>> 16))
endfunction

function init_idt
	idt_address = IDTBASE
	u32 i=0

	/* Initialisation des descripteurs systeme par defaut */
	while i < IDTSIZE
		init_idt_desc(0x08, function int_default, INTGATE, (idt_address+i*8))
		i++
	endwhile
	
	/* Les vecteurs 0 -> 31 sont reserves pour les exceptions */
	init_idt_desc(0x08, function int_gp, INTGATE, (idt_address+(13*8)))	/* #GP */
	init_idt_desc(0x08, function int_pf, INTGATE, (idt_address+(14*8)))  /* #PF */

	/* IRQ */
	init_idt_desc(0x08, function irq0x0, INTGATE, (idt_address+(32*8)))	/* horloge */
	init_idt_desc(0x08, function irq0x1, INTGATE, (idt_address+(33*8)))	/* clavier */
	asm ; clavier
	init_idt_desc(0x08, function irq0x2, INTGATE, (idt_address+(34*8)))
	init_idt_desc(0x08, function irq0x3, INTGATE, (idt_address+(35*8)))
	init_idt_desc(0x08, function irq0x4, INTGATE, (idt_address+(36*8)))
	init_idt_desc(0x08, function irq0x5, INTGATE, (idt_address+(37*8)))
	init_idt_desc(0x08, function irq0x6, INTGATE, (idt_address+(38*8)))
	init_idt_desc(0x08, function irq0x7, INTGATE, (idt_address+(39*8)))
	init_idt_desc(0x08, function irq0x8, INTGATE, (idt_address+(40*8)))
	init_idt_desc(0x08, function irq0x9, INTGATE, (idt_address+(41*8)))
	init_idt_desc(0x08, function irq0xA, INTGATE, (idt_address+(42*8)))
	init_idt_desc(0x08, function irq0xB, INTGATE, (idt_address+(43*8)))
	init_idt_desc(0x08, function irq0xC, INTGATE, (idt_address+(44*8)))
	init_idt_desc(0x08, function irq0xD, INTGATE, (idt_address+(45*8)))
	init_idt_desc(0x08, function irq0xE, INTGATE, (idt_address+(46*8)))
	init_idt_desc(0x08, function irq0xF, INTGATE, (idt_address+(47*8)))

	init_idt_desc(0x08, function syscall_handler, TRAPGATE, (idt_address+(48*8))) /* syscalls */
	
	/* Chargement du registre IDTR */
	asm lidt [idt_addressr]
	
endfunction

! registre idt
	! limite (u16)
asm idt_addressr: 
asm		.idt_addressr_limite: dw IDTSIZE*8
	! base (u32)
asm		.idt_addressr_base: 	dd IDTBASE