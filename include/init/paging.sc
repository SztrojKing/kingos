/*********************************
* This file is a part of King OS *
*  written by LANEZ Gaël - 2018  *
*********************************/
define PAGING_PAGES_SIZE {(4096*1025)}
define PAGING_FLAG_PRESENT 3
define PAGING_FLAG_NOT_PRESENT 0

sptr kernel_page_directory
sptr current_page_directory
su8 paging_enabled = false

function init_paging
	u32 page = 0
	
	kernel_page_directory = kernel_address + 10*1024*1024
	current_page_directory = kernel_page_directory

	! init directory
	paging_init_directory(kernel_page_directory)

	! map all memory
	paging_map_area(kernel_page_directory, 0, 0, PAGING_FLAG_PRESENT, 4*1024*1024*1024-4096)	
	
	! unmap NULL pointer
	paging_map(kernel_page_directory, 0,0,PAGING_FLAG_NOT_PRESENT)

	paging_enable()

	paging_enabled=true
endfunction

function paging_enable
	paging_set_tables(kernel_page_directory)
	asm mov eax, cr0
	asm or eax, 0x80000000
	asm mov cr0, eax
endfunction

function paging_set_tables: ptr tables
	current_page_directory = tables
	paging_set_tables_raw(tables)
endfunction

function paging_set_tables_raw: ptr tables
	eax = tables
	asm mov cr3, eax
endfunction

function paging_map: ptr directory, ptr virtual, ptr phys, u32 flags
	u32 page_directory_index = (virtual >> 22)
	u32 page_table_index = (virtual >> 12 ) 
	page_table_index = (page_table_index & 0x03FF)

	ptr page_table = directory + (4096 * (1+page_directory_index))
	#(page_table+page_table_index*4) = [ptr](phys | flags)
endfunction

function paging_map_area: ptr directory, ptr virtual, ptr phys, u32 flags, u32 length
	if length%4096>0
		length = length - (length%4096) + 4096
		!fatal_error("paging_map_area: length must be a multiple of 4096\n")
	endif

	! align to 4096
	virtual = virtual - (virtual%4096)
	phys = phys - (phys%4096)

	u32 i=0
	while i<length
		paging_map(directory, virtual+i, phys+i, flags)
		i+=4096
	endwhile
endfunction

function paging_flush_table
	asm mov eax, cr3
	asm mov cr3, eax
endfunction


! init a paging directory to all blocs not present
function paging_init_directory: ptr directory
	u32 page = 0
	! init page directory + all pages to 0 (not present)
	memset(directory, 0, PAGING_PAGES_SIZE)

	! create all pages
	while page < 1024
		#(directory+page*4) = [u32]((directory + (page+1) * 4096) | PAGING_FLAG_PRESENT)  ! tables are present but not data bloc
		page++
	endwhile
endfunction