/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/

define PIC1_CMD                    0x20
define PIC1_DATA                   0x21
define PIC2_CMD                    0xA0
define PIC2_DATA                   0xA1

function init_pic
	! remap les irq 0-15 vers 32-47 (les interruptions 0-32 sont r�s�rv�es par le processeur)
	reset_pic(32, 40)

endfunction

function reset_pic: u8 master, u8 slave
	/* Initialisation de ICW1 */
	outb(PIC1_CMD, 0x11)
	io_wait()
	outb(PIC2_CMD, 0x11)
	io_wait()
	/* Initialisation de ICW2 */
	outb(PIC1_DATA, master)
	io_wait()
	outb(PIC2_DATA, slave)
	io_wait()

	/* Initialisation de ICW3 */
	outb(PIC1_DATA, 0x04)
	io_wait()
	outb(PIC2_DATA, 0x02)
	io_wait()

	/* Initialisation de ICW4 */
	outb(PIC1_DATA, 0x01)
	io_wait()
	outb(PIC2_DATA, 0x01)
	io_wait()

	/* masque des interruptions */
	outb(PIC1_DATA, 0x00)
	io_wait()
	outb(PIC2_DATA, 0x00)
	io_wait()
endfunction

