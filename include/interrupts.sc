/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/

function__no_stack__ int_default
     asm pushad 
     asm   push ds
     asm   push es
     asm   push fs
     asm   push gs 
     asm   push ebx
     asm   mov bx,0x10
     asm   mov ds,bx
     asm   pop ebx
	int_hanlder_default()
	asm pop gs
    asm pop fs
    asm pop es
    asm pop ds
    asm popad
	asm iret
endfunction

function int_hanlder_default
	fatal_error("Unknown hardware interrupt !")
endfunction

function__no_stack__ int_gp
     asm pushad 
     asm   push ds
     asm   push es
     asm   push fs
     asm   push gs 
     asm   push ebx
     asm   mov bx,0x10
     asm   mov ds,bx
     asm   pop ebx
	int_handler_gp()
	asm pop gs
    asm pop fs
    asm pop es
    asm pop ds
    asm popad
	asm iret
endfunction

function int_handler_gp
	fatal_error("GP(Triple Fault) Exception\nFreezing system !")
	asm cli
	while 1
		asm hlt
	endwhile
endfunction

function__no_stack__ int_pf
    
    asm pop ebx
    asm mov eax, cr2
    eax = kernel_stack_address
    asm mov esp, eax
    asm mov ebp, eax
	int_handler_pf(eax, ebx)

    ! replace eip with our loop code
    asm add esp, 4
    asm push  dword infinite_loop
    asm iret

endfunction

function int_handler_pf: u32 linear_address, u32 error_code
	
    paging_set_tables_raw(kernel_page_directory)

	in_exception=true

    u32 page_directory_index = (linear_address >> 22)
    u32 page_table_index = ((linear_address >> 12 ) & 0x03FF)
    ! linear address 10:10:12 (pdir offset, ptbl offset, pg offset)
    klog("PF address 0x")
	klogx(linear_address)
    klog("\n")
    multitask_dumpTask(current_pid)

    asm mov eax, infinite_loop
    u32 infinite_loop_address
    infinite_loop_address = eax
    klog("inSyscall ")
    klogx(inSyscall)
    klog("\n")
    ! kill task and resume
    paging_set_tables(kernel_page_directory)
    multitask_kill(current_pid)


    klog("resuming from PF\n")

    !switch_to_text_mode()
    refresh_screen()
    loop
    paging_set_tables_raw(current_page_directory)

    in_exception=false
	!current_pid=0
	!fatal_error("PF Exception\nFreezing system !")
endfunction

function general_irq_handler: u8 irq
	!klog("IRQ ")
	!klogn(irq)
	!klog(" recue\n")
	if irq>=0x08
		outb(PIC2_CMD, 0x20)
		io_wait()
	endif
	outb(PIC1_CMD, 0x20)
endfunction

function__no_stack__ irq0x0
	! prepare la structure contenant les registres de la tache actuelle en cas de changement de tache

	asm mov [MULTITASKING_REGISTERS_ADDRESS], eax
	asm mov [MULTITASKING_REGISTERS_ADDRESS+4], ebx
	asm mov [MULTITASKING_REGISTERS_ADDRESS+8], ecx
	asm mov [MULTITASKING_REGISTERS_ADDRESS+12], edx
	
	asm mov [MULTITASKING_REGISTERS_ADDRESS+16], edi
	asm mov [MULTITASKING_REGISTERS_ADDRESS+20], esi
	
	asm mov [MULTITASKING_REGISTERS_ADDRESS+24], ds
	asm mov [MULTITASKING_REGISTERS_ADDRESS+28], es
	asm mov [MULTITASKING_REGISTERS_ADDRESS+32], fs
	asm mov [MULTITASKING_REGISTERS_ADDRESS+36], gs
	
	
	
	! eip
	asm pop eax
	asm mov [MULTITASKING_REGISTERS_ADDRESS+48], eax
	! cs
	asm pop eax
	asm mov [MULTITASKING_REGISTERS_ADDRESS+52], ax
	
	!eflag
	asm pop eax
	asm mov [MULTITASKING_REGISTERS_ADDRESS+56], eax
	
	! stack
	asm mov [MULTITASKING_REGISTERS_ADDRESS+44], esp
	asm mov [MULTITASKING_REGISTERS_ADDRESS+40], ebp
	! PIC
	asm mov eax, [PIC_ADDRESS]
	asm mov [MULTITASKING_REGISTERS_ADDRESS+60], eax
	

	paging_set_tables_raw(kernel_page_directory) 
	time_handler()
	shedule()
    paging_set_tables_raw(current_page_directory)

	!general_irq_handler(0x0)
	
	! met a jour les registres

	asm mov ebx, [MULTITASKING_REGISTERS_ADDRESS+4]
	asm mov ecx, [MULTITASKING_REGISTERS_ADDRESS+8]
	asm mov edx, [MULTITASKING_REGISTERS_ADDRESS+12]
	
	asm mov edi, [MULTITASKING_REGISTERS_ADDRESS+16]
	asm mov esi, [MULTITASKING_REGISTERS_ADDRESS+20]
	
	asm mov ds, [MULTITASKING_REGISTERS_ADDRESS+24]
	asm mov es, [MULTITASKING_REGISTERS_ADDRESS+28]
	asm mov fs, [MULTITASKING_REGISTERS_ADDRESS+32]
	asm mov gs, [MULTITASKING_REGISTERS_ADDRESS+36]
	
	! stack
	asm mov ebp, [MULTITASKING_REGISTERS_ADDRESS+40]
	asm mov esp, [MULTITASKING_REGISTERS_ADDRESS+44]
	
	! PIC
	asm mov eax, [MULTITASKING_REGISTERS_ADDRESS+60]
	asm mov [PIC_ADDRESS], eax

	!eflag
	asm mov eax, [MULTITASKING_REGISTERS_ADDRESS+56]
	asm push eax
	
	! cs
	!asm mov ax, [MULTITASKING_REGISTERS_ADDRESS+52]
	asm mov ax, CODE32_SEGMENT
	asm push eax
	
	! eip
	asm mov eax, [MULTITASKING_REGISTERS_ADDRESS+48]
	asm push eax

	asm mov al,0x20
	asm out 0x20,al
	asm mov eax, [MULTITASKING_REGISTERS_ADDRESS]
	
	asm iret
endfunction

function__no_stack__ irq0x1
     asm pushad 
     asm   push ds
     asm   push es
     asm   push fs
     asm   push gs 
	 /*
	 asm //Push xmm0
	asm sub     esp, 16
	asm movdqu  dqword [esp], xmm0
	*/

	 asm   push ebx
     asm   mov bx,0x10
     asm   mov ds,bx
     asm   pop ebx
	 keyboard_handler()
	general_irq_handler(0x1)
	/*
	asm //Pop xmm0
	asm movdqu  xmm0, dqword [esp]
	asm add     esp, 16
*/
	asm pop gs
    asm pop fs
    asm pop es
    asm pop ds
	! asm pop xmm0
    asm popad
	asm iret
endfunction

function__no_stack__ irq0x2
     asm pushad 
     asm   push ds
     asm   push es
     asm   push fs
     asm   push gs 
	 asm   push ebx
     asm   mov bx,0x10
     asm   mov ds,bx
     asm   pop ebx
	general_irq_handler(0x2)
	asm pop gs
    asm pop fs
    asm pop es
    asm pop ds
    asm popad
	asm iret
endfunction

function__no_stack__ irq0x3
     asm pushad 
     asm   push ds
     asm   push es
     asm   push fs
     asm   push gs 
	 asm   push ebx
     asm   mov bx,0x10
     asm   mov ds,bx
     asm   pop ebx
	general_irq_handler(0x3)
	asm pop gs
    asm pop fs
    asm pop es
    asm pop ds
    asm popad
	asm iret
endfunction

function__no_stack__ irq0x4
     asm pushad 
     asm   push ds
     asm   push es
     asm   push fs
     asm   push gs 
	 asm   push ebx
     asm   mov bx,0x10
     asm   mov ds,bx
     asm   pop ebx
	general_irq_handler(0x4)
	asm pop gs
    asm pop fs
    asm pop es
    asm pop ds
    asm popad
	asm iret
endfunction


function__no_stack__ irq0x5
     asm pushad 
     asm   push ds
     asm   push es
     asm   push fs
     asm   push gs 
	 asm   push ebx
     asm   mov bx,0x10
     asm   mov ds,bx
     asm   pop ebx
	general_irq_handler(0x5)
	asm pop gs
    asm pop fs
    asm pop es
    asm pop ds
    asm popad
	asm iret
endfunction


function__no_stack__ irq0x6
     asm pushad 
     asm   push ds
     asm   push es
     asm   push fs
     asm   push gs 
	 asm   push ebx
     asm   mov bx,0x10
     asm   mov ds,bx
     asm   pop ebx
	general_irq_handler(0x6)
	asm pop gs
    asm pop fs
    asm pop es
    asm pop ds
    asm popad
	asm iret
endfunction


function__no_stack__ irq0x7
     asm pushad 
     asm   push ds
     asm   push es
     asm   push fs
     asm   push gs 
	 asm   push ebx
     asm   mov bx,0x10
     asm   mov ds,bx
     asm   pop ebx
	general_irq_handler(0x7)
	asm pop gs
    asm pop fs
    asm pop es
    asm pop ds
    asm popad
	asm iret
endfunction


function__no_stack__ irq0x8
     asm pushad 
     asm   push ds
     asm   push es
     asm   push fs
     asm   push gs 
	 asm   push ebx
     asm   mov bx,0x10
     asm   mov ds,bx
     asm   pop ebx
	general_irq_handler(0x8)
	asm pop gs
    asm pop fs
    asm pop es
    asm pop ds
    asm popad
	asm iret
endfunction


function__no_stack__ irq0x9
     asm pushad 
     asm   push ds
     asm   push es
     asm   push fs
     asm   push gs 
	 asm   push ebx
     asm   mov bx,0x10
     asm   mov ds,bx
     asm   pop ebx
	general_irq_handler(0x9)
	asm pop gs
    asm pop fs
    asm pop es
    asm pop ds
    asm popad
	asm iret
endfunction


function__no_stack__ irq0xA
     asm pushad 
     asm   push ds
     asm   push es
     asm   push fs
     asm   push gs 
	 asm   push ebx
     asm   mov bx,0x10
     asm   mov ds,bx
     asm   pop ebx
	general_irq_handler(0xA)
	asm pop gs
    asm pop fs
    asm pop es
    asm pop ds
    asm popad
	asm iret
endfunction


function__no_stack__ irq0xB
     asm pushad 
     asm   push ds
     asm   push es
     asm   push fs
     asm   push gs 
	 asm   push ebx
     asm   mov bx,0x10
     asm   mov ds,bx
     asm   pop ebx
	general_irq_handler(0xB)
	asm pop gs
    asm pop fs
    asm pop es
    asm pop ds
    asm popad
	asm iret
endfunction


function__no_stack__ irq0xC
     asm pushad 
     asm   push ds
     asm   push es
     asm   push fs
     asm   push gs 
	 asm   push ebx
     asm   mov bx,0x10
     asm   mov ds,bx
     asm   pop ebx
	general_irq_handler(0xC)
	asm pop gs
    asm pop fs
    asm pop es
    asm pop ds
    asm popad
	asm iret
endfunction


function__no_stack__ irq0xD
     asm pushad 
     asm   push ds
     asm   push es
     asm   push fs
     asm   push gs 
	 asm   push ebx
     asm   mov bx,0x10
     asm   mov ds,bx
     asm   pop ebx
	general_irq_handler(0xD)
	asm pop gs
    asm pop fs
    asm pop es
    asm pop ds
    asm popad
	asm iret
endfunction

function__no_stack__ irq0xE
     asm pushad 
     asm   push ds
     asm   push es
     asm   push fs
     asm   push gs 
	 asm   push ebx
     asm   mov bx,0x10
     asm   mov ds,bx
     asm   pop ebx
	general_irq_handler(0xE)
	asm pop gs
    asm pop fs
    asm pop es
    asm pop ds
    asm popad
	asm iret
endfunction


function__no_stack__ irq0xF
     asm pushad 
     asm   push ds
     asm   push es
     asm   push fs
     asm   push gs 
	 asm   push ebx
     asm   mov bx,0x10
     asm   mov ds,bx
     asm   pop ebx
	general_irq_handler(0xF)
	asm pop gs
    asm pop fs
    asm pop es
    asm pop ds
    asm popad
	asm iret
endfunction

function__no_stack__ syscall_handler
	!asm pushad
	do_syscall(eax, ebx, ecx, edx)
	!asm popad
	asm iret
endfunction


