/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/

function inb: uint16_t port
	dx = port
	asm in al, dx
	return al
endfunction

function outb: uint16_t port, uint8_t data
	dx = port
	al = data
	asm out dx, al
endfunction

function inw: uint16_t port
	dx = port
	asm in ax, dx
	return ax
endfunction

function outw: uint16_t port, uint16_t data
	dx = port
	ax = data
	asm out dx, ax
endfunction


define WAIT_FOR_INTERRUPT {asm hlt}

function__no_stack__ io_wait

! credits: http://forum.osdev.org/viewtopic.php?f=1&t=30111
asm    pushad
asm 
asm    push 0x08
asm    push .next
asm    retf
asm 
asm .next:
asm    nop
asm    nop
asm    nop
asm    nop
asm    pause
asm 
asm    push 0x08
asm    push .next2
asm    retf
asm 
asm .next2:
asm    nop
asm    nop
asm    nop
asm    nop
asm    pause
asm 
asm    push 0x08
asm    push .next3
asm    retf
asm 
asm .next3:
asm    nop
asm    nop
asm    nop
asm    nop
asm    pause
asm 
asm    push 0x08
asm    push .next4
asm    retf
asm 
asm .next4:
asm    out 0x80, al
asm    out 0x80, al
asm 
asm    popad
asm    ret

endfunction