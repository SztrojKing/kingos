/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/


define MULTITASKING_REGISTERS_ADDRESS 0x512010
define STACK_SIZE 0x100000
define TASK_STRUCT_PROCESS_REGISTERS_SIZE 64
define PIC_ADDRESS 0x110000


defstruct _task
	u32 eax
	u32 ebx
	u32 ecx
	u32 edx
	
	u32 edi
	u32 esi
	
	u16 ds
	u16 es
	u16 fs
	u16 gs
	
	u32 ebp
	u32 esp
	
	u32 eip
	u16 cs
	
	u32 eflag
	u32 PIC
	
	ptr code
	ptr stack
	
	u32 wakeup_at
	
	u32 cpu_ticks
	u32 last_cpu_percentage
	
	ptr console_window
	ptr console_widget
	
	ptr windows_list
	ptr sse_registers
	
	! paging table wich we can free
	ptr unaligned_paging_tables

	ptr paging_tables

	ptr executable_path
	
endstruct

sptr tasks=null
su32 tasks_running=0
su32 isMultitaskingStarted=false
su32 multitaskingPaused=false

su32 sheduleTicks=0
su32 forceShedule=false
sptr process_to_kill_later=null
su32 current_pid=0
su32 eflag=0

define SHEDULES_DELAY 16

function start_multitasking
	struct _task kernelTask= malloc(_task.size)
	tasks=malloc(MAX_TASKS*4)

	#(kernelTask.console_window)=[ptr](window_create_console("Kernel Console"))
		
	window_attach_pid(#(kernelTask.console_window), 0)

	#(kernelTask.console_widget)=[ptr](window_get_console_widget([ptr]#(kernelTask.console_window)))
	kernel_console = #(kernelTask.console_widget)
	current_console=kernel_console


	console_set_displayed_buffer(kernel_console, CONSOLE_VIDEO_MEM)
	console_move_cursor(kernel_console, kernel_video_x, kernel_video_y)

	windows_manager_add_window(#(kernelTask.console_window), 0)
	#kernelTask.windows_list = [u32](object_list_create(128))
	
	! allocate sse registers saving buffer
	#(kernelTask.sse_registers) = malloc(128)
		
	! allocate kernel task struct
	#tasks = [ptr](kernelTask)
	process_to_kill_later = object_list_create(MAX_TASKS)

	#(kernelTask.paging_tables) = [ptr](kernel_page_directory)
	#(kernelTask.executable_path) = clone_str("KERNEL")

	
	! get eflag value
	asm pushfd
	asm pop eax
	eflag = eax
	
endfunction


function enable_multitasking
	isMultitaskingStarted=true
endfunction

function disable_multitasking
	! kernel PID = 0 so it's easy to get the address of its struct
	struct _task kernelTask = [ptr]#(tasks)
	!CONSOLE_VIDEO_MEM=0xb8000
	!kernel_video_mem=CONSOLE_VIDEO_MEM
	!memcpy(CONSOLE_VIDEO_MEM, [ptr]#(kernelTask.console_buffer), CONSOLE_VIDEO_MEM_SIZE)
	if kernel_console =/= null
		memcpy(CONSOLE_VIDEO_MEM, console_get_displayed_buffer(kernel_console), CONSOLE_VIDEO_MEM_SIZE)
	endif
	isMultitaskingStarted=false
endfunction


function stop_multitasking
	disable_multitasking()
	if tasks
		! free kernel task struct
		free([ptr](#tasks))
		! free tasks array
		free(tasks)
		! free processes to be killed list
		free(process_to_kill_later)
		process_to_kill_later = null
		
		tasks=null
	endif
	
endfunction

function shedule

	if isMultitaskingStarted==false
		exit
	endif
	
	sheduleTicks++
	
	struct _task current_task = #(tasks+current_pid*SIZEOF_PTR)
	

	#current_task.cpu_ticks = (#current_task.cpu_ticks)+1

	
	if inSyscall =/= false
		exit
	endif
	
	if forceShedule==true
		forceShedule=false
		sheduleTicks=0
	else
		if multitaskingPaused==true || sheduleTicks<SHEDULES_DELAY
			exit
		endif
	endif
	
	u32 previous_pid=current_pid
	struct _task task = [ptr](#(tasks+(previous_pid*4)))
	
	! save sse registers in current task
	ptr sse_registers
	
	if sse_enabled
		sse_registers = #(task.sse_registers)
		edi = sse_registers
		asm   movntdq [EDI+0], xmm0; //move data from registers to dest
		asm   movntdq [EDI+16], xmm1;
		asm   movntdq [EDI+32], xmm2;
		asm   movntdq [EDI+48], xmm3;
		asm   movntdq [EDI+64], xmm4;
		asm   movntdq [EDI+80], xmm5;
		asm   movntdq [EDI+96], xmm6;
		asm   movntdq [EDI+112], xmm7;
	endif


	! kill processes that are waiting to die
	multitask_kill_processes_waiting_to_die()
	
	! find the next process to switch on
	current_pid = multitask_find_next_pid(previous_pid)
	/*	klog("Switching from pid ")
		klogn(previous_pid)
		klog(" to ")
		klogn(current_pid)
		klog(" ticks = ")
		klogn(sheduleTicks)
		klog("\n")*/
	! if multitask isnt started or 0 or 1 process running
	if current_pid=/=previous_pid
		! save previous task
		if [u32]task=/= null
			memcpy([u32]task, [u32](MULTITASKING_REGISTERS_ADDRESS), [u32](TASK_STRUCT_PROCESS_REGISTERS_SIZE))
		endif

		task = [ptr](#(tasks+(current_pid*4)))
		if [u32]task =/= null
			! load current task
			memcpy([u32](MULTITASKING_REGISTERS_ADDRESS), [u32]task, [u32](TASK_STRUCT_PROCESS_REGISTERS_SIZE))

			! switch paging tables	
			current_page_directory = #[ptr](task.paging_tables)

			! update current_console
			current_console = #(task.console_widget)
			
			
			if sse_enabled
				sse_registers = #(task.sse_registers)
				esi = sse_registers
				asm   movdqa xmm0, [ESI]; //move data from src to registers
				asm   movdqa xmm1, [ESI+16];
				asm   movdqa xmm2, [ESI+32];
				asm   movdqa xmm3, [ESI+48];
				asm   movdqa xmm4, [ESI+64];
				asm   movdqa xmm5, [ESI+80];
				asm   movdqa xmm6, [ESI+96];
				asm   movdqa xmm7, [ESI+112];
			endif
		else
			current_pid=previous_pid
		endif

		!coords_printn(0,1,COLOR_RED,current_pid)
		sheduleTicks=0
	endif

endfunction

function multitask_exec:ptr path, ptr arg_list
	multitaskingPaused=true
	u32 size = fat12_get_file_size(path)
	ptr code
	u32 pid=0
	u32 stack
	struct _task task_struct
	bool start_with_short_jmp=false
	
	! create the arg list if it's null
	if arg_list == null
		arg_list = object_list_create(1)
	endif
	object_list_add_at(arg_list, clone_str(path), 0)
	
	if size > 0
		code = fat12_read_file(path)
		if code
		
			if [u8]#code == 0xeb
				start_with_short_jmp=true
			endif
			if ((start_with_short_jmp==true && [u32]#(code+2)==APPLICATION_MAGICAL_NUMBER)||(start_with_short_jmp==false && [u32]#(code+5)==APPLICATION_MAGICAL_NUMBER))
				! get a free pid
				pid=multitask_find_free_pid()
				if pid<MAX_TASKS
					! create the task
					task_struct=malloc(_task.size)
					stack = malloc(STACK_SIZE)
					memset(task_struct, 0, _task.size)
					
					! **** process registers ****
					! eax will contain the application pid at start
					#(task_struct.eax) = [u32]pid
					! ebx will contain the application args at start
					#(task_struct.ebx) = [u32]arg_list
					! ds
					#(task_struct.ds) = [u16]DATA32_SEGMENT
					! es
					#(task_struct.es) = [u16]DATA32_SEGMENT
					! fs
					#(task_struct.fs) = [u16]DATA32_SEGMENT
					! gs
					#(task_struct.gs) = [u16]DATA32_SEGMENT
					
					! ebp
					#(task_struct.ebp) = [u32](stack+STACK_SIZE/2)
					! esp
					#(task_struct.esp) = [u32](stack+STACK_SIZE/2)
					! eip
					#(task_struct.eip) = [u32]code
					! cs
					#(task_struct.cs) = [u16]CODE32_SEGMENT
					!eflag
					#(task_struct.eflag) = [u32]eflag
					
					! PIC
					#(task_struct.PIC) = [u32]code
					
					! **** task data ****
					#(task_struct.code) = [u32]code
					#(task_struct.stack) = [u32]stack
									
					! **** task console ****
					#(task_struct.console_window)=[ptr](window_create_console(path))
					#(task_struct.console_widget)=[ptr](window_get_console_widget([ptr]#(task_struct.console_window)))
					
					! list of windows attached to the process
					#(task_struct.windows_list) = object_list_create(128)
					
					! allocate sse registers saving buffer
					#(task_struct.sse_registers) = malloc(128)

					#(task_struct.executable_path) = clone_str(path)

					! allocate paging tables
					ptr alignedPagingTables = malloc(PAGING_PAGES_SIZE + 4096)

					#(task_struct.unaligned_paging_tables) = alignedPagingTables
					alignedPagingTables = alignedPagingTables + (4096 - alignedPagingTables%4096)

					#(task_struct.paging_tables) = alignedPagingTables

					asm cli

					paging_init_directory(alignedPagingTables)
					! emulate current_page_directory so paging_map is mapping into new task tables
					!current_page_directory = alignedPagingTables
					! map kernel code
					paging_map_area(alignedPagingTables, kernel_address, kernel_address, PAGING_FLAG_PRESENT, 4096*1024)
					paging_map_area(alignedPagingTables, kernel_stack_address-0x400, kernel_stack_address-0x400, PAGING_FLAG_PRESENT, 4096*1024)
					paging_map_area(alignedPagingTables, gdt_address, gdt_address, PAGING_FLAG_PRESENT, GDT_SIZE*GDT_DESC_SIZE)
					paging_map_area(alignedPagingTables, idt_address, idt_address, PAGING_FLAG_PRESENT, IDTSIZE*8)
					! map task paging tables
					paging_map_area(alignedPagingTables, alignedPagingTables, alignedPagingTables, PAGING_FLAG_PRESENT, PAGING_PAGES_SIZE)
					! map task allocated vars
					!paging_map_area(alignedPagingTables, arg_list, arg_list, PAGING_FLAG_PRESENT, malloc_get_size(arg_list))
					paging_map_area(alignedPagingTables, stack, stack, PAGING_FLAG_PRESENT, STACK_SIZE)
					paging_map_area(alignedPagingTables, code, code, PAGING_FLAG_PRESENT, malloc_get_size(code))
					!paging_map_area(alignedPagingTables, #(task_struct.sse_registers), #(task_struct.sse_registers), PAGING_FLAG_PRESENT, malloc_get_size(#(task_struct.sse_registers)))
					!paging_map_area(alignedPagingTables, #(task_struct.executable_path), #(task_struct.executable_path), PAGING_FLAG_PRESENT, malloc_get_size(#(task_struct.executable_path)))
					paging_map_area(alignedPagingTables, MULTITASKING_REGISTERS_ADDRESS, MULTITASKING_REGISTERS_ADDRESS, PAGING_FLAG_PRESENT, 4096)
					paging_map_area(alignedPagingTables, PIC_ADDRESS, PIC_ADDRESS, PAGING_FLAG_PRESENT, 4096)
					!paging_map_area(alignedPagingTables, #(task_struct.console_window), #(task_struct.console_window), PAGING_FLAG_PRESENT, malloc_get_size(#(task_struct.console_window)))
					!paging_map_area(alignedPagingTables, #(task_struct.console_widget), #(task_struct.console_widget), PAGING_FLAG_PRESENT, malloc_get_size(#(task_struct.console_widget)))
					!paging_map_area(alignedPagingTables, #(task_struct.executable_path), #(task_struct.executable_path), PAGING_FLAG_PRESENT, malloc_get_size(#(task_struct.executable_path)))
					!paging_map_area(code, code, PAGING_FLAG_PRESENT, malloc_get_size(code))
					!paging_map_area(code, code, PAGING_FLAG_PRESENT, malloc_get_size(code))
					!paging_map_area(code, code, PAGING_FLAG_PRESENT, malloc_get_size(code))
					!paging_map_area(code, code, PAGING_FLAG_PRESENT, malloc_get_size(code))

					!paging_map_area(0, 0, PAGING_FLAG_PRESENT, 4*1024*1024*1024-4096)	
					! restore current task paging directory
				!	current_page_directory = current_task_paging_directory
					asm sti
					klog("\ntask address ")
					klogx(code )


					window_attach_pid(#(task_struct.console_window), pid)
					windows_manager_add_window(#(task_struct.console_window), pid)

					tasks_running++
										
					! redraw desktop
					next_redraw_type=NEXT_REDRAW_TYPE_REFRESH_ALL
					
					! register our new task in the task registry
					#(tasks+(pid*4)) = [ptr](task_struct)

					/*
					klog("Task ")
					klog(path)
					klog(" created with pid ")
					klogn(pid)
					klog("\n")
					
					*/
					
				else
					free(code)
					klog("multitask_exec:Nombre de processus max atteint")	
				endif
			else
				free(code)
				klog("multitask_exec:Fichier non executable")
			endif
		else
			klog("multitask_exec:Fichier introuvable")
		endif
	endif

	multitaskingPaused=false
endfunction

! wont wake up a process util ms
function multitask_sleep: u32 pid, u32 ms
	struct _task task = #(tasks+(pid*SIZEOF_PTR))
	
	#task.wakeup_at = time_ticks+ms
	
endfunction


! find a free pid
function multitask_find_free_pid
	u32 pid=1 ! pid 0 is kernel
	while pid<MAX_TASKS
		if [u32]#(tasks+(pid*4)) == null
			return pid
		endif
		pid++
	endwhile
	! return an error
	return MAX_TASKS+1
endfunction

! find the next pid
function multitask_find_next_pid: u32 previous_pid
	u32 pid=previous_pid+1
	
	struct _task task = [u32]#(tasks+(pid*4))
	! pid 1 correspond to idle process
	bool sleeping_task = true
	
	if task =/= null
		if #task.wakeup_at <= time_ticks
			sleeping_task = false
		endif
	endif
		
	while sleeping_task == true && pid=/=previous_pid
	
		sleeping_task = true
		
		pid = (pid + 1) % MAX_TASKS
		task = [u32]#(tasks+(pid*4))
		
		if task =/= null
			if #task.wakeup_at <= time_ticks && pid =/= 1
				sleeping_task = false
			endif
		endif
		
	endwhile
	
	! if all tasks are sleeping, return idle pid
	if sleeping_task==true
		pid = 1
	endif
	/*
	! notify idle stats manager
	if pid == 1
		start_idle()
	else
		stop_idle()
	endif
	*/
	return pid
endfunction

! kill a task
function multitask_kill: u32 pid
	if pid == current_pid
		multitask_kill_later(pid)
		multitask_shedule_later()
		!multitask_kill_now(pid)
	else
		multitask_kill_now(pid)
	endif
endfunction

! kill a task immediatly (don't use this function directly)
function multitask_kill_now: u32 pid

	if pid < MAX_TASKS
		struct _task task
		u32 i = 0
		multitaskingPaused=true
		if [u32]#(tasks+(pid*4)) =/= null
			task = [ptr](#(tasks+(pid*4)))
			! don't close kernel console
			if pid>0
				! close console window
				windows_manager_close_window([u32]#(task.console_window))
				window_free([u32]#(task.console_window))
			endif
			
			! free all windows that hasn't been closed before exit
			/*while i < object_list_get_size(#task.windows_list)
				if object_list_get_at(#task.windows_list, i) =/= null
					windows_manager_close_window(object_list_get_at(#task.windows_list, i))
					window_free(object_list_get_at(#task.windows_list, i))
				endif
				object_list_remove_at(#task.windows_list, i)
				i++
			endwhile*/
			
			while object_list_get_size(#task.windows_list) > 0
				if object_list_get_at(#task.windows_list, 0) =/= null
					windows_manager_close_window(object_list_get_at(#task.windows_list, 0))
					window_free(object_list_get_at(#task.windows_list, 0))
				endif
				object_list_remove_at(#task.windows_list, 0)
				!i++
			endwhile
			
			! free sse buffer
			free(#(task.sse_registers))
			
			! free code
			free([ptr](#task.code))
			! free stack
			free([ptr](#task.stack))
			! free task struct
			free(task)
			
			if pid>0
				free(#(task.unaligned_paging_tables))
				free([ptr](#task.executable_path))
			endif

			#(tasks+(pid*4))=[u32](null)
			
		
			if pid=/=0
				tasks_running--
			endif
			multitaskingPaused=false
			return true
		else
			multitaskingPaused=false
			return false
		endif
	else
		klog("PID: ")
		klogn(current_pid)
		klog(" -> ")
		fatal_error("multitask_kill_now(): trying to kill pid > MAX_TASKS")
	endif
endfunction

! kill a process after the next shedule(must be used if a process want to kill hisself)
function multitask_kill_later: u32 pid
	object_list_add(process_to_kill_later, pid)
endfunction

! kill a process after the next shedule
function multitask_kill_processes_waiting_to_die
	u32 i=0
	u32 pid
	while i<object_list_get_size(process_to_kill_later)
		pid=object_list_get_at(process_to_kill_later, i)
		if pid =/= current_pid
			multitask_kill_now(pid)
			object_list_remove_at(process_to_kill_later, i)
		else
			i++
		endif
	endwhile
endfunction


! force to shedule asap
function multitask_shedule_later
	forceShedule=true
endfunction

! return console window
function multitask_get_console: ptr pid
	if pid < MAX_TASKS
		struct _task task = [ptr](#(tasks+(pid*4)))
		return [u32]#(task.console_window)
	endif
	return null
endfunction

! add a window to a process
function multitask_add_window_to_process: u32 pid, ptr win
	struct _task task
	if [u32]#(tasks+(pid*4)) =/= null
		task = [ptr](#(tasks+(pid*4)))
		object_list_add(#task.windows_list, win)
	else
		if pid > 0
			klog("\nPID: ")
			klogn(pid)
			klog(" -> ")
			fatal_error("multitask_add_window_to_process(): pid not found") 
		endif
	endif
endfunction

! remove a window from a process
function multitask_remove_window_from_process: u32 pid, ptr win
	struct _task task
	if [u32]#(tasks+(pid*4)) =/= null
		task = [ptr](#(tasks+(pid*4)))
		object_list_remove_object(#task.windows_list, win)
	endif
endfunction

! return the number of running processes
function multitask_get_number_processes
	u32 pid=0
	u32 processes=0
	
	while pid<MAX_TASKS
		if [u32]#(tasks+(pid*4))=/=null
			processes++
		endif
		pid++
	endwhile
	return processes
endfunction


function multitask_dumpTask: u32 pid
	struct _task task
	if [u32]#(tasks+(pid*4)) =/= null
		task = [ptr](#(tasks+(pid*4)))
		klog("DUMPING TASK WITH PID ")
		klogn(pid)
		klog("\nPath: ")
		klog(#(task.executable_path))
		klog("\n")
	endif
endfunction