/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/

! Basic screen operations

define CONSOLE_DEFAULT_COLOR 0x07

define CONSOLE_COLOR_BLACK 0x00
define CONSOLE_COLOR_DARK_BLUE 0x01
define CONSOLE_COLOR_GREEN 0x02
define CONSOLE_COLOR_AQUA 0x03
define CONSOLE_COLOR_BROWN 0x04
define CONSOLE_COLOR_PURPLE 0x05
define CONSOLE_COLOR_KHAKI 0x06
define CONSOLE_COLOR_LIGHT_GREY 0x07
define CONSOLE_COLOR_GREY 0x08
define CONSOLE_COLOR_LIGHT_BLUE 0x09
define CONSOLE_COLOR_LIGHT_GREEN 0x0a
define CONSOLE_COLOR_CYAN 0x0b
define CONSOLE_COLOR_RED 0x0c
define CONSOLE_COLOR_PINK 0x0d
define CONSOLE_COLOR_YELLOW 0x0e
define CONSOLE_COLOR_WHITE {([uint8_t]0x0f)}


define CONSOLE_VIDEO_MEM {(0xb8000)}

define CONSOLE_VIDEO_MEM_SIZE 0xfa0
define CONSOLE_MAX_CHAR_WIDTH 80
define CONSOLE_MAX_CHAR_HEIGHT 25

! video coords

suint32_t kernel_video_x=0
suint32_t kernel_video_y=0

sptr current_console=null
sptr kernel_console=null

function klog:ptr str
	color_klog(CONSOLE_DEFAULT_COLOR, str)
endfunction

function klogc:u8 c
	color_klogc(CONSOLE_DEFAULT_COLOR, c)
endfunction

function klogn: uint32_t number
	color_klogn(CONSOLE_DEFAULT_COLOR,number)
endfunction

function klogx: uint32_t number
	color_klogx(CONSOLE_DEFAULT_COLOR,number)
endfunction

function color_klog: u8 color, ptr str
	if kernel_console =/= null
		console_color_prints(kernel_console, color, str)
	else
		text_mode_console_color_prints(color, str)
	endif
endfunction

function color_klogc: u8 color, u8 c
	suint8_t buf={"?"}
	buf=c
	if kernel_console =/= null
		console_color_prints(kernel_console, color, @buf)
	else
		text_mode_console_color_prints(color, @buf)
	endif
	
	
endfunction

function color_klogn: u8 color, u32 number
	memset(@_printn_buffer, 0, 21)
	itoa(@_printn_buffer, number)
	
	if kernel_console =/= null
		console_color_prints(kernel_console, color, @_printn_buffer)
	else
		text_mode_console_color_prints(color, @_printn_buffer)
	endif
endfunction

function color_klogx: u8 color, u32 number
	memset(@_printn_buffer, 0, 21)
	itox(@_printn_buffer, number)
	
	if kernel_console =/= null
		console_color_prints(kernel_console, color, @_printn_buffer)
	else
		text_mode_console_color_prints(color, @_printn_buffer)
	endif
endfunction

! color_prints with default color
function log:ptr str
	color_log(CONSOLE_DEFAULT_COLOR, str)
endfunction

function logc:u8 c
	color_logc(CONSOLE_DEFAULT_COLOR, c)
endfunction

function logn: uint32_t number
	color_logn(CONSOLE_DEFAULT_COLOR, number)
endfunction

function logx: uint32_t number
	color_logx(CONSOLE_DEFAULT_COLOR, number)
endfunction

function color_log: u8 color, ptr str
	if current_console =/= null
		console_color_prints(current_console, color, str)
	else
		color_klog(color, str)
	endif
endfunction

function color_logc: u8 color, u8 c
	suint8_t buf={"?"}
	buf=c
	if current_console =/= null
		console_color_prints(current_console, color, @buf)
	else
		color_klogc(color, c)
	endif
endfunction

function color_logn: u8 color, u32 number
	if current_console =/= null
		console_printn(current_console, color, number)
	else
		color_klogn(color, number)
	endif
endfunction

function color_logx: u8 color, u32 number
	if current_console =/= null
		console_printx(current_console, color, number)
	else
		color_klogx(color, number)
	endif
endfunction

function cls
	if current_console=/=null
		console_cls(current_console)
	else
		text_mode_console_cls()
	endif
endfunction
! raw clear screen
function text_mode_console_cls
	text_mode_console_move_cursor(0,0)
	cpu_memset32(CONSOLE_VIDEO_MEM, 0x07200720, 25*80*2)
endfunction

! move writing position

function text_mode_console_move_cursor:uint8_t x, uint8_t y
	kernel_video_x=x
	kernel_video_y=y
endfunction

/*
! coords + color prints
function console_coords_color_prints:uint8_t x, uint8_t y, uint8_t color, ptr str
	if str == null
		exit
	endif
	if x>=80
		exit
	endif
	if y>=25
		exit
	endif
	ptr video=[ptr](CONSOLE_VIDEO_MEM+((y*80+x)*2))
	while [uint8_t]#str =/= 0
		! LF
		if [uint8_t]#str == 10
			y++
			video=[ptr](CONSOLE_VIDEO_MEM+((y*80+x)*2))
		endif
		! CR
		if [uint8_t]#str == 13
			x=0
			video=[ptr](CONSOLE_VIDEO_MEM+((y*80+x)*2))
		endif
		
		! tab
		if  [uint8_t]#str == 9
			x=x+4
			video=[ptr](CONSOLE_VIDEO_MEM+((y*80+x)*2))
		endif
		! normal char
		if [uint8_t]#str >= ' '
			#video=[uint8_t]#str
			video++
			#video=color
			video++
						
						
			x++

		endif
		if x>=80
			x=x-80
			y++
			video=[ptr](CONSOLE_VIDEO_MEM+((y*80+x)*2))
		endif
		str++	
	
	endwhile
endfunction
*/


! prints with color
function text_mode_console_color_prints:uint8_t color, ptr str
	if str == null
		exit
	endif
	ptr video=[ptr](CONSOLE_VIDEO_MEM+((kernel_video_y*80+kernel_video_x)*2))
	while [uint8_t]#str =/= 0
		! LF
		if [uint8_t]#str == 10
			!#video=[uint8_t]#str
			kernel_video_y++
			video=[ptr](CONSOLE_VIDEO_MEM+((kernel_video_y*80+kernel_video_x)*2))
			if kernel_video_y>=25

				text_mode_console_scrollup(1)
				video=[ptr](CONSOLE_VIDEO_MEM+((kernel_video_y*80+kernel_video_x)*2))
			endif
		endif
		! CR
		if [uint8_t]#str == 13
			!#video=[uint8_t]#str
			kernel_video_x=0
			video=[ptr](CONSOLE_VIDEO_MEM+((kernel_video_y*80+kernel_video_x)*2))
		endif
		
		! tab
		if  [uint8_t]#str == 9
			!#video=[uint8_t]#str
			kernel_video_x=kernel_video_x+4
			video=[ptr](CONSOLE_VIDEO_MEM+((kernel_video_y*80+kernel_video_x)*2))
		endif
		
		! normal char
		if [uint8_t]#str >= ' '
			#video=[uint8_t]#str
			video++
			#video=color
			video++
						
			kernel_video_x++

		endif
		
		if kernel_video_x>=80
			kernel_video_x=kernel_video_x-80
			kernel_video_y++
			video=[ptr](CONSOLE_VIDEO_MEM+((kernel_video_y*80+kernel_video_x)*2))
		endif
		if kernel_video_y>=25
			text_mode_console_scrollup(1)
			video=[ptr](CONSOLE_VIDEO_MEM+((kernel_video_y*80+kernel_video_x)*2))
		endif
		
		str++	
	
	endwhile
	
endfunction



! print [OK]
function console_printOk
	u32 x
	
	if current_console=/=null
		x=console_get_x(current_console)
	else
		x=kernel_video_x
	endif
	
	while x<71
		log(" ")
		x++
	endwhile
	
	color_log(CONSOLE_COLOR_GREEN, "[  OK  ]")

endfunction

! print [ERREUR]
function console_printError
	u32 x
	
	if current_console=/=null
		x=console_get_x(current_console)
	else
		x=kernel_video_x
	endif
	
	while x<71
		log(" ")
		x++
	endwhile
	
	color_log(CONSOLE_COLOR_RED, "[ERREUR]")

endfunction


/*
function console_coords_printn:u8 x, u8 y, uint8_t color, uint32_t number
	memset(@_printn_buffer, 0, 21)
	itoa(@_printn_buffer, number)
	console_coords_color_prints(x,y,color, @_printn_buffer)
endfunction
*/

function text_mode_console_scrollup:uint32_t nlines
	if kernel_video_y>nlines

		kernel_video_y=kernel_video_y-nlines
		memcpy(CONSOLE_VIDEO_MEM, CONSOLE_VIDEO_MEM+(160*(nlines)), CONSOLE_VIDEO_MEM_SIZE-(160*(nlines)))
		memset(CONSOLE_VIDEO_MEM+((kernel_video_y)*160), 0, (160*nlines))

	endif
endfunction


function get_24bit_color_from_console: u8 console_color
	if console_color==CONSOLE_COLOR_BLACK
		return COLOR_BLACK
	endif
	if console_color==CONSOLE_COLOR_LIGHT_GREY
		return COLOR_LIGHT_GREY
	endif
	if console_color==CONSOLE_COLOR_WHITE
		return COLOR_WHITE
	endif
	if console_color==CONSOLE_COLOR_GREEN
		return COLOR_GREEN
	endif
	if console_color==CONSOLE_COLOR_RED
		return COLOR_RED
	endif
	if console_color==CONSOLE_COLOR_DARK_BLUE
		return COLOR_BLUE
	endif
	return COLOR_WHITE
endfunction
