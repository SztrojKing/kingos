/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/


function memcmp:ptr src1, ptr src2, uint32_t size
	
	while size > 0
		if #src1[uint8_t] =/= #src2[uint8_t]
			return 1
		endif
		src1++
		src2++
		size--
	endwhile
	return 0
endfunction
/*
function memcmp:ptr dest, ptr src, uint32_t size
	if size%4 == 0
		return memcmp32(dest, src, size/4)
	else
		if size%2 == 0
			return memcmp16(dest, src, size/2)
		else
			return memcmp8(dest, src, size)
		endif
	endif
endfunction
*/
/*
function memcmp8: ptr dest, ptr src, uint32_t size
	! use cpu
	eax = src
	ebx = dest
	ecx = size
	asm mov esi, eax
	asm mov edi, ebx
	asm cld
	asm repe cmpsb
	
	asm jne .mismatch
	return 0
	asm .mismatch:
	return 1
endfunction

function memcmp16: ptr dest, ptr src, uint32_t size
	! use cpu
	eax = src
	ebx = dest
	ecx = size
	asm mov esi, eax
	asm mov edi, ebx
	asm cld
	asm repe cmpsw
		
	asm jne .mismatch
	return 0
	asm .mismatch:
	return 1
endfunction

function memcmp32: ptr dest, ptr src, uint32_t size
	! use cpu
	eax = src
	ebx = dest
	ecx = size
	asm mov esi, eax
	asm mov edi, ebx
	asm cld
	asm repe cmpsd
	
	asm jne .mismatch
	return 0
	asm .mismatch:
	return 1
endfunction*/