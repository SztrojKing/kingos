/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/

! Swaps the contents of two nonoverlapping memory areas.
function memswap: ptr array, uint32_t length
	uint32_t offset=0
	uint32_t max_progress=length / 2
	uint8_t tmp=0
	length--
	while offset<max_progress
		tmp = #(array+offset)
		#(array+offset) = [uint8_t]#(array+(length-offset))
		#(array+(length-offset)) = tmp
		offset++
	endwhile
endfunction