/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/

function setBit:uint8_t var, uint8_t offset, bool bit_state

	if bit_state==0
		return [u8](var & (~(1<<offset)))
	else
		return [u8](var | (1<<offset))
	endif
endfunction

function checkBit: uint8_t var, uint8_t offset
	return [u8]((var) & (1<<(offset)))
endfunction