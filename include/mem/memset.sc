/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/

define memset memset32
!define memset32 memset

sptr memset_sse2_buffer = null

function cpu_memset8: ptr dest, uint8_t value, uint32_t size
	! use cpu
	al = value
	ebx = dest
	ecx = size
	asm mov edi, ebx
	asm cld
	asm rep stosb
endfunction

function cpu_memset16: ptr dest, uint16_t value, uint32_t size
	! use cpu
	ax = value
	ebx = dest
	ecx = size
	asm mov edi, ebx
	asm cld
	asm rep stosw
endfunction

function cpu_memset32: ptr dest, uint32_t value, uint32_t size
	! use cpu
	eax = value
	ebx = dest
	ecx = size
	asm mov edi, ebx
	asm cld
	asm rep stosd
endfunction 


function memset8_software:ptr dest, uint8_t value, uint32_t length
	uint32_t prog=0
	while prog<length
		#dest=[uint8_t]value
		dest++
		prog++
	endwhile
endfunction

function memset32:ptr dest, uint32_t value, uint32_t length
	/*
	if size%4 == 0
		memcpy32(dest, src, size/4)
	else
		if size%2 == 0
			memcpy16(dest, src, size/2)
		else
			memcpy8(dest, src, size)
		endif
	endif
	*/
	u32 size_left=0
		! optimize large buffers transfer with sse2
	if length >= 2048 && sse_enabled == true && dest%0x10 == 0
	
		size_left = length % 128
		X_aligned_memset32_sse2(dest, value, (length-size_left))
		if size_left>0
			memset32(dest+length-size_left, value, size_left)
		endif
		
	else
		if length > 0xf
			size_left = length % 4
			cpu_memset32(dest, value, (length-size_left)/4)
			if size_left > 0
				cpu_memset8(dest+length-size_left, value, size_left)
			endif
		else
			cpu_memset8(dest, value, length)
		endif
	!memset8_software(dest, value,  length)
	endif
endfunction

/*
function memset32:ptr dest, uint32_t value, uint32_t length
	uint32_t prog=0
	while prog<length
		#dest=[uint32_t]value
		dest = dest + 4
		prog++
	endwhile
endfunction
*/

function X_aligned_memset32_sse2: ptr dest, u32 value, uint32_t size
	u32 i=0
	! copy value into buffer
	while i<4
		#(memset_sse2_buffer+i*SIZEOF_UINT32_T) = [u32](value)
		i++
	endwhile
	esi = memset_sse2_buffer
	
    eax = value
	edi = dest

    ebx = size
	
    asm shr ebx, 7;      //divide by 128 (8 * 128bit registers)

	
    asm   movdqa xmm0, [ESI]
    asm   movdqa xmm1, [ESI]
    asm   movdqa xmm2, [ESI]
    asm   movdqa xmm3, [ESI]
    asm   movdqa xmm4, [ESI]
    asm   movdqa xmm5, [ESI]
    asm   movdqa xmm6, [ESI]
    asm   movdqa xmm7, [ESI]

    asm .loop_copy:



    asm   movntdq [EDI+0], xmm0; //move data from registers to dest
    asm   movntdq [EDI+16], xmm1;
    asm   movntdq [EDI+32], xmm2;
    asm   movntdq [EDI+48], xmm3;
    asm   movntdq [EDI+64], xmm4;
    asm   movntdq [EDI+80], xmm5;
    asm   movntdq [EDI+96], xmm6;
    asm   movntdq [EDI+112], xmm7;

    asm   add edi, 128;
    asm   dec ebx;

    asm   jnz .loop_copy; //loop please
  
endfunction