/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/


function memcpy:ptr dest, ptr src, uint32_t size
	/*
	if size%4 == 0
		memcpy32(dest, src, size/4)
	else
		if size%2 == 0
			memcpy16(dest, src, size/2)
		else
			memcpy8(dest, src, size)
		endif
	endif
	*/
	u32 size_left
	
	! optimize large buffers transfer with sse2
	if size >= 256 && sse_enabled == true && dest%0x10 == 0 && src%0x10 == 0
	
		size_left = size % 128
		X_aligned_memcpy_sse2(dest, src, (size-size_left))
		if size_left>0
			memcpy(dest+size-size_left, src+size-size_left, size_left)
		endif
		
	else
	
		if size > 0xf
			size_left = size % 4
			memcpy32(dest, src, (size-size_left)/4)
			if size_left>0
				memcpy8(dest+size-size_left, src+size-size_left, size_left)
			endif
		else
			memcpy8(dest, src, size)
		endif
	endif
endfunction



function memcpy8_software:ptr dest, ptr src, uint32_t size
	uint32_t prog=0
	while prog<size
		#dest = [uint8_t]#src
		prog++
		src++
		dest++
	endwhile
endfunction


function memcpy8: ptr dest, ptr src, uint32_t size
	! use cpu
	eax = src
	ebx = dest
	ecx = size
	asm mov esi, eax
	asm mov edi, ebx
	asm cld
	asm rep movsb
endfunction
/*
function memcpy16: ptr dest, ptr src, uint32_t size
	! use cpu
	eax = src
	ebx = dest
	ecx = size
	asm mov esi, eax
	asm mov edi, ebx
	asm cld
	asm rep movsw
endfunction
*/
function memcpy32: ptr dest, ptr src, uint32_t size
	! use cpu
	eax = src
	ebx = dest
	ecx = size
	asm mov esi, eax
	asm mov edi, ebx
	asm cld
	asm rep movsd
endfunction

function memcpy_from_end: ptr dest, ptr src, uint32_t size
	while size > 0
		#(dest+size-1) = [uint8_t]#(src+size-1)
		size--
	endwhile
endfunction

function X_aligned_memcpy_sse2: ptr dest, ptr src, uint32_t size

    esi = src
	edi = dest

    ebx = size
	
    asm shr ebx, 7;      //divide by 128 (8 * 128bit registers)


    asm .loop_copy:
    asm   prefetchnta [ESI+128]; //SSE2 prefetch
    asm   prefetchnta [ESI+160];
    asm   prefetchnta [ESI+192];
    asm   prefetchnta [ESI+224];

    asm   movdqa xmm0, [ESI]; //move data from src to registers
    asm   movdqa xmm1, [ESI+16];
    asm   movdqa xmm2, [ESI+32];
    asm   movdqa xmm3, [ESI+48];
    asm   movdqa xmm4, [ESI+64];
    asm   movdqa xmm5, [ESI+80];
    asm   movdqa xmm6, [ESI+96];
    asm   movdqa xmm7, [ESI+112];

    asm   movntdq [EDI+0], xmm0; //move data from registers to dest
    asm   movntdq [EDI+16], xmm1;
    asm   movntdq [EDI+32], xmm2;
    asm   movntdq [EDI+48], xmm3;
    asm   movntdq [EDI+64], xmm4;
    asm   movntdq [EDI+80], xmm5;
    asm   movntdq [EDI+96], xmm6;
    asm   movntdq [EDI+112], xmm7;

    asm   add esi, 128;
    asm   add edi, 128;
    asm   dec ebx;

    asm   jnz .loop_copy; //loop please
    asm loop_copy_end:
  
endfunction
