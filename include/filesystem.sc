/*********************************
* This file is a part of King OS *
*  written by LANEZ Gaël - 2017  *
*********************************/

defstruct File
	ptr path
	u32 size
	bool read
	bool write
	ptr data
endstruct

function fopen: ptr path, ptr mode


endfunction


function fclose: File f

endfunction
