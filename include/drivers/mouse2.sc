/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/

define MOUSE_ACKNOWLEDGE 0xFA

define MOUSE_BUTTON_UP 0
define MOUSE_BUTTON_DOWN 1

enum MOUSE_LEFT_BUTTON, MOUSE_RIGHT_BUTTON
enum MOUSE_BUTTON_STATE_UP, MOUSE_BUTTON_STATE_DOWN, MOUSE_BUTTON_STATE_CLICKED, MOUSE_BUTTON_STATE_DOUBLE_CLICKED
su8 mouse_cycle=0     !unsigned char
su8 mouse_byte0=0   ! //signed char
su8 mouse_byte1=0
su8 mouse_byte2=0
su32 mouse_x=CURSOR_WIDTH     ! //signed char
su32 mouse_y=CURSOR_HEIGHT       ! //signed char
su32 previous_mouse_x=0
su32 previous_mouse_y=0
su8 isMouseInstalled=false
sptr underCursorImage=null
su32 mouse_irq_count=0

su8 mouse_left_button_physycal_state=MOUSE_BUTTON_UP
su8 mouse_right_button_physycal_state=MOUSE_BUTTON_UP

su32 mouse_left_button_state=MOUSE_BUTTON_STATE_UP
su32 mouse_left_button_last_state_time=0

su32 mouse_status_request=0
su8 mouse_status_byte0=0
su8 mouse_status_byte1=0
su8 mouse_status_byte2=0

sbool is_mouse_paused=false


function mouse_init
!	asm cli
! mouse handler must be installed 
	mouse_x=vres_x/2
	mouse_y=vres_y/2
	previous_mouse_x=mouse_x
	previous_mouse_y=mouse_y
	mouse_event_init()
  u8 status=0
  !//Enable the auxiliary mouse device
  !asm cli
  klog("PS/2")
  while mouse_read(false) <= 0xff
  endwhile
  mouse_wait(1)
  outb(0x64, 0xA8)
    mouse_wait(1)

 ! klog(" | Reset")
 !   // Reset the mouse
! if mouse_write(0xFF) == false || [u8](mouse_read(true)) =/= MOUSE_ACKNOWLEDGE ! //Acknowledge
	!return false
 ! endif
  !asm sti

 klog(" | Default")
 ! //Tell the mouse to use default settings
  if mouse_write(0xF6) == false
	return false
  endif
  if [u8](mouse_read(true)) =/= MOUSE_ACKNOWLEDGE ! //Acknowledge
	return false
  endif
 while mouse_read(false) <= 0xff
 endwhile
 
   klog(" | Vitesse(60/sec)")
  if mouse_set_sample_rate(60) == false
	klog("[ERREUR]")
  endif
   
 ! remote mode
  !klog(" | Activation")
  /*if mouse_write(0xF0) == false || mouse_read(true) =/= MOUSE_ACKNOWLEDGE ! //Acknowledge
	return false
  endif
 while mouse_read(false) <= 0xff
 endwhile*/
 
   !//Enable the interrupts
  klog(" | Interruptions")

  mouse_wait(1)
  outb(0x64, 0x20)
  mouse_wait(0)
  status=inb(0x60)
 ! klog(" (Statut=0x")
 ! klogx(status)
  status = [u8](setBit((status | 2), 5, false))
 !  klog(" | new=0x")
 ! klogx(status)
  mouse_wait(1)
  outb(0x64, 0x60)
  mouse_wait(1)
  outb(0x60, status)
  
  underCursorImage=malloc(CURSOR_WIDTH*CURSOR_HEIGHT*4)
	!klog(")")
	
	 ! //Enable the mouse
 !klog(" | Activation")
  if mouse_write(0xF4) == false || mouse_read(true) =/= MOUSE_ACKNOWLEDGE ! //Acknowledge
	return false
  endif
 while mouse_read(false) <= 0xff
 endwhile
 

  isMouseInstalled=true
!  asm sti
  return true
endfunction

function mouse_pause
	u8 status=0
	if isMouseInstalled==true && is_mouse_paused==false
		  if mouse_write(0xF5) == false || mouse_read(true) =/= MOUSE_ACKNOWLEDGE ! //Acknowledge
			return false
		  endif
		 while mouse_read(false) <= 0xff
		 endwhile
 
		is_mouse_paused=true
	endif
endfunction

function mouse_resume
	u8 status=0
	if isMouseInstalled==true && is_mouse_paused==true
		 if mouse_write(0xF4) == false || mouse_read(true) =/= MOUSE_ACKNOWLEDGE ! //Acknowledge
			return false
		  endif
		 while mouse_read(false) <= 0xff
		 endwhile
		is_mouse_paused=false
	endif
endfunction

!//Mouse functions
!void mouse_handler(struct regs *a_r) //struct regs *a_r (not used but just there)
function mouse_handler
	mouse_irq_count++
	! return if mouse isn't installed yet
	if isMouseInstalled==false
		exit
	endif
	!if mouse_status_request==0
  if mouse_cycle == 0
      mouse_byte0=inb(0x60)
	  ! check packet alignement
	  if mouse_byte0 & 0x8 == 0
		! wrong alignement, ignore this packet
		mouse_cycle=0
				klog("Mouse alignement error\n")
	 else
		mouse_cycle++
	 endif
	
	  ! mouse right button state
	 ! mouse_set_button_state(MOUSE_RIGHT_BUTTON, (mouse_byte0&0x02)>>1)
  else
	if mouse_cycle == 1
      mouse_byte1=inb(0x60)
      mouse_cycle++
    else
      mouse_byte2=inb(0x60)
	  
	  if previous_mouse_x>=vres_x-CURSOR_WIDTH
		previous_mouse_x=0
	  endif
	  if previous_mouse_y>=vres_y-CURSOR_HEIGHT
		previous_mouse_y=0
	  endif
	  ! x < 0 ?
	  if mouse_byte0 & 0x10 > 0
		if mouse_x > (0xff-mouse_byte1+1) + CURSOR_WIDTH
			mouse_x = mouse_x - (0xff-mouse_byte1+1)
		else
			mouse_x=CURSOR_WIDTH
		endif
	  else
		mouse_x=mouse_x+mouse_byte1
	  endif
	  
	  ! y < 0 ?
	  if mouse_byte0 & 0x20 > 0
			mouse_y = mouse_y + (0xff-mouse_byte2+1)
	  else
		if mouse_y > CURSOR_HEIGHT
			if mouse_y>mouse_byte2
				mouse_y=mouse_y-mouse_byte2
			else
				mouse_y=CURSOR_HEIGHT
			endif
		else
			mouse_y=CURSOR_HEIGHT
		endif
	  endif
	  
	  
	  if mouse_x>= vres_x-CURSOR_WIDTH
		mouse_x = vres_x-CURSOR_WIDTH-1
	  endif
	  
	 if mouse_y>= vres_y-CURSOR_HEIGHT
		mouse_y = vres_y-CURSOR_HEIGHT-1
	  endif
	  
      !mouse_y=mouse_y+mouse_byte2
      mouse_cycle=0
	 ! console_coords_printn(20, 0, CONSOLE_DEFAULT_COLOR, mouse_x)
	  !console_coords_printn(25, 0, CONSOLE_DEFAULT_COLOR, mouse_y)

	  !if previous_mouse_x>0 && previous_mouse_y>0
	mouse_set_button_state(MOUSE_LEFT_BUTTON, [u8](mouse_byte0&0x01))
	 ! endif
	 draw_cursor()
	  !console_coords_color_prints(mouse_x/10, mouse_y/10, CONSOLE_COLOR_RED, {"#"})
	   
	   	  	  ! mouse left button state
	 /* mouse_set_button_state(MOUSE_LEFT_BUTTON, [u8](mouse_byte0&0x01))
	  klog("mouse_byte0=0x")
	  klogx(mouse_byte0)
	  klog(" mouse_byte1=0x")
	  klogx(mouse_byte1)
	  klog(" mouse_byte2=0x")
	  klogx(mouse_byte2)
	  klog("\n")*/
	!endif
  endif
endif
endfunction
function mouse_query_buttons_state
	!u8 res = 0
	!inSyscall=true
	! mouse status request
	!mouse_status_request=1
	!mouse_write(0xe9)
	!inSyscall=false
	
	!if [u8](mouse_read(true)) =/= MOUSE_ACKNOWLEDGE ! //Acknowledge
	!	return false
	!endif
	! mouse will respond 3 packets
	!res = mouse_read(false)
	!mouse_read(false)
	!mouse_read(false)
	
	! mouse left button state
	!  mouse_set_button_state(MOUSE_LEFT_BUTTON, (res&0x04)>>2)
	! mouse right button state
	 ! mouse_set_button_state(MOUSE_RIGHT_BUTTON, (res&0x02)>>1)
	 return true
endfunction

function mouse_set_sample_rate: u8 rate
	! mouse status request
	
	
	
	if mouse_write(0xf3) =/= true || [u8](mouse_read(true)) =/= MOUSE_ACKNOWLEDGE ! //Acknowledge
		return false
	endif
	
	if mouse_write(rate) =/= true || [u8](mouse_read(true)) =/= MOUSE_ACKNOWLEDGE ! //Acknowledge
		return false
	endif
	return true
endfunction

function mouse_set_button_state: u32 button, u8 state
	if button == MOUSE_LEFT_BUTTON
		if state == MOUSE_BUTTON_DOWN
			if mouse_left_button_state == MOUSE_BUTTON_STATE_UP
				mouse_left_button_last_state_time=time_ticks
			endif
			mouse_left_button_state=MOUSE_BUTTON_STATE_DOWN
			mouse_event_add(MOUSE_LEFT_BUTTON, MOUSE_EVENT_DRAG)
			!klog("Left button down !\n")
		
		else ! state == MOUSE_BUTTON_UP
			last_drag_x=0
			last_drag_y=0
			if mouse_left_button_state==MOUSE_BUTTON_STATE_DOWN
				mouse_event_add(MOUSE_LEFT_BUTTON, MOUSE_EVENT_DROP)
			
				if time_ticks-mouse_left_button_last_state_time<500
					mouse_left_button_state=MOUSE_BUTTON_STATE_CLICKED
					mouse_event_add(MOUSE_LEFT_BUTTON, MOUSE_EVENT_CLICK)
					!klog("Left click!\n")
				endif
			endif
			mouse_left_button_state = MOUSE_BUTTON_STATE_UP
		endif
	endif
endfunction


function draw_cursor
	if isMouseInstalled==true && vram=/=null
		if previous_mouse_x>0 && previous_mouse_y>0
			 
			 draw_rect_from_buffer(previous_mouse_x, previous_mouse_y, previous_mouse_x+CURSOR_WIDTH, CURSOR_HEIGHT, underCursorImage)
			 
			 draw_rect_from_buffer_direct_draw(previous_mouse_x, previous_mouse_y, previous_mouse_x+CURSOR_WIDTH, CURSOR_HEIGHT, underCursorImage)
		endif
		 save_rect_to_buffer(mouse_x, mouse_y, mouse_x+CURSOR_WIDTH, CURSOR_HEIGHT, underCursorImage)
	 /*
	 draw_rect(mouse_x-(CURSOR_WIDTH/2), mouse_y-(CURSOR_HEIGHT/2), mouse_x+(CURSOR_WIDTH/2), CURSOR_HEIGHT, COLOR_WHITE)
	  draw_rect_direct_draw(mouse_x-(CURSOR_WIDTH/2), mouse_y-(CURSOR_HEIGHT/2), mouse_x+(CURSOR_WIDTH/2), CURSOR_HEIGHT, COLOR_WHITE)*/
	  draw_cursor_to_buff(mouse_x, mouse_y)
	  !draw_cursor_to_buff(hardware_vram, mouse_x, mouse_y)
	  previous_mouse_x=mouse_x
	  previous_mouse_y=mouse_y
	endif
endfunction

function draw_cursor_to_buff: u32 x, u32 y
	if x>vres_x || y>vres_y
		exit
	endif
	u32 i=0
	while i<CURSOR_HEIGHT
		draw_horizontal_line(x, y+i, x+i, COLOR_WHITE) 
		draw_horizontal_line_direct_draw(x, y+i, x+i, COLOR_WHITE) 
		i++
	endwhile
endfunction

function mouse_wait: uint8_t a_type
  uint32_t time_out=0x1ffff!; //unsigned int
  uint8_t tmp
  if a_type==0
    while time_out>0 !//Data
		time_out--
		tmp = inb(0x64)
		if(tmp & 1) == 1
			return true
		endif
		asm pause
    endwhile
  else
   while time_out>0 !//signal
		time_out--
		tmp = inb(0x64)
		if(tmp & 2) == 0
			return true
		endif
		asm pause
    endwhile
  endif
  return false
endfunction

function mouse_write: uint8_t a_write
  !//Wait to be able to send a command
  if mouse_wait(1) == false
    klog("Commande souris non envoyee\n")
	return false
  endif
 ! //Tell the mouse we are sending a command
  outb(0x64, 0xD4)
 ! //Wait for the final part
   if mouse_wait(1) == false
	klog("Commande souris non envoyee\n")
	return false
  endif
 ! //Finally write
  outb(0x60, a_write)
  return true
endfunction


function mouse_read: bool verbose
	uint8_t res=0
  !//Get's response from mouse
  if [u8](mouse_wait(0)) == true
	res = inb(0x60)
	return res
  endif
  if verbose
	klog("Commande souris non lue\n")
  endif
  return 0xffff
endfunction

