/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/

define READ_DISK_RAW_BLOCK_SIZE 1
define READ_WRITE_DISK_BUFFER_ADDRESS 0x4000 

su8 readOnly=false
suint8_t bootdrive=0
! default value will be overrided be the one passed by bootloader
sbool disk_only_use_bios


function init_drives
	reset_drive(0x00)
	reset_drive(0x01)
	reset_drive(0x80)
	reset_drive(0x81)
endfunction

! raw read from disk (always use this function)
function read_disk_raw:uint8_t drive, uint32_t lba, uint32_t size, ptr buff
	u32 res=0
	if drive >= 0x80 && disk_only_use_bios==false
		! use ata driver
		res = ata_read(drive-0x80, lba, size, buff)
	else
		! read using bios method 
		res = read_disk_raw_bios(drive, lba, size, buff)
	endif

endfunction

! read disk using bios
function read_disk_raw_bios:uint8_t drive, uint32_t lba, uint32_t size, ptr buff

	uint32_t blocksWritten=0
	if size == 0
		fatal_error("read_disk_raw>Trying to read disk with size == 0\n")
	endif
	
	if buff==null
		fatal_error("read_disk_raw>buff==null\n")
	endif
	
	
	! read by blocks of READ_DISK_RAW_BLOCK_SIZE sectors
	while size > READ_DISK_RAW_BLOCK_SIZE
		
		if read_disk_block(drive, lba, READ_DISK_RAW_BLOCK_SIZE, READ_WRITE_DISK_BUFFER_ADDRESS/0x10, 0) =/= 0
			mouse_resume()
			return 1
		endif

		memcpy(buff, READ_WRITE_DISK_BUFFER_ADDRESS, (READ_DISK_RAW_BLOCK_SIZE*512))
		size = size - READ_DISK_RAW_BLOCK_SIZE
		lba = lba + READ_DISK_RAW_BLOCK_SIZE
		buff = buff + (READ_DISK_RAW_BLOCK_SIZE*512)
		
		blocksWritten=blocksWritten+READ_DISK_RAW_BLOCK_SIZE

	endwhile
	
	! read last sectors ( < READ_DISK_RAW_BLOCK_SIZE)
	if read_disk_block(drive, lba, size,READ_WRITE_DISK_BUFFER_ADDRESS/0x10, 0) =/= 0
		mouse_resume()
		return 1
	endif

	blocksWritten=blocksWritten+size
	

	
	memcpy(buff, READ_WRITE_DISK_BUFFER_ADDRESS, (size * 512))
	
	return 0
endfunction

! raw write from disk (always use this function)
function write_disk_raw:uint8_t drive, uint32_t lba, uint32_t size, ptr buff
	u32 res=0
	if readOnly=/=true
		if drive >= 0x80 && disk_only_use_bios==false
			! use ata driver
			res = ata_write(drive-0x80, lba, size, buff)
		else
			! write using bios method 
			res = write_disk_raw_bios(drive, lba, size, buff)
		endif
	endif
endfunction

! write disk using bios
function write_disk_raw_bios:uint8_t drive, uint32_t lba, uint32_t size, ptr buff

	uint32_t blocksWritten=0
	
	if size == 0
		klog("ERROR: Trying to read disk with size = 0\n")
		pause()
		return 1
	endif

	
	! read by blocks of READ_DISK_RAW_BLOCK_SIZE sectors
	while size > READ_DISK_RAW_BLOCK_SIZE
		
		memcpy(READ_WRITE_DISK_BUFFER_ADDRESS, buff, (READ_DISK_RAW_BLOCK_SIZE*512))
		
		if write_disk_block(drive, lba, READ_DISK_RAW_BLOCK_SIZE, READ_WRITE_DISK_BUFFER_ADDRESS/0x10, 0) =/= 0
			return 1
		endif

		
		size = size - READ_DISK_RAW_BLOCK_SIZE
		lba = lba + READ_DISK_RAW_BLOCK_SIZE
		buff = buff + (READ_DISK_RAW_BLOCK_SIZE*512)
		
		blocksWritten=blocksWritten+READ_DISK_RAW_BLOCK_SIZE

	endwhile
	
	memcpy(READ_WRITE_DISK_BUFFER_ADDRESS, buff, (size * 512))
	
	! read last sectors ( < READ_DISK_RAW_BLOCK_SIZE)
	if write_disk_block(drive, lba, size,READ_WRITE_DISK_BUFFER_ADDRESS/0x10, 0) =/= 0
		return 1
	endif
	
	blocksWritten=blocksWritten+size
	

	return 0
endfunction

! effectue un reset d'un disque
function reset_drive:uint8_t drive
	int16(0x13,0,0,0,drive,0,0,0)
endfunction



function read_disk_block:uint8_t drive, uint32_t lba, uint16_t size, uint16_t seg_buff, uint16_t buff
	uint8_t error_code=0
	uint8_t n_try=2
	! int 0x13 ah=0x42 bios packet
	!						packet size		0			blckcnt		buff			seg_buff     	 		lba
	!suint8_t read_disk_packet={0x10,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}
	ptr read_disk_packet=0x7A00
	ptr packet_seeker
	uint16_t cylinder=0
	uint8_t head=0
	uint8_t sector=0
	uint8_t size_8_bits=[uint8_t]size
	uint16_t reg_cx=0
	! floppy
	if drive < 0x80
		ptr chs_array
		chs_array = malloc(4)
		lba2chs(drive, [uint16_t]lba, [ptr](chs_array), [ptr](chs_array+2), [ptr](chs_array+3))
		cylinder = [uint16_t]#chs_array
		head = [uint8_t]#(chs_array+2)
		sector = [uint8_t]#(chs_array+3)
		free(chs_array)
		reg_cx=[uint16_t]((cylinder&255)<<8) | ( (cylinder&768)>>2) | sector
		while n_try =/= 0
			

			error_code = ((int16(0x13, ((0x0200)|size_8_bits), buff, reg_cx, ((head<<0x08)|drive), seg_buff, 0, 0)) >>8 ) & 0xff
			

			if error_code =/= 0
				!call print_bios_disk_error:error_code, drive, lba, size
				reset_drive(drive)
				n_try--
			else

				return 0
			endif
			
		endwhile
	! hdd
	else
		while n_try =/= 0
		
			packet_seeker = read_disk_packet
			! clear previous data
			memset((read_disk_packet),0,0x10)
			#(packet_seeker) = [uint8_t]0x10
			
			#(packet_seeker+2) = [uint16_t]size
			
			#(packet_seeker+4) = [uint16_t]buff
			

			#(packet_seeker+6) = [uint16_t]seg_buff
			

			#(packet_seeker+8) = [uint32_t]lba
			

			
			!si = packet_seeker
			!ah = 0x42
			!dl = drive
			!_int 0x13
			
		
			error_code = ((int16(0x13, 0x4200, 0, 0, drive, 0, packet_seeker, 0) )  >>8 ) & 0xff
			
			! unsupported mode
			
			/*
			if error_code = 0x01
				! check normal mode
				call prints:{"Read use default\n"}
				call pause
				drive = drive - 0x80
				call bios_read_disk_raw: drive, lba, size, seg_buff, buff
			endif
			*/
			! check error
			if error_code =/= 0
				reset_drive(drive)
				!call print_bios_disk_error:error_code, drive, lba, size
				n_try--
			else
				return 0
			endif
			
		endwhile
	endif
	return error_code
endfunction

function write_disk_block:uint8_t drive, uint32_t lba, uint16_t size, uint16_t seg_buff, uint16_t buff
	uint8_t error_code=0
	uint8_t n_try=2
	! int 0x13 ah=0x42 bios packet
	!						packet size		0			blckcnt		buff			seg_buff     	 		lba
	!suint8_t write_disk_packet={0x10,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}
	ptr write_disk_packet=0x7A00
	ptr packet_seeker
	uint16_t cylinder=0
	uint8_t head=0
	uint8_t sector=0
	uint8_t size_8_bits=[uint8_t]size
	uint16_t reg_cx=0
	! floppy
	if drive < 0x80
		ptr chs_array
		chs_array = malloc(4)
		lba2chs(drive, [uint16_t]lba, [ptr](chs_array), [ptr](chs_array+2), [ptr](chs_array+3))
		cylinder = [uint16_t]#chs_array
		head = [uint8_t]#(chs_array+2)
		sector = [uint8_t]#(chs_array+3)
		free(chs_array)
		reg_cx=[uint16_t]((cylinder&255)<<8) | ( (cylinder&768)>>2) | sector
		while n_try =/= 0
			

			error_code = ((int16(0x13, ((0x0300)|size_8_bits), buff, reg_cx, ((head<<0x08)|drive), seg_buff, 0, 0)) >>8 ) & 0xff
			

			if error_code =/= 0

				!call print_bios_disk_error:error_code, drive, lba, size
				reset_drive(drive)
				n_try--
			else

				return 0
			endif
			
		endwhile
	! hdd
	else
		while n_try =/= 0
		
			packet_seeker = write_disk_packet
			! clear previous data
			memset((write_disk_packet),0,0x10)
			#(packet_seeker) = [uint8_t]0x10
			
			#(packet_seeker+2) = [uint16_t]size
			
			#(packet_seeker+4) = [uint16_t]buff
			

			#(packet_seeker+6) = [uint16_t]seg_buff
			

			#(packet_seeker+8) = [uint32_t]lba
			

			
			!si = packet_seeker
			!ah = 0x42
			!dl = drive
			!_int 0x13
			
		
			error_code = ((int16(0x13, 0x4300, 0, 0, drive, 0, packet_seeker, 0))  >>8 ) & 0xff
			
			! unsupported mode
			
			/*
			if error_code = 0x01
				! check normal mode
				call prints:{"Write use default\n"}
				call pause
				drive = drive - 0x80
				call bios_read_disk_raw: drive, lba, size, seg_buff, buff
			endif
			*/
			! check error
			if error_code =/= 0
				reset_drive(drive)
				!call print_bios_disk_error:error_code, drive, lba, size
				n_try--
			else
				return 0
			endif
			
		endwhile
	endif
	return error_code
endfunction

! convert lba to chs
function lba2chs:uint8_t drive, uint16_t lba, ptr sptr_cylinder, ptr sptr_head, ptr sptr_sector
	uint8_t heads_per_cylinder
	uint8_t sectors_per_track
	uint16_t cylinders_per_disk
	
	ptr chs_array
	chs_array = malloc(4)
	
	uint16_t temp=0
	
	! call bios_get_drive_parameters:drive, @cylinders_per_disk, @heads_per_cylinder, @sectors_per_track
	get_drive_parameters(drive, chs_array, (chs_array+2), (chs_array+3))
	cylinders_per_disk = #chs_array 
	heads_per_cylinder = #(chs_array+2)
	sectors_per_track = #(chs_array+3)
	free(chs_array)
	
	temp = [uint16_t] lba % (heads_per_cylinder * sectors_per_track)


	#sptr_cylinder = [uint16_t](lba / (heads_per_cylinder * sectors_per_track))
	
	#sptr_head = [uint8_t](temp / sectors_per_track)

	#sptr_sector = [uint8_t]((temp % sectors_per_track) + 1)
	
	
endfunction

! retourne le nombre max de cylindres, secteurs et tetes (ptr to uint8_t)
function get_drive_parameters:uint8_t drive, ptr max_cylinder, ptr max_head, ptr max_sector
	uint16_t c=0
	uint8_t s=0
	uint8_t h = 0
	uint16_t reg_cx=0
	ptr reg_buff
	reg_buff = malloc(8)
	/*
	if drive < 0x80
		#max_cylinder = [uint16_t]80
		#max_sector = [uint8_t]18
		#max_head = [uint8_t]2
	else
		#max_cylinder = [uint16_t]1023
		#max_sector = [uint8_t]63
		#max_head = [uint8_t]16
	endif*/
	! es:di = 0
	/*ax = 0
	push_register es
	es = ax
	di = 0
	
	ah = 0x8
	dl = drive

	asm int 0x13
	pop_register es*/
	!klog("get_drive_parameters\n")
	!sleep(2000)
	int16_out(0x13,(0x0800), 0, 0, drive, 0, 0, 0, null, null, reg_buff, reg_buff+4, null, null, null)
	
	reg_cx = [u16]#(reg_buff+2)
	
	h = [u8]#(reg_buff+6)
	free(reg_buff)
	h++
	c = ((reg_cx >> 8) & 0xff) | ((reg_cx & 0xc0) << 8)
	c++
	s= [uint8_t](reg_cx & 0x3f)

	if c > 0 && s > 0 && h > 0
		#max_cylinder = c
		#max_sector = s
		#max_head = h
	else
		if drive < 0x80
			#max_cylinder = [uint16_t]80
			#max_sector = [uint8_t]18
			#max_head = [uint8_t]2
		else
			#max_cylinder = [uint16_t]1023
			#max_sector = [uint8_t]63
			#max_head = [uint8_t]16
		endif
	endif
	/* klog("\nmax_cylinder ")
	 klogn(c)
	 klog("\nmax_sector ")
	 klogn(s)
	 klog("\nmax_head ")
	 klogn(h)
	 klog("\n")*/
	 !sleep(2000)
	 !pause()
endfunction