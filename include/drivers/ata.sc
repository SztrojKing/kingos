/*********************************
* This file is a part of King OS *
*  written by LANEZ Gaël - 2017  *
*********************************/

define ATA_MASTER 0x00
define ATA_SLAVE 0x01
define ATA_SECTOR 512
define ATA_MAX_BLOCKS_PER_OPERATION 1

function ata_common:u32 drive, u32 lba, u32 size

	outb(0x1F1, 0x00)      /* NULL byte to port 0x1F1 */
	outb(0x1F2, size)     /* Sector count */
	outb(0x1F3, lba)  /* Low 8 bits of the block address */
	outb(0x1F4, lba >> 8)   /* Next 8 bits of the block address */
	outb(0x1F5, lba >> 16)  /* Next 8 bits of the block address */

    /* Drive indicator, magic bits, and highest 4 bits of the block address */
    outb(0x1F6, 0xE0 | (drive << 4) | ((lba >> 24) & 0x0F))

    return 0
endfunction

! can't read more than 256 sectors at a time
function ata_block_read:u32 drive, u32 lba, u8 size, ptr buf

		
	u16 tmpword
	u32 idx=0
	
	! Wait for the drive to signal that it's ready
	ata_common(drive, lba, size)
	outb(0x1F7, 0x20)

	
	while ((inb(0x1F7) & 0x08)==0)
	endwhile
	
	while idx < ATA_SECTOR * size
			tmpword = inw(0x1F0)
			#(buf+idx) = [u8](tmpword)
			#(buf+idx+1) = [u16](tmpword>>8)
			idx+=2
	endwhile
	
	return size
	
	
/*
eax = lba
cl = size

edi = buf

asm               push eax
asm               push ebx
asm               push ecx
asm               push edx
asm               push edi
asm 
asm               mov ebx, eax         ; Save LBA in RBX
asm 
asm               mov edx, 0x01F6      ; Port to send drive and bit 24 - 27 of LBA
asm               shr eax, 24          ; Get bit 24 - 27 in al
asm               or al, 11100000b     ; Set bit 6 in al for LBA mode
asm               out dx, al
asm 
asm               mov edx, 0x01F2      ; Port to send number of sectors
asm               mov al, cl           ; Get number of sectors from CL
asm               out dx, al
asm 
asm               mov edx, 0x1F3       ; Port to send bit 0 - 7 of LBA
asm               mov eax, ebx         ; Get LBA from EBX
asm               out dx, al
asm 
asm               mov edx, 0x1F4       ; Port to send bit 8 - 15 of LBA
asm               mov eax, ebx         ; Get LBA from EBX
asm               shr eax, 8           ; Get bit 8 - 15 in AL
asm               out dx, al
asm 
asm 
asm               mov edx, 0x1F5       ; Port to send bit 16 - 23 of LBA
asm               mov eax, ebx         ; Get LBA from EBX
asm               shr eax, 16          ; Get bit 16 - 23 in AL
asm               out dx, al
asm 
asm               mov edx, 0x1F7       ; Command port
asm               mov al, 0x20         ; Read with retry.
asm               out dx, al
asm 
asm .still_going: 
asm in al, dx
asm               test al, 8           ; the sector buffer requires servicing.
asm               jz .still_going      ; until the sector buffer is ready.
asm 
asm               mov eax, 256         ; to read 256 words = 1 sector
asm               xor bx, bx
asm               mov bl, cl           ; read CL sectors
asm               mul bx
asm               mov ecx, eax         ; RCX is counter for INSW
asm               mov edx, 0x1F0       ; Data port, in and out
asm               rep insw             ; in to [RDI]
asm 
asm               pop edi
asm               pop edx
asm               pop ecx
asm               pop ebx
asm               pop eax

*/
			   return 1

endfunction

function ata_read:u32 drive, u32 lba, u32 size, ptr buf

	u32 i=0
	
	! read by blocks of ATA_MAX_BLOCKS_PER_OPERATION
	while size > ATA_MAX_BLOCKS_PER_OPERATION
		if ata_block_read(drive, lba, ATA_MAX_BLOCKS_PER_OPERATION, buf)==0
			return 1
		endif
		buf+=ATA_MAX_BLOCKS_PER_OPERATION*ATA_SECTOR
		size-=ATA_MAX_BLOCKS_PER_OPERATION
		lba+=ATA_MAX_BLOCKS_PER_OPERATION
	endwhile
	
	if ata_block_read(drive, lba, size, buf)==0
		return 1
	endif
	
	return 0
endfunction


function ata_block_write:u32 drive, u32 lba, u8 size, ptr buf

	u16 tmpword=0
	u32 idx=0
	u8 error=0xff
	u8 ntry=5
	ata_common(drive, lba, size)
	outb(0x1F7, 0x31)

	! Wait for the drive to signal that it's ready
	while ((inb(0x1F7) & 0x08)==0 && ntry>0)
		ntry--
	endwhile
	
	

	while idx<ATA_SECTOR*size
	

			
			tmpword=[u8]#(buf+idx+1)
			tmpword = ((tmpword<<8) & 0xff00)
			tmpword =  (tmpword |  ([u8]#(buf+idx)))
			outw(0x1F0, tmpword)
			
			error = inb(0x1f1)
			if(error>0)
				var_dump("Write error", error)
				return 0
			endif

			
			idx+=2
	endwhile

	return size
	/*
eax = lba
cl = size
edi = buf

asm    push eax
asm    push ebx
asm    push ecx
asm    push edx
asm    push edi
asm 
asm    mov ebx, eax         ; Save LBA in RBX
asm 
asm    mov edx, 0x01F6      ; Port to send drive and bit 24 - 27 of LBA
asm    shr eax, 24          ; Get bit 24 - 27 in al
asm    or al, 11100000b     ; Set bit 6 in al for LBA mode
asm    out dx, al
asm 
asm    mov edx, 0x01F2      ; Port to send number of sectors
asm    mov al, cl           ; Get number of sectors from CL
asm    out dx, al
asm 
asm    mov edx, 0x1F3       ; Port to send bit 0 - 7 of LBA
asm    mov eax, ebx         ; Get LBA from EBX
asm    out dx, al
asm 
asm    mov edx, 0x1F4       ; Port to send bit 8 - 15 of LBA
asm    mov eax, ebx         ; Get LBA from EBX
asm    shr eax, 8           ; Get bit 8 - 15 in AL
asm    out dx, al
asm 
asm 
asm    mov edx, 0x1F5       ; Port to send bit 16 - 23 of LBA
asm    mov eax, ebx         ; Get LBA from EBX
asm    shr eax, 16          ; Get bit 16 - 23 in AL
asm    out dx, al
asm 
asm    mov edx, 0x1F7       ; Command port
asm    mov al, 0x30         ; Write with retry.
asm    out dx, al
asm 
asm .still_going:  
asm 	in al, dx
asm    test al, 8           ; the sector buffer requires servicing.
asm    jz .still_going      ; until the sector buffer is ready.
asm 
asm    mov eax, 256         ; to read 256 words = 1 sector
asm    xor bx, bx
asm    mov bl, cl           ; write CL sectors
asm    mul bx
asm    mov ecx, eax         ; RCX is counter for OUTSW
asm    mov edx, 0x1F0       ; Data port, in and out
asm    mov esi, edi
asm    rep outsw            ; out
asm 
asm    pop edi
asm    pop edx
asm    pop ecx
asm    pop ebx
asm    pop eax
asm
*/
/*
   mov     dx,1f6h         ;Drive and head port
   mov     al,0a0h         ;Drive 0, head 0
   out     dx,al

   mov     dx,1f2h         ;Sector count port
   mov     al,1            ;Write one sector
   out     dx,al

   mov     dx,1f3h         ;Sector number port
   mov     al,1           ;Wrote to sector one
   out     dx,al

   mov     dx,1f4h         ;Cylinder low port
   mov     al,0            ;Cylinder 0
   out     dx,al

   mov     dx,1f5h         ;Cylinder high port
   mov     al,0            ;The rest of the cylinder 0
   out     dx,al

   mov     dx,1f7h         ;Command port
   mov     al,30h          ;Write with retry.
   out     dx,al
oogle:
   in      al,dx
   test    al,8            ;Wait for sector buffer ready.
   jz      oogle

   mov     cx,512/2        ;One sector /2
   mov     si,offset buffer
   mov     dx,1f0h         ;Data port - data comes in and out of here.
   rep     outsw           ;Send it.*/
	return 1
endfunction
/*
 Technical Information on the ports:
;      Port    Read/Write   Misc
;     ------  ------------ -------------------------------------------------
;       1f0       r/w       data register, the bytes are written/read here
;       1f1       r         error register  (look these values up yourself)
;       1f2       r/w       sector count, how many sectors to read/write
;       1f3       r/w       sector number, the actual sector wanted
;       1f4       r/w       cylinder low, cylinders is 0-1024
;       1f5       r/w       cylinder high, this makes up the rest of the 1024
;       1f6       r/w       drive/head
;                              bit 7 = 1
;                              bit 6 = 0
;                              bit 5 = 1
;                              bit 4 = 0  drive 0 select
;                                    = 1  drive 1 select
;                              bit 3-0    head select bits
;       1f7       r         status register
;                              bit 7 = 1  controller is executing a command
;                              bit 6 = 1  drive is ready
;                              bit 5 = 1  write fault
;                              bit 4 = 1  seek complete
;                              bit 3 = 1  sector buffer requires servicing
;                              bit 2 = 1  disk data read corrected
;                              bit 1 = 1  index - set to 1 each revolution
;                              bit 0 = 1  previous command ended in an error
;       1f7       w         command register
;                            commands:
;                              50h format track
;                              20h read sectors with retry
;                              21h read sectors without retry
;                              22h read long with retry
;                              23h read long without retry
;                              30h write sectors with retry
;                              31h write sectors without retry
;                              32h write long with retry
;                              33h write long without retry
;
*/
function ata_write:u32 drive, u32 lba, u32 size, ptr buf

u32 i=0
	
	! read by blocks of ATA_MAX_BLOCKS_PER_OPERATION
	while size > ATA_MAX_BLOCKS_PER_OPERATION
	
		if ata_block_write(drive, lba, ATA_MAX_BLOCKS_PER_OPERATION, buf)==0
			return 1
		endif
		buf+=ATA_MAX_BLOCKS_PER_OPERATION*ATA_SECTOR
		size-=ATA_MAX_BLOCKS_PER_OPERATION
		lba+=ATA_MAX_BLOCKS_PER_OPERATION

	endwhile

	if ata_block_write(drive, lba, size, buf)==0
		return 1
	endif
	
	return 0
endfunction
