/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/

define KEYBOARD_KEY_STATE_MAP_SIZE 0x80
define KEYBOARD_MAX_KEYS 32

define KEY_LSHIFT 0x29
define KEY_RSHIFT 0x35
define KEY_CTRL 0x1C
define KEY_ALT 0x37

! keys state map
sptr keyboard_key_state_map=null

! keys code to azerty map
sptr keyboard_key_map=null

! queued keyboard buffer
sptr keyboard_buffer=null
suint32_t keyboard_n_keys=0

sbool keyboard_in_reading=false

function keyboard_init
	u32 i = 0
	
	keyboard_buffer = malloc(KEYBOARD_MAX_KEYS)
	
	keyboard_key_state_map=malloc(KEYBOARD_KEY_STATE_MAP_SIZE)
	! set all keys to upper state
	while i < KEYBOARD_KEY_STATE_MAP_SIZE
		keyboard_set_key_state(i, false)
		i++
	endwhile
	
	keyboard_key_map = fat12_read_file("root0/system/x86/settings/keyboard/azerty.map")
	if keyboard_key_map == null
		return false
	endif
	
	return true
endfunction

! set the current state of a key
function keyboard_set_key_state:u8 key, bool state
	if key < 0x80 && keyboard_key_state_map =/= null
		#(keyboard_key_state_map+key) = [bool]state
	else
		return false
	endif
	return true
endfunction

! get the current state of a key
function keyboard_get_key_state:u8 key
	if key < 0x80 && keyboard_key_state_map =/= null
		return [bool]#(keyboard_key_state_map+key)
	else
		return 0
	endif
endfunction

! add a char to the keyboard buffer
function keyboard_add_char: u8 key
	if keyboard_buffer=/=null && keyboard_n_keys < KEYBOARD_MAX_KEYS
		! free some space if our buffer is full
	!	while keyboard_buffer_current_offset>=KEYBOARD_BUFFER_SIZE
		!	klog("[keyboard_add_char] keyboard buffer is full ! Deleting the oldest key\n")
		!	keyboard_get_last_char()
	!	endwhile
		
		! add new key
		#(keyboard_buffer+keyboard_n_keys) = [u8]key
		keyboard_n_keys++
	else
		klog("keyboard_add_char() : keyboard buffer full\n")
	endif
endfunction

! return the oldest (but not read) char of the keyboard buffer
function keyboard_get_last_char
	u8 res=0
	if keyboard_n_keys > 0
		res=#(keyboard_buffer)
		memcpy(keyboard_buffer, keyboard_buffer+1, keyboard_n_keys-1)
		keyboard_n_keys--
	endif
	return res
endfunction

! simulate a key press
function keyboard_add_input: u8 key
	u8 char
	
	if key < 0x80
		keyboard_set_key_state(key, true)
		char = scancode_to_char(key) 
		if char < 0xff && char =/= 0x1B && char > 0x09 || char == 0x08
			keyboard_add_char(scancode_to_char(key))
		endif
	else
		key = key - 0x80
		keyboard_set_key_state(key, false)
	endif


endfunction

sbool inMouseFlush=false
function keyboard_handler
	
	/*
	uint8_t i=0
    while (i & 0x01) == 0
          i = inb(0x64)
    endwhile

    keyboard_add_input(inb(0x60) - 1)
*/


	
	u8 c=0
	
	mouse_trigger_tick=time_ticks+500
		
    while true
        if inb(0x60)=/=c
            c=inb(0x60)
            if c>0
                keyboard_add_input(c - 1)
				exit
			endif
        endif
    endwhile
	


	
endfunction

! return the char associated to the key according to the current state of special keys(maj, alt, ...)

function scancode_to_char: u8 key
	if keyboard_key_map
		return [u8]#(keyboard_key_map+key*4 + (keyboard_get_key_state(KEY_LSHIFT) | keyboard_get_key_state(KEY_RSHIFT)) + (keyboard_get_key_state(KEY_CTRL)*2))
	else
		return key
	endif
endfunction

function scans: ptr buffer, u32 buffer_lenght, ptr end_chars
	u8 char = 0
	while char == 0
		char = keyboard_get_last_char()
		WAIT_FOR_INTERRUPT
	endwhile
	
	u32 i = 0
	while strchr(end_chars, char) == false && i<buffer_lenght
		klogc(char)
		#(buffer+i) = [u8]char
		char = 0
		while char == 0
			char = keyboard_get_last_char()
			WAIT_FOR_INTERRUPT
		endwhile
		i++
	endwhile
endfunction

function pause
	fatal_error("PAUSE()")
	klog("Appuyez sur une touche pour continuer...")
	while keyboard_get_last_char() == 0
		WAIT_FOR_INTERRUPT
	endwhile
endfunction
