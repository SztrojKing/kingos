/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/

define MOUSE_EVENT_MAX_EVENTS 64

enum MOUSE_EVENT_CLICK, MOUSE_EVENT_DOUBLE_CLICK, MOUSE_EVENT_DRAG, MOUSE_EVENT_DROP

defstruct mouse_event_struct
	u32 button
	u8 event_type
	u32 mouse_x
	u32 mouse_y
	u32 first_mouse_x
	u32 first_mouse_y
endstruct

sptr mouse_event_queue=null
su32 n_events=0xfffff

function mouse_event_add: u32 button, u8 type
	if n_events<MOUSE_EVENT_MAX_EVENTS && isMouseInstalled==true
		struct mouse_event_struct event = (mouse_event_queue+(n_events*mouse_event_struct.size))
		#(event.button) = [u32]button
		#(event.event_type) = [u8]type
		#(event.mouse_x) = [u32]mouse_x
		#(event.mouse_y) = [u32]mouse_y
		
		if m_first_drag_x > 0 
			#(event.first_mouse_x) = [u32](m_first_drag_x)
		else
			#(event.first_mouse_x) = [u32]mouse_x
		endif
		
		if m_first_drag_y > 0 
			#(event.first_mouse_y) = [u32](m_first_drag_y)
		else
			#(event.first_mouse_y) = [u32]mouse_y
		endif
		
		
		n_events++
	else
		klog("Mouse event buffer full\n")
	endif
endfunction

function mouse_event_get
	if n_events>0  && isMouseInstalled==true
		return [ptr](mouse_event_queue)
	endif
	return null
endfunction

function mouse_event_delete
	if n_events>0  && isMouseInstalled==true
		!free last event
		free([ptr]#mouse_event_queue)
		
		! override its address
		if n_events>1
			memcpy(mouse_event_queue, mouse_event_queue+mouse_event_struct.size, (mouse_event_struct.size*(n_events-1)))
		endif
		n_events--
	endif
endfunction

function mouse_event_init
	mouse_event_queue=malloc(MOUSE_EVENT_MAX_EVENTS*mouse_event_struct.size)
	n_events=0
endfunction

function mouse_event_clone: ptr hevent
	struct mouse_event_struct event_copy = malloc(mouse_event_struct.size)
	memcpy(event_copy, hevent, mouse_event_struct.size)
	return event_copy
endfunction
