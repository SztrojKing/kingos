/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/

define DO_SYSCALL {asm int 48}

function memset: ptr dest, uint32_t value, uint32_t size
	eax = SYSCALL_MEMSET
	ebx = dest
	ecx = value
	edx = size
	DO_SYSCALL
endfunction

function memcpy: ptr dest, ptr src, uint32_t size
	eax = SYSCALL_MEMCPY
	ebx = dest
	ecx = src
	edx = size
	DO_SYSCALL
endfunction

function cls
	eax = SYSCALL_CLS
	DO_SYSCALL
endfunction

function log: ptr str
	eax = SYSCALL_LOG
	ebx = str
	DO_SYSCALL
endfunction

function logn: u32 n
	eax = SYSCALL_LOGN
	ebx = n
	DO_SYSCALL
endfunction

function logx: u32 n
	eax = SYSCALL_LOGX
	ebx = n
	DO_SYSCALL
endfunction

function task_kill: u32 pid
	eax = SYSCALL_TASK_KILL
	ebx = pid
	DO_SYSCALL
endfunction

function exec: ptr path, ptr arg_list
	eax = SYSCALL_EXEC
	ebx = path
	ecx = arg_list
	DO_SYSCALL
endfunction

function get_time_ticks
	eax = SYSCALL_GET_TIME_TICKS
	DO_SYSCALL
endfunction

function shedule
	eax = SYSCALL_SHEDULE_LATER
	DO_SYSCALL
	! halt to shedule
	asm hlt
endfunction

function refresh_screen
	eax = SYSCALL_REFRESH_SCREEN
	DO_SYSCALL
endfunction

function get_video_info
	eax = SYSCALL_GET_VIDEO_INFO
	DO_SYSCALL
endfunction

function malloc: u32 size
	eax = SYSCALL_MALLOC
	ebx = size
	DO_SYSCALL
endfunction

function free: ptr address
	eax = SYSCALL_FREE
	ebx = address
	DO_SYSCALL
endfunction

function rand: u32 min, u32 max
	eax = SYSCALL_RAND
	ebx = min
	ecx = max
	DO_SYSCALL
endfunction

function window_create: u32 x, u32 y, ptr title
	eax = SYSCALL_WINDOW_CREATE
	ebx = x
	ecx = y
	edx = title
	DO_SYSCALL
endfunction

function button_create:ptr win, u32 x, u32 y, u32 width, ptr title
	ptr params = object_list_create(5)
	u32 res = 0
	object_list_add(params, win)
	object_list_add(params, x)
	object_list_add(params, y)
	object_list_add(params, width)
	object_list_add(params, title)
	eax = SYSCALL_BUTTON_CREATE
	ebx = params
	DO_SYSCALL
	res = eax
	free(params)
	return res
endfunction

function textbox_create:ptr win, u32 x, u32 y, u32 width, ptr title, u32 max_text_length
	
	ptr params = object_list_create(6)
	u32 res = 0
	object_list_add(params, win)
	object_list_add(params, x)
	object_list_add(params, y)
	object_list_add(params, width)
	object_list_add(params, title)
	object_list_add(params, max_text_length)
	eax = SYSCALL_TEXTBOX_CREATE
	ebx = params
	DO_SYSCALL
	res = eax
	free(params)
	return res
endfunction

function label_create:ptr win, u32 x, u32 y, u32 width, u32 height, ptr title
	ptr params = object_list_create(6)
	u32 res = 0
	object_list_add(params, win)
	object_list_add(params, x)
	object_list_add(params, y)
	object_list_add(params, width)
	object_list_add(params, height)
	object_list_add(params, title)
	eax = SYSCALL_LABEL_CREATE
	ebx = params
	DO_SYSCALL
	res = eax
	free(params)
	return res
	
endfunction

function label_set_text:ptr widget, ptr text
	eax = SYSCALL_LABEL_SET_TEXT
	ebx = widget
	ecx = text
	DO_SYSCALL
endfunction

function window_lock: ptr win, u32 lock_state
	eax = SYSCALL_WINDOW_LOCK
	ebx = win
	ecx = lock_state
	DO_SYSCALL
endfunction

function windows_manager_add_window: ptr win
	eax = SYSCALL_WINDOWS_MANAGER_ADD_WINDOW
	ebx = win
	DO_SYSCALL
endfunction

function windows_manager_set_focus: ptr win
	eax = SYSCALL_WINDOWS_MANAGER_SET_FOCUS
	ebx = win 
	DO_SYSCALL
endfunction

function window_get_last_event: ptr win
	eax = SYSCALL_WINDOW_GET_LAST_EVENT
	ebx = win 
	DO_SYSCALL
endfunction

function window_remove_widget: ptr win, ptr widget
	eax = SYSCALL_WINDOW_REMOVE_WIDGET
	ebx = win 
	ecx = widget
	DO_SYSCALL
endfunction 

function widget_free: ptr widget
	eax = SYSCALL_WIDGET_FREE
	ebx = widget
	DO_SYSCALL
endfunction

function window_delete_last_event: ptr win
	eax = SYSCALL_WINDOW_DELETE_LAST_EVENT
	ebx = win 
	DO_SYSCALL
endfunction

function windows_manager_close_window: ptr win
	eax = SYSCALL_WINDOWS_MANAGER_CLOSE_WINDOW
	ebx = win 
	DO_SYSCALL
endfunction

function window_free: ptr win
	eax = SYSCALL_WINDOW_FREE
	ebx = win 
	DO_SYSCALL
endfunction

function textbox_get_text: ptr textbox
	eax = SYSCALL_TEXTBOX_GET_TEXT
	ebx = textbox
	DO_SYSCALL
endfunction

function window_event_get_widget: ptr event
	eax = SYSCALL_WINDOW_EVENT_GET_WIDGET
	ebx = event 
	DO_SYSCALL
endfunction

function window_event_get_cat: ptr event
	eax = SYSCALL_WINDOW_EVENT_GET_CAT
	ebx = event
	DO_SYSCALL
endfunction

function window_attach_pid: ptr win, u32 pid
	eax = SYSCALL_WINDOW_ATTACH_PID
	ebx = win 
	ecx = pid
	DO_SYSCALL
endfunction

function window_hide: ptr win
	eax = SYSCALL_WINDOW_HIDE
	ebx = win 
	DO_SYSCALL
endfunction

function get_console_from_pid: u32 pid
	eax = SYSCALL_GET_CONSOLE_FROM_PID
	ebx = pid
	DO_SYSCALL
endfunction

function fatal_error: ptr msg
	eax = SYSCALL_FATAL_ERROR
	ebx = msg
	DO_SYSCALL
endfunction

function read_file: ptr path
	eax = SYSCALL_READ_FILE
	ebx = path
	DO_SYSCALL
endfunction

function get_file_size: ptr path
	eax = SYSCALL_GET_FILE_SIZE
	ebx = path
	DO_SYSCALL
endfunction

function write_file: ptr path, ptr data, u32 size
	eax = SYSCALL_WRITE_FILE
	ebx = path
	ecx = data
	edx = size
	DO_SYSCALL
endfunction

function create_directory: ptr path
	eax = SYSCALL_CREATE_DIRECTORY
	ebx = path
	DO_SYSCALL
endfunction

function read_disk_raw:uint8_t drive, uint32_t lba, uint32_t size, ptr buff
	ptr params = object_list_create(4)
	u32 res = 0
	
	object_list_add(params, drive)
	object_list_add(params, lba)
	object_list_add(params, size)
	object_list_add(params, buff)
	
	eax = SYSCALL_READ_DISK_RAW
	ebx = params
	
	DO_SYSCALL
	
	res = eax
	free(params)
	return res
endfunction

function write_disk_raw:uint8_t drive, uint32_t lba, uint32_t size, ptr buff
	ptr params = object_list_create(4)
	u32 res = 0
	
	object_list_add(params, drive)
	object_list_add(params, lba)
	object_list_add(params, size)
	object_list_add(params, buff)
	
	eax = SYSCALL_WRITE_DISK_RAW
	ebx = params
	
	DO_SYSCALL
	
	res = eax
	free(params)
	return res
endfunction

function textbox_set_text:ptr widget, ptr text
	eax = SYSCALL_TEXTBOX_SET_TEXT
	ebx = widget
	ecx = text
	DO_SYSCALL
endfunction

function window_free_content: ptr hwin
	eax = SYSCALL_WINDOW_FREE_CONTENT
	ebx = hwin
	DO_SYSCALL
endfunction

function textbox_set_color: ptr hwidget, u32 color
	eax = SYSCALL_TEXTBOX_SET_COLOR
	ebx = hwidget
	ecx = color
	DO_SYSCALL
endfunction

function button_set_color: ptr hwidget, u32 color
	eax = SYSCALL_BUTTON_SET_COLOR
	ebx = hwidget
	ecx = color
	DO_SYSCALL
endfunction

function button_set_background_color: ptr hwidget, u32 color
	eax = SYSCALL_BUTTON_SET_BACKGROUND_COLOR
	ebx = hwidget
	ecx = color
	DO_SYSCALL
endfunction

function vscroll_create: ptr parent_hcontainer, u32 x, u32 y, u32 width, u32 height
	ptr params = object_list_create(5)
	u32 res = 0
	
	object_list_add(params, parent_hcontainer)
	object_list_add(params, x)
	object_list_add(params, y)
	object_list_add(params, width)
	object_list_add(params, height)
	
	eax = SYSCALL_VSCROLL_CREATE
	ebx = params
	
	DO_SYSCALL
	
	res = eax
	free(params)
	return res
endfunction

function container_get_container: ptr hwidget
	eax = SYSCALL_CONTAINER_GET_CONTAINER
	ebx = hwidget
	DO_SYSCALL
endfunction

function ask_sleep: u32 ms
	eax = SYSCALL_SLEEP
	ebx = ms
	DO_SYSCALL
endfunction

function reboot
	eax = SYSCALL_REBOOT
	DO_SYSCALL
endfunction

function shutdown
	eax = SYSCALL_SHUTDOWN
	DO_SYSCALL
endfunction
