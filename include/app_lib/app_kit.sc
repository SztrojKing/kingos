/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/

include "../../include/common/color.sc"
include "../../include/scdef.sc"
include "../../include/common/syscall_header.sc"
include "../../include/common/kingdef.sc"

DEFINE_AS_APPLICATION

include "../../include/scdef.sc"
include "../../include/app_lib/syscall.sc"
include "../../include/app_lib/lib.sc"
include "../../include/string.sc"
include "../../include/mem.sc"
include "../../include/common/object_list.sc"


su32 current_pid

function entry
	ptr arg_list
	current_pid = eax
	arg_list = ebx
	
	
	main(arg_list)
	
	free(arg_list)
	
	task_kill(current_pid)
	asm hlt
	loop
endfunction

function hide_console
	window_hide(get_console_from_pid(current_pid))
endfunction
