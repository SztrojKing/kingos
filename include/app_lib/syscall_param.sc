/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/

function syscall_create_param_list: u32 number_of_param
	return malloc(number_of_param*4)
endfunction

function syscall_add_param_32: ptr param_list, u32 index, u32 param
	#(param_list+(index*4)) = [u32](param)
endfunction

function syscall_add_param_16: ptr param_list, u32 index, u32 param
	#(param_list+(index*4)) = [u16](param)
endfunction

function syscall_add_param_8: ptr param_list, u32 index, u32 param
	#(param_list+(index*4)) = [u8](param)
endfunction

function syscall_get_param: ptr param_list, u32 index
	return #(param_list+(index*4))
endfunction