/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/

include "../../include/mem/bit.sc"
include "../../include/mem/memcmp.sc"
include "../../include/mem/memswap.sc"

function sleep: u32 ms
	u32 stop=get_time_ticks()+ms
	
	! dont change task if we sleep for a short time
	/*if ms<5
		while stop>get_time_ticks()
			asm hlt
		endwhile
	else*/
		ask_sleep(ms)
		shedule()
	!endif
endfunction

function memcpy_from_end: ptr dest, ptr src, uint32_t size
	while size > 0
		#(dest+size-1) = [uint8_t]#(src+size-1)
		size--
	endwhile
endfunction

