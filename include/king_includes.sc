/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/

define CURSOR_WIDTH 8
define CURSOR_HEIGHT 10

enum NEXT_REDRAW_TYPE_NORMAL, NEXT_REDRAW_TYPE_REFRESH_ALL

sptr kernel_address=0
sptr kernel_stack_address=0

include "include/scdef.sc"
include "include/common/kingdef.sc"
include "include/init/gdt.sc"

! ****** Memory ******
include "include/mem.sc"
include "include/mem/memcpy.sc"
include "include/mem/memset.sc"
include "include/mem/memcmp.sc"
include "include/mem/memswap.sc"
include "include/mem/bit.sc"

! ****** Init ******
include "include/init/memmap.sc"
include "include/init/idt.sc"
include "include/init/pic.sc"
include "include/init/pit.sc"
include "include/init/paging.sc"

! ****** King internals ******
include "include/io.sc"
include "include/malloc.sc"
include "include/string.sc"
include "include/common/color.sc"
include "include/console.sc"
include "include/time.sc"
include "include/common/syscall_header.sc"
include "include/multitasking.sc"
include "include/syscall.sc"
include "include/common/object_list.sc"
include "include/exec.sc"
include "include/random.sc"
include "include/cpu_usage.sc"
include "include/power/power.sc"
include "include/interrupts.sc"

! ****** Graphics ******
include "include/graphics/struct.sc"
include "include/graphics/plot.sc"
include "include/graphics/print.sc"
include "include/graphics/shape.sc"
include "include/graphics/graphics.sc"
include "include/graphics/bmp.sc"

! ****** Drivers ******
include "include/x16_drivers/x16.sc"
include "include/drivers/disk.sc"
include "include/drivers/ata.sc"
include "include/drivers/fat12.sc"
include "include/drivers/keyboard.sc"
include "include/drivers/mouse_events.sc"
include "include/drivers/mouse.sc"
include "include/drivers/rtc.sc"

! ****** CPU ******
include "include/cpu/sse.sc"


! ****** GUI ******
include "include/gui/desktop.sc"
include "include/gui/widgets/container/container.sc"
include "include/gui/widget.sc"
include "include/gui/window.sc"
include "include/gui/windows_manager.sc"
include "include/gui/widgets/label.sc"
include "include/gui/widgets/button.sc"
include "include/gui/widgets/textbox.sc"
include "include/gui/widgets/vscrollbar.sc"
include "include/gui/widgets/container/vscroll.sc"
include "include/gui/widgets/console.sc"
