/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/

defstruct time_struct
	u8 hour
	u8 min
	u8 sec
endstruct

suint32_t time_ticks=0


function time_handler
	!if time_ticks%1000==0
		!console_coords_printn(0,0,CONSOLE_DEFAULT_COLOR,time_ticks)
		!if vram=/=null
			! refresh console
			!print_console_buffer(((vres_x/2)-320),vres_y/2-130,((vres_x/2)+320), (vres_y/2+120), CONSOLE_VIDEO_MEM)
			! refresh screen
			!refresh_screen()
		!endif
	!endif
	time_ticks++
	
	if time_ticks>mouse_trigger_tick
		mouse_refresh()
		mouse_trigger_tick+=40
	endif
	
	if time_ticks%1000 == 0
		cpu_usage_second_elapsed()
	endif
	
endfunction

/*
function sleep: u32 ms
	u32 stop=time_ticks+ms
	
	while stop>time_ticks
		asm hlt
	endwhile
endfunction
*/
function sleep: u32 ms
	u32 stop=time_ticks+ms
	
	while stop>time_ticks
		if stop+10 >= time_ticks && isMultitaskingStarted==true
			multitask_shedule_later()
		else
			asm hlt
		endif
		
	endwhile
endfunction


! get time
! returns a time_struct
function time_get_time
	su32 last_time_access=0

	struct time_struct time = malloc(time_struct.size)
	suint8_t hour=0
	suint8_t min=0
	suint8_t sec=0
	
	! don't do a syscall more than every 1 sec
	if time_ticks - last_time_access >= 1000 || last_time_access==0
	
		hour = rtc_get_hour()
		min = rtc_get_minute()
		sec = rtc_get_second()
		
		last_time_access=time_ticks
	endif
	/*
	#time.sec = [uint8_t](sec+time_ticks - last_time_access)%60
	#time.min = [uint8_t](min)+(sec+(time_ticks - last_time_access)/1000)/60
	#time.hour = [uint8_t](hour)
*/
	#time.hour = [u8](hour)
	#time.min = [u8](min)
	#time.sec = [u8](sec)
	
	return time
endfunction

function time_to_str_hms: u32 h, u32 m, u32 s
	ptr str = malloc(16)
	if h < 10
		strcat(str, "0")
	endif
	strcatn(str, h)
	strcat(str, ":")
	if m < 10
		strcat(str, "0")
	endif
	strcatn(str, m)
	strcat(str, ":")
	if s < 10
		strcat(str, "0")
	endif
	strcatn(str, s)
	return str
endfunction

function time_to_str_DMY_hms: u32 day, u32 month, u32 year, u32 h, u32 m, u32 s
	ptr str = malloc(64)

	if day < 10
		strcat(str, "0")
	endif
	strcatn(str, day)
	strcat(str, "/")
	if month < 10
		strcat(str, "0")
	endif
	strcatn(str, month)
	strcat(str, "/")

	strcatn(str, year)
	strcat(str, " ")

	if h < 10
		strcat(str, "0")
	endif
	strcatn(str, h)
	strcat(str, ":")
	if m < 10
		strcat(str, "0")
	endif
	strcatn(str, m)
	strcat(str, ":")
	if s < 10
		strcat(str, "0")
	endif
	strcatn(str, s)
	return str
endfunction

function time_to_str_hm: u32 h, u32 m
	ptr str = malloc(16)
	if h < 10
		strcat(str, "0")
	endif
	strcatn(str, h)
	strcat(str, ":")
	if m < 10
		strcat(str, "0")
	endif
	strcatn(str, m)
	return str
endfunction

function get_date_time_str
	return time_to_str_DMY_hms(rtc_get_day(), rtc_get_month(), rtc_get_year(), rtc_get_hour(), rtc_get_minute(), rtc_get_second())
endfunction
