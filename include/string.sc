/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/


define 16_BITS_MAX_DIGITS 0
define INT_DIGITS 19



! compare 2 chaines de caractere et retourne 0 si elles sont �gales
function strcmp:ptr str_1, ptr str_2
	while [uint8_t]#str_1 =/= 0
		if  [uint8_t]#str_1 =/= [uint8_t]#str_2
			return 1
		endif
		str_1++
		str_2++
	endwhile
	
	if  [uint8_t]#str_1 =/= [uint8_t]#str_2
		return 1
	endif
		
	return 0
endfunction

function strlen: ptr str
	uint32_t len=0
	if str =/= null
		while [uint8_t]#(str+len)=/=0
			len++
		endwhile
	endif
	return len
endfunction

function strcat: ptr dest, ptr src
	uint32_t destLen
	uint32_t srcLen
	destLen = strlen(dest)
	srcLen = strlen(src)
	
	memcpy((dest+destLen), src, (srcLen+1))
	return dest
endfunction

function strcatn: ptr dest, ptr n
	ptr number_str = malloc(INT_DIGITS+1)
	itoa(number_str, n)
	ptr res = [ptr](strcat(dest, number_str))
	free(number_str)
	return res
endfunction

function strcpy: ptr dest, ptr src
	uint32_t srcLen=0
	srcLen = strlen(src)
	
	memcpy(dest, src, (srcLen+1))
endfunction

function strchr: ptr str, u8 char
	while [u8]#(str) =/= 0
		if [u8]#(str) == char
			return true
		endif
		str++
	endwhile
	
	return false
endfunction

function itoa: ptr dest, uint32_t number
	uint32_t digits=0
	#dest = [uint8_t]'0'
	while number =/=0
		#(dest+digits) = [uint8_t]('0'+(number%10))
		number=number/10
		digits++
	endwhile
	memreverse(dest, digits)
endfunction

function itox: ptr dest, uint32_t number
	uint32_t digits=0
	#dest = [uint8_t]'0'
	while number =/=0
		if number%16 < 10
			#(dest+digits) = [uint8_t]('0'+(number%16))
		else
			#(dest+digits) = [uint8_t]('a'+(number%16) - 10)
		endif
		number=number/16
		digits++
	endwhile
	memreverse(dest, digits)
endfunction

function atoi:ptr str
	uint32_t res=0
	while [uint8_t]#(str) >= '0' && [uint8_t]#(str) <= '9'
		res = (res * 10) + ([uint8_t]#(str)-'0')
		str++
	endwhile
	return res
endfunction

function toUpper: ptr str
	while [uint8_t]#str =/= 0
		if [uint8_t](#str) >= [uint8_t]'a' && [uint8_t](#str) <= [uint8_t]'z'
			#str=[uint8_t]((#str)+('A'-'a'))
		endif
		str++
	endwhile

endfunction

function clone_str: ptr str
	u32 len = strlen(str)
	ptr res = malloc(len+1)
	memcpy(res, str, len)
	return res
endfunction


function str_del_pos: ptr str, u32 pos
	u32 len = strlen(str)
	if len > 0
		if pos <= len
			memcpy(str+pos, str+pos+1, len-pos)
			#(str+len-1) = [u8]0
		endif
	endif
endfunction