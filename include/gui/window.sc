/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/

enum WINDOW_TYPE_NORMAL, WINDOW_TYPE_CONSOLE


define WINDOW_DEFAULT_COORD_X 0
define WINDOW_DEFAULT_COORD_Y 0
define WINDOW_BORDER_SIZE 24
define WINDOW_SIDE_BORDER_SIZE 4
define WINDOW_BACKGROUND_COLOR 0x2c3234
define WINDOW_UNFOCUSED_BACKGROUND_COLOR 0x3c4244
define WINDOW_TEXT_COLOR 0xb7c0c8
define WINDOW_BACKGROUND_COLOR_LOCKED 0xaaaaaaaa
define WINDOW_MAX_WIDGETS 32
define WINDOW_MAX_EVENTS 32
define SCREEN_TOOL_BAR_HEIGHT 32



defstruct window_struct

	! extend container
	u32 container_type
	u32 content_width
	u32 content_height
	u32 content_coord_x
	u32 content_coord_y
	u32 background_color
	ptr widgets
		
	u8 type
	u32 width
	u32 height
	u32 coord_x
	u32 coord_y
	ptr console_widget
	ptr title
	
	bool hidden
	bool lock_state
	bool minimized
	
	u32 parent_pid
	
	ptr events
	u32 n_events
	
	ptr focused_widget
	
	ptr bmp
	bool needs_redraw
endstruct

defstruct window_event_struct
	u32 event_cat ! mouse or keyboard
	u32 x
	u32 y
	u32 value ! pressed button or key
	u32 type ! press or release ?
	ptr widget
endstruct
! creates a new window
function window_create: u32 width, u32 height, ptr title
	u32 abs_width = convert_relative_coord_to_absolute(width, virtual_vres_x)
	u32 abs_height = convert_relative_coord_to_absolute(height, virtual_vres_y)
	if abs_width+(+WINDOW_SIDE_BORDER_SIZE*2) >= virtual_vres_x-1
		abs_width = virtual_vres_x-(WINDOW_SIDE_BORDER_SIZE*2)-1
	endif
	
	if abs_height+WINDOW_BORDER_SIZE+WINDOW_SIDE_BORDER_SIZE >= virtual_vres_y-TOOLBAR_HEIGHT-1
		abs_height = virtual_vres_y-TOOLBAR_HEIGHT-WINDOW_BORDER_SIZE+WINDOW_SIDE_BORDER_SIZE-1
	endif
	return window_create_abs(abs_width, abs_height, title)
endfunction

! creates a new window with an absolute size
function window_create_abs: u32 abs_width, u32 abs_height, ptr title
	struct window_struct window = malloc(window_struct.size)
	#(window.container_type) = CONTAINER_WINDOW
	#(window.type) = [u8]WINDOW_TYPE_NORMAL
	#(window.width) = abs_width+(WINDOW_SIDE_BORDER_SIZE*2)
	#(window.height) = abs_height+(WINDOW_BORDER_SIZE+WINDOW_SIDE_BORDER_SIZE)
	#(window.coord_x) = [u32](n_windows*(virtual_vres_x/10))%(virtual_vres_x-(#(window.width)))
	#(window.coord_y) = [u32](n_windows*(virtual_vres_y/10))%(virtual_vres_y-(#(window.height))-TOOLBAR_HEIGHT)+TOOLBAR_HEIGHT
	#(window.content_width) = [u32](abs_width)
	#(window.content_height) = [u32](abs_height)
	#(window.content_coord_x) = WINDOW_SIDE_BORDER_SIZE
	#(window.content_coord_y) = WINDOW_BORDER_SIZE
	
	#(window.console_widget) = null
	#(window.title) = [ptr](clone_str(title))
	
	#(window.hidden) = false
	
	#(window.parent_pid) = [u32]MAX_TASKS
	
	#(window.widgets) = [ptr](malloc(WINDOW_MAX_WIDGETS*SIZEOF_PTR))
	#window.events = [ptr](malloc(WINDOW_MAX_EVENTS*SIZEOF_PTR))
	#window.n_events = 0
	
	#window.background_color = WINDOW_BACKGROUND_COLOR
	
	#window.needs_redraw = true
	#window.bmp = malloc([u32](#(window.width)) * (#(window.height)) * vbytepp)
	
	return window
endfunction

! creates a new console window
function window_create_console: ptr title
	struct window_struct window = window_create_abs(((CONSOLE_MAX_CHAR_WIDTH+2)*CHAR_X_SIZE), (CONSOLE_MAX_CHAR_HEIGHT*CHAR_Y_SIZE)+1, title)
	#(window.type) = [u8]WINDOW_TYPE_CONSOLE
	
	! alloc console buffer
	#(window.console_widget) = widget_get_child(console_create(window, 0, 0))
	

	return window
endfunction

! draw a window
function window_draw: ptr hwindow, ptr vram_buffer
	struct window_struct window = hwindow
	u32 window_background_color=WINDOW_UNFOCUSED_BACKGROUND_COLOR
	u32 i=0
	u32 coord_x=[u32]#(window.coord_x)
	u32 coord_y=[u32]#(window.coord_y)
	u32 width = [u32]#(window.width)
	u32 height = [u32]#(window.height)
	
	ptr bmp = [ptr]#window.bmp	
	bool need_redraw = #window.needs_redraw
	
	! is focused window?
	if window == window_manager_get_focused_window()
		window_background_color=WINDOW_BACKGROUND_COLOR
	endif
	
	#window.background_color = window_background_color
		
	if #(window.hidden) == false
			
			if need_redraw == true
				! draw window background
				draw_rect_buffer(bmp, width, height, 1, 1, width-1, height-2, window_background_color)
				! draw window borders
				draw_rect_border_buffer(bmp, width, height, 0, 0, width-1, height, 1, COLOR_BLACK)
				! close button
				plot_char_buffer(bmp, width, height, width-15, 5, 0xff0000, 'X')
				! title
				prints_rect_buffer(bmp, width, height, get_centered_text_coord(#(window.title), (width/2)), 4, width, 4+CHAR_Y_SIZE, WINDOW_TEXT_COLOR, [ptr]#(window.title))
				! minimize button
				plot_char_buffer(bmp, width, height, width-30, 5, COLOR_WHITE, '_')
				
				#window.needs_redraw = false
			endif
			
			if #window.minimized == false
				if #window.lock_state == false
					
					! draw all widgets
					while i < WINDOW_MAX_WIDGETS*4
						!klog("Drawing widget ")
						ptr widget = #(window.widgets)
						if [ptr]#(widget+i) =/= null
						
							if need_redraw == true
								widget_need_redraw(#(widget+i))
							endif
							
							widget_draw(#window.bmp, #window.width, [ptr]#(widget+i), #(window.content_coord_x), #(window.content_coord_y))
						endif
						i+=4
					endwhile
				endif
					
				! draw window
				window_to_vram(window, vram_buffer)
				
			endif


	endif
endfunction

! copy window bmp to vram
function window_to_vram: ptr hwindow, ptr vram_buffer
	! copy window to vram
	struct window_struct window = hwindow
	u32 i=0
	u32 height = [u32]#(window.height)
	u32 width = [u32]#(window.width)
	ptr bmp = #window.bmp
	
	u32 absolute_x1 = [u32]#window.coord_x
	u32 absolute_y1 = [u32]#window.coord_y
	
	! optimization
	u32 width_in_bytepp = width*vbytepp
	
	while i<height
		memcpy(vram_buffer+(absolute_x1+(absolute_y1+i)*vres_x)*vbytepp, bmp+i*width_in_bytepp, width_in_bytepp)
		i++
	endwhile
endfunction

! refresh buffer
function window_need_redraw: ptr hwindow

	struct window_struct window = hwindow
	#window.needs_redraw = true
	
endfunction

! return the console buffer of the window
function window_get_console_widget: ptr hwindow
	struct window_struct window = hwindow
	return [ptr]#(window.console_widget)
endfunction

function window_hide: ptr hwindow
	struct window_struct window = hwindow
	#(window.hidden) = true
	windows_manager_loose_focus(window)
endfunction

function window_show: ptr hwindow
	struct window_struct window = hwindow
	#(window.hidden) = false
	windows_manager_loose_focus(window)
endfunction

function window_set_title: ptr win, ptr title
	struct window_struct window = win
	#(window.title) = [ptr](clone_str(title))
	#window.needs_redraw = true
	
endfunction

function window_set_coords: ptr hwindow, u32 x, u32 y
	struct window_struct window = hwindow
	u32 i = 0
	ptr widgets = #(window.widgets)
	! don't move the window out of the screen
	! DONT MODIFY THIS CHECK
	if #window.minimized == false
		if x>=virtual_vres_x || y>=virtual_vres_y || x + [u32]#(window.width) >= virtual_vres_x || y + [u32]#(window.height) >= virtual_vres_y || y<toolbar_height
			exit
		endif
	else
		if x>=virtual_vres_x || y>=virtual_vres_y || x + [u32]#(window.width) >= virtual_vres_x || y + WINDOW_BORDER_SIZE >= virtual_vres_y || y<toolbar_height
			exit
		endif
	endif
	#(window.coord_x) = [u32]x
	#(window.coord_y) = [u32]y
	
	! update position to all his childs
	/*
	while i < WINDOW_MAX_WIDGETS*4
		struct widget_struct widget = [ptr]#(widgets+i)
		if widget =/= null
			widget_container_position_changed(window, widget)
		endif
		i+=4
	endwhile
	*/
endfunction

! lock or unlock a window
function window_lock: ptr hwindow, bool lock_state
	struct window_struct window = hwindow
	#window.lock_state = lock_state
	
	#window.needs_redraw = true
endfunction


! delete a window and it's children
function window_free: ptr hwindow
	struct window_struct window = hwindow
	windows_manager_close_window(window)
	window_free_content(window)
	! delete widget list
	free(#(window.widgets))
	
	free(#(window.title))
	free(#window.bmp)
	
	
	! dont free the console buffer, it will be freed when the task will be killed

	
	free(#window.events)
	free(hwindow)
endfunction

! attach a pid to a window (closing the window will stop the process)
function window_attach_pid: ptr hwindow, u32 pid
	struct window_struct window = hwindow
	#(window.parent_pid) = pid
endfunction

! toggle the minimized state
function window_toggle_minimized: ptr hwindow
	struct window_struct window = hwindow
	if #(window.minimized) == false
		#(window.minimized) = [bool]true
	else
		! if the window is out of the screen, move it
		if [u32]#(window.coord_y) + [u32]#(window.height) >= virtual_vres_y
			window_set_coords(window, [u32]#(window.coord_x), 0 )
		endif
		#(window.minimized) = [bool]false
	endif
	
	#window.needs_redraw = true
	
	! refresh instantly
	next_redraw_type = NEXT_REDRAW_TYPE_REFRESH_ALL
endfunction

! add a widget
function window_add_widget: ptr hwindow, ptr hwidget
	struct window_struct window = hwindow
	struct widget_struct widget = hwidget
	u32 i = 0
	
	! check the widget we are adding has the window as parent container
	if #widget.parent_hcontainer =/= window
		fatal_error("window_add_widget() : widget parent container =/= window")
	endif
	
	! get free widget pos
	while [ptr]#(#(window.widgets)+i) =/= null && i<WINDOW_MAX_WIDGETS*4
		i+=4
	endwhile
	
	! -1 because last widget must be null
	if i/4 >= WINDOW_MAX_WIDGETS-1
		return false
	endif
	
	#(#(window.widgets)+i) = [ptr](hwidget)
	
	return true
endfunction
! remove a widget
function window_remove_widget: ptr hwindow, ptr hwidget
	struct window_struct window = hwindow
	u32 i = 0
	
	! get widget pos
	while [ptr]#(#(window.widgets)+i) =/= hwidget && i < WINDOW_MAX_WIDGETS*4
		i+=4
	endwhile
	
	! didnt we found it ?
	if i >= (WINDOW_MAX_WIDGETS-1)*4
		return false
	endif
	
	if [ptr]#window.focused_widget == hwidget && hwidget=/=null
		widget_unfocus(hwidget)
		#window.focused_widget = [ptr](null)
	endif
	#(#(window.widgets)+i) = [ptr](null)
	
	return true
endfunction


function window_parse_mouse_event: ptr hwindow, ptr hevent
	struct window_struct window = hwindow
	struct mouse_event_struct event = hevent
	struct widget_struct widget
	u32 i = 0

	! test all widgets
	while i < WINDOW_MAX_WIDGETS*4
		widget = [ptr]#((#window.widgets)+i)
		if widget =/= null
			! if collision
			if #event.first_mouse_x >= #widget.abs_x1 && #event.first_mouse_x <= #widget.abs_x2
				if #event.first_mouse_y >= #widget.abs_y1 && #event.first_mouse_y <= #widget.abs_y2
					! change the focus
					if [ptr]#window.focused_widget =/= [ptr](widget)
						if [ptr]#window.focused_widget =/= null
							widget_unfocus(#window.focused_widget)
						endif
						#window.focused_widget = [ptr](widget)
					endif
					
					! create a copy of the event and modify it
					struct mouse_event_struct new_event = mouse_event_clone(event)
					
					! convert coords relative to the widget
					if #new_event.mouse_x > #widget.abs_x1
						#new_event.mouse_x = [u32](#event.mouse_x - #widget.abs_x1)
					else
						#new_event.mouse_x = 0
					endif
					
					if #new_event.mouse_y > #widget.abs_y1
						#new_event.mouse_y = [u32](#event.mouse_y - #widget.abs_y1)
					else
						#new_event.mouse_y = 0
					endif
					
					#new_event.first_mouse_x = [u32](#event.first_mouse_x - #widget.abs_x1)
					#new_event.first_mouse_y = [u32](#event.first_mouse_y - #widget.abs_y1)
					
					widget_parse_mouse_event(hwindow, widget, new_event)
					
					free(new_event)
					!exit
				endif
			endif
		endif
		i+=4
	endwhile
	
endfunction


! add an event to the window
function window_add_event: ptr hwindow, u32 event_type, ptr hevent, ptr hwidget
		struct window_struct window = hwindow
		struct window_event_struct event = malloc(window_event_struct.size)
		struct mouse_event_struct mouse_event = hevent
		
		if #window.n_events < WINDOW_MAX_EVENTS
			if event_type == MOUSE_EVENT

				#event.event_cat = [u32](event_type)
				#event.x = [u32]#mouse_event.mouse_x
				#event.y = [u32]#mouse_event.mouse_y
				#event.value = [u32]#mouse_event.button
				#event.type = [u8]#mouse_event.event_type
				#event.widget = [ptr](hwidget)
			else
				! TODO: keyboard events
				klog("window_add_event(): keyboard event non implemente")
			endif
			
			if event_type == SIGNAL_EVENT_KILL
				#event.event_cat = [u32](event_type)
			endif
			#(#window.events+(#window.n_events*SIZEOF_PTR)) = [ptr](event)
			#window.n_events = [u32](#window.n_events+1)
		else
			klog("window_add_event() buffer full\n")
		endif
endfunction


! get the oldest event ( return window_event_struct )
function window_get_last_event: ptr hwindow
	struct window_struct window = hwindow
	if #window.n_events>0
		return [ptr]#(#window.events)
	endif
	return null
endfunction

function window_delete_last_event: ptr hwindow
	struct window_struct window = hwindow
	if #window.n_events>0
		
		!free last event
		free(#(#window.events))
		
		! override its address
		if #window.n_events>1
			memcpy(#window.events, #window.events+SIZEOF_PTR, (SIZEOF_PTR*(#window.n_events-1)))
		endif
		
		#window.n_events = [u32](#window.n_events-1)
	endif
endfunction

function window_add_keyboard_char: ptr hwindow, u8 char
	struct window_struct window = hwindow
	struct widget_struct focused_widget = [ptr](#window.focused_widget)
	if focused_widget =/= null
		if #focused_widget.type == WIDGET_TEXTBOX
			textbox_add_char(#focused_widget.child_struct, char)
		endif
	endif

endfunction

function window_event_get_cat: ptr mouse_event
	struct window_event_struct event = mouse_event
	return #event.event_cat
endfunction

function window_event_get_widget: ptr mouse_event
	struct window_event_struct event = mouse_event
	return #event.widget
endfunction

! delete the content of a window without deleting the window itself
function window_free_content: ptr hwindow
	u32 i = 0
	struct window_struct window = hwindow
	! delete all widgets
	while [ptr]#(#(window.widgets)+i) =/= null
		widget_free(#(#(window.widgets)+i))
		#(#(window.widgets)+i) = [ptr](null)
		i+=4
	endwhile
	#window.focused_widget = [ptr](null)
endfunction