/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/

defstruct button_struct
	ptr parent_widget
	
	ptr text
	ptr text_x_offset
	u32 color
	u32 background_color
	u32 state
	
	
endstruct

define BUTTON_BACKGROUND_COLOR 0x00264768

function button_create: ptr parent_hwindow, u32 x, u32 y, u32 width, ptr text
	struct button_struct button = malloc(button_struct.size)
	struct window_struct window = parent_hwindow
	
	#button.text = [ptr](clone_str(text))
	#button.text_x_offset = [u32](((convert_relative_coord_to_absolute(width, #window.content_width)/2)-(strlen(text)<<2)))
	#button.state = [u8]BUTTON_STATE_UP
	#button.color = [u32]WINDOW_TEXT_COLOR
	#button.background_color = [u32]BUTTON_BACKGROUND_COLOR
	ptr parent = widget_create(parent_hwindow, x, y, width, (CHAR_Y_SIZE+8)*1000/(#window.content_height), WIDGET_BUTTON, function button_draw , button)
	
	#button.parent_widget = parent
	
	return parent
endfunction

function button_draw: ptr hbutton, u32 width, u32 height
	struct button_struct button = hbutton
	u32 external_border_color = COLOR_BLACK
	u32 internal_border_color = COLOR_DARK_GREY
	
	if #button.state == BUTTON_STATE_DOWN
		external_border_color = COLOR_DARK_GREY
		internal_border_color = COLOR_BLACK
	endif
	
	struct widget_struct widget = [ptr]#button.parent_widget
	
	! filled background
	fill_buffer(#widget.bmp, #widget.abs_width, #widget.abs_height, #button.background_color)
	
	! draw button border
	draw_rect_border_buffer(#widget.bmp, #widget.abs_width, #widget.abs_height, 0, 0, width-1, height-1, 1, external_border_color)
	
	
	! draw text
	if #button.text_x_offset < 5
		#button.text_x_offset=[u32]5
	endif
	prints_rect_buffer(#widget.bmp, #widget.abs_width, #widget.abs_height, #button.text_x_offset, height/2-3, width-5, height-2, #button.color, #button.text)
endfunction


! free a button
function button_free: ptr hbutton
	struct button_struct button = hbutton
	free(#button.text)
	free(button)
endfunction

! parse a mouse event
function button_parse_mouse_event: ptr hwindow, ptr hwidget, ptr hevent
	struct mouse_event_struct event = hevent
	struct widget_struct widget = hwidget
	struct button_struct button = [ptr](#widget.child_struct)
	if #event.button == MOUSE_LEFT_BUTTON && #event.event_type == MOUSE_EVENT_CLICK
		window_add_event(hwindow, MOUSE_EVENT, hevent, widget)
		exit
	endif
	
	if #event.event_type == MOUSE_EVENT_DRAG
		#button.state = BUTTON_STATE_DOWN
		widget_need_redraw(#button.parent_widget)
		exit
	endif
	
	if #event.event_type == MOUSE_EVENT_DROP
		#button.state = BUTTON_STATE_UP
		widget_need_redraw(#button.parent_widget)
		exit
	endif
endfunction


! set the text color
function button_set_color: ptr hwidget, u32 color
	struct widget_struct widget = hwidget
	struct button_struct button = [ptr](#widget.child_struct)
	#button.color = [u32](color)
	
	widget_need_redraw(#button.parent_widget)
endfunction

! set the text color
function button_set_background_color: ptr hwidget, u32 color
	struct widget_struct widget = hwidget
	struct button_struct button = [ptr](#widget.child_struct)
	#button.background_color = [u32](color)
	
	widget_need_redraw(#button.parent_widget)
endfunction
