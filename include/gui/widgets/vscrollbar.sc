/*********************************
* This file is a part of King OS *
*  written by LANEZ Gaël - 2018  *
*********************************/


define VSCROLL_BUTTON_MIN_HEIGHT 20
define VSCROLL_BAR_WIDTH 14
define VSCROLL_SCROLL_BUTTON_COLOR 0x0
define SCROLLBAR_COLOR 0x767b87

defstruct vscrollbar_struct
	ptr parent_widget

	u32 scroll
	u32 min
	u32 max
	
	ptr linked_widget
endstruct

! create widget
function vscrollbar_create: ptr parent_hcontainer, u32 x, u32 y, u32 height, u32 min, u32 max
	struct vscrollbar_struct vscrollbar = malloc(vscrollbar_struct.size)
	struct container_struct parent_container = parent_hcontainer
	
	#vscrollbar.min = min
	#vscrollbar.max = max
	#vscrollbar.scroll = min*1000

	ptr parent = widget_create(parent_hcontainer, x, y, VSCROLL_BAR_WIDTH*1000/[u32](#parent_container.content_width), height, WIDGET_VSCROLL_BAR, function vscrollbar_draw , vscrollbar)
	#vscrollbar.parent_widget = parent
	
	return parent
endfunction

! draw widget
function vscrollbar_draw: ptr hvscrollbar, u32 width, u32 height
	struct vscrollbar_struct vscrollbar = hvscrollbar
	u32 scroll = #vscrollbar.scroll
	u32 min = #vscrollbar.min
	u32 max = #vscrollbar.max
	u32 delta = max - min
	
	u32 button_height = VSCROLL_BUTTON_MIN_HEIGHT
	
	u32 real_height = height - button_height
	struct widget_struct widget = [ptr]#vscrollbar.parent_widget
			
	! draw background
	widget_fill_background(#vscrollbar.parent_widget)
	
	! draw border
	draw_rect_border_buffer(#widget.bmp, #widget.abs_width, #widget.abs_height, 0, 0, width-1, height-1, 1, VSCROLL_SCROLL_BUTTON_COLOR)
	

	! draw scrolling button
	if min < max
		draw_rect_buffer(#widget.bmp, #widget.abs_width, #widget.abs_height,2, 1+(scroll*real_height/delta/1000), width-2, button_height-2, SCROLLBAR_COLOR)
	else
		draw_rect_buffer(#widget.bmp, #widget.abs_width, #widget.abs_height,2, 2, width-2, height-4, SCROLLBAR_COLOR)
	endif

endfunction

function vscrollbar_parse_mouse_event: ptr hwindow, ptr hwidget, ptr hevent

	struct mouse_event_struct event = hevent
	struct widget_struct vscrollbar_widget = hwidget
	struct vscrollbar_struct vscrollbar = [ptr](#vscrollbar_widget.child_struct)
	
	u32 y = #(event.mouse_y)
	u32 height = #vscrollbar_widget.abs_y2 - #vscrollbar_widget.abs_y1 - VSCROLL_BUTTON_MIN_HEIGHT
	u32 min = #vscrollbar.min
	u32 max = #vscrollbar.max
	
	! update scroll
	if min < max
		u32 delta = max-min
		if y < VSCROLL_BUTTON_MIN_HEIGHT/2
			y = 0
		else
			y-= VSCROLL_BUTTON_MIN_HEIGHT/2
		endif
		
		if y > height
			y = height
		endif
		#vscrollbar.scroll = [u32](y*delta*1000/height) ! multiply by 1000 to be precise whith small ranges
	endif
	
	widget_need_redraw(#vscrollbar.parent_widget)
	
	! will force redrawing of linked widget
	if #vscrollbar.linked_widget =/= null
		widget_need_redraw(#vscrollbar.linked_widget)
	endif
endfunction
! get current scroll
function vscrollbar_get_scroll: ptr hvscrollbar
	struct vscrollbar_struct vscrollbar = hvscrollbar
	
	return [u32](#vscrollbar.scroll/1000)
endfunction

function vscrollbar_set_scroll: ptr hvscrollbar, u32 scroll
	struct vscrollbar_struct vscrollbar = hvscrollbar
	u32 max = [u32](#vscrollbar.max)
	if max < scroll
		scroll = max
	endif
	#vscrollbar.scroll = scroll * 1000
endfunction

function vscrollbar_get_max: ptr hvscrollbar
	struct vscrollbar_struct vscrollbar = hvscrollbar
	
	return [u32](#vscrollbar.max)
endfunction

function vscrollbar_get_min: ptr hvscrollbar
	struct vscrollbar_struct vscrollbar = hvscrollbar
	
	return [u32](#vscrollbar.min)
endfunction

function vscrollbar_set_min: ptr hvscrollbar, u32 min
	struct vscrollbar_struct vscrollbar = hvscrollbar
	
	#vscrollbar.min = min
endfunction

function vscrollbar_set_max: ptr hvscrollbar, u32 max
	struct vscrollbar_struct vscrollbar = hvscrollbar
	
	#vscrollbar.max = max
endfunction

function vscrollbar_set_linked_widget: ptr hvscrollbar, ptr hwidget
	struct vscrollbar_struct vscrollbar = hvscrollbar
	#vscrollbar.linked_widget = hwidget
endfunction
