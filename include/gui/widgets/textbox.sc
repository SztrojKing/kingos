/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/

defstruct textbox_struct
	ptr parent_widget

	ptr text
	ptr initial_text
	u32 color 
	u32 cursor_pos
	u32 max_length
	u32 isFocused
	u32 displayOffset
endstruct

define TEXTBOX_BACKGROUND_COLOR 0x282828
define TEXTBOX_BORDER_COLOR 0x595d59


function textbox_create: ptr parent_hwindow, u32 x, u32 y, u32 width, ptr initial_text, u32 max_text_length
	struct textbox_struct textbox = malloc(textbox_struct.size)
	struct window_struct window = parent_hwindow
	
	#textbox.initial_text = [ptr](clone_str(initial_text))
	#textbox.text = null
	#textbox.cursor_pos=0
	#textbox.max_length = [u32](max_text_length)
	#textbox.color = [u32](WINDOW_TEXT_COLOR)
	ptr parent = widget_create(parent_hwindow, x, y, width, (CHAR_Y_SIZE+8)*1000/(#window.content_height), WIDGET_TEXTBOX, function textbox_draw , textbox)
	
	#textbox.parent_widget = parent
	return parent
endfunction

function textbox_draw: ptr htextbox, u32 width, u32 height
	struct textbox_struct textbox = htextbox
	struct widget_struct widget = [ptr]#textbox.parent_widget
		
	fill_buffer(#widget.bmp, #widget.abs_width, #widget.abs_height, TEXTBOX_BACKGROUND_COLOR)
	
	
	draw_rect_border_buffer(#widget.bmp, #widget.abs_width, #widget.abs_height, 0, 0, width-1, height-1, 1, TEXTBOX_BORDER_COLOR)
	
	if #textbox.isFocused == true
		! draw text cursor_pos
		draw_vertical_line_buffer(#widget.bmp, #widget.abs_width, #widget.abs_height, 2+(#textbox.cursor_pos*CHAR_X_SIZE), 2, height-2, WINDOW_TEXT_COLOR)
	endif
		
	if strlen(#textbox.text) > 0
		! print text
		prints_rect_buffer(#widget.bmp, #widget.abs_width, #widget.abs_height, 2, height/2-3, width-1, height, #textbox.color, #textbox.text)
	else
		! print description
		prints_rect_buffer(#widget.bmp, #widget.abs_width, #widget.abs_height, 2, height/2-3, width-1, height, COLOR_DARK_GREY, #textbox.initial_text)
	endif

endfunction


! free a textbox
function textbox_free: ptr htextbox
	struct textbox_struct textbox = htextbox
	free(#textbox.text)
	free(textbox)
endfunction

! called when the textbox in unfocused
function textbox_unfocus: ptr htextbox
	struct textbox_struct textbox = htextbox
	#textbox.isFocused = false
	
	widget_need_redraw(#textbox.parent_widget)
endfunction

! parse a mouse event
function textbox_parse_mouse_event: ptr hwindow, ptr hwidget, ptr hevent
	struct mouse_event_struct event = hevent
	struct widget_struct widget = hwidget
	struct textbox_struct textbox = [ptr](#widget.child_struct)
	u32 new_cursor_pos
	
	if #event.button == MOUSE_LEFT_BUTTON && #event.event_type == MOUSE_EVENT_CLICK
		#textbox.isFocused = true
		
		! if first clic on the widget
		if #textbox.text == null
			#textbox.text = [ptr](malloc(1))
		else
			! we wan't to move the cursor position
			new_cursor_pos = [u32](#event.mouse_x/CHAR_X_SIZE)
			if new_cursor_pos <= strlen(#textbox.text)
				#textbox.cursor_pos = [u32](new_cursor_pos)
			else
				#textbox.cursor_pos = [u32](strlen(#textbox.text))
			endif
		endif
	endif
	
	widget_need_redraw(#textbox.parent_widget)
endfunction

! private function to add a char to a string (only use it with textbox)
function __textbox_custom_str_add_char: ptr str1, u32 pos, u8 char
	u32 malloc_len=1
	u32 str1_len = [u32](strlen(str1))
	ptr res
	if str1 =/= null
		malloc_len+=str1_len+2
	endif
	res = [ptr](malloc(malloc_len))
	if pos <= str1_len
		if str1 =/= null
			!strcat(res, str1)
			memcpy(res, str1, pos)
			#(res+pos) = [u8](char)
			strcpy(res+pos+1, str1+pos)
			free(str1)
		else
			#(res) = [u8](char)
		endif
	endif
	return res
endfunction

function textbox_add_char: ptr htextbox, u8 char
	struct textbox_struct textbox = htextbox
	if htextbox == null
		fatal_error("textbox_add_char() : htextbox == null\n")
	endif
		! del
		/*
		if char == 127
			if  #textbox.cursor_pos<strlen(#textbox.text)
				str_del_pos(#textbox.text, #textbox.cursor_pos+1)
			endif
		endif
		*/
		
	! backspace
	if char == 8 && #textbox.cursor_pos>0
		#textbox.cursor_pos = [u32](#textbox.cursor_pos - 1)
		str_del_pos(#textbox.text, #textbox.cursor_pos)
	else
		if strlen(#textbox.text) < [u32](#textbox.max_length)	

			! normal char
			if char >= ' ' && char <= '}'
				#textbox.text = [ptr](__textbox_custom_str_add_char([ptr](#textbox.text), #textbox.cursor_pos, char))
				#textbox.cursor_pos = [u32](#textbox.cursor_pos+1)
			endif
		endif
	endif
	
	widget_need_redraw(#textbox.parent_widget)
	
endfunction

function textbox_get_text: ptr hwidget
	struct widget_struct widget = hwidget
	if widget =/= null
		struct textbox_struct textbox = [ptr](#widget.child_struct)
		! will return an empty string if #textbox.text == null
		return [ptr](clone_str(#textbox.text))
	else
		fatal_error("textbox_get_text(): Invalid textbox\n")
	endif
endfunction

! set the text of a textbox
function textbox_set_text: ptr hwidget, ptr text
	struct widget_struct widget = hwidget
	struct textbox_struct textbox = [ptr](#widget.child_struct)
	free(#textbox.text)
	#textbox.cursor_pos=[u32](0)
	#textbox.text = [ptr](clone_str(text))
	
	! update the size of the widget
!	widget_change_relative_width(widget, ((strlen(text))*CHAR_X_SIZE+1)*1000/(#window.content_width))

	widget_need_redraw(#textbox.parent_widget)


endfunction

! set the text color
function textbox_set_color: ptr hwidget, u32 color
	struct widget_struct widget = hwidget
	struct textbox_struct textbox = [ptr](#widget.child_struct)
	#textbox.color = [u32](color)
	
	widget_need_redraw(#textbox.parent_widget)
	
endfunction