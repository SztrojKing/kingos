/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/

defstruct list_struct

	!extend container
	u32 container_type
	u32 content_width
	u32 content_height
	u32 content_coord_x
	u32 content_coord_y
	u32 background_color
	ptr widgets
	
	ptr parent_widget
		
	ptr rows ! object_list
	u32 scrollbar
endstruct

defstruct listRow_struct
	ptr title
	u32 width
endstruct


function list_create: ptr parent_hcontainer, u32 x, u32 y, u32 width, u32 height, ptr colTitles, u32 maxRows
	struct list_struct list = malloc(list_struct.size)
	struct container_struct container = parent_hcontainer
	
	#list.colunmsTitle = [ptr](colTitles)
	#list.rows = [ptr](object_list_create(maxRows))
	#list.scroll = 0
	
	struct widget_struct widget = widget_create(parent_hcontainer, x, y, width, height, WIDGET_LIST, function list_draw , list)
	
	! create vscrollbar widget
	struct widget_struct widget_scrollbar =  [u32](vscrollbar_create(parent_hcontainer, x+width-((VSCROLL_BAR_WIDTH)*1000/[u32](#container.content_width)), y, height, 0, 0))
	#list.scrollbar =[ptr](#widget_scrollbar.child_struct)
	
	#list.parent_widget = widget
	
	vscrollbar_set_linked_widget(#list.scrollbar, widget)
	
	return widget
endfunction

function list_draw: ptr hlist, u32 width, u32 height
	struct list_struct list = hlist
	struct widget_struct widget_parent = [ptr]#list.parent_widget
	
	! draw background
	widget_fill_background(#list.parent_widget)
		
	! draw border
	draw_rect_border_buffer(#widget_parent.bmp, #widget_parent.abs_width, #widget_parent.abs_height, 0, 0, width-1, height-1, 1, VSCROLL_SCROLL_BUTTON_COLOR)
endfunction


! free a button
function list_free: ptr hlist
	/* TODO
	struct list_struct list = hlist
	*/
endfunction
