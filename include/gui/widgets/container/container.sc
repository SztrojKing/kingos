/*********************************
* This file is a part of King OS *
*  written by LANEZ Gaël - 2018  *
*********************************/

enum CONTAINER_WINDOW, CONTAINER_VSCROLL, CONTAINER_HSCROLL
define CONTAINER_MAX_WIDGETS 64

defstruct container_struct
	u32 container_type
	u32 content_width
	u32 content_height
	u32 content_coord_x
	u32 content_coord_y
	u32 background_color
	ptr widgets
	
	
endstruct

function container_get_container: ptr hwidget
	struct widget_struct widget = hwidget
	if widget == null
		fatal_error("container_get_container(): Invalid container\n")
	endif
	
	return [ptr](#widget.child_struct)
endfunction


! add a widget to his parent
function container_add_child: ptr parent_container, ptr child_widget
	struct container_struct container = parent_container

	switch #container.container_type
		case CONTAINER_WINDOW
			window_add_widget(container, child_widget)
		endcase
		case CONTAINER_VSCROLL
			vscroll_add_widget(container, child_widget)
		endcase
		case CONTAINER_HSCROLL
			!hscroll_add_widget(container, child_widget)
			fatal_error("container_add_child(): CONTAINER_HSCROLL not implemented yet.")
		endcase
		case default
			fatal_error("container_add_child(): unknown parent type")
		endcase
	endswitch
endfunction
