/*********************************
* This file is a part of King OS *
*  written by LANEZ Gaël - 2018  *
*********************************/

defstruct vscroll_struct

	!extend container
	u32 container_type
	u32 content_width
	u32 content_height
	u32 content_coord_x
	u32 content_coord_y
	u32 background_color
	ptr widgets
	
	ptr parent_widget
	
	ptr scrollbar
endstruct


function vscroll_create: ptr parent_hcontainer, u32 x, u32 y, u32 width, u32 height
	struct vscroll_struct vscroll = malloc(vscroll_struct.size)
	struct container_struct container = parent_hcontainer
	

	
	#vscroll.widgets = malloc(CONTAINER_MAX_WIDGETS)
	
	
	#vscroll.container_type = CONTAINER_VSCROLL

	
	struct widget_struct widget = widget_create(parent_hcontainer, x, y, width, height, WIDGET_VSCROLL, function vscroll_draw , vscroll)
	
	#vscroll.content_width = [u32](#widget.abs_x2-#widget.abs_x1)-VSCROLL_BAR_WIDTH-2
	#vscroll.content_height = [u32](#widget.abs_y2-#widget.abs_y1)-4
	
	
	#vscroll.content_coord_x = [u32](#widget.abs_x1+#container.content_coord_x)+2
	#vscroll.content_coord_y = [u32](#widget.abs_y1+#container.content_coord_y)+2
	
	#vscroll.background_color = WINDOW_BACKGROUND_COLOR
		
	! create vscrollbar widget
	struct widget_struct widget_scrollbar =  [u32](vscrollbar_create(parent_hcontainer, x+width-((VSCROLL_BAR_WIDTH-4)*1000/[u32](#container.content_height)), y, height, 0, 0))
	#vscroll.scrollbar =[ptr](#widget_scrollbar.child_struct)
	
	#vscroll.parent_widget = widget
	
	
	vscrollbar_set_linked_widget(#vscroll.scrollbar, widget)
	
	return widget
	
endfunction

! add a widget
function vscroll_add_widget: ptr hvscroll, ptr hwidget
	struct vscroll_struct vscroll = hvscroll
	struct widget_struct widget = hwidget
	u32 i = 0
	u32 content_height = #vscroll.content_height
	u32 max_scroll = vscrollbar_get_max(#vscroll.scrollbar)
	struct vscrollbar_struct vscrollbar = [ptr]#(vscroll.scrollbar)
	
	! check the widget we are adding has the vscroll as parent container
	if #widget.parent_hcontainer =/= vscroll
		fatal_error("vscroll_add_widget() : widget parent container =/= vscroll")
	endif
	
	! get free widget pos
	while [ptr]#(#(vscroll.widgets)+i) =/= null && i<CONTAINER_MAX_WIDGETS*4
		i+=4
	endwhile
	
	! -1 because last widget must be null
	if i/4 >= CONTAINER_MAX_WIDGETS-1
		return false
	endif
	
	#(#(vscroll.widgets)+i) = [ptr](hwidget)
	
	if #widget.abs_y2 > content_height + max_scroll
		! update scrollbar max

		vscrollbar_set_max(#vscroll.scrollbar, #widget.abs_y2 - content_height)
	endif
	
	widget_need_redraw(#vscroll.parent_widget)
	widget_need_redraw(#vscrollbar.parent_widget)
	
	return true
endfunction

function vscroll_draw: ptr hvscroll, u32 width, u32 height
	struct vscroll_struct vscroll = hvscroll
	u32 scroll = vscrollbar_get_scroll(#vscroll.scrollbar)
	u32 content_coord_x = #vscroll.content_coord_x
	u32 content_coord_y = #vscroll.content_coord_y
	u32 content_height = #vscroll.content_height
	u32 content_width = #vscroll.content_width
	ptr widgets = #vscroll.widgets
	u32 i = 0
	struct widget_struct widget_parent = [ptr]#vscroll.parent_widget
	
	! handle redraw needs himself
	if #widget_parent.needs_redraw == true
		! draw background
		widget_fill_background(#vscroll.parent_widget)
		
		! draw border
		draw_rect_border_buffer(#widget_parent.bmp, #widget_parent.abs_width, #widget_parent.abs_height, 0, 0, width-1, height-1, 1, VSCROLL_SCROLL_BUTTON_COLOR)
	endif
	
	! draw all widgets
	while i < CONTAINER_MAX_WIDGETS*4
		!klog("Drawing widget ")

		struct widget_struct widget = [ptr]#(widgets+i)
		
		if widget =/= null 
			if (#widget.needs_redraw == true || #widget_parent.needs_redraw == true ) && #widget.abs_y2 < scroll+content_height && #widget.abs_y1 >= scroll
				widget_draw(#widget_parent.bmp, #widget_parent.abs_width, widget , 0, 0-scroll)
			endif
		endif
		
		i+=4
	endwhile
	
endfunction


! free a vscroll and its childs
function vscroll_free: ptr hvscroll
	struct vscroll_struct vscroll = hvscroll
	ptr widgets = #vscroll.widgets
	u32 i=0
	! free all widgets
	while i < CONTAINER_MAX_WIDGETS*4
		struct widget_struct widget = [ptr]#(widgets+i)
		if widget =/= null
			widget_free(widget)
		endif
		i+=4
	endwhile
endfunction

! called when the textbox in unfocused
function vscroll_unfocus: ptr htextbox
	struct vscroll_struct textbox = htextbox
endfunction

! parse a mouse event
function vscroll_parse_mouse_event: ptr hwindow, ptr hwidget, ptr hevent
	
	struct mouse_event_struct event = hevent
	struct widget_struct vscroll_widget = hwidget
	struct vscroll_struct vscroll = [ptr](#vscroll_widget.child_struct)
	!struct container_struct parent_container = hparent_container
	
	struct window_struct parent_window = hwindow
	u32 x = #(event.first_mouse_x)
	u32 y = #(event.first_mouse_y)
	u32 i=0
	ptr widgets = #vscroll.widgets
	u32 scroll = vscrollbar_get_scroll(#vscroll.scrollbar)
	
	! TODO: retrouver la window parent meme si elle n'est pas le parent direct
	if #parent_window.container_type =/= CONTAINER_WINDOW
		fatal_error("vscroll_parse_mouse_event(): parent_container is not a window")
	endif
	
	
	u32 abs_x1 = #vscroll_widget.abs_x1
	u32 abs_y1 = #vscroll_widget.abs_y1
	u32 abs_x2 = #vscroll_widget.abs_x2
	u32 abs_y2 = #vscroll_widget.abs_y2

	u32 content_height = #vscroll.content_height

	! find the widget that has been clicked
	while i < CONTAINER_MAX_WIDGETS*4
		struct widget_struct widget = [ptr]#(widgets+i)
		if widget =/= null
			! if collision
			if x >= #widget.abs_x1 && x <= #widget.abs_x2
				if y >= #widget.abs_y1 - scroll && y <= #widget.abs_y2 - scroll
					! change the focus
					if [ptr]#parent_window.focused_widget =/= [ptr](widget)
						if [ptr]#parent_window.focused_widget =/= null
							widget_unfocus(#parent_window.focused_widget)
						endif
						#parent_window.focused_widget = [ptr](widget)
					endif
					
					if #event.mouse_x > #widget.abs_x1
						#event.mouse_x = [u32](#event.mouse_x - #widget.abs_x1)
					else
						#event.mouse_x = 0
					endif
					
					if #event.mouse_y > #widget.abs_y1
						#event.mouse_y = [u32](#event.mouse_y - #widget.abs_y1)
					else
						#event.mouse_y = 0
					endif
					
					#event.first_mouse_x = [u32](#event.first_mouse_x - #widget.abs_x1)
					#event.first_mouse_y = [u32](#event.first_mouse_y - #widget.abs_y1 - scroll)
					
					widget_parse_mouse_event(hwindow, widget, event)
					exit
				endif
			endif
		endif

		i+=4
	endwhile
	
endfunction


