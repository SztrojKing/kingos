/*********************************
* This file is a part of King OS *
*  written by LANEZ Gaël - 2018  *
*********************************/

! realloc n lines
define CONSOLE_REALLOC_CHUNK_SIZE 25 

defstruct console_struct
	ptr parent_widget
	
	ptr scrollbar
	u32 content_width
	ptr buffer
	
	u32 video_x
	u32 video_y
	
	u32 max_y
endstruct


function console_create: ptr parent_hcontainer, u32 x, u32 y
	struct console_struct console = malloc(console_struct.size)
	struct container_struct container = parent_hcontainer
	
	u32 width = (CONSOLE_MAX_CHAR_WIDTH+2) * CHAR_X_SIZE*1000/[u32](#container.content_width)
	u32 height = (CONSOLE_MAX_CHAR_HEIGHT * CHAR_Y_SIZE - 1)*1000/[u32](#container.content_height)
	
	struct widget_struct widget = widget_create(parent_hcontainer, x, y, width, height, WIDGET_CONSOLE, function console_draw , console)
	
	#console.content_width = [u32](#widget.abs_x2-#widget.abs_x1)-VSCROLL_BAR_WIDTH-2

	
	! create vscrollbar widget
	struct widget_struct widget_scrollbar =  [u32](vscrollbar_create(parent_hcontainer, x+width-((VSCROLL_BAR_WIDTH)*1000/[u32](#container.content_width)), y, height, 0, 0))
	#console.scrollbar =[ptr](#widget_scrollbar.child_struct)
	
	#console.buffer = malloc(30*80*2)
	#console.max_y = 30
	
	#console.parent_widget = widget
	
	vscrollbar_set_linked_widget(#console.scrollbar, widget)
	
	return widget
	
endfunction

function console_draw: ptr hconsole, u32 width, u32 height
	struct console_struct console = hconsole
	u32 scroll = vscrollbar_get_scroll(#console.scrollbar)
	struct widget_struct widget = [ptr]#console.parent_widget
	
	! filled background
	fill_buffer(#widget.bmp, #widget.abs_width, #widget.abs_height, COLOR_BLACK)
	
	! print console
	print_console_buffer(#widget.bmp, #widget.abs_width, #widget.abs_height, 0, 0, width-VSCROLL_BAR_WIDTH, height, console_get_displayed_buffer(console))

endfunction


function console_set_displayed_buffer: ptr hconsole, ptr buffer
	struct console_struct console = hconsole
	memcpy(#console.buffer, buffer, CONSOLE_VIDEO_MEM_SIZE)
endfunction

function console_get_displayed_buffer:ptr hconsole
	struct console_struct console = hconsole
	ptr res = #(console.buffer)
	u32 scroll = vscrollbar_get_scroll(#(console.scrollbar))
	
	res += scroll*80*2

	
	return res
endfunction
! free a vscroll and its childs
function console_free: ptr hconsole
	struct console_struct console = hconsole
	free(#console.buffer)
endfunction

! called when the textbox in unfocused
function console_unfocus: ptr hconsole
	!struct vscroll_struct textbox = htextbox
endfunction


! prints with color
function console_color_prints:ptr hconsole, uint8_t color, ptr str
	struct console_struct console = hconsole
	struct vscrollbar_struct vscrollbar = [ptr]#(console.scrollbar)
	ptr video_mem = [u32]#(console.buffer)
	u32 video_x = [u32]#(console.video_x)
	u32 video_y = [u32]#(console.video_y)
	
	if str == null
		exit
	endif
	
	widget_need_redraw(#console.parent_widget)
	widget_need_redraw(#vscrollbar.parent_widget)
	
	
	ptr video=[ptr](video_mem+((video_y*80+video_x)*2))
	while [uint8_t]#str =/= 0
		! LF
		if [uint8_t]#str == 10
			!#video=[uint8_t]#str
			video_y++
			video=[ptr](video_mem+((video_y*80+video_x)*2))
			if video_y>= #console.max_y
				console_expand_buffer(console)
			endif
		endif
		! CR
		if [uint8_t]#str == 13
			!#video=[uint8_t]#str
			video_x=0
			video=[ptr](video_mem+((video_y*80+video_x)*2))
		endif
		
		! tab
		if  [uint8_t]#str == 9
			!#video=[uint8_t]#str
			video_x=video_x+4
			video=[ptr](video_mem+((video_y*80+video_x)*2))
		endif
		
		! normal char
		if [uint8_t]#str >= ' '
			#video=[uint8_t]#str
			video++
			#video=color
			video++
						
			video_x++

		endif
		
		if video_x>=80
			video_x=video_x-80
			video_y++
			video=[ptr](video_mem+((video_y*80+video_x)*2))
		endif
		if video_y>=#console.max_y
			console_expand_buffer(console)
		endif
		
		str++	
	
	endwhile
	
	console_move_cursor(console, video_x, video_y)
	
endfunction

function console_expand_buffer: ptr hconsole
	struct console_struct console = hconsole
	
	#(console.max_y) = [u32]#(console.max_y) + CONSOLE_REALLOC_CHUNK_SIZE
		
	#console.buffer = realloc(#console.buffer, [u32]#(console.max_y)*80*2)

endfunction

! printn & coords_printn internal buffer
suint8_t _printn_buffer={"                                 "}

function console_printn:ptr hconsole, uint8_t color, uint32_t number
	memset(@_printn_buffer, 0, 21)
	itoa(@_printn_buffer, number)
	console_color_prints(hconsole, color, @_printn_buffer)
endfunction

function console_printx:ptr hconsole, uint8_t color, uint32_t number
	memset(@_printn_buffer, 0, 21)
	itox(@_printn_buffer, number)
	console_color_prints(hconsole, color, @_printn_buffer)
endfunction


! move writing position

function console_move_cursor:ptr console, uint32_t x, uint32_t y
	console_set_x(console, x)
	console_set_y(console, y)
endfunction

! raw clear screen
function console_cls: ptr hconsole
	console_move_cursor(hconsole,0,0)
	!cpu_memset32(CONSOLE_VIDEO_MEM, 0x07200720, 25*80*2)
endfunction

function console_set_x: ptr hconsole, u32 x
	struct console_struct console = hconsole
	#(console.video_x) = x
endfunction

function console_set_y: ptr hconsole, u32 y
	struct console_struct console = hconsole
	#(console.video_y) = y
	ptr scrollbar = #console.scrollbar
	if y > 25
		y-=25
	else
		y=0
	endif
	! scrolldown if user is not scrolling
	if vscrollbar_get_scroll(scrollbar) == vscrollbar_get_max(scrollbar)
		vscrollbar_set_max(scrollbar, y)
		vscrollbar_set_scroll(scrollbar, y)
	else
		vscrollbar_set_max(scrollbar, y)
	endif
	
endfunction

function console_get_x: ptr hconsole
	struct console_struct console = hconsole
	return [u32]#(console.video_x)
endfunction

function console_get_y: ptr hconsole
	struct console_struct console = hconsole
	return [u32]#(console.video_y) - vscrollbar_get_scroll(#(console.scrollbar))
endfunction

/*
! parse a mouse event
function vscroll_parse_mouse_event: ptr hwindow, ptr hwidget, ptr hevent
	
	struct mouse_event_struct event = hevent
	struct widget_struct vscroll_widget = hwidget
	struct vscroll_struct vscroll = [ptr](#vscroll_widget.child_struct)
	!struct container_struct parent_container = hparent_container
	
	struct window_struct parent_window = hwindow
	u32 x = #(event.first_mouse_x)
	u32 y = #(event.first_mouse_y)
	u32 i=0
	ptr widgets = #vscroll.widgets
	u32 scroll = vscrollbar_get_scroll(#vscroll.scrollbar)
	
	! TODO: retrouver la window parent meme si elle n'est pas le parent direct
	if #parent_window.container_type =/= CONTAINER_WINDOW
		fatal_error("vscroll_parse_mouse_event(): parent_container is not a window")
	endif
	
	
	u32 abs_x1 = #vscroll_widget.abs_x1
	u32 abs_y1 = #vscroll_widget.abs_y1
	u32 abs_x2 = #vscroll_widget.abs_x2
	u32 abs_y2 = #vscroll_widget.abs_y2

	u32 content_height = #vscroll.content_height

	! find the widget that has been clicked
	while i < CONTAINER_MAX_WIDGETS*4
		struct widget_struct widget = [ptr]#(widgets+i)
		! if collision
		if x >= #widget.abs_x1 && x <= #widget.abs_x2
			if y >= #widget.abs_y1 - scroll && y <= #widget.abs_y2 - scroll
				! change the focus
				if [ptr]#parent_window.focused_widget =/= [ptr](widget)
					if [ptr]#parent_window.focused_widget =/= null
						widget_unfocus(#parent_window.focused_widget)
					endif
					#parent_window.focused_widget = [ptr](widget)
				endif
				
				if #event.mouse_x > #widget.abs_x1
					#event.mouse_x = [u32](#event.mouse_x - #widget.abs_x1)
				else
					#event.mouse_x = 0
				endif
				
				if #event.mouse_y > #widget.abs_y1
					#event.mouse_y = [u32](#event.mouse_y - #widget.abs_y1)
				else
					#event.mouse_y = 0
				endif
				
				#event.first_mouse_x = [u32](#event.first_mouse_x - #widget.abs_x1)
				#event.first_mouse_y = [u32](#event.first_mouse_y - #widget.abs_y1 - scroll)
				
				widget_parse_mouse_event(hwindow, widget, event)
				exit
			endif
		endif

		i+=4
	endwhile
	
endfunction
*/

