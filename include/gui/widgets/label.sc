/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/

defstruct label_struct
	ptr parent_widget

	ptr text
	u32 color
endstruct

function label_create: ptr parent_hcontainer, u32 x, u32 y, u32 width, u32 height, ptr text
	struct label_struct label = malloc(label_struct.size)
	#label.text = [ptr](clone_str(text))
	#label.color = [u32](WINDOW_TEXT_COLOR)
	ptr parent = widget_create(parent_hcontainer, x, y, /*((strlen(text))*CHAR_X_SIZE+1)*1000/(#window.content_width)*/width, /*(CHAR_Y_SIZE+1)*1000/(#window.content_height)*/height, WIDGET_LABEL, function label_draw , label)

	#label.parent_widget = parent
	
	return parent
endfunction

function label_draw: ptr hlabel, u32 width, u32 height
	struct label_struct label = hlabel
	struct widget_struct widget = [ptr]#label.parent_widget
	
	widget_fill_background(#label.parent_widget)
	
	prints_rect_buffer(#widget.bmp, #widget.abs_width, #widget.abs_height, 0, 4, width-1, height-5, #label.color, #label.text)
endfunction

function label_free: ptr hlabel
	struct label_struct label = hlabel
	
	free(#label.text)
	free(label)
endfunction


! set the text of a label
function label_set_text: ptr hwidget, ptr text
	struct widget_struct widget = hwidget
	struct label_struct label = [ptr](#widget.child_struct)
	struct container_struct container = [ptr](#widget.parent_hcontainer)
	free(#label.text)
	#label.text = [ptr](clone_str(text))
	
	! update the size of the widget
!	widget_change_relative_width(widget, ((strlen(text))*CHAR_X_SIZE+1)*1000/(#window.content_width))
endfunction