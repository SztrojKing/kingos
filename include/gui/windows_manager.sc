/********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/
define MAX_WINDOWS 64

sptr windows_stack=null
su32 n_windows=0

su32 last_drag_x=0
su32 last_drag_y=0


su32 dragging_window = false
sbool gui_enabled = false

function init_gui
	windows_stack=malloc(MAX_WINDOWS*SIZEOF_PTR)
	memset(windows_stack, 0, MAX_WINDOWS*SIZEOF_PTR)
	gui_enabled=true
	return true
endfunction

! only draw the focused window
function windows_manager_draw
	window_draw(window_manager_get_focused_window(), hardware_vram)
endfunction

! draw all windows
function windows_manager_draw_all
	u32 i = n_windows
	while i>0
		window_draw([ptr](#(windows_stack+(i-1)*SIZEOF_PTR)), vram)
		i--
	endwhile
endfunction


! add a window
function windows_manager_add_window: ptr hwindow, u32 pid
	struct window_struct window = hwindow
		
	if n_windows < MAX_WINDOWS-1 && n_windows<1
		#(windows_stack+n_windows*SIZEOF_PTR) = [ptr](window)
		n_windows++
		if #window.hidden == false
			windows_manager_set_focus(window)
		endif
		next_redraw_type=NEXT_REDRAW_TYPE_REFRESH_ALL
		
		multitask_add_window_to_process(pid, window)
	else
		!fatal_error("windows_manager_add_window(): nombre max de windows atteint")
	endif
 
endfunction

! close a window (do NOT delete it)
function windows_manager_close_window: ptr window
	
	u32 id = windows_manager_get_window_id(window)
	! ensure this window exists
	if id<MAX_WINDOWS
		! set this window on top to make things easier to work with
		windows_manager_set_focus(window)
		
		! override our window
		if n_windows>0
			memcpy(windows_stack, windows_stack+SIZEOF_PTR, (n_windows*SIZEOF_PTR))
		endif
		n_windows--
		#(windows_stack+n_windows*SIZEOF_PTR)=[u32](null)
		
		multitask_remove_window_from_process(current_pid, window)
		
		! check new focused window is not a hidden one
		struct window_struct focused_window = [ptr]#(windows_stack)
		u32 i=0
		while #focused_window.hidden == true && i<n_windows
			windows_manager_set_focus(#(windows_stack+i*SIZEOF_PTR))
			focused_window = [ptr]#(windows_stack)
			i++
		endwhile
		
		window_need_redraw(#windows_stack)
		! redraw desktop
		next_redraw_type = NEXT_REDRAW_TYPE_REFRESH_ALL
	endif
endfunction

! set the main focus to the given window
function windows_manager_set_focus: ptr window
	u32 id = windows_manager_get_window_id(window)
	! is window not already on top of the screen ?
	if id > 0 && id<MAX_WINDOWS
		ptr old_focused_window = #windows_stack

		
		next_redraw_type=NEXT_REDRAW_TYPE_REFRESH_ALL
		memcpy_from_end(windows_stack+SIZEOF_PTR, windows_stack, (id*SIZEOF_PTR))
		#windows_stack = [ptr](window)
		
		! ask redraw to windows that changed focus
		window_need_redraw(#windows_stack)
		window_need_redraw(old_focused_window)
	endif
endfunction

!  return the focused window
function window_manager_get_focused_window
	return [ptr]#(windows_stack)
endfunction

!  loose the focus on a window
function windows_manager_loose_focus: ptr hwindow
	! check if window is currently focused
	if #(windows_stack) == hwindow
		u32 i=1
		! try to find a window that's not hidden and set it as the focused window
		while i<MAX_WINDOWS
			struct window_struct window = #(windows_stack+i*SIZEOF_PTR)

			if window =/= null
				if #(window.hidden) =/= true
					windows_manager_set_focus(window)

					i=MAX_WINDOWS
				endif
			endif
			i++
		endwhile
	endif
endfunction

! return the pos of a window in the window stack
function windows_manager_get_window_id: ptr window
	u32 i = 0
	if window =/= null
		while i<n_windows
			if [ptr](#(windows_stack+i*SIZEOF_PTR)) == [ptr](window)
				return i
			endif
			i++
		endwhile
	endif
	return MAX_WINDOWS
endfunction

! parse mouse events
function windows_manager_mouse_parse_events
	struct mouse_event_struct event = mouse_event_get()
	u32 timeout = time_ticks
	while event =/= null && time_ticks-timeout<20
		windows_manager_parse_event(event, windows_manager_find_window_of_event(event))
		mouse_event_delete()
		event = mouse_event_get()
	endwhile
	
endfunction

function windows_manager_parse_keyboard_events
	u8 key = keyboard_get_last_char()
	u32 timeout = time_ticks
	while key =/= 0 && time_ticks-timeout<20
		if [u32]#windows_stack =/= null
			window_add_keyboard_char(#windows_stack, key)
		endif
		key = keyboard_get_last_char()
		
	endwhile
endfunction


! parse an event on the given window
function windows_manager_parse_event: ptr ev, ptr hwindow
	struct mouse_event_struct event = ev
	struct window_struct win=hwindow
	
	if win =/= null && ev =/= null

		u32 win_x = [u32]#(win.coord_x)
		u32 win_y = [u32]#(win.coord_y)
		u32 win_width = [u32]#(win.width)
		u32 win_height = [u32]#(win.height)
		
		u32 first_drag_x = [u32]#(event.first_mouse_x)
		u32 first_drag_y = [u32]#(event.first_mouse_y)
		
		! if window hasn't the focus yet
		if win =/= window_manager_get_focused_window()
			! if we clicked on it, set the focus
			if #(event.event_type) == MOUSE_EVENT_CLICK
				windows_manager_set_focus(win)
			endif
		else
			if #(event.event_type) == MOUSE_EVENT_CLICK
				
				! close button ?
				if first_drag_x >= win_x+win_width-15 && first_drag_y >= win_y+5 && first_drag_x <= win_x+win_width-5 && first_drag_y <= win_y+12
					
					! don't kill kernel or screen refresh processes
					if #(win.parent_pid) > 1 
						! kill process if it's registered 
						if #(win.parent_pid) < MAX_TASKS
							multitask_kill([u32]#(win.parent_pid))
						else
						! only close the window
							if #win.type == WINDOW_TYPE_CONSOLE
								! close the window if it's a console
								windows_manager_close_window(hwindow)
								window_free(hwindow)
							else
								! send a kill signal if it's a window
								window_add_event(hwindow, SIGNAL_EVENT_KILL, null, null)
							endif
						endif
					endif
				endif
			
				if [u32]#(event.mouse_x) >= [u32]#(win.coord_x)+#(win.width)-30 && [u32]#(event.mouse_y) >= [u32]#(win.coord_y)+5 && [u32]#(event.mouse_x) <= [u32]#(win.coord_x)+#(win.width)-20 && [u32]#(event.mouse_y) <= [u32]#(win.coord_y)+12
					window_toggle_minimized(hwindow)
				endif
			else
				if  #(event.event_type) == MOUSE_EVENT_DRAG
					
					!if [u32]#(event.mouse_x) < [u32]#(win.content_coord_x) || [u32]#(event.mouse_x) > [u32]#(win.content_coord_x)+[u32]#(win.content_width) || [u32]#(event.mouse_y) < [u32]#(win.content_coord_y) || [u32]#(event.mouse_y) > [u32]#(win.content_coord_y)+[u32]#(win.content_height) || (first_drag_x > 0 || first_drag_y > 0)
					if first_drag_x >= win_x && first_drag_x <= win_x + win_width && first_drag_y >= win_y && first_drag_y <= win_y + #(win.content_coord_y) || dragging_window == true
					! did we  already started to drag?
						if last_drag_x > 0 || last_drag_y > 0
							! wich direction?
							if [u32]#(event.mouse_x) > last_drag_x
								if [u32]#(event.mouse_y) > last_drag_y
									window_set_coords(win, [u32]#(win.coord_x)+[u32]#(event.mouse_x)-last_drag_x, [u32]#(win.coord_y)+[u32]#(event.mouse_y)-last_drag_y)
								else
									window_set_coords(win, [u32]#(win.coord_x)+[u32]#(event.mouse_x)-last_drag_x, [u32]#(win.coord_y)-(last_drag_y-[u32]#(event.mouse_y)))
								endif
							else
								if [u32]#(event.mouse_y) > last_drag_y
									window_set_coords(win, [u32]#(win.coord_x)-(last_drag_x-[u32]#(event.mouse_x)), [u32]#(win.coord_y)+[u32]#(event.mouse_y)-last_drag_y)
								else
									window_set_coords(win, [u32]#(win.coord_x)-(last_drag_x-[u32]#(event.mouse_x)), [u32]#(win.coord_y)-(last_drag_y-[u32]#(event.mouse_y)))
								endif
							endif
							next_redraw_type=NEXT_REDRAW_TYPE_REFRESH_ALL

						endif
						last_drag_x=[u32]#(event.mouse_x)
						last_drag_y=[u32]#(event.mouse_y)
						
						dragging_window=true
						exit
					endif
				endif
			endif
			
			! send the event to the window to handle click on widget
			! modify the event so the mouse x and y will correspond to the position in the window
			if #event.mouse_x > win_x+#win.content_coord_x ! coords might be negative after substraction, so correct them
				#event.mouse_x = #event.mouse_x - win_x - #win.content_coord_x
			else
				#event.mouse_x = 0
			endif
			
			if #event.mouse_y > win_y+#win.content_coord_y
				#event.mouse_y = #event.mouse_y - win_y - #win.content_coord_y
			else
				#event.mouse_y = 0
			endif
			
			if #event.first_mouse_x > win_x+#win.content_coord_x
				#event.first_mouse_x = #event.first_mouse_x - win_x - #win.content_coord_x
			else
				#event.first_mouse_x = 0
			endif
			
			if #event.first_mouse_y > win_y+#win.content_coord_y
				#event.first_mouse_y = #event.first_mouse_y - win_y - #win.content_coord_y
			else
				#event.first_mouse_y = 0
			endif
			window_parse_mouse_event(win, event)
			
		endif
	endif
	
endfunction

! find the window on wich the event occured
function windows_manager_find_window_of_event: ptr ev
	struct mouse_event_struct event = ev
	struct window_struct win=0
	u32 i = 0
	
	! return the focused window if we're dragging
	if  #(event.event_type) == MOUSE_EVENT_DRAG
		return [ptr]#windows_stack
	endif
	
	! if event occured on toolbar
	if [u32]#(event.mouse_y) < TOOLBAR_HEIGHT
		! send event directly to desktop handler
		desktop_handle_mouse_event(event)
	else
	
		while i<n_windows
			! locate on wich window the cursor is
			win = [ptr](#(windows_stack+i*SIZEOF_PTR))
			if #(win.hidden) == false
				if [u32]#(event.mouse_x) >= [u32]#(win.coord_x) && [u32]#(event.mouse_x) <= [u32]#(win.coord_x)+[u32]#(win.width)
					if [u32]#(event.mouse_y) >= [u32]#(win.coord_y) && [u32]#(event.mouse_y) <= [u32]#(win.coord_y)+[u32]#(win.height)
						! act like their is no window if the window is locked
						if #win.lock_state == true
							return null
						endif
						! ignore this window if it's minimized and we didnt clicked on the top border
						if #win.minimized == true
							if [u32]#(event.mouse_y) <= [u32]#(win.coord_y)+WINDOW_BORDER_SIZE
								return win
							endif
						else
							return win
						endif
						
					endif
				endif
			endif
			i++
		endwhile
	endif
	return null
endfunction

