/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/

define WIDGET_MAGIC 0x98fa3b0d
enum WIDGET_LABEL, WIDGET_BUTTON, WIDGET_TEXTBOX, WIDGET_LIST, WIDGET_CONTAINER, WIDGET_VSCROLL, WIDGET_HSCROLL, WIDGET_VSCROLL_BAR, WIDGET_CONSOLE
enum BUTTON_STATE_UP, BUTTON_STATE_DOWN

defstruct widget_struct
	u32 magic
	u32 x
	u32 y
	u32 width
	u32 height
	
	u32 abs_x1
	u32 abs_y1
	u32 abs_x2
	u32 abs_y2
	
	u32 abs_width
	u32 abs_height
	
	u32 type
	ptr child_struct
	! optimized way to call it's drawing function
	ptr draw_callback
	
	bool needs_redraw
	ptr bmp
	
	ptr parent_hcontainer

endstruct

function convert_relative_coord_to_absolute: u32 relative_coord, u32 max_coord
	u32 res = (max_coord*relative_coord/1000)
	!if res >= max_coord
	!	res = max_coord-1
	!endif
	return res
endfunction

function widget_create: ptr parent_hcontainer, u32 relative_x, u32 relative_y, u32 relative_width, u32 relative_height, u32 type, ptr draw_callback, ptr child_struct
	if parent_hcontainer == null
		fatal_error("widget_create() : parent_container == null")
	endif
	
	struct widget_struct widget = malloc(widget_struct.size)
	struct container_struct container = parent_hcontainer
	
	#widget.magic = [u32](WIDGET_MAGIC)
	#widget.x = [u32](relative_x)
	#widget.y = [u32](relative_y)
	#widget.width = [u32](relative_width)
	#widget.height = [u32](relative_height)
	#widget.type = [u32](type)
	#widget.child_struct = [ptr](child_struct)
	#widget.draw_callback = [ptr](draw_callback)
	
	#widget.abs_x1 = [u32](convert_relative_coord_to_absolute(#widget.x, #container.content_width))
	#widget.abs_y1 = [u32](convert_relative_coord_to_absolute(#widget.y, #container.content_height))
	#widget.abs_x2 = [u32](#widget.abs_x1 + convert_relative_coord_to_absolute(#widget.width, #container.content_width))
	
	! correct its size if the widget is outside a fixed container
	if #container.container_type =/= CONTAINER_HSCROLL && #widget.abs_x2 >= #container.content_width
		#widget.abs_x2 = [u32](#container.content_width-1)
	endif
	#widget.abs_y2 = [u32](#widget.abs_y1 + convert_relative_coord_to_absolute(#widget.height, #container.content_height))
	
	! correct its size if the widget is outside a fixed container
	if #container.container_type =/= CONTAINER_VSCROLL && #widget.abs_y2 >= #container.content_height
		#widget.abs_y2 = [u32](#container.content_height-1)
	endif
	
	#widget.abs_width = [u32]#(widget.abs_x2) - #(widget.abs_x1)
	#widget.abs_height = [u32]#(widget.abs_y2) - #(widget.abs_y1)
	
	#widget.parent_hcontainer = [ptr](parent_hcontainer)
	
	! need to be drawn after creation
	#widget.needs_redraw = true

	! drawing buffer
	#widget.bmp = malloc([u32]#(widget.abs_height)*#(widget.abs_width)*vbytepp)
	
	! add widget to parent
	container_add_child(parent_hcontainer, widget)
	
	return widget
endfunction

function widget_draw: ptr parent_bmp, u32 parent_res_x, ptr hwidget, u32 window_abs_x, u32 window_abs_y
	struct widget_struct widget = hwidget

	! check good magic
	if #widget.magic =/= WIDGET_MAGIC
		fatal_error("widget_draw(): #widget.magic =/= WIDGET_MAGIC")
	endif
	
	if #widget.needs_redraw == true || #widget.type == WIDGET_VSCROLL || #widget.type == WIDGET_HSCROLL
		
		ptr callback = [ptr]#(widget.draw_callback)
		u32 width = [u32]#widget.abs_width
		u32 height = [u32]#widget.abs_height
		ptr child_struct = [ptr]#widget.child_struct
		
		! call the drawing function of the widget
		eax = child_struct
		asm push eax
		
		eax = width
		asm push eax
		
		eax = height
		asm push eax
				
		eax = callback
		asm call eax
		
		asm add esp, 12
		
		#widget.needs_redraw = false
	endif
	
	widget_draw_bmp(parent_bmp, parent_res_x, widget, window_abs_x, window_abs_y)

	
	!draw_rect_border_buffer(parent_bmp, parent_res_x, vres_y, #widget.abs_x1+window_abs_x, #widget.abs_y1+window_abs_y, #widget.abs_x2+window_abs_x, #widget.abs_y2+window_abs_y, 1, COLOR_RED)
endfunction

! draw bitmap to screen
function widget_draw_bmp: ptr parent_bmp, u32 parent_res_x, ptr hwidget, u32 window_abs_x, u32 window_abs_y
	struct widget_struct widget = hwidget
	u32 i=0
	u32 height = [u32]#(widget.abs_height)
	u32 width = [u32]#(widget.abs_width)
	ptr bmp = #widget.bmp
	
	u32 absolute_x1 = [u32]#widget.abs_x1+window_abs_x
	u32 absolute_y1 = [u32]#widget.abs_y1+window_abs_y
	
	! optimization
	u32 width_in_bytepp = width*vbytepp
	
	while i<height
	
		memcpy(parent_bmp+(absolute_x1+(absolute_y1+i)*parent_res_x)*vbytepp, bmp+i*width_in_bytepp, width_in_bytepp)
		i++
	endwhile
endfunction

! fill background color with parent background color
function widget_fill_background: ptr hwidget
	struct widget_struct widget = hwidget
	struct container_struct parent = [ptr]#(widget.parent_hcontainer)
	
	fill_buffer(#widget.bmp, #widget.abs_width, #widget.abs_height, #parent.background_color)
endfunction

! refresh a widget
function widget_need_redraw: ptr hwidget

	struct widget_struct widget = hwidget
	#widget.needs_redraw = true
	
endfunction

! change the width of a widget
function widget_change_relative_width: ptr hwidget, u32 relative_width
	struct widget_struct widget = hwidget
	struct container_struct container = [ptr](#widget.parent_hcontainer)
	#widget.width = [u32](relative_width)
	#widget.abs_x2 = [u32](convert_relative_coord_to_absolute(#widget.x, #container.content_width) + convert_relative_coord_to_absolute(#widget.width, #container.content_width))
	
	#widget.abs_width = [u32]#(widget.abs_x2) - #(widget.abs_x1)
	
	free(#widget.bmp)
	! drawing buffer
	#widget.bmp = malloc([u32]#(widget.abs_height)*#(widget.abs_width)*vbytepp)
endfunction

! change the height of a widget
function widget_change_relative_height: ptr hwidget, u32 relative_height
	struct widget_struct widget = hwidget
	struct container_struct container = [ptr](#widget.parent_hcontainer)
	#widget.height = [u32](relative_height)
	#widget.abs_y2 = [u32](convert_relative_coord_to_absolute(#widget.y, #container.content_height) + convert_relative_coord_to_absolute(#widget.height, #container.content_height))
	
	#widget.abs_height = [u32]#(widget.abs_y2) - #(widget.abs_y1)
	
	free(#widget.bmp)
	! drawing buffer
	#widget.bmp = malloc([u32]#(widget.abs_height)*#(widget.abs_width)*vbytepp)
endfunction


! free a widget
function widget_free: ptr hwidget
	struct widget_struct widget = hwidget
	
	! delete widget
	switch #widget.type
	
		case WIDGET_LABEL
			label_free(#widget.child_struct)
		endcase
		
		case WIDGET_BUTTON
			button_free(#widget.child_struct)
		endcase
		
		case WIDGET_TEXTBOX
			textbox_free(#widget.child_struct)
		endcase
		
		case WIDGET_VSCROLL
			vscroll_free(#widget.child_struct)
		endcase
		
		case WIDGET_CONSOLE
			console_free(#widget.child_struct)
		endcase
		
	endswitch
	! delete the abstract widget
	free(#widget.bmp)
		
	free(widget)
endfunction

function widget_parse_mouse_event: ptr hwindow, ptr hwidget, ptr hevent
	struct widget_struct widget = hwidget
	
	! send the event to the widget
	switch #widget.type
		case WIDGET_BUTTON
			button_parse_mouse_event(hwindow, widget, hevent)
		endcase
		
		case WIDGET_TEXTBOX
			textbox_parse_mouse_event(hwindow, widget, hevent)
		endcase
		
		case WIDGET_VSCROLL
			vscroll_parse_mouse_event(hwindow, widget, hevent)
		endcase
		
		case WIDGET_VSCROLL_BAR
			vscrollbar_parse_mouse_event(hwindow, widget, hevent)
		endcase
	endswitch
endfunction


function widget_unfocus: ptr hwidget
	struct widget_struct widget = hwidget
	
	! send the event to the button
	if #widget.type == WIDGET_TEXTBOX
		textbox_unfocus(#widget.child_struct)
	endif
endfunction


! return the widget child
function widget_get_child: ptr hwidget
	struct widget_struct widget = hwidget
	return [ptr]#(widget.child_struct)
endfunction
