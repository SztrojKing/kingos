/*********************************
* This file is a part of King OS *
*  written by LANEZ Gaël - 2018  *
*********************************/

define TOOLBAR_BACKGROUND_COLOR 0x1c1c1c
define DESKTOP_BACKGROUND_COLOR 0x0a1e3b
define TOOLBAR_COLOR 0x111111
define TOOLBAR_HEIGHT 24

su32 toolbar_height = TOOLBAR_HEIGHT
sptr wallpaper = null
sptr power_button_bmp = null
sptr apps_bmp = null

function draw_toolbar
	struct time_struct time = time_get_time()
	ptr  time_str = time_to_str_hm(#time.hour, #time.min)
	
	if vbytepp == 4
		memset32(vram, TOOLBAR_COLOR, TOOLBAR_HEIGHT*vres_x*4)
	else
		draw_rect(0, 0, vres_x-1, TOOLBAR_HEIGHT, TOOLBAR_COLOR)
	endif
	
	! print time
	prints_rect(get_left_aligned_text_coord(time_str, vres_x-30), TOOLBAR_HEIGHT/2-CHAR_X_SIZE/2, vres_x-30, TOOLBAR_HEIGHT, COLOR_WHITE, time_str)
	
	! draw poweroff icon
	bmp_draw(power_button_bmp, vres_x-26, 0)
	
	! draw apps icon
	bmp_draw(apps_bmp, 0, 0)
	prints_rect(24, TOOLBAR_HEIGHT/2-CHAR_X_SIZE/2, vres_x, TOOLBAR_HEIGHT, COLOR_WHITE, " Applications")
	
	free(time)
	free(time_str)
	
endfunction

function raw_cls
	if video_mode==VIDEO_GRAPHICAL && wallpaper =/= null
		!if vbytepp == 4
		!	memset32([ptr](vram+TOOLBAR_HEIGHT*vres_x*4), DESKTOP_BACKGROUND_COLOR, vram_size-TOOLBAR_HEIGHT*vres_x*4)
		!else
		!	draw_rect(0, TOOLBAR_HEIGHT, vres_x-1, virtual_vres_y-TOOLBAR_HEIGHT, DESKTOP_BACKGROUND_COLOR)
		!endif
		
		! replace screen with wallpaper
		memcpy(vram, wallpaper, vram_size)
		draw_toolbar()
	endif
endfunction

function init_desktop

	! generate wallpaper
	wallpaper = malloc(vram_size)
	u32 x=0
	u32 y=0
	
	ptr colors = malloc(12)
	#colors = [u32]0x41525d
	#(colors+4) = [u32]0x40505c
	#(colors+8) = [u32]0x394853
	
	while y<vres_y
		x=0
		while x<vres_x
			plot_pixel_buffer(wallpaper, vres_x, vres_y, x, y, [u32]#(colors+(rand(0, 3)*4)))
			x++
		endwhile
		y++
	endwhile
	
	free(colors)
	
	! load power off button
	power_button_bmp = bmp_read("root0/system/x86/kernel/img/pwroff.bmp")
	
	! load apps button
	apps_bmp = bmp_read("root0/system/x86/kernel/img/apps.bmp")
	
endfunction


function log_system_stats
	ptr str = malloc(512)
	strcat(str, "RAM: ")
	strcatn(str, ramUsed*100/ramSize)
	strcat(str, "% (")
	strcatn(str, ramUsed/1024/1024)
	strcat(str, "/")
	strcatn(str, ramSize/1024/1024)
	strcat(str, "mo) | FPS: ")
	strcatn(str, fps)
	strcat(str, " | CPU: ")
	strcatn(str, get_cpu_usage())
	strcat(str, "% | INT16: ")
	strcatn(str, int16_count)
	strcat(str, " | Processes:  ")
	strcatn(str, multitask_get_number_processes())
	prints_rect(vres_x-(strlen(str)*CHAR_X_SIZE), vres_y-CHAR_Y_SIZE-4, vres_x, vres_y, COLOR_WHITE, str)
	free(str)
endfunction

function desktop_handle_mouse_event: ptr ev
	struct mouse_event_struct event = ev
	
	! if we clicked on apps logo
	if #event.mouse_x < 24+str_get_size_px(" Applications") && #event.event_type == MOUSE_EVENT_CLICK 
		! launch file browser
		ptr args = object_list_create(2)
		object_list_add(args, "root0/system/x86/apps")
		exec("root0/system/x86/apps/flbrwsr.app", args)
	endif
	
	! if we clicked on power button
	if #event.mouse_x > vres_x-24 && #event.event_type == MOUSE_EVENT_CLICK 
		! launch shutdown app
		exec("root0/system/x86/apps/power.app", null)
	endif
endfunction


