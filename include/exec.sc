/*********************************
* This file is a part of King OS *
*  written by LANEZ Gaël - 2018  *
*********************************/


function exec:ptr path, ptr arg_list

	multitask_exec(path, arg_list)
	exit
	
	ptr vm_args = object_list_create(8)
	
		! create the arg list if it's null
	if arg_list == null
		arg_list = object_list_create(1)
	endif
	object_list_add_at(arg_list, clone_str(path), 0)
	
	
	object_list_add(vm_args, path)
	object_list_add(vm_args, arg_list)
	
	multitask_exec("root0/system/x86/apps/vm.app", vm_args)
	
	
endfunction
