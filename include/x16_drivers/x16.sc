/*********************************
* This file is a part of King OS *
*  written by LANEZ Ga�l - 2016  *
*********************************/
su32 int16_count=0

function int16:u8 int, u32 reg_eax, u32 reg_ebx, u32 reg_ecx, u32 reg_edx, u16 reg_es, u32 reg_esi, u32 reg_edi
	return int16_out( int, reg_eax, reg_ebx, reg_ecx, reg_edx, reg_es, reg_esi, reg_edi, null, null, null, null, null, null, null)
endfunction

function int16_out:u8 int, u32 reg_eax, u32 reg_ebx, u32 reg_ecx, u32 reg_edx, u16 reg_es, u32 reg_esi, u32 reg_edi, ptr reg_eax_out, ptr reg_ebx_out, ptr reg_ecx_out, ptr reg_edx_out, ptr reg_es_out, ptr reg_esi_out, ptr reg_edi_out
	klog("Begining int16\n")

	u32 res
	u32 reg_ebp
	ptr registers_buffer=0x7800
	
	! remap int16 ivt
	if paging_enabled == true
		paging_map(kernel_page_directory, 0,0,3)
		paging_flush_table()
	endif

	#(registers_buffer) = reg_eax
	#(registers_buffer+4) = reg_ebx
	#(registers_buffer+8) = reg_ecx
	#(registers_buffer+12) = reg_edx
	#(registers_buffer+16) = reg_es
	#(registers_buffer+18) = reg_esi
	#(registers_buffer+22) = reg_edi
	/*klog("int16_out(int=")
	klogn(int)
	klog(", eax=")
	klogn(reg_eax)
	klog(", ebx=")
	klogn(reg_ebx)
	klog(", ecx=")
	klogn(reg_ecx)
	klog(", edx=")
	klogn(reg_edx)
	klog(", es=")
	klogn(reg_es)
	klog(", esi=")
	klogn(reg_esi)
	klog(", edi=")
	klogn(reg_edi)
	klog(")\n")*/
	
	
	asm cli
	! reset pic to real mode settings
	reset_pic(8, 112)
	
	reg_ebp = ebp
	#(registers_buffer+26) = reg_ebp
	
	asm mov ebx, resume_32_out
	ecx = esp
	dl = int
	asm jmp word CODE16_SEGMENT:0x7000
	asm resume_32_out:
	ax = DATA32_SEGMENT	
    ds = ax
	es = ax
	fs = ax
	gs = ax
	ss = ax
	
	init_pic()
	init_idt()
	
	! save res before enabling interrupts
	res=[u32]#(registers_buffer)
	if reg_eax_out =/= null
		#reg_eax_out = [u32]#(registers_buffer)
	endif
	if reg_ebx_out =/= null
		#reg_ebx_out = [u32]#(registers_buffer+4)
	endif
	if reg_ecx_out =/= null
		#reg_ecx_out = [u32]#(registers_buffer+8)
	endif
	if reg_edx_out =/= null
		#reg_edx_out = [u32]#(registers_buffer+12)
	endif
	if reg_es_out =/= null
		#reg_es_out = [u16]#(registers_buffer+16)
	endif
	if reg_esi_out =/= null
		#reg_esi_out = [u32]#(registers_buffer+18)
	endif
	if reg_edi_out =/= null
		#reg_edi_out = [u32]#(registers_buffer+22)
	endif
	
	
	asm sti
	
	int16_count++
	! return eax

	if paging_enabled == true
		paging_map(kernel_page_directory, 0,0,0)
		paging_flush_table()
	endif

	klog("End of int16\n")

	return res
endfunction
