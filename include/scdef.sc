/*********************************
* This file is a part of King OS *
*  written by LANEZ Gaël - 2016  *
*********************************/

define push_register {asm push}
define pop_register {asm pop}
define push_all_registers {asm pusha}
define pop_all_registers {asm popa}
define _goto {asm jmp}
define halt {asm hlt}

define true 1
define false 0

define null 0

define bool uint8_t

define SIZEOF_UINT8_T 1
define SIZEOF_UINT16_T 2
define SIZEOF_UINT32_T 4
define SIZEOF_PTR 4


define u8 uint8_t
define u16 uint16_t
define u32 uint32_t
define su8 suint8_t
define su16 suint16_t
define su32 suint32_t
define sbool suint8_t
